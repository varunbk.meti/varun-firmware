#include <ArduinoBLE.h>
#include <METI_IMU.h>

#define IIR_FILTER_VALUE	0.75

int accelX = 1;
int accelY = 1;
float x, y, z;

float afIIRFilter[9]= {1,1,1,1,1,1,1,1,1};

#define M5600_DATA_SIZE 10

typedef union
{
	int16_t array[M5600_DATA_SIZE];
} imuDataTDF;

imuDataTDF imuData;

// 1101 is a custom service UUID (do not change this...)
BLEService userDataService("181C");

//9001 is a characteristic defined for IMU data packet
BLECharacteristic imuDataChar("9001", BLENotify, sizeof imuData);

BLEService deviceInfoService("180A");
BLECharacteristic manufacturerChar("2A29", BLERead, "UPEKSHA HEALTH");
BLECharacteristic modelNoChar("2A24", BLERead, "001");
BLECharacteristic hwRevChar("2A27", BLERead, "100");
BLECharacteristic swRevChar("2A28", BLERead, "101");

BLEDevice central;

uint8_t ui8PacketCounter=0;
uint8_t ui8BatPercent=0;


void setup()
{

	delay(100);
	Serial.begin(115200);

	/* start IMU */
	mimu.Begin();

	pinMode(LED_BUILTIN, OUTPUT);

	if (!BLE.begin())
	{
		Serial.println("BLE failed to Initiate");
		delay(500);
		while (1)
			;
	}

	BLE.setLocalName("Upeksha Health");
	BLE.setAdvertisingInterval(500);

	deviceInfoService.addCharacteristic(manufacturerChar);
	deviceInfoService.addCharacteristic(modelNoChar);
	deviceInfoService.addCharacteristic(hwRevChar);
	deviceInfoService.addCharacteristic(swRevChar);

	BLE.addService(deviceInfoService);
	BLE.setAdvertisedService(deviceInfoService);
	BLE.setAdvertisingInterval(500);

	userDataService.addCharacteristic(imuDataChar);
	BLE.addService(userDataService);
	
	BLE.advertise();

	Serial.println("Bluetooth device is now active, waiting for connections...");

	imuData.array[0] = 0x7FF0;
}

void loop()
{
	static bool central_connected = false;

	central = BLE.central();

	if (central)
	{
		if (central_connected == false)
		{
			central_connected = true;
			digitalWrite(LED_BUILTIN, HIGH);
			Serial.print("Connected to central: ");
			Serial.println(central.address());
		}

		while (central.connected())
		{
			delay(100);
			mimu.Read_IMU();
			
			ui8PacketCounter++;
			ui8BatPercent = 100;
			imuData.array[0] = ui8PacketCounter;
			imuData.array[0] <<= 8;
			imuData.array[0] |= ui8BatPercent;

			afIIRFilter[0] = afIIRFilter[0] * IIR_FILTER_VALUE + mimu.fAccx * (1.0 - IIR_FILTER_VALUE);
			imuData.array[1] = afIIRFilter[0] * 100;

			afIIRFilter[1] = afIIRFilter[1] * IIR_FILTER_VALUE + mimu.fAccy * (1.0 - IIR_FILTER_VALUE);
			imuData.array[2] = afIIRFilter[1] * 100;

			afIIRFilter[2] = afIIRFilter[2] * IIR_FILTER_VALUE + mimu.fAccz * (1.0 - IIR_FILTER_VALUE);
			imuData.array[3] = afIIRFilter[2] * 100;

			afIIRFilter[3] = afIIRFilter[3] * IIR_FILTER_VALUE + mimu.fGyrox * (1.0 - IIR_FILTER_VALUE);
			imuData.array[4] = afIIRFilter[3] * 100;

			afIIRFilter[4] = afIIRFilter[4] * IIR_FILTER_VALUE + mimu.fGyroy * (1.0 - IIR_FILTER_VALUE);
			imuData.array[5] = afIIRFilter[4] * 100;

			afIIRFilter[5] = afIIRFilter[5] * IIR_FILTER_VALUE + mimu.fGyroz * (1.0 - IIR_FILTER_VALUE);
			imuData.array[6] = afIIRFilter[5] * 100;

			afIIRFilter[6] = afIIRFilter[6] * IIR_FILTER_VALUE + mimu.fMagx * (1.0 - IIR_FILTER_VALUE);
			imuData.array[7] = afIIRFilter[6] * 100;

			afIIRFilter[7] = afIIRFilter[7] * IIR_FILTER_VALUE + mimu.fMagy * (1.0 - IIR_FILTER_VALUE);
			imuData.array[8] = afIIRFilter[7] * 100;

			afIIRFilter[8] = afIIRFilter[8] * IIR_FILTER_VALUE + mimu.fMagz * (1.0 - IIR_FILTER_VALUE);
			imuData.array[9] = afIIRFilter[8] * 100;

			imuDataChar.writeValue(imuData.array, sizeof imuData.array);

			Serial.println("Sent to BLE");
		}
	}
	else
	{
		if (central_connected == true)
		{
			central_connected = false;

			digitalWrite(LED_BUILTIN, LOW);
			Serial.print("Disconnected from central: ");
		}
	}
	delay(1000);
}