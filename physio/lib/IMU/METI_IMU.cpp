/*******************************************************************************
*File Name: METI_IMU.h
*
* Version: 1.0
*
* Description:
* In this header for IMU functions
*
*
* Owner:
* Yogesh M Iggalore
*
********************************************************************************
* Copyright (2020-21) , METI
*******************************************************************************/
#include <Arduino.h>
#include <METI_IMU.h>

METI_IMU::METI_IMU(){}

void METI_IMU::Begin(void){
	if (!IMU.begin()) {
    	Serial.println("Failed to initialize IMU!");
    	while (1);
  	}

	Serial.print("Accelerometer sample rate = ");
  	Serial.print(IMU.accelerationSampleRate());
  	Serial.println(" Hz");
  	Serial.println();
  	Serial.println("Acceleration in G's");
  	Serial.println("X\tY\tZ");

	Serial.print("Magnetic field sample rate = ");
  	Serial.print(IMU.magneticFieldSampleRate());
 	Serial.println(" uT");
  	Serial.println();
  	Serial.println("Magnetic Field in uT");
  	Serial.println("X\tY\tZ");

	Serial.print("Gyroscope sample rate = ");
  	Serial.print(IMU.gyroscopeSampleRate());
  	Serial.println(" Hz");
  	Serial.println();
 	Serial.println("Gyroscope in degrees/second");
  	Serial.println("X\tY\tZ");
}

void METI_IMU::Read_IMU(void){

  	if(IMU.accelerationAvailable()) {
    	IMU.readAcceleration(fAccx, fAccy, fAccz);

    	Serial.print(fAccx,4);
    	Serial.print('\t');
    	Serial.print(fAccy,4);
    	Serial.print('\t');
    	Serial.print(fAccz,4);
		Serial.print('\t');
  	}

 	if (IMU.gyroscopeAvailable()) {
    	IMU.readGyroscope(fGyrox, fGyroy, fGyroz);

    	Serial.print(fGyrox,4);
    	Serial.print('\t');
    	Serial.print(fGyroy,4);
    	Serial.print('\t');
    	Serial.print(fGyroz,4);
		Serial.print('\t');
  	}

  	if (IMU.magneticFieldAvailable()) {
    	IMU.readMagneticField(fMagx, fMagy, fMagz);

    	Serial.print(fMagx,4);
    	Serial.print('\t');
    	Serial.print(fMagy,4);
    	Serial.print('\t');
    	Serial.println(fMagz,4);
  	}
}

// Preinstantiate Objects //////////////////////////////////////////////////////
#if !defined(NO_GLOBAL_INSTANCES) && !defined(NO_GLOBAL_METI_IMU)
METI_IMU mimu;
#endif
