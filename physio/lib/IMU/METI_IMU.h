/*******************************************************************************
*File Name: METI_IMU.c
*
* Version: 1.0
*
* Description:
* In this source for IMU functions
*
*
* Owner:
* Yogesh M Iggalore
*
********************************************************************************
* Copyright (2020-21) , METI
*******************************************************************************/

#ifndef METI_IMU_h
#define METI_IMU_h

#include <Arduino.h>
#include <Arduino_LSM9DS1.h>

class METI_IMU{

public:
    METI_IMU();
    float fAccx;
    float fAccy;
    float fAccz;

    float fGyrox;
    float fGyroy;
    float fGyroz;

    float fMagx;
    float fMagy;
    float fMagz;
    void Begin(void);
    void Read_IMU(void);
};

#if !defined(NO_GLOBAL_INSTANCES) && !defined(NO_GLOBAL_METIIMU)
extern METI_IMU mimu;
#endif
#endif
