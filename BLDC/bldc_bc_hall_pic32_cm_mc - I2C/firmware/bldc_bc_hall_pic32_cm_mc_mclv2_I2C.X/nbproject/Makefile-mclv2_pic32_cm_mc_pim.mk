#
# Generated Makefile - do not edit!
#
# Edit the Makefile in the project folder instead (../Makefile). Each target
# has a -pre and a -post target defined where you can add customized code.
#
# This makefile implements configuration specific macros and targets.


# Include project Makefile
ifeq "${IGNORE_LOCAL}" "TRUE"
# do not include local makefile. User is passing all local related variables already
else
include Makefile
# Include makefile containing local settings
ifeq "$(wildcard nbproject/Makefile-local-mclv2_pic32_cm_mc_pim.mk)" "nbproject/Makefile-local-mclv2_pic32_cm_mc_pim.mk"
include nbproject/Makefile-local-mclv2_pic32_cm_mc_pim.mk
endif
endif

# Environment
MKDIR=gnumkdir -p
RM=rm -f 
MV=mv 
CP=cp 

# Macros
CND_CONF=mclv2_pic32_cm_mc_pim
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
IMAGE_TYPE=debug
OUTPUT_SUFFIX=elf
DEBUGGABLE_SUFFIX=elf
FINAL_IMAGE=dist/${CND_CONF}/${IMAGE_TYPE}/bldc_bc_hall_pic32_cm_mc_mclv2_I2C.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}
else
IMAGE_TYPE=production
OUTPUT_SUFFIX=hex
DEBUGGABLE_SUFFIX=elf
FINAL_IMAGE=dist/${CND_CONF}/${IMAGE_TYPE}/bldc_bc_hall_pic32_cm_mc_mclv2_I2C.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}
endif

ifeq ($(COMPARE_BUILD), true)
COMPARISON_BUILD=-mafrlcsj
else
COMPARISON_BUILD=
endif

ifdef SUB_IMAGE_ADDRESS

else
SUB_IMAGE_ADDRESS_COMMAND=
endif

# Object Directory
OBJECTDIR=build/${CND_CONF}/${IMAGE_TYPE}

# Distribution Directory
DISTDIR=dist/${CND_CONF}/${IMAGE_TYPE}

# Source Files Quoted if spaced
SOURCEFILES_QUOTED_IF_SPACED=../src/config/mclv2_pic32_cm_mc_pim/peripheral/adc/plib_adc1.c ../src/config/mclv2_pic32_cm_mc_pim/peripheral/clock/plib_clock.c ../src/config/mclv2_pic32_cm_mc_pim/peripheral/divas/plib_divas.c ../src/config/mclv2_pic32_cm_mc_pim/peripheral/eic/plib_eic.c ../src/config/mclv2_pic32_cm_mc_pim/peripheral/evsys/plib_evsys.c ../src/config/mclv2_pic32_cm_mc_pim/peripheral/nvic/plib_nvic.c ../src/config/mclv2_pic32_cm_mc_pim/peripheral/nvmctrl/plib_nvmctrl.c ../src/config/mclv2_pic32_cm_mc_pim/peripheral/pdec/plib_pdec.c ../src/config/mclv2_pic32_cm_mc_pim/peripheral/port/plib_port.c ../src/config/mclv2_pic32_cm_mc_pim/peripheral/sercom/i2c_slave/plib_sercom3_i2c_slave.c ../src/config/mclv2_pic32_cm_mc_pim/peripheral/tc/plib_tc3.c ../src/config/mclv2_pic32_cm_mc_pim/peripheral/tc/plib_tc4.c ../src/config/mclv2_pic32_cm_mc_pim/peripheral/tcc/plib_tcc0.c ../src/config/mclv2_pic32_cm_mc_pim/stdio/xc32_monitor.c ../src/config/mclv2_pic32_cm_mc_pim/initialization.c ../src/config/mclv2_pic32_cm_mc_pim/interrupts.c ../src/config/mclv2_pic32_cm_mc_pim/exceptions.c ../src/config/mclv2_pic32_cm_mc_pim/startup_xc32.c ../src/config/mclv2_pic32_cm_mc_pim/libc_syscalls.c ../src/mc_app.c ../src/q14_generic_mcLib.c ../src/main_mclv2_pic32_cm_mc.c

# Object Files Quoted if spaced
OBJECTFILES_QUOTED_IF_SPACED=${OBJECTDIR}/_ext/1345482930/plib_adc1.o ${OBJECTDIR}/_ext/226039712/plib_clock.o ${OBJECTDIR}/_ext/226880533/plib_divas.o ${OBJECTDIR}/_ext/1345486929/plib_eic.o ${OBJECTDIR}/_ext/228189198/plib_evsys.o ${OBJECTDIR}/_ext/1239297264/plib_nvic.o ${OBJECTDIR}/_ext/362106206/plib_nvmctrl.o ${OBJECTDIR}/_ext/1239255104/plib_pdec.o ${OBJECTDIR}/_ext/1239244113/plib_port.o ${OBJECTDIR}/_ext/1596168270/plib_sercom3_i2c_slave.o ${OBJECTDIR}/_ext/1151781917/plib_tc3.o ${OBJECTDIR}/_ext/1151781917/plib_tc4.o ${OBJECTDIR}/_ext/1345501158/plib_tcc0.o ${OBJECTDIR}/_ext/1276539452/xc32_monitor.o ${OBJECTDIR}/_ext/1867106698/initialization.o ${OBJECTDIR}/_ext/1867106698/interrupts.o ${OBJECTDIR}/_ext/1867106698/exceptions.o ${OBJECTDIR}/_ext/1867106698/startup_xc32.o ${OBJECTDIR}/_ext/1867106698/libc_syscalls.o ${OBJECTDIR}/_ext/1360937237/mc_app.o ${OBJECTDIR}/_ext/1360937237/q14_generic_mcLib.o ${OBJECTDIR}/_ext/1360937237/main_mclv2_pic32_cm_mc.o
POSSIBLE_DEPFILES=${OBJECTDIR}/_ext/1345482930/plib_adc1.o.d ${OBJECTDIR}/_ext/226039712/plib_clock.o.d ${OBJECTDIR}/_ext/226880533/plib_divas.o.d ${OBJECTDIR}/_ext/1345486929/plib_eic.o.d ${OBJECTDIR}/_ext/228189198/plib_evsys.o.d ${OBJECTDIR}/_ext/1239297264/plib_nvic.o.d ${OBJECTDIR}/_ext/362106206/plib_nvmctrl.o.d ${OBJECTDIR}/_ext/1239255104/plib_pdec.o.d ${OBJECTDIR}/_ext/1239244113/plib_port.o.d ${OBJECTDIR}/_ext/1596168270/plib_sercom3_i2c_slave.o.d ${OBJECTDIR}/_ext/1151781917/plib_tc3.o.d ${OBJECTDIR}/_ext/1151781917/plib_tc4.o.d ${OBJECTDIR}/_ext/1345501158/plib_tcc0.o.d ${OBJECTDIR}/_ext/1276539452/xc32_monitor.o.d ${OBJECTDIR}/_ext/1867106698/initialization.o.d ${OBJECTDIR}/_ext/1867106698/interrupts.o.d ${OBJECTDIR}/_ext/1867106698/exceptions.o.d ${OBJECTDIR}/_ext/1867106698/startup_xc32.o.d ${OBJECTDIR}/_ext/1867106698/libc_syscalls.o.d ${OBJECTDIR}/_ext/1360937237/mc_app.o.d ${OBJECTDIR}/_ext/1360937237/q14_generic_mcLib.o.d ${OBJECTDIR}/_ext/1360937237/main_mclv2_pic32_cm_mc.o.d

# Object Files
OBJECTFILES=${OBJECTDIR}/_ext/1345482930/plib_adc1.o ${OBJECTDIR}/_ext/226039712/plib_clock.o ${OBJECTDIR}/_ext/226880533/plib_divas.o ${OBJECTDIR}/_ext/1345486929/plib_eic.o ${OBJECTDIR}/_ext/228189198/plib_evsys.o ${OBJECTDIR}/_ext/1239297264/plib_nvic.o ${OBJECTDIR}/_ext/362106206/plib_nvmctrl.o ${OBJECTDIR}/_ext/1239255104/plib_pdec.o ${OBJECTDIR}/_ext/1239244113/plib_port.o ${OBJECTDIR}/_ext/1596168270/plib_sercom3_i2c_slave.o ${OBJECTDIR}/_ext/1151781917/plib_tc3.o ${OBJECTDIR}/_ext/1151781917/plib_tc4.o ${OBJECTDIR}/_ext/1345501158/plib_tcc0.o ${OBJECTDIR}/_ext/1276539452/xc32_monitor.o ${OBJECTDIR}/_ext/1867106698/initialization.o ${OBJECTDIR}/_ext/1867106698/interrupts.o ${OBJECTDIR}/_ext/1867106698/exceptions.o ${OBJECTDIR}/_ext/1867106698/startup_xc32.o ${OBJECTDIR}/_ext/1867106698/libc_syscalls.o ${OBJECTDIR}/_ext/1360937237/mc_app.o ${OBJECTDIR}/_ext/1360937237/q14_generic_mcLib.o ${OBJECTDIR}/_ext/1360937237/main_mclv2_pic32_cm_mc.o

# Source Files
SOURCEFILES=../src/config/mclv2_pic32_cm_mc_pim/peripheral/adc/plib_adc1.c ../src/config/mclv2_pic32_cm_mc_pim/peripheral/clock/plib_clock.c ../src/config/mclv2_pic32_cm_mc_pim/peripheral/divas/plib_divas.c ../src/config/mclv2_pic32_cm_mc_pim/peripheral/eic/plib_eic.c ../src/config/mclv2_pic32_cm_mc_pim/peripheral/evsys/plib_evsys.c ../src/config/mclv2_pic32_cm_mc_pim/peripheral/nvic/plib_nvic.c ../src/config/mclv2_pic32_cm_mc_pim/peripheral/nvmctrl/plib_nvmctrl.c ../src/config/mclv2_pic32_cm_mc_pim/peripheral/pdec/plib_pdec.c ../src/config/mclv2_pic32_cm_mc_pim/peripheral/port/plib_port.c ../src/config/mclv2_pic32_cm_mc_pim/peripheral/sercom/i2c_slave/plib_sercom3_i2c_slave.c ../src/config/mclv2_pic32_cm_mc_pim/peripheral/tc/plib_tc3.c ../src/config/mclv2_pic32_cm_mc_pim/peripheral/tc/plib_tc4.c ../src/config/mclv2_pic32_cm_mc_pim/peripheral/tcc/plib_tcc0.c ../src/config/mclv2_pic32_cm_mc_pim/stdio/xc32_monitor.c ../src/config/mclv2_pic32_cm_mc_pim/initialization.c ../src/config/mclv2_pic32_cm_mc_pim/interrupts.c ../src/config/mclv2_pic32_cm_mc_pim/exceptions.c ../src/config/mclv2_pic32_cm_mc_pim/startup_xc32.c ../src/config/mclv2_pic32_cm_mc_pim/libc_syscalls.c ../src/mc_app.c ../src/q14_generic_mcLib.c ../src/main_mclv2_pic32_cm_mc.c

# Pack Options 
PACK_COMMON_OPTIONS=-I "${CMSIS_DIR}/CMSIS/Core/Include"



CFLAGS=
ASFLAGS=
LDLIBSOPTIONS=

############# Tool locations ##########################################
# If you copy a project from one host to another, the path where the  #
# compiler is installed may be different.                             #
# If you open this project with MPLAB X in the new host, this         #
# makefile will be regenerated and the paths will be corrected.       #
#######################################################################
# fixDeps replaces a bunch of sed/cat/printf statements that slow down the build
FIXDEPS=fixDeps

.build-conf:  ${BUILD_SUBPROJECTS}
ifneq ($(INFORMATION_MESSAGE), )
	@echo $(INFORMATION_MESSAGE)
endif
	${MAKE}  -f nbproject/Makefile-mclv2_pic32_cm_mc_pim.mk dist/${CND_CONF}/${IMAGE_TYPE}/bldc_bc_hall_pic32_cm_mc_mclv2_I2C.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}

MP_PROCESSOR_OPTION=32CM1216MC00048
MP_LINKER_FILE_OPTION=,--script="..\src\config\mclv2_pic32_cm_mc_pim\PIC32CM1216MC00048.ld"
# ------------------------------------------------------------------------------------
# Rules for buildStep: assemble
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
else
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: assembleWithPreprocess
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
else
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: compile
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
${OBJECTDIR}/_ext/1345482930/plib_adc1.o: ../src/config/mclv2_pic32_cm_mc_pim/peripheral/adc/plib_adc1.c  .generated_files/flags/mclv2_pic32_cm_mc_pim/7009c3e3d695d0d74d97fa22bdbbe88e88ea213 .generated_files/flags/mclv2_pic32_cm_mc_pim/ebc7eb1707317225d922e38c7cea395c4d1e499
	@${MKDIR} "${OBJECTDIR}/_ext/1345482930" 
	@${RM} ${OBJECTDIR}/_ext/1345482930/plib_adc1.o.d 
	@${RM} ${OBJECTDIR}/_ext/1345482930/plib_adc1.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG   -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -fdata-sections -O1 -I"../src" -I"../src/config/mclv2_pic32_cm_mc_pim" -I"../src/packs/CMSIS/" -I"../src/packs/CMSIS/CMSIS/Core/Include" -I"../src/packs/PIC32CM1216MC00048_DFP" -Werror -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/1345482930/plib_adc1.o.d" -o ${OBJECTDIR}/_ext/1345482930/plib_adc1.o ../src/config/mclv2_pic32_cm_mc_pim/peripheral/adc/plib_adc1.c    -DXPRJ_mclv2_pic32_cm_mc_pim=$(CND_CONF)    $(COMPARISON_BUILD)  -Wformat=2 -Wundef -Wshadow -Wpointer-arith -Wbad-function-cast -Wwrite-strings -Waggregate-return -Wstrict-prototypes -Wmissing-format-attribute -Wno-deprecated-declarations -Wredundant-decls -Wnested-externs -Winline -Wlong-long -Wunreachable-code -Wmissing-noreturn -mdfp="${DFP_DIR}/PIC32CM1216MC00048" ${PACK_COMMON_OPTIONS} 
	
${OBJECTDIR}/_ext/226039712/plib_clock.o: ../src/config/mclv2_pic32_cm_mc_pim/peripheral/clock/plib_clock.c  .generated_files/flags/mclv2_pic32_cm_mc_pim/2b57ac1f40306bd4c028f09ed521bb0309c6f5f .generated_files/flags/mclv2_pic32_cm_mc_pim/ebc7eb1707317225d922e38c7cea395c4d1e499
	@${MKDIR} "${OBJECTDIR}/_ext/226039712" 
	@${RM} ${OBJECTDIR}/_ext/226039712/plib_clock.o.d 
	@${RM} ${OBJECTDIR}/_ext/226039712/plib_clock.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG   -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -fdata-sections -O1 -I"../src" -I"../src/config/mclv2_pic32_cm_mc_pim" -I"../src/packs/CMSIS/" -I"../src/packs/CMSIS/CMSIS/Core/Include" -I"../src/packs/PIC32CM1216MC00048_DFP" -Werror -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/226039712/plib_clock.o.d" -o ${OBJECTDIR}/_ext/226039712/plib_clock.o ../src/config/mclv2_pic32_cm_mc_pim/peripheral/clock/plib_clock.c    -DXPRJ_mclv2_pic32_cm_mc_pim=$(CND_CONF)    $(COMPARISON_BUILD)  -Wformat=2 -Wundef -Wshadow -Wpointer-arith -Wbad-function-cast -Wwrite-strings -Waggregate-return -Wstrict-prototypes -Wmissing-format-attribute -Wno-deprecated-declarations -Wredundant-decls -Wnested-externs -Winline -Wlong-long -Wunreachable-code -Wmissing-noreturn -mdfp="${DFP_DIR}/PIC32CM1216MC00048" ${PACK_COMMON_OPTIONS} 
	
${OBJECTDIR}/_ext/226880533/plib_divas.o: ../src/config/mclv2_pic32_cm_mc_pim/peripheral/divas/plib_divas.c  .generated_files/flags/mclv2_pic32_cm_mc_pim/1d4abef6334fa5c50cddb54aa781d22238191343 .generated_files/flags/mclv2_pic32_cm_mc_pim/ebc7eb1707317225d922e38c7cea395c4d1e499
	@${MKDIR} "${OBJECTDIR}/_ext/226880533" 
	@${RM} ${OBJECTDIR}/_ext/226880533/plib_divas.o.d 
	@${RM} ${OBJECTDIR}/_ext/226880533/plib_divas.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG   -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -fdata-sections -O1 -I"../src" -I"../src/config/mclv2_pic32_cm_mc_pim" -I"../src/packs/CMSIS/" -I"../src/packs/CMSIS/CMSIS/Core/Include" -I"../src/packs/PIC32CM1216MC00048_DFP" -Werror -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/226880533/plib_divas.o.d" -o ${OBJECTDIR}/_ext/226880533/plib_divas.o ../src/config/mclv2_pic32_cm_mc_pim/peripheral/divas/plib_divas.c    -DXPRJ_mclv2_pic32_cm_mc_pim=$(CND_CONF)    $(COMPARISON_BUILD)  -Wformat=2 -Wundef -Wshadow -Wpointer-arith -Wbad-function-cast -Wwrite-strings -Waggregate-return -Wstrict-prototypes -Wmissing-format-attribute -Wno-deprecated-declarations -Wredundant-decls -Wnested-externs -Winline -Wlong-long -Wunreachable-code -Wmissing-noreturn -mdfp="${DFP_DIR}/PIC32CM1216MC00048" ${PACK_COMMON_OPTIONS} 
	
${OBJECTDIR}/_ext/1345486929/plib_eic.o: ../src/config/mclv2_pic32_cm_mc_pim/peripheral/eic/plib_eic.c  .generated_files/flags/mclv2_pic32_cm_mc_pim/b4fb2a3441c29eadaba066c23b6b54bb7df15859 .generated_files/flags/mclv2_pic32_cm_mc_pim/ebc7eb1707317225d922e38c7cea395c4d1e499
	@${MKDIR} "${OBJECTDIR}/_ext/1345486929" 
	@${RM} ${OBJECTDIR}/_ext/1345486929/plib_eic.o.d 
	@${RM} ${OBJECTDIR}/_ext/1345486929/plib_eic.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG   -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -fdata-sections -O1 -I"../src" -I"../src/config/mclv2_pic32_cm_mc_pim" -I"../src/packs/CMSIS/" -I"../src/packs/CMSIS/CMSIS/Core/Include" -I"../src/packs/PIC32CM1216MC00048_DFP" -Werror -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/1345486929/plib_eic.o.d" -o ${OBJECTDIR}/_ext/1345486929/plib_eic.o ../src/config/mclv2_pic32_cm_mc_pim/peripheral/eic/plib_eic.c    -DXPRJ_mclv2_pic32_cm_mc_pim=$(CND_CONF)    $(COMPARISON_BUILD)  -Wformat=2 -Wundef -Wshadow -Wpointer-arith -Wbad-function-cast -Wwrite-strings -Waggregate-return -Wstrict-prototypes -Wmissing-format-attribute -Wno-deprecated-declarations -Wredundant-decls -Wnested-externs -Winline -Wlong-long -Wunreachable-code -Wmissing-noreturn -mdfp="${DFP_DIR}/PIC32CM1216MC00048" ${PACK_COMMON_OPTIONS} 
	
${OBJECTDIR}/_ext/228189198/plib_evsys.o: ../src/config/mclv2_pic32_cm_mc_pim/peripheral/evsys/plib_evsys.c  .generated_files/flags/mclv2_pic32_cm_mc_pim/1271c58ccfca41ca4a676e8aa873ebeb2ad6c5d5 .generated_files/flags/mclv2_pic32_cm_mc_pim/ebc7eb1707317225d922e38c7cea395c4d1e499
	@${MKDIR} "${OBJECTDIR}/_ext/228189198" 
	@${RM} ${OBJECTDIR}/_ext/228189198/plib_evsys.o.d 
	@${RM} ${OBJECTDIR}/_ext/228189198/plib_evsys.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG   -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -fdata-sections -O1 -I"../src" -I"../src/config/mclv2_pic32_cm_mc_pim" -I"../src/packs/CMSIS/" -I"../src/packs/CMSIS/CMSIS/Core/Include" -I"../src/packs/PIC32CM1216MC00048_DFP" -Werror -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/228189198/plib_evsys.o.d" -o ${OBJECTDIR}/_ext/228189198/plib_evsys.o ../src/config/mclv2_pic32_cm_mc_pim/peripheral/evsys/plib_evsys.c    -DXPRJ_mclv2_pic32_cm_mc_pim=$(CND_CONF)    $(COMPARISON_BUILD)  -Wformat=2 -Wundef -Wshadow -Wpointer-arith -Wbad-function-cast -Wwrite-strings -Waggregate-return -Wstrict-prototypes -Wmissing-format-attribute -Wno-deprecated-declarations -Wredundant-decls -Wnested-externs -Winline -Wlong-long -Wunreachable-code -Wmissing-noreturn -mdfp="${DFP_DIR}/PIC32CM1216MC00048" ${PACK_COMMON_OPTIONS} 
	
${OBJECTDIR}/_ext/1239297264/plib_nvic.o: ../src/config/mclv2_pic32_cm_mc_pim/peripheral/nvic/plib_nvic.c  .generated_files/flags/mclv2_pic32_cm_mc_pim/ec6ea97612feeffd8dd294742069fc78c4fa29c .generated_files/flags/mclv2_pic32_cm_mc_pim/ebc7eb1707317225d922e38c7cea395c4d1e499
	@${MKDIR} "${OBJECTDIR}/_ext/1239297264" 
	@${RM} ${OBJECTDIR}/_ext/1239297264/plib_nvic.o.d 
	@${RM} ${OBJECTDIR}/_ext/1239297264/plib_nvic.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG   -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -fdata-sections -O1 -I"../src" -I"../src/config/mclv2_pic32_cm_mc_pim" -I"../src/packs/CMSIS/" -I"../src/packs/CMSIS/CMSIS/Core/Include" -I"../src/packs/PIC32CM1216MC00048_DFP" -Werror -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/1239297264/plib_nvic.o.d" -o ${OBJECTDIR}/_ext/1239297264/plib_nvic.o ../src/config/mclv2_pic32_cm_mc_pim/peripheral/nvic/plib_nvic.c    -DXPRJ_mclv2_pic32_cm_mc_pim=$(CND_CONF)    $(COMPARISON_BUILD)  -Wformat=2 -Wundef -Wshadow -Wpointer-arith -Wbad-function-cast -Wwrite-strings -Waggregate-return -Wstrict-prototypes -Wmissing-format-attribute -Wno-deprecated-declarations -Wredundant-decls -Wnested-externs -Winline -Wlong-long -Wunreachable-code -Wmissing-noreturn -mdfp="${DFP_DIR}/PIC32CM1216MC00048" ${PACK_COMMON_OPTIONS} 
	
${OBJECTDIR}/_ext/362106206/plib_nvmctrl.o: ../src/config/mclv2_pic32_cm_mc_pim/peripheral/nvmctrl/plib_nvmctrl.c  .generated_files/flags/mclv2_pic32_cm_mc_pim/6917577001dcb571d5c26f4283027ea5218a438f .generated_files/flags/mclv2_pic32_cm_mc_pim/ebc7eb1707317225d922e38c7cea395c4d1e499
	@${MKDIR} "${OBJECTDIR}/_ext/362106206" 
	@${RM} ${OBJECTDIR}/_ext/362106206/plib_nvmctrl.o.d 
	@${RM} ${OBJECTDIR}/_ext/362106206/plib_nvmctrl.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG   -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -fdata-sections -O1 -I"../src" -I"../src/config/mclv2_pic32_cm_mc_pim" -I"../src/packs/CMSIS/" -I"../src/packs/CMSIS/CMSIS/Core/Include" -I"../src/packs/PIC32CM1216MC00048_DFP" -Werror -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/362106206/plib_nvmctrl.o.d" -o ${OBJECTDIR}/_ext/362106206/plib_nvmctrl.o ../src/config/mclv2_pic32_cm_mc_pim/peripheral/nvmctrl/plib_nvmctrl.c    -DXPRJ_mclv2_pic32_cm_mc_pim=$(CND_CONF)    $(COMPARISON_BUILD)  -Wformat=2 -Wundef -Wshadow -Wpointer-arith -Wbad-function-cast -Wwrite-strings -Waggregate-return -Wstrict-prototypes -Wmissing-format-attribute -Wno-deprecated-declarations -Wredundant-decls -Wnested-externs -Winline -Wlong-long -Wunreachable-code -Wmissing-noreturn -mdfp="${DFP_DIR}/PIC32CM1216MC00048" ${PACK_COMMON_OPTIONS} 
	
${OBJECTDIR}/_ext/1239255104/plib_pdec.o: ../src/config/mclv2_pic32_cm_mc_pim/peripheral/pdec/plib_pdec.c  .generated_files/flags/mclv2_pic32_cm_mc_pim/7a3aaf6559efdcf5f5053b46f273c41ffae72a43 .generated_files/flags/mclv2_pic32_cm_mc_pim/ebc7eb1707317225d922e38c7cea395c4d1e499
	@${MKDIR} "${OBJECTDIR}/_ext/1239255104" 
	@${RM} ${OBJECTDIR}/_ext/1239255104/plib_pdec.o.d 
	@${RM} ${OBJECTDIR}/_ext/1239255104/plib_pdec.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG   -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -fdata-sections -O1 -I"../src" -I"../src/config/mclv2_pic32_cm_mc_pim" -I"../src/packs/CMSIS/" -I"../src/packs/CMSIS/CMSIS/Core/Include" -I"../src/packs/PIC32CM1216MC00048_DFP" -Werror -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/1239255104/plib_pdec.o.d" -o ${OBJECTDIR}/_ext/1239255104/plib_pdec.o ../src/config/mclv2_pic32_cm_mc_pim/peripheral/pdec/plib_pdec.c    -DXPRJ_mclv2_pic32_cm_mc_pim=$(CND_CONF)    $(COMPARISON_BUILD)  -Wformat=2 -Wundef -Wshadow -Wpointer-arith -Wbad-function-cast -Wwrite-strings -Waggregate-return -Wstrict-prototypes -Wmissing-format-attribute -Wno-deprecated-declarations -Wredundant-decls -Wnested-externs -Winline -Wlong-long -Wunreachable-code -Wmissing-noreturn -mdfp="${DFP_DIR}/PIC32CM1216MC00048" ${PACK_COMMON_OPTIONS} 
	
${OBJECTDIR}/_ext/1239244113/plib_port.o: ../src/config/mclv2_pic32_cm_mc_pim/peripheral/port/plib_port.c  .generated_files/flags/mclv2_pic32_cm_mc_pim/1f4e76650f99318ad043ceaa22426564da8dc8f8 .generated_files/flags/mclv2_pic32_cm_mc_pim/ebc7eb1707317225d922e38c7cea395c4d1e499
	@${MKDIR} "${OBJECTDIR}/_ext/1239244113" 
	@${RM} ${OBJECTDIR}/_ext/1239244113/plib_port.o.d 
	@${RM} ${OBJECTDIR}/_ext/1239244113/plib_port.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG   -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -fdata-sections -O1 -I"../src" -I"../src/config/mclv2_pic32_cm_mc_pim" -I"../src/packs/CMSIS/" -I"../src/packs/CMSIS/CMSIS/Core/Include" -I"../src/packs/PIC32CM1216MC00048_DFP" -Werror -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/1239244113/plib_port.o.d" -o ${OBJECTDIR}/_ext/1239244113/plib_port.o ../src/config/mclv2_pic32_cm_mc_pim/peripheral/port/plib_port.c    -DXPRJ_mclv2_pic32_cm_mc_pim=$(CND_CONF)    $(COMPARISON_BUILD)  -Wformat=2 -Wundef -Wshadow -Wpointer-arith -Wbad-function-cast -Wwrite-strings -Waggregate-return -Wstrict-prototypes -Wmissing-format-attribute -Wno-deprecated-declarations -Wredundant-decls -Wnested-externs -Winline -Wlong-long -Wunreachable-code -Wmissing-noreturn -mdfp="${DFP_DIR}/PIC32CM1216MC00048" ${PACK_COMMON_OPTIONS} 
	
${OBJECTDIR}/_ext/1596168270/plib_sercom3_i2c_slave.o: ../src/config/mclv2_pic32_cm_mc_pim/peripheral/sercom/i2c_slave/plib_sercom3_i2c_slave.c  .generated_files/flags/mclv2_pic32_cm_mc_pim/47411963751f8d5e5716669dc043e1e7e362c1dc .generated_files/flags/mclv2_pic32_cm_mc_pim/ebc7eb1707317225d922e38c7cea395c4d1e499
	@${MKDIR} "${OBJECTDIR}/_ext/1596168270" 
	@${RM} ${OBJECTDIR}/_ext/1596168270/plib_sercom3_i2c_slave.o.d 
	@${RM} ${OBJECTDIR}/_ext/1596168270/plib_sercom3_i2c_slave.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG   -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -fdata-sections -O1 -I"../src" -I"../src/config/mclv2_pic32_cm_mc_pim" -I"../src/packs/CMSIS/" -I"../src/packs/CMSIS/CMSIS/Core/Include" -I"../src/packs/PIC32CM1216MC00048_DFP" -Werror -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/1596168270/plib_sercom3_i2c_slave.o.d" -o ${OBJECTDIR}/_ext/1596168270/plib_sercom3_i2c_slave.o ../src/config/mclv2_pic32_cm_mc_pim/peripheral/sercom/i2c_slave/plib_sercom3_i2c_slave.c    -DXPRJ_mclv2_pic32_cm_mc_pim=$(CND_CONF)    $(COMPARISON_BUILD)  -Wformat=2 -Wundef -Wshadow -Wpointer-arith -Wbad-function-cast -Wwrite-strings -Waggregate-return -Wstrict-prototypes -Wmissing-format-attribute -Wno-deprecated-declarations -Wredundant-decls -Wnested-externs -Winline -Wlong-long -Wunreachable-code -Wmissing-noreturn -mdfp="${DFP_DIR}/PIC32CM1216MC00048" ${PACK_COMMON_OPTIONS} 
	
${OBJECTDIR}/_ext/1151781917/plib_tc3.o: ../src/config/mclv2_pic32_cm_mc_pim/peripheral/tc/plib_tc3.c  .generated_files/flags/mclv2_pic32_cm_mc_pim/32d04e78d36e91dc782f3386b3ff15313cdb98 .generated_files/flags/mclv2_pic32_cm_mc_pim/ebc7eb1707317225d922e38c7cea395c4d1e499
	@${MKDIR} "${OBJECTDIR}/_ext/1151781917" 
	@${RM} ${OBJECTDIR}/_ext/1151781917/plib_tc3.o.d 
	@${RM} ${OBJECTDIR}/_ext/1151781917/plib_tc3.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG   -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -fdata-sections -O1 -I"../src" -I"../src/config/mclv2_pic32_cm_mc_pim" -I"../src/packs/CMSIS/" -I"../src/packs/CMSIS/CMSIS/Core/Include" -I"../src/packs/PIC32CM1216MC00048_DFP" -Werror -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/1151781917/plib_tc3.o.d" -o ${OBJECTDIR}/_ext/1151781917/plib_tc3.o ../src/config/mclv2_pic32_cm_mc_pim/peripheral/tc/plib_tc3.c    -DXPRJ_mclv2_pic32_cm_mc_pim=$(CND_CONF)    $(COMPARISON_BUILD)  -Wformat=2 -Wundef -Wshadow -Wpointer-arith -Wbad-function-cast -Wwrite-strings -Waggregate-return -Wstrict-prototypes -Wmissing-format-attribute -Wno-deprecated-declarations -Wredundant-decls -Wnested-externs -Winline -Wlong-long -Wunreachable-code -Wmissing-noreturn -mdfp="${DFP_DIR}/PIC32CM1216MC00048" ${PACK_COMMON_OPTIONS} 
	
${OBJECTDIR}/_ext/1151781917/plib_tc4.o: ../src/config/mclv2_pic32_cm_mc_pim/peripheral/tc/plib_tc4.c  .generated_files/flags/mclv2_pic32_cm_mc_pim/72d939b99e05da97f5bf9b862ba269d16a7a58aa .generated_files/flags/mclv2_pic32_cm_mc_pim/ebc7eb1707317225d922e38c7cea395c4d1e499
	@${MKDIR} "${OBJECTDIR}/_ext/1151781917" 
	@${RM} ${OBJECTDIR}/_ext/1151781917/plib_tc4.o.d 
	@${RM} ${OBJECTDIR}/_ext/1151781917/plib_tc4.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG   -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -fdata-sections -O1 -I"../src" -I"../src/config/mclv2_pic32_cm_mc_pim" -I"../src/packs/CMSIS/" -I"../src/packs/CMSIS/CMSIS/Core/Include" -I"../src/packs/PIC32CM1216MC00048_DFP" -Werror -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/1151781917/plib_tc4.o.d" -o ${OBJECTDIR}/_ext/1151781917/plib_tc4.o ../src/config/mclv2_pic32_cm_mc_pim/peripheral/tc/plib_tc4.c    -DXPRJ_mclv2_pic32_cm_mc_pim=$(CND_CONF)    $(COMPARISON_BUILD)  -Wformat=2 -Wundef -Wshadow -Wpointer-arith -Wbad-function-cast -Wwrite-strings -Waggregate-return -Wstrict-prototypes -Wmissing-format-attribute -Wno-deprecated-declarations -Wredundant-decls -Wnested-externs -Winline -Wlong-long -Wunreachable-code -Wmissing-noreturn -mdfp="${DFP_DIR}/PIC32CM1216MC00048" ${PACK_COMMON_OPTIONS} 
	
${OBJECTDIR}/_ext/1345501158/plib_tcc0.o: ../src/config/mclv2_pic32_cm_mc_pim/peripheral/tcc/plib_tcc0.c  .generated_files/flags/mclv2_pic32_cm_mc_pim/de6798ac0d83a847d669503f2d72a03c976dcca1 .generated_files/flags/mclv2_pic32_cm_mc_pim/ebc7eb1707317225d922e38c7cea395c4d1e499
	@${MKDIR} "${OBJECTDIR}/_ext/1345501158" 
	@${RM} ${OBJECTDIR}/_ext/1345501158/plib_tcc0.o.d 
	@${RM} ${OBJECTDIR}/_ext/1345501158/plib_tcc0.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG   -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -fdata-sections -O1 -I"../src" -I"../src/config/mclv2_pic32_cm_mc_pim" -I"../src/packs/CMSIS/" -I"../src/packs/CMSIS/CMSIS/Core/Include" -I"../src/packs/PIC32CM1216MC00048_DFP" -Werror -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/1345501158/plib_tcc0.o.d" -o ${OBJECTDIR}/_ext/1345501158/plib_tcc0.o ../src/config/mclv2_pic32_cm_mc_pim/peripheral/tcc/plib_tcc0.c    -DXPRJ_mclv2_pic32_cm_mc_pim=$(CND_CONF)    $(COMPARISON_BUILD)  -Wformat=2 -Wundef -Wshadow -Wpointer-arith -Wbad-function-cast -Wwrite-strings -Waggregate-return -Wstrict-prototypes -Wmissing-format-attribute -Wno-deprecated-declarations -Wredundant-decls -Wnested-externs -Winline -Wlong-long -Wunreachable-code -Wmissing-noreturn -mdfp="${DFP_DIR}/PIC32CM1216MC00048" ${PACK_COMMON_OPTIONS} 
	
${OBJECTDIR}/_ext/1276539452/xc32_monitor.o: ../src/config/mclv2_pic32_cm_mc_pim/stdio/xc32_monitor.c  .generated_files/flags/mclv2_pic32_cm_mc_pim/3abb217c0cfaabdb2a433f608bd1341511dce1f7 .generated_files/flags/mclv2_pic32_cm_mc_pim/ebc7eb1707317225d922e38c7cea395c4d1e499
	@${MKDIR} "${OBJECTDIR}/_ext/1276539452" 
	@${RM} ${OBJECTDIR}/_ext/1276539452/xc32_monitor.o.d 
	@${RM} ${OBJECTDIR}/_ext/1276539452/xc32_monitor.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG   -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -fdata-sections -O1 -I"../src" -I"../src/config/mclv2_pic32_cm_mc_pim" -I"../src/packs/CMSIS/" -I"../src/packs/CMSIS/CMSIS/Core/Include" -I"../src/packs/PIC32CM1216MC00048_DFP" -Werror -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/1276539452/xc32_monitor.o.d" -o ${OBJECTDIR}/_ext/1276539452/xc32_monitor.o ../src/config/mclv2_pic32_cm_mc_pim/stdio/xc32_monitor.c    -DXPRJ_mclv2_pic32_cm_mc_pim=$(CND_CONF)    $(COMPARISON_BUILD)  -Wformat=2 -Wundef -Wshadow -Wpointer-arith -Wbad-function-cast -Wwrite-strings -Waggregate-return -Wstrict-prototypes -Wmissing-format-attribute -Wno-deprecated-declarations -Wredundant-decls -Wnested-externs -Winline -Wlong-long -Wunreachable-code -Wmissing-noreturn -mdfp="${DFP_DIR}/PIC32CM1216MC00048" ${PACK_COMMON_OPTIONS} 
	
${OBJECTDIR}/_ext/1867106698/initialization.o: ../src/config/mclv2_pic32_cm_mc_pim/initialization.c  .generated_files/flags/mclv2_pic32_cm_mc_pim/937fe17aef40c294e5c32c3321d29a34dd39debd .generated_files/flags/mclv2_pic32_cm_mc_pim/ebc7eb1707317225d922e38c7cea395c4d1e499
	@${MKDIR} "${OBJECTDIR}/_ext/1867106698" 
	@${RM} ${OBJECTDIR}/_ext/1867106698/initialization.o.d 
	@${RM} ${OBJECTDIR}/_ext/1867106698/initialization.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG   -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -fdata-sections -O1 -I"../src" -I"../src/config/mclv2_pic32_cm_mc_pim" -I"../src/packs/CMSIS/" -I"../src/packs/CMSIS/CMSIS/Core/Include" -I"../src/packs/PIC32CM1216MC00048_DFP" -Werror -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/1867106698/initialization.o.d" -o ${OBJECTDIR}/_ext/1867106698/initialization.o ../src/config/mclv2_pic32_cm_mc_pim/initialization.c    -DXPRJ_mclv2_pic32_cm_mc_pim=$(CND_CONF)    $(COMPARISON_BUILD)  -Wformat=2 -Wundef -Wshadow -Wpointer-arith -Wbad-function-cast -Wwrite-strings -Waggregate-return -Wstrict-prototypes -Wmissing-format-attribute -Wno-deprecated-declarations -Wredundant-decls -Wnested-externs -Winline -Wlong-long -Wunreachable-code -Wmissing-noreturn -mdfp="${DFP_DIR}/PIC32CM1216MC00048" ${PACK_COMMON_OPTIONS} 
	
${OBJECTDIR}/_ext/1867106698/interrupts.o: ../src/config/mclv2_pic32_cm_mc_pim/interrupts.c  .generated_files/flags/mclv2_pic32_cm_mc_pim/19802570d3bcdf9d161cde2188c80b7384cffd25 .generated_files/flags/mclv2_pic32_cm_mc_pim/ebc7eb1707317225d922e38c7cea395c4d1e499
	@${MKDIR} "${OBJECTDIR}/_ext/1867106698" 
	@${RM} ${OBJECTDIR}/_ext/1867106698/interrupts.o.d 
	@${RM} ${OBJECTDIR}/_ext/1867106698/interrupts.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG   -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -fdata-sections -O1 -I"../src" -I"../src/config/mclv2_pic32_cm_mc_pim" -I"../src/packs/CMSIS/" -I"../src/packs/CMSIS/CMSIS/Core/Include" -I"../src/packs/PIC32CM1216MC00048_DFP" -Werror -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/1867106698/interrupts.o.d" -o ${OBJECTDIR}/_ext/1867106698/interrupts.o ../src/config/mclv2_pic32_cm_mc_pim/interrupts.c    -DXPRJ_mclv2_pic32_cm_mc_pim=$(CND_CONF)    $(COMPARISON_BUILD)  -Wformat=2 -Wundef -Wshadow -Wpointer-arith -Wbad-function-cast -Wwrite-strings -Waggregate-return -Wstrict-prototypes -Wmissing-format-attribute -Wno-deprecated-declarations -Wredundant-decls -Wnested-externs -Winline -Wlong-long -Wunreachable-code -Wmissing-noreturn -mdfp="${DFP_DIR}/PIC32CM1216MC00048" ${PACK_COMMON_OPTIONS} 
	
${OBJECTDIR}/_ext/1867106698/exceptions.o: ../src/config/mclv2_pic32_cm_mc_pim/exceptions.c  .generated_files/flags/mclv2_pic32_cm_mc_pim/4b40b0952641d4a0b42d6d9835a621bc6fb62ccf .generated_files/flags/mclv2_pic32_cm_mc_pim/ebc7eb1707317225d922e38c7cea395c4d1e499
	@${MKDIR} "${OBJECTDIR}/_ext/1867106698" 
	@${RM} ${OBJECTDIR}/_ext/1867106698/exceptions.o.d 
	@${RM} ${OBJECTDIR}/_ext/1867106698/exceptions.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG   -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -fdata-sections -O1 -I"../src" -I"../src/config/mclv2_pic32_cm_mc_pim" -I"../src/packs/CMSIS/" -I"../src/packs/CMSIS/CMSIS/Core/Include" -I"../src/packs/PIC32CM1216MC00048_DFP" -Werror -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/1867106698/exceptions.o.d" -o ${OBJECTDIR}/_ext/1867106698/exceptions.o ../src/config/mclv2_pic32_cm_mc_pim/exceptions.c    -DXPRJ_mclv2_pic32_cm_mc_pim=$(CND_CONF)    $(COMPARISON_BUILD)  -Wformat=2 -Wundef -Wshadow -Wpointer-arith -Wbad-function-cast -Wwrite-strings -Waggregate-return -Wstrict-prototypes -Wmissing-format-attribute -Wno-deprecated-declarations -Wredundant-decls -Wnested-externs -Winline -Wlong-long -Wunreachable-code -Wmissing-noreturn -mdfp="${DFP_DIR}/PIC32CM1216MC00048" ${PACK_COMMON_OPTIONS} 
	
${OBJECTDIR}/_ext/1867106698/startup_xc32.o: ../src/config/mclv2_pic32_cm_mc_pim/startup_xc32.c  .generated_files/flags/mclv2_pic32_cm_mc_pim/9e92363da1fae0eb026c62cf02bce52954cd39f6 .generated_files/flags/mclv2_pic32_cm_mc_pim/ebc7eb1707317225d922e38c7cea395c4d1e499
	@${MKDIR} "${OBJECTDIR}/_ext/1867106698" 
	@${RM} ${OBJECTDIR}/_ext/1867106698/startup_xc32.o.d 
	@${RM} ${OBJECTDIR}/_ext/1867106698/startup_xc32.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG   -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -fdata-sections -O1 -I"../src" -I"../src/config/mclv2_pic32_cm_mc_pim" -I"../src/packs/CMSIS/" -I"../src/packs/CMSIS/CMSIS/Core/Include" -I"../src/packs/PIC32CM1216MC00048_DFP" -Werror -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/1867106698/startup_xc32.o.d" -o ${OBJECTDIR}/_ext/1867106698/startup_xc32.o ../src/config/mclv2_pic32_cm_mc_pim/startup_xc32.c    -DXPRJ_mclv2_pic32_cm_mc_pim=$(CND_CONF)    $(COMPARISON_BUILD)  -Wformat=2 -Wundef -Wshadow -Wpointer-arith -Wbad-function-cast -Wwrite-strings -Waggregate-return -Wstrict-prototypes -Wmissing-format-attribute -Wno-deprecated-declarations -Wredundant-decls -Wnested-externs -Winline -Wlong-long -Wunreachable-code -Wmissing-noreturn -mdfp="${DFP_DIR}/PIC32CM1216MC00048" ${PACK_COMMON_OPTIONS} 
	
${OBJECTDIR}/_ext/1867106698/libc_syscalls.o: ../src/config/mclv2_pic32_cm_mc_pim/libc_syscalls.c  .generated_files/flags/mclv2_pic32_cm_mc_pim/e2388211f8873c87cbb2635a3e85ac10a7846c17 .generated_files/flags/mclv2_pic32_cm_mc_pim/ebc7eb1707317225d922e38c7cea395c4d1e499
	@${MKDIR} "${OBJECTDIR}/_ext/1867106698" 
	@${RM} ${OBJECTDIR}/_ext/1867106698/libc_syscalls.o.d 
	@${RM} ${OBJECTDIR}/_ext/1867106698/libc_syscalls.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG   -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -fdata-sections -O1 -I"../src" -I"../src/config/mclv2_pic32_cm_mc_pim" -I"../src/packs/CMSIS/" -I"../src/packs/CMSIS/CMSIS/Core/Include" -I"../src/packs/PIC32CM1216MC00048_DFP" -Werror -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/1867106698/libc_syscalls.o.d" -o ${OBJECTDIR}/_ext/1867106698/libc_syscalls.o ../src/config/mclv2_pic32_cm_mc_pim/libc_syscalls.c    -DXPRJ_mclv2_pic32_cm_mc_pim=$(CND_CONF)    $(COMPARISON_BUILD)  -Wformat=2 -Wundef -Wshadow -Wpointer-arith -Wbad-function-cast -Wwrite-strings -Waggregate-return -Wstrict-prototypes -Wmissing-format-attribute -Wno-deprecated-declarations -Wredundant-decls -Wnested-externs -Winline -Wlong-long -Wunreachable-code -Wmissing-noreturn -mdfp="${DFP_DIR}/PIC32CM1216MC00048" ${PACK_COMMON_OPTIONS} 
	
${OBJECTDIR}/_ext/1360937237/mc_app.o: ../src/mc_app.c  .generated_files/flags/mclv2_pic32_cm_mc_pim/b82df9ed35547f30a6852688bc2d48dc1d489982 .generated_files/flags/mclv2_pic32_cm_mc_pim/ebc7eb1707317225d922e38c7cea395c4d1e499
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/mc_app.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/mc_app.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG   -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -fdata-sections -O1 -I"../src" -I"../src/config/mclv2_pic32_cm_mc_pim" -I"../src/packs/CMSIS/" -I"../src/packs/CMSIS/CMSIS/Core/Include" -I"../src/packs/PIC32CM1216MC00048_DFP" -Werror -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/1360937237/mc_app.o.d" -o ${OBJECTDIR}/_ext/1360937237/mc_app.o ../src/mc_app.c    -DXPRJ_mclv2_pic32_cm_mc_pim=$(CND_CONF)    $(COMPARISON_BUILD)  -Wformat=2 -Wundef -Wshadow -Wpointer-arith -Wbad-function-cast -Wwrite-strings -Waggregate-return -Wstrict-prototypes -Wmissing-format-attribute -Wno-deprecated-declarations -Wredundant-decls -Wnested-externs -Winline -Wlong-long -Wunreachable-code -Wmissing-noreturn -mdfp="${DFP_DIR}/PIC32CM1216MC00048" ${PACK_COMMON_OPTIONS} 
	
${OBJECTDIR}/_ext/1360937237/q14_generic_mcLib.o: ../src/q14_generic_mcLib.c  .generated_files/flags/mclv2_pic32_cm_mc_pim/8682569a0173c644c330ff7f4d824645bc1a1da8 .generated_files/flags/mclv2_pic32_cm_mc_pim/ebc7eb1707317225d922e38c7cea395c4d1e499
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/q14_generic_mcLib.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/q14_generic_mcLib.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG   -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -fdata-sections -O1 -I"../src" -I"../src/config/mclv2_pic32_cm_mc_pim" -I"../src/packs/CMSIS/" -I"../src/packs/CMSIS/CMSIS/Core/Include" -I"../src/packs/PIC32CM1216MC00048_DFP" -Werror -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/1360937237/q14_generic_mcLib.o.d" -o ${OBJECTDIR}/_ext/1360937237/q14_generic_mcLib.o ../src/q14_generic_mcLib.c    -DXPRJ_mclv2_pic32_cm_mc_pim=$(CND_CONF)    $(COMPARISON_BUILD)  -Wformat=2 -Wundef -Wshadow -Wpointer-arith -Wbad-function-cast -Wwrite-strings -Waggregate-return -Wstrict-prototypes -Wmissing-format-attribute -Wno-deprecated-declarations -Wredundant-decls -Wnested-externs -Winline -Wlong-long -Wunreachable-code -Wmissing-noreturn -mdfp="${DFP_DIR}/PIC32CM1216MC00048" ${PACK_COMMON_OPTIONS} 
	
${OBJECTDIR}/_ext/1360937237/main_mclv2_pic32_cm_mc.o: ../src/main_mclv2_pic32_cm_mc.c  .generated_files/flags/mclv2_pic32_cm_mc_pim/c6b6781613fcacf2718fe8a07808ea427e47b21c .generated_files/flags/mclv2_pic32_cm_mc_pim/ebc7eb1707317225d922e38c7cea395c4d1e499
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/main_mclv2_pic32_cm_mc.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/main_mclv2_pic32_cm_mc.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG   -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -fdata-sections -O1 -I"../src" -I"../src/config/mclv2_pic32_cm_mc_pim" -I"../src/packs/CMSIS/" -I"../src/packs/CMSIS/CMSIS/Core/Include" -I"../src/packs/PIC32CM1216MC00048_DFP" -Werror -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/1360937237/main_mclv2_pic32_cm_mc.o.d" -o ${OBJECTDIR}/_ext/1360937237/main_mclv2_pic32_cm_mc.o ../src/main_mclv2_pic32_cm_mc.c    -DXPRJ_mclv2_pic32_cm_mc_pim=$(CND_CONF)    $(COMPARISON_BUILD)  -Wformat=2 -Wundef -Wshadow -Wpointer-arith -Wbad-function-cast -Wwrite-strings -Waggregate-return -Wstrict-prototypes -Wmissing-format-attribute -Wno-deprecated-declarations -Wredundant-decls -Wnested-externs -Winline -Wlong-long -Wunreachable-code -Wmissing-noreturn -mdfp="${DFP_DIR}/PIC32CM1216MC00048" ${PACK_COMMON_OPTIONS} 
	
else
${OBJECTDIR}/_ext/1345482930/plib_adc1.o: ../src/config/mclv2_pic32_cm_mc_pim/peripheral/adc/plib_adc1.c  .generated_files/flags/mclv2_pic32_cm_mc_pim/93e3ab2aed34689bb4c0ed35ece58112c289a600 .generated_files/flags/mclv2_pic32_cm_mc_pim/ebc7eb1707317225d922e38c7cea395c4d1e499
	@${MKDIR} "${OBJECTDIR}/_ext/1345482930" 
	@${RM} ${OBJECTDIR}/_ext/1345482930/plib_adc1.o.d 
	@${RM} ${OBJECTDIR}/_ext/1345482930/plib_adc1.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -fdata-sections -O1 -I"../src" -I"../src/config/mclv2_pic32_cm_mc_pim" -I"../src/packs/CMSIS/" -I"../src/packs/CMSIS/CMSIS/Core/Include" -I"../src/packs/PIC32CM1216MC00048_DFP" -Werror -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/1345482930/plib_adc1.o.d" -o ${OBJECTDIR}/_ext/1345482930/plib_adc1.o ../src/config/mclv2_pic32_cm_mc_pim/peripheral/adc/plib_adc1.c    -DXPRJ_mclv2_pic32_cm_mc_pim=$(CND_CONF)    $(COMPARISON_BUILD)  -Wformat=2 -Wundef -Wshadow -Wpointer-arith -Wbad-function-cast -Wwrite-strings -Waggregate-return -Wstrict-prototypes -Wmissing-format-attribute -Wno-deprecated-declarations -Wredundant-decls -Wnested-externs -Winline -Wlong-long -Wunreachable-code -Wmissing-noreturn -mdfp="${DFP_DIR}/PIC32CM1216MC00048" ${PACK_COMMON_OPTIONS} 
	
${OBJECTDIR}/_ext/226039712/plib_clock.o: ../src/config/mclv2_pic32_cm_mc_pim/peripheral/clock/plib_clock.c  .generated_files/flags/mclv2_pic32_cm_mc_pim/4c635eb9d5631fab0b7152adfaf78af1d3ba6d1a .generated_files/flags/mclv2_pic32_cm_mc_pim/ebc7eb1707317225d922e38c7cea395c4d1e499
	@${MKDIR} "${OBJECTDIR}/_ext/226039712" 
	@${RM} ${OBJECTDIR}/_ext/226039712/plib_clock.o.d 
	@${RM} ${OBJECTDIR}/_ext/226039712/plib_clock.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -fdata-sections -O1 -I"../src" -I"../src/config/mclv2_pic32_cm_mc_pim" -I"../src/packs/CMSIS/" -I"../src/packs/CMSIS/CMSIS/Core/Include" -I"../src/packs/PIC32CM1216MC00048_DFP" -Werror -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/226039712/plib_clock.o.d" -o ${OBJECTDIR}/_ext/226039712/plib_clock.o ../src/config/mclv2_pic32_cm_mc_pim/peripheral/clock/plib_clock.c    -DXPRJ_mclv2_pic32_cm_mc_pim=$(CND_CONF)    $(COMPARISON_BUILD)  -Wformat=2 -Wundef -Wshadow -Wpointer-arith -Wbad-function-cast -Wwrite-strings -Waggregate-return -Wstrict-prototypes -Wmissing-format-attribute -Wno-deprecated-declarations -Wredundant-decls -Wnested-externs -Winline -Wlong-long -Wunreachable-code -Wmissing-noreturn -mdfp="${DFP_DIR}/PIC32CM1216MC00048" ${PACK_COMMON_OPTIONS} 
	
${OBJECTDIR}/_ext/226880533/plib_divas.o: ../src/config/mclv2_pic32_cm_mc_pim/peripheral/divas/plib_divas.c  .generated_files/flags/mclv2_pic32_cm_mc_pim/508fa4a47bbd5241e8ac3aacf8fb2abe066be2de .generated_files/flags/mclv2_pic32_cm_mc_pim/ebc7eb1707317225d922e38c7cea395c4d1e499
	@${MKDIR} "${OBJECTDIR}/_ext/226880533" 
	@${RM} ${OBJECTDIR}/_ext/226880533/plib_divas.o.d 
	@${RM} ${OBJECTDIR}/_ext/226880533/plib_divas.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -fdata-sections -O1 -I"../src" -I"../src/config/mclv2_pic32_cm_mc_pim" -I"../src/packs/CMSIS/" -I"../src/packs/CMSIS/CMSIS/Core/Include" -I"../src/packs/PIC32CM1216MC00048_DFP" -Werror -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/226880533/plib_divas.o.d" -o ${OBJECTDIR}/_ext/226880533/plib_divas.o ../src/config/mclv2_pic32_cm_mc_pim/peripheral/divas/plib_divas.c    -DXPRJ_mclv2_pic32_cm_mc_pim=$(CND_CONF)    $(COMPARISON_BUILD)  -Wformat=2 -Wundef -Wshadow -Wpointer-arith -Wbad-function-cast -Wwrite-strings -Waggregate-return -Wstrict-prototypes -Wmissing-format-attribute -Wno-deprecated-declarations -Wredundant-decls -Wnested-externs -Winline -Wlong-long -Wunreachable-code -Wmissing-noreturn -mdfp="${DFP_DIR}/PIC32CM1216MC00048" ${PACK_COMMON_OPTIONS} 
	
${OBJECTDIR}/_ext/1345486929/plib_eic.o: ../src/config/mclv2_pic32_cm_mc_pim/peripheral/eic/plib_eic.c  .generated_files/flags/mclv2_pic32_cm_mc_pim/30d5430a3450780df8e1d147dcf223caadc2a2c3 .generated_files/flags/mclv2_pic32_cm_mc_pim/ebc7eb1707317225d922e38c7cea395c4d1e499
	@${MKDIR} "${OBJECTDIR}/_ext/1345486929" 
	@${RM} ${OBJECTDIR}/_ext/1345486929/plib_eic.o.d 
	@${RM} ${OBJECTDIR}/_ext/1345486929/plib_eic.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -fdata-sections -O1 -I"../src" -I"../src/config/mclv2_pic32_cm_mc_pim" -I"../src/packs/CMSIS/" -I"../src/packs/CMSIS/CMSIS/Core/Include" -I"../src/packs/PIC32CM1216MC00048_DFP" -Werror -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/1345486929/plib_eic.o.d" -o ${OBJECTDIR}/_ext/1345486929/plib_eic.o ../src/config/mclv2_pic32_cm_mc_pim/peripheral/eic/plib_eic.c    -DXPRJ_mclv2_pic32_cm_mc_pim=$(CND_CONF)    $(COMPARISON_BUILD)  -Wformat=2 -Wundef -Wshadow -Wpointer-arith -Wbad-function-cast -Wwrite-strings -Waggregate-return -Wstrict-prototypes -Wmissing-format-attribute -Wno-deprecated-declarations -Wredundant-decls -Wnested-externs -Winline -Wlong-long -Wunreachable-code -Wmissing-noreturn -mdfp="${DFP_DIR}/PIC32CM1216MC00048" ${PACK_COMMON_OPTIONS} 
	
${OBJECTDIR}/_ext/228189198/plib_evsys.o: ../src/config/mclv2_pic32_cm_mc_pim/peripheral/evsys/plib_evsys.c  .generated_files/flags/mclv2_pic32_cm_mc_pim/26e1efdab0fc6195fa2cb7fd6b9d7b072acc4858 .generated_files/flags/mclv2_pic32_cm_mc_pim/ebc7eb1707317225d922e38c7cea395c4d1e499
	@${MKDIR} "${OBJECTDIR}/_ext/228189198" 
	@${RM} ${OBJECTDIR}/_ext/228189198/plib_evsys.o.d 
	@${RM} ${OBJECTDIR}/_ext/228189198/plib_evsys.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -fdata-sections -O1 -I"../src" -I"../src/config/mclv2_pic32_cm_mc_pim" -I"../src/packs/CMSIS/" -I"../src/packs/CMSIS/CMSIS/Core/Include" -I"../src/packs/PIC32CM1216MC00048_DFP" -Werror -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/228189198/plib_evsys.o.d" -o ${OBJECTDIR}/_ext/228189198/plib_evsys.o ../src/config/mclv2_pic32_cm_mc_pim/peripheral/evsys/plib_evsys.c    -DXPRJ_mclv2_pic32_cm_mc_pim=$(CND_CONF)    $(COMPARISON_BUILD)  -Wformat=2 -Wundef -Wshadow -Wpointer-arith -Wbad-function-cast -Wwrite-strings -Waggregate-return -Wstrict-prototypes -Wmissing-format-attribute -Wno-deprecated-declarations -Wredundant-decls -Wnested-externs -Winline -Wlong-long -Wunreachable-code -Wmissing-noreturn -mdfp="${DFP_DIR}/PIC32CM1216MC00048" ${PACK_COMMON_OPTIONS} 
	
${OBJECTDIR}/_ext/1239297264/plib_nvic.o: ../src/config/mclv2_pic32_cm_mc_pim/peripheral/nvic/plib_nvic.c  .generated_files/flags/mclv2_pic32_cm_mc_pim/b7f37bf795d638736e0182c9ae2fa0e388b8c889 .generated_files/flags/mclv2_pic32_cm_mc_pim/ebc7eb1707317225d922e38c7cea395c4d1e499
	@${MKDIR} "${OBJECTDIR}/_ext/1239297264" 
	@${RM} ${OBJECTDIR}/_ext/1239297264/plib_nvic.o.d 
	@${RM} ${OBJECTDIR}/_ext/1239297264/plib_nvic.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -fdata-sections -O1 -I"../src" -I"../src/config/mclv2_pic32_cm_mc_pim" -I"../src/packs/CMSIS/" -I"../src/packs/CMSIS/CMSIS/Core/Include" -I"../src/packs/PIC32CM1216MC00048_DFP" -Werror -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/1239297264/plib_nvic.o.d" -o ${OBJECTDIR}/_ext/1239297264/plib_nvic.o ../src/config/mclv2_pic32_cm_mc_pim/peripheral/nvic/plib_nvic.c    -DXPRJ_mclv2_pic32_cm_mc_pim=$(CND_CONF)    $(COMPARISON_BUILD)  -Wformat=2 -Wundef -Wshadow -Wpointer-arith -Wbad-function-cast -Wwrite-strings -Waggregate-return -Wstrict-prototypes -Wmissing-format-attribute -Wno-deprecated-declarations -Wredundant-decls -Wnested-externs -Winline -Wlong-long -Wunreachable-code -Wmissing-noreturn -mdfp="${DFP_DIR}/PIC32CM1216MC00048" ${PACK_COMMON_OPTIONS} 
	
${OBJECTDIR}/_ext/362106206/plib_nvmctrl.o: ../src/config/mclv2_pic32_cm_mc_pim/peripheral/nvmctrl/plib_nvmctrl.c  .generated_files/flags/mclv2_pic32_cm_mc_pim/4746b1464dd3bf2fee801955a03dd178d41a1223 .generated_files/flags/mclv2_pic32_cm_mc_pim/ebc7eb1707317225d922e38c7cea395c4d1e499
	@${MKDIR} "${OBJECTDIR}/_ext/362106206" 
	@${RM} ${OBJECTDIR}/_ext/362106206/plib_nvmctrl.o.d 
	@${RM} ${OBJECTDIR}/_ext/362106206/plib_nvmctrl.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -fdata-sections -O1 -I"../src" -I"../src/config/mclv2_pic32_cm_mc_pim" -I"../src/packs/CMSIS/" -I"../src/packs/CMSIS/CMSIS/Core/Include" -I"../src/packs/PIC32CM1216MC00048_DFP" -Werror -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/362106206/plib_nvmctrl.o.d" -o ${OBJECTDIR}/_ext/362106206/plib_nvmctrl.o ../src/config/mclv2_pic32_cm_mc_pim/peripheral/nvmctrl/plib_nvmctrl.c    -DXPRJ_mclv2_pic32_cm_mc_pim=$(CND_CONF)    $(COMPARISON_BUILD)  -Wformat=2 -Wundef -Wshadow -Wpointer-arith -Wbad-function-cast -Wwrite-strings -Waggregate-return -Wstrict-prototypes -Wmissing-format-attribute -Wno-deprecated-declarations -Wredundant-decls -Wnested-externs -Winline -Wlong-long -Wunreachable-code -Wmissing-noreturn -mdfp="${DFP_DIR}/PIC32CM1216MC00048" ${PACK_COMMON_OPTIONS} 
	
${OBJECTDIR}/_ext/1239255104/plib_pdec.o: ../src/config/mclv2_pic32_cm_mc_pim/peripheral/pdec/plib_pdec.c  .generated_files/flags/mclv2_pic32_cm_mc_pim/20eba3d4459bd2da9e576b704338a65d06ff5116 .generated_files/flags/mclv2_pic32_cm_mc_pim/ebc7eb1707317225d922e38c7cea395c4d1e499
	@${MKDIR} "${OBJECTDIR}/_ext/1239255104" 
	@${RM} ${OBJECTDIR}/_ext/1239255104/plib_pdec.o.d 
	@${RM} ${OBJECTDIR}/_ext/1239255104/plib_pdec.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -fdata-sections -O1 -I"../src" -I"../src/config/mclv2_pic32_cm_mc_pim" -I"../src/packs/CMSIS/" -I"../src/packs/CMSIS/CMSIS/Core/Include" -I"../src/packs/PIC32CM1216MC00048_DFP" -Werror -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/1239255104/plib_pdec.o.d" -o ${OBJECTDIR}/_ext/1239255104/plib_pdec.o ../src/config/mclv2_pic32_cm_mc_pim/peripheral/pdec/plib_pdec.c    -DXPRJ_mclv2_pic32_cm_mc_pim=$(CND_CONF)    $(COMPARISON_BUILD)  -Wformat=2 -Wundef -Wshadow -Wpointer-arith -Wbad-function-cast -Wwrite-strings -Waggregate-return -Wstrict-prototypes -Wmissing-format-attribute -Wno-deprecated-declarations -Wredundant-decls -Wnested-externs -Winline -Wlong-long -Wunreachable-code -Wmissing-noreturn -mdfp="${DFP_DIR}/PIC32CM1216MC00048" ${PACK_COMMON_OPTIONS} 
	
${OBJECTDIR}/_ext/1239244113/plib_port.o: ../src/config/mclv2_pic32_cm_mc_pim/peripheral/port/plib_port.c  .generated_files/flags/mclv2_pic32_cm_mc_pim/a58b6abc09e9733b56805cd1a30894d71f2ca98f .generated_files/flags/mclv2_pic32_cm_mc_pim/ebc7eb1707317225d922e38c7cea395c4d1e499
	@${MKDIR} "${OBJECTDIR}/_ext/1239244113" 
	@${RM} ${OBJECTDIR}/_ext/1239244113/plib_port.o.d 
	@${RM} ${OBJECTDIR}/_ext/1239244113/plib_port.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -fdata-sections -O1 -I"../src" -I"../src/config/mclv2_pic32_cm_mc_pim" -I"../src/packs/CMSIS/" -I"../src/packs/CMSIS/CMSIS/Core/Include" -I"../src/packs/PIC32CM1216MC00048_DFP" -Werror -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/1239244113/plib_port.o.d" -o ${OBJECTDIR}/_ext/1239244113/plib_port.o ../src/config/mclv2_pic32_cm_mc_pim/peripheral/port/plib_port.c    -DXPRJ_mclv2_pic32_cm_mc_pim=$(CND_CONF)    $(COMPARISON_BUILD)  -Wformat=2 -Wundef -Wshadow -Wpointer-arith -Wbad-function-cast -Wwrite-strings -Waggregate-return -Wstrict-prototypes -Wmissing-format-attribute -Wno-deprecated-declarations -Wredundant-decls -Wnested-externs -Winline -Wlong-long -Wunreachable-code -Wmissing-noreturn -mdfp="${DFP_DIR}/PIC32CM1216MC00048" ${PACK_COMMON_OPTIONS} 
	
${OBJECTDIR}/_ext/1596168270/plib_sercom3_i2c_slave.o: ../src/config/mclv2_pic32_cm_mc_pim/peripheral/sercom/i2c_slave/plib_sercom3_i2c_slave.c  .generated_files/flags/mclv2_pic32_cm_mc_pim/865f9e79cc231062419f8067d3cff0d65a7a05b8 .generated_files/flags/mclv2_pic32_cm_mc_pim/ebc7eb1707317225d922e38c7cea395c4d1e499
	@${MKDIR} "${OBJECTDIR}/_ext/1596168270" 
	@${RM} ${OBJECTDIR}/_ext/1596168270/plib_sercom3_i2c_slave.o.d 
	@${RM} ${OBJECTDIR}/_ext/1596168270/plib_sercom3_i2c_slave.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -fdata-sections -O1 -I"../src" -I"../src/config/mclv2_pic32_cm_mc_pim" -I"../src/packs/CMSIS/" -I"../src/packs/CMSIS/CMSIS/Core/Include" -I"../src/packs/PIC32CM1216MC00048_DFP" -Werror -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/1596168270/plib_sercom3_i2c_slave.o.d" -o ${OBJECTDIR}/_ext/1596168270/plib_sercom3_i2c_slave.o ../src/config/mclv2_pic32_cm_mc_pim/peripheral/sercom/i2c_slave/plib_sercom3_i2c_slave.c    -DXPRJ_mclv2_pic32_cm_mc_pim=$(CND_CONF)    $(COMPARISON_BUILD)  -Wformat=2 -Wundef -Wshadow -Wpointer-arith -Wbad-function-cast -Wwrite-strings -Waggregate-return -Wstrict-prototypes -Wmissing-format-attribute -Wno-deprecated-declarations -Wredundant-decls -Wnested-externs -Winline -Wlong-long -Wunreachable-code -Wmissing-noreturn -mdfp="${DFP_DIR}/PIC32CM1216MC00048" ${PACK_COMMON_OPTIONS} 
	
${OBJECTDIR}/_ext/1151781917/plib_tc3.o: ../src/config/mclv2_pic32_cm_mc_pim/peripheral/tc/plib_tc3.c  .generated_files/flags/mclv2_pic32_cm_mc_pim/5b10ba1a715736d71265b69171e97fa237463ddd .generated_files/flags/mclv2_pic32_cm_mc_pim/ebc7eb1707317225d922e38c7cea395c4d1e499
	@${MKDIR} "${OBJECTDIR}/_ext/1151781917" 
	@${RM} ${OBJECTDIR}/_ext/1151781917/plib_tc3.o.d 
	@${RM} ${OBJECTDIR}/_ext/1151781917/plib_tc3.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -fdata-sections -O1 -I"../src" -I"../src/config/mclv2_pic32_cm_mc_pim" -I"../src/packs/CMSIS/" -I"../src/packs/CMSIS/CMSIS/Core/Include" -I"../src/packs/PIC32CM1216MC00048_DFP" -Werror -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/1151781917/plib_tc3.o.d" -o ${OBJECTDIR}/_ext/1151781917/plib_tc3.o ../src/config/mclv2_pic32_cm_mc_pim/peripheral/tc/plib_tc3.c    -DXPRJ_mclv2_pic32_cm_mc_pim=$(CND_CONF)    $(COMPARISON_BUILD)  -Wformat=2 -Wundef -Wshadow -Wpointer-arith -Wbad-function-cast -Wwrite-strings -Waggregate-return -Wstrict-prototypes -Wmissing-format-attribute -Wno-deprecated-declarations -Wredundant-decls -Wnested-externs -Winline -Wlong-long -Wunreachable-code -Wmissing-noreturn -mdfp="${DFP_DIR}/PIC32CM1216MC00048" ${PACK_COMMON_OPTIONS} 
	
${OBJECTDIR}/_ext/1151781917/plib_tc4.o: ../src/config/mclv2_pic32_cm_mc_pim/peripheral/tc/plib_tc4.c  .generated_files/flags/mclv2_pic32_cm_mc_pim/2f1432b34e0195fe42c1fa8391888f202d57a4dd .generated_files/flags/mclv2_pic32_cm_mc_pim/ebc7eb1707317225d922e38c7cea395c4d1e499
	@${MKDIR} "${OBJECTDIR}/_ext/1151781917" 
	@${RM} ${OBJECTDIR}/_ext/1151781917/plib_tc4.o.d 
	@${RM} ${OBJECTDIR}/_ext/1151781917/plib_tc4.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -fdata-sections -O1 -I"../src" -I"../src/config/mclv2_pic32_cm_mc_pim" -I"../src/packs/CMSIS/" -I"../src/packs/CMSIS/CMSIS/Core/Include" -I"../src/packs/PIC32CM1216MC00048_DFP" -Werror -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/1151781917/plib_tc4.o.d" -o ${OBJECTDIR}/_ext/1151781917/plib_tc4.o ../src/config/mclv2_pic32_cm_mc_pim/peripheral/tc/plib_tc4.c    -DXPRJ_mclv2_pic32_cm_mc_pim=$(CND_CONF)    $(COMPARISON_BUILD)  -Wformat=2 -Wundef -Wshadow -Wpointer-arith -Wbad-function-cast -Wwrite-strings -Waggregate-return -Wstrict-prototypes -Wmissing-format-attribute -Wno-deprecated-declarations -Wredundant-decls -Wnested-externs -Winline -Wlong-long -Wunreachable-code -Wmissing-noreturn -mdfp="${DFP_DIR}/PIC32CM1216MC00048" ${PACK_COMMON_OPTIONS} 
	
${OBJECTDIR}/_ext/1345501158/plib_tcc0.o: ../src/config/mclv2_pic32_cm_mc_pim/peripheral/tcc/plib_tcc0.c  .generated_files/flags/mclv2_pic32_cm_mc_pim/d68d37936aaff1f6e8829301d6b90337335aad64 .generated_files/flags/mclv2_pic32_cm_mc_pim/ebc7eb1707317225d922e38c7cea395c4d1e499
	@${MKDIR} "${OBJECTDIR}/_ext/1345501158" 
	@${RM} ${OBJECTDIR}/_ext/1345501158/plib_tcc0.o.d 
	@${RM} ${OBJECTDIR}/_ext/1345501158/plib_tcc0.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -fdata-sections -O1 -I"../src" -I"../src/config/mclv2_pic32_cm_mc_pim" -I"../src/packs/CMSIS/" -I"../src/packs/CMSIS/CMSIS/Core/Include" -I"../src/packs/PIC32CM1216MC00048_DFP" -Werror -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/1345501158/plib_tcc0.o.d" -o ${OBJECTDIR}/_ext/1345501158/plib_tcc0.o ../src/config/mclv2_pic32_cm_mc_pim/peripheral/tcc/plib_tcc0.c    -DXPRJ_mclv2_pic32_cm_mc_pim=$(CND_CONF)    $(COMPARISON_BUILD)  -Wformat=2 -Wundef -Wshadow -Wpointer-arith -Wbad-function-cast -Wwrite-strings -Waggregate-return -Wstrict-prototypes -Wmissing-format-attribute -Wno-deprecated-declarations -Wredundant-decls -Wnested-externs -Winline -Wlong-long -Wunreachable-code -Wmissing-noreturn -mdfp="${DFP_DIR}/PIC32CM1216MC00048" ${PACK_COMMON_OPTIONS} 
	
${OBJECTDIR}/_ext/1276539452/xc32_monitor.o: ../src/config/mclv2_pic32_cm_mc_pim/stdio/xc32_monitor.c  .generated_files/flags/mclv2_pic32_cm_mc_pim/bbdfcf51440bbd213f66df14eb7fc5f68051056e .generated_files/flags/mclv2_pic32_cm_mc_pim/ebc7eb1707317225d922e38c7cea395c4d1e499
	@${MKDIR} "${OBJECTDIR}/_ext/1276539452" 
	@${RM} ${OBJECTDIR}/_ext/1276539452/xc32_monitor.o.d 
	@${RM} ${OBJECTDIR}/_ext/1276539452/xc32_monitor.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -fdata-sections -O1 -I"../src" -I"../src/config/mclv2_pic32_cm_mc_pim" -I"../src/packs/CMSIS/" -I"../src/packs/CMSIS/CMSIS/Core/Include" -I"../src/packs/PIC32CM1216MC00048_DFP" -Werror -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/1276539452/xc32_monitor.o.d" -o ${OBJECTDIR}/_ext/1276539452/xc32_monitor.o ../src/config/mclv2_pic32_cm_mc_pim/stdio/xc32_monitor.c    -DXPRJ_mclv2_pic32_cm_mc_pim=$(CND_CONF)    $(COMPARISON_BUILD)  -Wformat=2 -Wundef -Wshadow -Wpointer-arith -Wbad-function-cast -Wwrite-strings -Waggregate-return -Wstrict-prototypes -Wmissing-format-attribute -Wno-deprecated-declarations -Wredundant-decls -Wnested-externs -Winline -Wlong-long -Wunreachable-code -Wmissing-noreturn -mdfp="${DFP_DIR}/PIC32CM1216MC00048" ${PACK_COMMON_OPTIONS} 
	
${OBJECTDIR}/_ext/1867106698/initialization.o: ../src/config/mclv2_pic32_cm_mc_pim/initialization.c  .generated_files/flags/mclv2_pic32_cm_mc_pim/9bae31c78dd2b31ce9786c7a752ca5dbcc5fbe53 .generated_files/flags/mclv2_pic32_cm_mc_pim/ebc7eb1707317225d922e38c7cea395c4d1e499
	@${MKDIR} "${OBJECTDIR}/_ext/1867106698" 
	@${RM} ${OBJECTDIR}/_ext/1867106698/initialization.o.d 
	@${RM} ${OBJECTDIR}/_ext/1867106698/initialization.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -fdata-sections -O1 -I"../src" -I"../src/config/mclv2_pic32_cm_mc_pim" -I"../src/packs/CMSIS/" -I"../src/packs/CMSIS/CMSIS/Core/Include" -I"../src/packs/PIC32CM1216MC00048_DFP" -Werror -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/1867106698/initialization.o.d" -o ${OBJECTDIR}/_ext/1867106698/initialization.o ../src/config/mclv2_pic32_cm_mc_pim/initialization.c    -DXPRJ_mclv2_pic32_cm_mc_pim=$(CND_CONF)    $(COMPARISON_BUILD)  -Wformat=2 -Wundef -Wshadow -Wpointer-arith -Wbad-function-cast -Wwrite-strings -Waggregate-return -Wstrict-prototypes -Wmissing-format-attribute -Wno-deprecated-declarations -Wredundant-decls -Wnested-externs -Winline -Wlong-long -Wunreachable-code -Wmissing-noreturn -mdfp="${DFP_DIR}/PIC32CM1216MC00048" ${PACK_COMMON_OPTIONS} 
	
${OBJECTDIR}/_ext/1867106698/interrupts.o: ../src/config/mclv2_pic32_cm_mc_pim/interrupts.c  .generated_files/flags/mclv2_pic32_cm_mc_pim/b13f31194547667b3e3d81816be0e3caaa700a60 .generated_files/flags/mclv2_pic32_cm_mc_pim/ebc7eb1707317225d922e38c7cea395c4d1e499
	@${MKDIR} "${OBJECTDIR}/_ext/1867106698" 
	@${RM} ${OBJECTDIR}/_ext/1867106698/interrupts.o.d 
	@${RM} ${OBJECTDIR}/_ext/1867106698/interrupts.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -fdata-sections -O1 -I"../src" -I"../src/config/mclv2_pic32_cm_mc_pim" -I"../src/packs/CMSIS/" -I"../src/packs/CMSIS/CMSIS/Core/Include" -I"../src/packs/PIC32CM1216MC00048_DFP" -Werror -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/1867106698/interrupts.o.d" -o ${OBJECTDIR}/_ext/1867106698/interrupts.o ../src/config/mclv2_pic32_cm_mc_pim/interrupts.c    -DXPRJ_mclv2_pic32_cm_mc_pim=$(CND_CONF)    $(COMPARISON_BUILD)  -Wformat=2 -Wundef -Wshadow -Wpointer-arith -Wbad-function-cast -Wwrite-strings -Waggregate-return -Wstrict-prototypes -Wmissing-format-attribute -Wno-deprecated-declarations -Wredundant-decls -Wnested-externs -Winline -Wlong-long -Wunreachable-code -Wmissing-noreturn -mdfp="${DFP_DIR}/PIC32CM1216MC00048" ${PACK_COMMON_OPTIONS} 
	
${OBJECTDIR}/_ext/1867106698/exceptions.o: ../src/config/mclv2_pic32_cm_mc_pim/exceptions.c  .generated_files/flags/mclv2_pic32_cm_mc_pim/5f7c2c1771206d54ce0cb9eacdd07b05d67a34b5 .generated_files/flags/mclv2_pic32_cm_mc_pim/ebc7eb1707317225d922e38c7cea395c4d1e499
	@${MKDIR} "${OBJECTDIR}/_ext/1867106698" 
	@${RM} ${OBJECTDIR}/_ext/1867106698/exceptions.o.d 
	@${RM} ${OBJECTDIR}/_ext/1867106698/exceptions.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -fdata-sections -O1 -I"../src" -I"../src/config/mclv2_pic32_cm_mc_pim" -I"../src/packs/CMSIS/" -I"../src/packs/CMSIS/CMSIS/Core/Include" -I"../src/packs/PIC32CM1216MC00048_DFP" -Werror -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/1867106698/exceptions.o.d" -o ${OBJECTDIR}/_ext/1867106698/exceptions.o ../src/config/mclv2_pic32_cm_mc_pim/exceptions.c    -DXPRJ_mclv2_pic32_cm_mc_pim=$(CND_CONF)    $(COMPARISON_BUILD)  -Wformat=2 -Wundef -Wshadow -Wpointer-arith -Wbad-function-cast -Wwrite-strings -Waggregate-return -Wstrict-prototypes -Wmissing-format-attribute -Wno-deprecated-declarations -Wredundant-decls -Wnested-externs -Winline -Wlong-long -Wunreachable-code -Wmissing-noreturn -mdfp="${DFP_DIR}/PIC32CM1216MC00048" ${PACK_COMMON_OPTIONS} 
	
${OBJECTDIR}/_ext/1867106698/startup_xc32.o: ../src/config/mclv2_pic32_cm_mc_pim/startup_xc32.c  .generated_files/flags/mclv2_pic32_cm_mc_pim/29d4ac55c4323b734bc67e04257ea7542d3ffebb .generated_files/flags/mclv2_pic32_cm_mc_pim/ebc7eb1707317225d922e38c7cea395c4d1e499
	@${MKDIR} "${OBJECTDIR}/_ext/1867106698" 
	@${RM} ${OBJECTDIR}/_ext/1867106698/startup_xc32.o.d 
	@${RM} ${OBJECTDIR}/_ext/1867106698/startup_xc32.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -fdata-sections -O1 -I"../src" -I"../src/config/mclv2_pic32_cm_mc_pim" -I"../src/packs/CMSIS/" -I"../src/packs/CMSIS/CMSIS/Core/Include" -I"../src/packs/PIC32CM1216MC00048_DFP" -Werror -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/1867106698/startup_xc32.o.d" -o ${OBJECTDIR}/_ext/1867106698/startup_xc32.o ../src/config/mclv2_pic32_cm_mc_pim/startup_xc32.c    -DXPRJ_mclv2_pic32_cm_mc_pim=$(CND_CONF)    $(COMPARISON_BUILD)  -Wformat=2 -Wundef -Wshadow -Wpointer-arith -Wbad-function-cast -Wwrite-strings -Waggregate-return -Wstrict-prototypes -Wmissing-format-attribute -Wno-deprecated-declarations -Wredundant-decls -Wnested-externs -Winline -Wlong-long -Wunreachable-code -Wmissing-noreturn -mdfp="${DFP_DIR}/PIC32CM1216MC00048" ${PACK_COMMON_OPTIONS} 
	
${OBJECTDIR}/_ext/1867106698/libc_syscalls.o: ../src/config/mclv2_pic32_cm_mc_pim/libc_syscalls.c  .generated_files/flags/mclv2_pic32_cm_mc_pim/da2befdd1595ac9808bc6b2e8e327f64dc6daa59 .generated_files/flags/mclv2_pic32_cm_mc_pim/ebc7eb1707317225d922e38c7cea395c4d1e499
	@${MKDIR} "${OBJECTDIR}/_ext/1867106698" 
	@${RM} ${OBJECTDIR}/_ext/1867106698/libc_syscalls.o.d 
	@${RM} ${OBJECTDIR}/_ext/1867106698/libc_syscalls.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -fdata-sections -O1 -I"../src" -I"../src/config/mclv2_pic32_cm_mc_pim" -I"../src/packs/CMSIS/" -I"../src/packs/CMSIS/CMSIS/Core/Include" -I"../src/packs/PIC32CM1216MC00048_DFP" -Werror -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/1867106698/libc_syscalls.o.d" -o ${OBJECTDIR}/_ext/1867106698/libc_syscalls.o ../src/config/mclv2_pic32_cm_mc_pim/libc_syscalls.c    -DXPRJ_mclv2_pic32_cm_mc_pim=$(CND_CONF)    $(COMPARISON_BUILD)  -Wformat=2 -Wundef -Wshadow -Wpointer-arith -Wbad-function-cast -Wwrite-strings -Waggregate-return -Wstrict-prototypes -Wmissing-format-attribute -Wno-deprecated-declarations -Wredundant-decls -Wnested-externs -Winline -Wlong-long -Wunreachable-code -Wmissing-noreturn -mdfp="${DFP_DIR}/PIC32CM1216MC00048" ${PACK_COMMON_OPTIONS} 
	
${OBJECTDIR}/_ext/1360937237/mc_app.o: ../src/mc_app.c  .generated_files/flags/mclv2_pic32_cm_mc_pim/dc11a5f3e1b4fc5aac66e878c5ce8730b7a0fa25 .generated_files/flags/mclv2_pic32_cm_mc_pim/ebc7eb1707317225d922e38c7cea395c4d1e499
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/mc_app.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/mc_app.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -fdata-sections -O1 -I"../src" -I"../src/config/mclv2_pic32_cm_mc_pim" -I"../src/packs/CMSIS/" -I"../src/packs/CMSIS/CMSIS/Core/Include" -I"../src/packs/PIC32CM1216MC00048_DFP" -Werror -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/1360937237/mc_app.o.d" -o ${OBJECTDIR}/_ext/1360937237/mc_app.o ../src/mc_app.c    -DXPRJ_mclv2_pic32_cm_mc_pim=$(CND_CONF)    $(COMPARISON_BUILD)  -Wformat=2 -Wundef -Wshadow -Wpointer-arith -Wbad-function-cast -Wwrite-strings -Waggregate-return -Wstrict-prototypes -Wmissing-format-attribute -Wno-deprecated-declarations -Wredundant-decls -Wnested-externs -Winline -Wlong-long -Wunreachable-code -Wmissing-noreturn -mdfp="${DFP_DIR}/PIC32CM1216MC00048" ${PACK_COMMON_OPTIONS} 
	
${OBJECTDIR}/_ext/1360937237/q14_generic_mcLib.o: ../src/q14_generic_mcLib.c  .generated_files/flags/mclv2_pic32_cm_mc_pim/2c35dbe7b70103acbc098a9680c7c4f4525e78b4 .generated_files/flags/mclv2_pic32_cm_mc_pim/ebc7eb1707317225d922e38c7cea395c4d1e499
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/q14_generic_mcLib.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/q14_generic_mcLib.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -fdata-sections -O1 -I"../src" -I"../src/config/mclv2_pic32_cm_mc_pim" -I"../src/packs/CMSIS/" -I"../src/packs/CMSIS/CMSIS/Core/Include" -I"../src/packs/PIC32CM1216MC00048_DFP" -Werror -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/1360937237/q14_generic_mcLib.o.d" -o ${OBJECTDIR}/_ext/1360937237/q14_generic_mcLib.o ../src/q14_generic_mcLib.c    -DXPRJ_mclv2_pic32_cm_mc_pim=$(CND_CONF)    $(COMPARISON_BUILD)  -Wformat=2 -Wundef -Wshadow -Wpointer-arith -Wbad-function-cast -Wwrite-strings -Waggregate-return -Wstrict-prototypes -Wmissing-format-attribute -Wno-deprecated-declarations -Wredundant-decls -Wnested-externs -Winline -Wlong-long -Wunreachable-code -Wmissing-noreturn -mdfp="${DFP_DIR}/PIC32CM1216MC00048" ${PACK_COMMON_OPTIONS} 
	
${OBJECTDIR}/_ext/1360937237/main_mclv2_pic32_cm_mc.o: ../src/main_mclv2_pic32_cm_mc.c  .generated_files/flags/mclv2_pic32_cm_mc_pim/e357e01fa4106f04010777f68e376c19ae930400 .generated_files/flags/mclv2_pic32_cm_mc_pim/ebc7eb1707317225d922e38c7cea395c4d1e499
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/main_mclv2_pic32_cm_mc.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/main_mclv2_pic32_cm_mc.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -fdata-sections -O1 -I"../src" -I"../src/config/mclv2_pic32_cm_mc_pim" -I"../src/packs/CMSIS/" -I"../src/packs/CMSIS/CMSIS/Core/Include" -I"../src/packs/PIC32CM1216MC00048_DFP" -Werror -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/1360937237/main_mclv2_pic32_cm_mc.o.d" -o ${OBJECTDIR}/_ext/1360937237/main_mclv2_pic32_cm_mc.o ../src/main_mclv2_pic32_cm_mc.c    -DXPRJ_mclv2_pic32_cm_mc_pim=$(CND_CONF)    $(COMPARISON_BUILD)  -Wformat=2 -Wundef -Wshadow -Wpointer-arith -Wbad-function-cast -Wwrite-strings -Waggregate-return -Wstrict-prototypes -Wmissing-format-attribute -Wno-deprecated-declarations -Wredundant-decls -Wnested-externs -Winline -Wlong-long -Wunreachable-code -Wmissing-noreturn -mdfp="${DFP_DIR}/PIC32CM1216MC00048" ${PACK_COMMON_OPTIONS} 
	
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: compileCPP
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
else
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: link
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
dist/${CND_CONF}/${IMAGE_TYPE}/bldc_bc_hall_pic32_cm_mc_mclv2_I2C.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}: ${OBJECTFILES}  nbproject/Makefile-${CND_CONF}.mk    ../src/config/mclv2_pic32_cm_mc_pim/PIC32CM1216MC00048.ld
	@${MKDIR} dist/${CND_CONF}/${IMAGE_TYPE} 
	${MP_CC} $(MP_EXTRA_LD_PRE) -g   -mprocessor=$(MP_PROCESSOR_OPTION) -mno-device-startup-code -o dist/${CND_CONF}/${IMAGE_TYPE}/bldc_bc_hall_pic32_cm_mc_mclv2_I2C.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX} ${OBJECTFILES_QUOTED_IF_SPACED}          -DXPRJ_mclv2_pic32_cm_mc_pim=$(CND_CONF)    $(COMPARISON_BUILD)  -Wl,--defsym=__MPLAB_BUILD=1$(MP_EXTRA_LD_POST)$(MP_LINKER_FILE_OPTION),--defsym=__ICD2RAM=1,--defsym=__MPLAB_DEBUG=1,--defsym=__DEBUG=1,-D=__DEBUG_D,--defsym=_min_heap_size=512,--gc-sections,-Map="${DISTDIR}/${PROJECTNAME}.${IMAGE_TYPE}.map",--memorysummary,dist/${CND_CONF}/${IMAGE_TYPE}/memoryfile.xml -mdfp="${DFP_DIR}/PIC32CM1216MC00048"
	
else
dist/${CND_CONF}/${IMAGE_TYPE}/bldc_bc_hall_pic32_cm_mc_mclv2_I2C.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}: ${OBJECTFILES}  nbproject/Makefile-${CND_CONF}.mk   ../src/config/mclv2_pic32_cm_mc_pim/PIC32CM1216MC00048.ld
	@${MKDIR} dist/${CND_CONF}/${IMAGE_TYPE} 
	${MP_CC} $(MP_EXTRA_LD_PRE)  -mprocessor=$(MP_PROCESSOR_OPTION) -mno-device-startup-code -o dist/${CND_CONF}/${IMAGE_TYPE}/bldc_bc_hall_pic32_cm_mc_mclv2_I2C.X.${IMAGE_TYPE}.${DEBUGGABLE_SUFFIX} ${OBJECTFILES_QUOTED_IF_SPACED}          -DXPRJ_mclv2_pic32_cm_mc_pim=$(CND_CONF)    $(COMPARISON_BUILD)  -Wl,--defsym=__MPLAB_BUILD=1$(MP_EXTRA_LD_POST)$(MP_LINKER_FILE_OPTION),--defsym=_min_heap_size=512,--gc-sections,-Map="${DISTDIR}/${PROJECTNAME}.${IMAGE_TYPE}.map",--memorysummary,dist/${CND_CONF}/${IMAGE_TYPE}/memoryfile.xml -mdfp="${DFP_DIR}/PIC32CM1216MC00048"
	${MP_CC_DIR}\\xc32-bin2hex dist/${CND_CONF}/${IMAGE_TYPE}/bldc_bc_hall_pic32_cm_mc_mclv2_I2C.X.${IMAGE_TYPE}.${DEBUGGABLE_SUFFIX} 
endif


# Subprojects
.build-subprojects:


# Subprojects
.clean-subprojects:

# Clean Targets
.clean-conf: ${CLEAN_SUBPROJECTS}
	${RM} -r build/mclv2_pic32_cm_mc_pim
	${RM} -r dist/mclv2_pic32_cm_mc_pim

# Enable dependency checking
.dep.inc: .depcheck-impl

DEPFILES=$(shell mplabwildcard ${POSSIBLE_DEPFILES})
ifneq (${DEPFILES},)
include ${DEPFILES}
endif
