/*******************************************************************************
  Main Source File

  Company:
    Microchip Technology Inc.

  File Name:
    main.c

  Summary:
    This file contains the "main" function for a project.

  Description:
    This file contains the "main" function for a project.  The
    "main" function calls the "SYS_Initialize" function to initialize the state
    machines of all modules in the system
 *******************************************************************************/
// DOM-IGNORE-BEGIN
/*******************************************************************************
 * Copyright (C) 2019 Microchip Technology Inc. and its subsidiaries.
 *
 * Subject to your compliance with these terms, you may use Microchip software
 * and any derivatives exclusively with Microchip products. It is your
 * responsibility to comply with third party license terms applicable to your
 * use of third party software (including open source software) that may
 * accompany Microchip software.
 *
 * THIS SOFTWARE IS SUPPLIED BY MICROCHIP "AS IS". NO WARRANTIES, WHETHER
 * EXPRESS, IMPLIED OR STATUTORY, APPLY TO THIS SOFTWARE, INCLUDING ANY IMPLIED
 * WARRANTIES OF NON-INFRINGEMENT, MERCHANTABILITY, AND FITNESS FOR A
 * PARTICULAR PURPOSE.
 *
 * IN NO EVENT WILL MICROCHIP BE LIABLE FOR ANY INDIRECT, SPECIAL, PUNITIVE,
 * INCIDENTAL OR CONSEQUENTIAL LOSS, DAMAGE, COST OR EXPENSE OF ANY KIND
 * WHATSOEVER RELATED TO THE SOFTWARE, HOWEVER CAUSED, EVEN IF MICROCHIP HAS
 * BEEN ADVISED OF THE POSSIBILITY OR THE DAMAGES ARE FORESEEABLE. TO THE
 * FULLEST EXTENT ALLOWED BY LAW, MICROCHIP'S TOTAL LIABILITY ON ALL CLAIMS IN
 * ANY WAY RELATED TO THIS SOFTWARE WILL NOT EXCEED THE AMOUNT OF FEES, IF ANY,
 * THAT YOU HAVE PAID DIRECTLY TO MICROCHIP FOR THIS SOFTWARE.
 *******************************************************************************/
// DOM-IGNORE-END
// *****************************************************************************
// *****************************************************************************
// Section: Included Files
// *****************************************************************************
// *****************************************************************************

#include <stddef.h>                     // Defines NULL
#include <stdbool.h>                    // Defines true
#include <stdlib.h>                     // Defines EXIT_FAILURE
#include "definitions.h"                // SYS function prototypes
#include "mc_app.h"
#include "stdio.h"
#include "string.h"


extern motor_state_params_t Motor_StateParams;

typedef struct {
    uint32_t inputVal; /* read value of button input pin */
    uint16_t state;
    uint16_t cnt;
} button_response_t;

//I2C Variables

typedef struct {
    uint8_t motor_on;
    uint8_t direction;
    uint16_t set_rpm;
    uint16_t current_rpm;
    uint8_t motor_fault;
} bldc_struct;

bldc_struct bldc;
bldc_variables_struct_t my_variables;

#define EEPROM_PAGE_SIZE_BYTES                  32
#define EEPROM_PAGE_SIZE_MASK                   0xFF
#define EEPROM_SIZE_BYTES                       32

#define FAULT_RESTART_TIMEOUT           50

typedef enum {
    EEPROM_CMD_WRITE,
    EEPROM_CMD_IDLE,
} EEPROM_CMD;

typedef struct {
    /* currentAddrPtr - to allow for sequential read (from the current address) */
    uint16_t currentAddrPtr;
    /* addrIndex - used to copy 2 bytes of EEPROM memory address */
    uint8_t addrIndex;
    /* wrBuffer - holds the incoming data from the I2C master */
    uint8_t wrBuffer[EEPROM_PAGE_SIZE_BYTES];
    /* wrBufferIndex - Index into the wrBuffer[] */
    uint16_t wrBufferIndex;
    /* wrAddr - indicates the starting address of the EEPROM emulation memory to write to */
    volatile uint16_t wrAddr;
    /* nWrBytes - indicates the number of bytes to write to EEPROM emulation buffer */
    volatile uint8_t nWrBytes;
    /* internalWriteInProgress - indicates that EEPROM is busy with internal writes */
    bool internalWriteInProgress;
    /* eepromCommand - used to trigger write to the EEPROM emulation buffer */
    EEPROM_CMD eepromCommand;
} EEPROM_DATA;

EEPROM_DATA eepromData;

uint8_t EEPROM_EmulationBuffer[EEPROM_SIZE_BYTES] ={
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
};

button_response_t button_S2_data;
button_response_t button_S3_data;

extern motor_bc_params_t Motor_BCParams;

void EEPROM_StateMachine(void);
void buttonStartStopToggle(void);
void buttonDirectionToggle(void);

void buttonRespond(button_response_t * buttonResData, void (* buttonJob)(void));

//I2C Function Declarations
void bldc_init(void);
void bldc_write_out_packet(void);
void bldc_read_in_packet(void);
void meti_bldc_current_rpm(void);
void fault_status(void);
void applyRpmFilter(void);


//State machine implementation

#define FAULT_RESTART_TIMEOUT           50
#define CONFIG_CHANGE_RESTART_TIMEOUT   50

uint8_t meti_sm_state = METI_SM_MOTOR_IDLE;

void METI_StateMachine(void);

//void METI_FaultClearRestart(void);

bool APP_SERCOM_I2C_Callback(SERCOM_I2C_SLAVE_TRANSFER_EVENT event, uintptr_t contextHandle) {
    bool isSuccess = true;

    switch (event) {
        case SERCOM_I2C_SLAVE_TRANSFER_EVENT_ADDR_MATCH:
            if ((SERCOM3_I2C_TransferDirGet() == SERCOM_I2C_SLAVE_TRANSFER_DIR_WRITE) && (eepromData.internalWriteInProgress == true)) {
                /* EEPROM is busy. Send NAK */
                isSuccess = false;
            } else {
                /* Reset the indexes */
                eepromData.addrIndex = 0;
                eepromData.wrBufferIndex = 0;
            }
            bldc_write_out_packet();
            break;

        case SERCOM_I2C_SLAVE_TRANSFER_EVENT_RX_READY:
            /* Read the data sent by I2C Master */
            if (eepromData.addrIndex < 2) {
                ((uint8_t*) & eepromData.currentAddrPtr)[eepromData.addrIndex++] = SERCOM3_I2C_ReadByte();
            } else {
                eepromData.wrBuffer[(eepromData.wrBufferIndex & EEPROM_PAGE_SIZE_MASK)] = SERCOM3_I2C_ReadByte();
                eepromData.wrBufferIndex++;
            }
            bldc_read_in_packet();
            break;

        case SERCOM_I2C_SLAVE_TRANSFER_EVENT_TX_READY:
            /* Provide the EEPROM data requested by the I2C Master */
            SERCOM3_I2C_WriteByte(EEPROM_EmulationBuffer[eepromData.currentAddrPtr++]);
            if (eepromData.currentAddrPtr >= EEPROM_SIZE_BYTES) {
                eepromData.currentAddrPtr = 0;
            }
            break;

        case SERCOM_I2C_SLAVE_TRANSFER_EVENT_STOP_BIT_RECEIVED:
            if (eepromData.wrBufferIndex > 0) {
                if (eepromData.wrBufferIndex > EEPROM_PAGE_SIZE_BYTES) {
                    eepromData.wrBufferIndex = EEPROM_PAGE_SIZE_BYTES;
                }
                eepromData.wrAddr = eepromData.currentAddrPtr;
                eepromData.nWrBytes = eepromData.wrBufferIndex;

                /* Update the current address pointer to allow for sequential read */
                eepromData.currentAddrPtr += eepromData.wrBufferIndex;

                /* Reset the indexes */
                eepromData.addrIndex = 0;
                eepromData.wrBufferIndex = 0;

                /* Set busy flag to send NAK for any write requests */
                eepromData.internalWriteInProgress = true;
                eepromData.eepromCommand = EEPROM_CMD_WRITE;
            }
            break;
        default:
            break;
    }
    return isSuccess;
}


// *****************************************************************************
// *****************************************************************************
// Section: Main Entry Point
// *****************************************************************************
// *****************************************************************************

int main(void) {
    //    char buff[128];
    /* Initialize all modules */
    SYS_Initialize(NULL);

    //Set I2C Idle State
    eepromData.eepromCommand = EEPROM_CMD_IDLE;
    SERCOM3_I2C_CallbackRegister(APP_SERCOM_I2C_Callback, 0);

    LED1_OC_FAULT_Clear();

    MCAPP_Start();

    while (true) {
        SYS_Tasks();

        EEPROM_StateMachine();

        if (1U == Motor_StateParams.var_time_10ms) {
            button_S2_data.inputVal = BTN_START_STOP_Get();
            buttonRespond(&button_S2_data, &buttonStartStopToggle);

            button_S3_data.inputVal = BTN_DIR_TGL_Get();
            buttonRespond(&button_S3_data, &buttonDirectionToggle);

            Motor_StateParams.var_time_10ms = 0;

            METI_StateMachine();
        }

        if (my_variables.data_ready_flag == 1) {
            my_variables.data_ready_flag = 0;
            applyRpmFilter();
        }

    }

    /* Execution should not come here during normal operation */
    return ( EXIT_FAILURE);
}

void buttonRespond(button_response_t * buttonResData, void (* buttonJob)(void)) {
    switch (buttonResData->state) {
        case 0u: /* Detect if button is pressed. */
            if (buttonResData->inputVal == 0u) {
                buttonJob();
                buttonResData->cnt = 0u;
                buttonResData->state = 1u;
            }
            break;
        case 1u: /* Stay idle for 500ms, and then return to detect. */
            buttonResData->cnt++;
            if (buttonResData->cnt >= 50) {
                buttonResData->cnt = 0u;
                buttonResData->state = 0u;
            }
            break;
        default:
            break;

    }
}

void buttonStartStopToggle(void) {
    Motor_StateParams.switch_state ^= 1;

    if (1U == Motor_StateParams.switch_state) {
        Motor_StateParams.state_run = 1;
        Motor_Start();
    } else {
        Motor_StateParams.state_run = 0;
        Motor_Stop();
    }
}

void buttonDirectionToggle(void) {
    // Direction can be changed only when motor is stopped
    if (!Motor_StateParams.state_run) {
        Motor_StateParams.direction ^= 1;
        LED2_DIRECTION_Toggle();

        if (!Motor_StateParams.direction) {
            Motor_StateParams.direction_offset = 0;
        } else {
            Motor_StateParams.direction_offset = 8;
        }
    }
}

void directionToggle(void) {
    // Direction can be changed only when motor is stopped
    if (!Motor_StateParams.state_run) {

        if (!Motor_StateParams.direction) {
            Motor_StateParams.direction_offset = 0;
        } else {
            Motor_StateParams.direction_offset = 8;
        }
    }
}

void EEPROM_StateMachine(void) {
    switch (eepromData.eepromCommand) {
        case EEPROM_CMD_WRITE:
            memcpy(&EEPROM_EmulationBuffer[eepromData.wrAddr], &eepromData.wrBuffer[0], eepromData.nWrBytes);
            eepromData.internalWriteInProgress = false;
            eepromData.eepromCommand = EEPROM_CMD_IDLE;
            
            bldc_read_in_packet();
            //If any write command is received through I2C, stop the motor first
            meti_sm_state = METI_SM_MOTOR_OFF;  
            break;
        case EEPROM_CMD_IDLE:
            /* Do Nothing */
            break;
    }
}

void bldc_init(void) {
    memset(EEPROM_EmulationBuffer, 0, EEPROM_SIZE_BYTES);
    bldc.direction = 0;
    bldc.motor_on = 0;
    bldc.set_rpm = 0;
    bldc.current_rpm = 0;
    bldc.motor_fault = MOTOR_FAULT_CLEARED;
    memcpy(&EEPROM_EmulationBuffer, &bldc, sizeof (bldc));
}

void bldc_write_out_packet(void) {
    meti_bldc_current_rpm();
    fault_status();
    memcpy(&EEPROM_EmulationBuffer, &bldc, sizeof (bldc));
}

void bldc_read_in_packet(void) {
    memcpy(&bldc, &EEPROM_EmulationBuffer, sizeof (bldc));
}

void meti_bldc_current_rpm(void) {
    //bldc.current_rpm=Motor_BCParams.actual_speed; //working code-without filter
    bldc.current_rpm = my_variables.filtered_rpm;
}

void fault_status(void) {
    bldc.motor_fault = Motor_StateParams.motor_fault;
}

void applyRpmFilter(void) {
    static uint16_t prev_actual_speed = 0;
    static float temp_val;
    static uint8_t counter = 10;

    counter--;
    if (counter == 0) {
        counter = 10;
        //myParameters.actual_speed will come from PDEC_VLC_InterruptHandler 
        temp_val = (prev_actual_speed * 0.75) + (myParams.actual_speed * .25);

        my_variables.filtered_rpm = temp_val;
        prev_actual_speed = temp_val;
    }
}

void METI_StateMachine(void)
{
	static uint8_t fault10msCounter = 0;
	static uint8_t restart10msCounter = 0;
    static uint8_t waitBeforeStop10msCounter = 0;
    uint16_t speed_target =0;
        
    switch(meti_sm_state)
    {    
        case METI_SM_MOTOR_IDLE:
            break;

        case METI_SM_MOTOR_ON:
            Motor_StateParams.state_run = 1;
            Motor_StateParams.direction = bldc.direction;

            if (!Motor_StateParams.direction) {
                Motor_StateParams.direction_offset = 0;
            } else {
                Motor_StateParams.direction_offset = 8;
            }

            speed_target = (bldc.set_rpm * 0.82) * 10;

            if (speed_target > METI_MAX_ALLOWED_RPM) {
                Motor_BCParams.set_speed_target = METI_MAX_ALLOWED_RPM;
            } else if (speed_target < METI_MIN_ALLOWED_RPM) {
                Motor_BCParams.set_speed_target = METI_MIN_ALLOWED_RPM;
            } else {
                Motor_BCParams.set_speed_target = speed_target;
            }
            
            Motor_Start();

            meti_sm_state = METI_SM_MOTOR_RUNNING;       
            break;

        case METI_SM_MOTOR_RUNNING:
            break;

        case METI_SM_MOTOR_OFF:
            //If motor is running above 600 RPM, Slow down first and then stop
            if (myParams.actual_speed > 600) {
                //Slow down to 200 RPM and wait....
                speed_target = (200 * 0.82) * 10;
                Motor_BCParams.set_speed_target = speed_target;
                waitBeforeStop10msCounter = 10;
                meti_sm_state = METI_SM_MOTOR_WAIT_BEFORE_STOP;
            } else {
                Motor_StateParams.state_run = 0;
                Motor_StateParams.direction = 0;
                Motor_BCParams.set_speed_target = 0;
                Motor_Stop();

                if (Motor_StateParams.motor_fault == MOTOR_FAULT_DETECTED) {
                    fault10msCounter = FAULT_RESTART_TIMEOUT;
                    meti_sm_state = METI_SM_MOTOR_FAULT;
                }
                else if (bldc.motor_on == 1) {
                    restart10msCounter = CONFIG_CHANGE_RESTART_TIMEOUT;
                    meti_sm_state = METI_SM_WAIT_BEFORE_RESTART;
                } else {
                    meti_sm_state = METI_SM_MOTOR_IDLE;
                }
            }
            break;        

        case METI_SM_MOTOR_WAIT_BEFORE_STOP:
            waitBeforeStop10msCounter--;
            if (waitBeforeStop10msCounter == 0) {
                meti_sm_state = METI_SM_MOTOR_OFF;
            }
            break;
            
        case METI_SM_WAIT_BEFORE_RESTART:
        	restart10msCounter--;
	        if (restart10msCounter == 0) {
	            meti_sm_state = METI_SM_MOTOR_ON;
	        }			
            break;

        case METI_SM_MOTOR_FAULT:
	        fault10msCounter--;
	        if (fault10msCounter == 0) {
	            fault10msCounter = FAULT_RESTART_TIMEOUT;
                Motor_StateParams.motor_fault = MOTOR_FAULT_CLEARED;
                LED1_OC_FAULT_Clear();
                MCAPP_MotorControlVarsInit();
                meti_sm_state = METI_SM_MOTOR_ON;     
            }
            break;
    }
}

/*******************************************************************************
 End of File
 */
