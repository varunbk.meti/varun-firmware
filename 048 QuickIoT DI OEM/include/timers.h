#ifndef _HEADER_TIMERS_H_
#define _HEADER_TIMERS_H_

//Constatnts
#define MAX_TIMERS 6
#define TIMER_1_MS 0
#define TIMER_50_MS 1
#define TIMER_100_MS 2
#define TIMER_500_MS 3
#define TIMER_1000_MS 4
#define TIMER_10_MS 5

//Variables
extern bool timeout;
struct timeSlots
{
    int ms;
    int dc;
    bool done;
};
extern struct timeSlots ts[MAX_TIMERS];

//Functions
void timer_start(void);
void timer_check(void);

#endif