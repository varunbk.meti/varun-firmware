#ifndef _HEADER_MAIN_H_
#define _HEADER_MAIN_H_
#include <Arduino.h>
#include <ArduinoJson.h>
#include <ESPAsyncWebServer.h>
#include <logger.h>

//Constatnts
#define PIN_FORCED_AP_MODE_SWITCH GPIO_NUM_39

#define ENABLE_MQTT
#define ENABLE_WIFI
//#define ENABLE_MODBUS_MASTER
// #define ENABLE_NVS
// #define ENABLE_I2C_MASTER
// #define ENABLE_SCALING
// #define ENABLE_NON_LINEAR_SCALING
//#define ENABLE_OUTPUT_CONTROL
//#define ENABLE_5110_DISPLAY
#define ENABLE_MODBUS_SLAVE
#define Enable_Water_Lvl_Ctrl
// #define ENABLE_DEVICE_SUBSCRIPTION_CHECK

#define PIN_RELAY_1 GPIO_NUM_27

//Variables
extern AsyncWebServer server;
extern Logger *g_pLogger;
extern uint32_t active_driver_id;
extern bool g_valid_subscription;

//Functions
void timer_timeout_1ms(void);
void timer_timeout_10ms(void);
void timer_timeout_50ms(void);
void timer_timeout_100ms(void);
void timer_timeout_500ms(void);
void timer_timeout_1000ms(void);

void start_file_storage(void);
void update_device_sensor_values(void);
void setup_device_params(void);
void store_sta_ip_address(void);

void InitializeIndicators(void);
void InitializeBlinkProcess(void);
void update_params(void);
void meti_device_restart(void);

void InitializeMBRConfig(void);
bool InitializeNode(void);
void eventDataReceived(DynamicJsonDocument doc);
void test_routine(void);
String md5(String str);
void i2cm_update_sensor_data(void);
void process_output_setup(void);
void process_output_control(void);
void validateDeviceSubscription(void);
void lockDevice(void);

void DI_Watrer_Lvl_Read(void);
void update_DI_data(void);


#endif