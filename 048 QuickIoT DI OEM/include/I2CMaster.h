/*******************************************************************************
* File Name: I2CMaster.h
*
* Version: 1.0
*
* Description:
* This is header file for i2c master functions All the functions related to
* i2c master are here.
*
* Written By:
* Yogesh M Iggalore
*
*
********************************************************************************
* Copyright (2021-22) , METI
********************************************************************************/

#ifndef I2CMaster_h
#define I2CMaster_h
#include <Arduino.h>

#define I2C_ADDRESS 10
#define SENSOR_OPEN_STATE -1
#define SENSOR_OPEN_CURRENT 300
#define SENSOR_OPEN_VAL 9999999.99

#define CURRENT_MIN  400   
#define CURRENT_MAX  2000
#define LEVEL_MIN  0   
#define LEVEL_MAX  1000

#define IDAC_CURRENT_STEP_SIZE      2.4  


extern bool calibrate_sensor;

class I2CMaster
{
public:
    I2CMaster();

    void Start(void);
    void Read_From_Device(void);

    int16_t Get_ADCRawCount(void);

    double Get_milliVolts(void);
    double Get_ScaledCurrent(uint8_t decimals);
    double Get_ScaledValue(uint8_t decimals);
    double Get_ScaledValue_NL(uint8_t decimals);
    void Update_ScaledValues(void);
    double Get_FinalValue(uint8_t value_idx, uint8_t decimals);
    void Calibrate_IDAC(void);
    double Get_IDAC_Current(void);
    uint16_t Get_Resistance_Value(void); 

private:
    uint16_t ui16_voltage;
    uint16_t ui16_scaledValue;
    int16_t i16_adcCount;
    uint8_t ui8_dacValue;
};

#if !defined(NO_GLOBAL_INSTANCES) && !defined(NO_GLOBAL_I2CMASTER)
extern I2CMaster I2CM;
#endif
#endif