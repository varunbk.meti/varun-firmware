#ifndef _MBR_CONFIG_H_INCLUDED_
#define _MBR_CONFIG_H_INCLUDED_ 1

#include<Arduino.h>
#include<ArduinoJson.h>
#include<string>
#include<accesscontrol.h>
#include<modbusaddresstype.h>
#include<speedtype.h>
#include<paritytype.h>
#include<modbusfunction.h>
// #include<resultset.h>
#include<cdate.h>
#include<cthread.h>
#include<logger.h>
#include<sensor.h>
// #include<packet.h>
// #include<packet_mbr_cp_v4.h>

#define IPV4_ADDR_LEN 		4
#define MAX_MOBILE_NO_LEN 	15

#define MAX_SENSORS_PER_MBR 64

using namespace std;

class MBRConfig
{
public:
	bool IsScanTriggered(CDate& p_pDate);
	bool IsScanActive();
	void RunScan();
	static void SetCallBack( void (*p_pFunction)( DynamicJsonDocument value));

	MBRConfig();
	~MBRConfig();
	string ToString();

	void SetRepeatInterval(uint16 value);
	uint16 GetRepeatInterval();
	void SetActive(bool value);
	bool IsActive();
	void SetPort(ModbusAddressType value);
	ModbusAddressType GetPort();
	string GetName();
	void SetName(string value);
	void SetCommandIndex(uint8 value);
	uint8 GetCommandIndex();
	void SetComSpeed(SpeedType value);
	SpeedType GetComSpeed();
	void SetParity(ParityType value);
	ParityType GetParity();
	void SetDataBits(uint8 value);
	uint8 GetDataBits();
	void SetStopBit(uint8 value);
	uint8 GetStopBit();

	void SetSlaveAddress(uint8 value);
	uint8 GetSlaveAddress();
	void SetFunction(ModbusFunction value);
	ModbusFunction GetFunction();
	void SetStartRegAddress(uint16 value);
	uint16 GetStartRegAddress();
	void SetRegisterCount(uint16 value);
	uint16 GetRegisterCount();

	void SetTimeoutMilliSec(uint16 value);
	uint16 GetTimeoutMilliSec();
	void SetRetryCount(uint8 value);
	uint8 GetRetryCount();

	string GetSlaveIpAddress();
	void SetSlaveIpAddress(uint8* value);
	uint16 GetSlaveIpPort();
	void SetSlaveIpPort(uint16 value);
	uint8 GetDuplicateCRCTxFreq();
	void SetDuplicateCRCTxFreq(uint8 value);
	bool IsStorageEnabled();
	void EnableStorage();
	void DisableStorage();
	void SetHighSpeedDevice(bool value);
	bool IsHighSpeedDevice();
	uint8 GetAddressType();
	void SetAddressType( uint8 value);

	void Parse(JsonObject src);
	static void Initialize(Logger* p_pLogger);
	static void EnableGlobalAbsoluteTimeScan();
	static void DisableGlobalAbsoluteTimeScan();
	static void Release();

private:
	ulong	m_lCommandPK;
	string	m_sName;
	uint8   m_iCommandIndex;
	ModbusAddressType m_oPort;
	bool    m_bActive;
	uint16  m_iRepeatInterval;

	SpeedType m_oComSpeed;
	ParityType m_oParity;
	uint8   m_iDataBits;
	uint8   m_iStopBit;

	uint8   m_iSlaveAddress;
	ModbusFunction   m_oFunction;
	uint16  m_iStartRegAddress;
	uint16  m_iRegisterCount;
	uint8	m_iRegisterSize;

	uint16  m_iTimeoutMilliSec;
	uint8   m_iRetryCount;

	uint8   m_iAddressType;
	uint8   m_pSlaveIpAddress[IPV4_ADDR_LEN];
	uint16  m_iSlaveIpPort;

	uint8   m_iDuplicateCRCTxFreq;

	bool	m_bStorageEnabled;
	bool	m_bHighSpeedDevice;

	AccessControl m_oDataAccess;

	bool m_bInAbsoluteTimeScan;
	uint16  m_iLastReceivedCRC;
	bool m_bScanTriggered;
	bool m_bScanActive;
	CDate m_oScanTriggeredTime;

	uint8 	m_iSensorCount;
	Sensor m_pSensors[MAX_SENSORS_PER_MBR];

	static void ThreadRunModbusScan(void*);
	static bool m_bGlobalAbsTimeScanMode;
	static bool m_bDeleteLogger;
	static void (*dataReceivedCallback)( DynamicJsonDocument value);
	static Logger* m_pLogger;
};

#endif
