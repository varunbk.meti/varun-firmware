#ifndef _DATA_SERVICE_H_INCLUDED_
#define _DATA_SERVICE_H_INCLUDED_ 1

#include<string>
#include<defs.h>
#include<logger.h>
// #include<dataset.h>

using namespace std;

class DataService
{
public:
	virtual void Start() = 0;
	virtual void Stop() = 0;
	// virtual DataSet* GetData() = 0;
	virtual void StateMachine() = 0;

	void Initialize(Logger* p_pLogger);
	DataService();
	virtual ~DataService();
	string ToString();
protected:
	bool m_bDeleteLogger;
	Logger* m_pLogger;
};

#endif
