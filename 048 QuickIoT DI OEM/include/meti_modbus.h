#ifndef _METI_MODBUS_H_INCLUDED_
#define _METI_MODBUS_H_INCLUDED_ 1
#include <Arduino.h>

#ifdef ENABLE_MODBUS_MASTER
//Constants
#define ModbusSerial Serial2

#define PIN_MODBUS_TX_ENABLE GPIO_NUM_21
#define PIN_MODBUS_TX GPIO_NUM_17
#define PIN_MODBUS_RX GPIO_NUM_16

#define MODBUS_PARITY_NONE 0
#define MODBUS_PARITY_EVEN 1
#define MODBUS_PARITY_ODD 2

#define MODBUS_STOP_BIT_1 1
#define MODBUS_STOP_BIT_2 2

// #define MODBUS_DATA_BITS_5 5
// #define MODBUS_DATA_BITS_6 6
// #define MODBUS_DATA_BITS_7 7
#define MODBUS_DATA_BITS_8 8

//Variables
extern uint32_t g_modbusSerialConfig;

//Functions
uint32_t get_uart_config(uint8_t ui8Parity, uint8_t ui8Stopbit);
String get_uart_config_label(uint32_t config_val);

#endif
#endif