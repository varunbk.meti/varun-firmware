#ifndef _HEADER_METI_NVS_H_
#define _HEADER_METI_NVS_H_
#include <Arduino.h>
#include "nvs_flash.h"
#include "nvs.h"

//Constatnts
#define FORCED_AP_NVS_TRUE 1
#define FORCED_AP_NVS_FALSE 0

#define METI_TRUE       1
#define METI_FALSE      0


//Variables
struct params_t
{
    uint32_t drc;       //device_restart_count
    uint32_t rrc;       //router_restart_count
    uint32_t forced_ap;     //this will be set to 1 when entering into forced_ap and set to zero on exit
    uint32_t wifi_test;
};

extern params_t params;

//Functions
void meti_nvs_initialize(void);
void meti_nvs_read_all(const char *name);
void meti_nvs_get_val(const char *name, const char *key, uint32_t *out_value);
void meti_nvs_commit_val(const char *name, const char *key, uint32_t out_value);

#endif