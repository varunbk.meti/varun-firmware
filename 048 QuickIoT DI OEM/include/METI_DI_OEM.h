#ifndef _HEADER_METI_DI_OEM_H_
#define _HEADER_METI_DI_OEM_H_
#include "Arduino.h"

#define Input1 GPIO_NUM_35
#define Input2 GPIO_NUM_32
#define Input3 GPIO_NUM_33
#define Input4 GPIO_NUM_25
#define Input5 GPIO_NUM_26
#define Input6 GPIO_NUM_27
#define Input7 GPIO_NUM_14
#define Input8 GPIO_NUM_13 // Input 9 in Schematic
#define Input9 GPIO_NUM_15 // Input 10 in Schematic

// Water Lvl Controller Macros
#define Pump1 Input1
#define Pump2 Input9
#define OHT_Percent_100 Input2
#define OHT_Percent_75 Input3
#define OHT_Percent_50 Input4
#define OHT_Percent_25 Input5
#define SUMP_Percent_100 Input6
#define SUMP_Percent_50 Input7
#define SUMP_Percent_25 Input8

#define Total_DI_Data_Size 4

#define Output1 GPIO_NUM_19
#define Output2 GPIO_NUM_18

// Water Lvl Controller Structure
typedef struct
{
    uint8_t Pump_1 : 1;
    uint8_t OHT_Prcnt_100 : 1;
    uint8_t OHT_Prcnt_75 : 1;
    uint8_t OHT_Prcnt_50 : 1;
    uint8_t OHT_Prcnt_25 : 1;
    uint8_t Pump_2 : 1;
    uint8_t SUMP_Prcnt_100 : 1;
    uint8_t SUMP_Prcnt_50 : 1;
    uint8_t SUMP_Prcnt_25 : 1;

    uint8_t OHT_Level;
    uint8_t SUMP_Level;
} DI_Wtr_Lvl;


extern DI_Wtr_Lvl Wtr_Lvl;

// Water Lvl Controller Function Declarations
void DI_Init(void);
uint8_t *Read_and_Process_Digital_Inputs(void);

#endif