/*******************************************************************************
 * File Name: ModbusRTUSlave.h
 *
 * Version: 1.0
 *
 * Description:
 * This is header file for ESP modbus slave. All the functions related to
 * modbus are here.
 *
 * Owner:
 * Yogesh M Iggalore
 *
 * Code Tested With:
 * 1. platformio and esp32
 *
 ********************************************************************************
 * Copyright (2020-21) , METI M2M India Pvt Ltd
 ********************************************************************************/
#ifndef ModbusRTUSlave_h
#define ModbusRTUSlave_h

#include <Arduino.h>

#define RTU_MODBUSSLAVE_DATA_RECEIVED 0x01
#define RTU_MODBUSSLAVE_READ_ERROR 0x02
#define RTU_MODBUSSLAVE_NO_DATA 0x03

#define MODBUS_RTU_MIN_BYTE_SIZE 0x05
#define MODBUS_RTU_DEFAULT_BYTE_SIZE 0x08

#define HOLDING_REGISTER_SIZE 20

#define MODBUSSLAVE_MAX_READ_SIZE HOLDING_REGISTER_SIZE
#define MODBUSSLAVE_HOLDING_REG_END_INDEX HOLDING_REGISTER_SIZE

#define INPUT_BUFFER_SIZE 20
#define MODBUS_SLAVE_FIXED_ADDR 7

/* ModBus status flags */
#define MODBUS_ERROR_NO_RESPONSE 1
#define MODBUS_ERROR_SLAVE_ID 2
#define MODBUS_ERROR_CRC 3
#define MODBUS_ERROR_EXCEPTION 4

struct modbus_struct
{
    // uint16_t process_val;
    // bool relay_on;
    // int32_t value1;
    // int32_t value2;
    // int32_t value3;
    // int32_t value4;
    // int32_t value5;  //Varun Commented

    uint8_t val1;
    uint8_t val2 : 1;
    uint8_t val3;
    uint8_t val4 : 1;
    uint8_t val5;
    uint8_t val6;
    uint8_t val7;
    uint8_t val8;
    uint8_t val9;
};

extern modbus_struct modbus_registers;

typedef struct
{
    uint16_t ui16ReadResult = 0;
    uint16_t ui16ProcessResult = 0;
    uint16_t ui16WriteResult = 0;
    uint16_t ui16RTURXCounter;
    int16_t aui16HoldingReg[HOLDING_REGISTER_SIZE];
} RTUSLAVE_PARAMETER;

class ModbusRTUSlave
{
public:
    RTUSLAVE_PARAMETER tdfRTUSlave;
    uint8_t input_data[INPUT_BUFFER_SIZE];
    uint8_t modbus_cmd[INPUT_BUFFER_SIZE];
    bool modbus_rx_complete = false;

    ModbusRTUSlave();
    void Update(void);
    void Read(void);
    void Process(void);
    void Write(void);
    void uart_read(void);
    void modbus_process_cmd(uint8_t rx_len);
    uint16_t Is_DataReceived(void);
    void Exception_Handling(uint8_t ui8ModbusSlaveId, uint8_t ui8ErrorCode, uint8_t ui8ErrorValue);
    void Process_ErrorCode(uint8_t ui8ModbusSlaveId, uint8_t ui8FunctionError, uint8_t ui8ErrorCode);
    void Process_Modbus_Parameters(void);
    void Process_Modbus_Coils(void);
    void Process_Modbus_Inputs(void);
    void Process_Modbus_Holding_Reg(void);
    void Process_Modbus_Input_Reg(void);
    void Process_Modbus_Force_Coil(void);
    void Process_Modbus_Preset_Reg(void);
    void Process_Modbus_Force_Mul_Coils(void);
    void Process_Modbus_Preset_Mul_Reg(void);
    void Process_Value_Register(uint16_t ui16StartRegister, uint16_t ui16NumberofRegister);
    void Update_Holding_Registers(void);
    void Read_Data_From_Serial(void);

    void Test(void);
};

#if !defined(NO_GLOBAL_INSTANCES) && !defined(NO_GLOBAL_MODBUSRTUSLAVE)
extern ModbusRTUSlave MRTUS;
#endif
#endif