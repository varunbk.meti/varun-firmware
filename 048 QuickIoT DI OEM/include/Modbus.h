/*******************************************************************************
 * File Name: Modbus.h
 *
 * Version: 1.0
 *
 * Description:
 * This is header file for ESP modbus. All the functions related to
 * modbus are here.
 *
 * Owner:
 * Yogesh M Iggalore
 *
 * Code Tested With:
 * 1. platformio and esp32
 *
 ********************************************************************************
 * Copyright (2020-21) , METI M2M India Pvt Ltd
 ********************************************************************************/
#ifndef Modbus_h
#define Modbus_h

#include <Arduino.h>

#define MODBUS_SERIAL Serial1

#define MODBUS_TX_ENABLE_PIN GPIO_NUM_23
#define MODBUS_TX_PIN GPIO_NUM_17
#define MODBUS_RX_PIN GPIO_NUM_16

// #define ENABLE_MODBUS_LED

// #define MODBUS_TX_LED_PIN GPIO_NUM_32
// #define MODBUS_RX_LED_PIN GPIO_NUM_4
// #define MODBUS_SPARE_LED_PIN GPIO_NUM_19

#define MODBUS_ENABLE 0
// #define MODBUS_RX_LED 2
// #define MODBUS_TX_LED 3
// #define MODBUS_SPARE_LED 4

#define MODBUS_PARITY_NONE 0
#define MODBUS_PARITY_EVEN 1
#define MODBUS_PARITY_ODD 2
#define MODBUS_PARITY_MASK 3

#define MODBUS_STOP_BIT_0 0
#define MODBUS_STOP_BIT_1 1
#define MODBUS_STOP_BIT_2 2

#define MODBUS_DATA_BITS_5 5
#define MODBUS_DATA_BITS_6 6
#define MODBUS_DATA_BITS_7 7
#define MODBUS_DATA_BITS_8 8

#define MODBUS_RX_BUFFER_SIZE 30
#define MODBUS_TX_BUTTER_SIZE 300

// ModBus Function Codes
#define MODBUS_FUNC_CODE_COIL_REG 0x01
#define MODBUS_FUNC_CODE_INPUT_STATUS 0x02
#define MODBUS_FUNC_CODE_HOLDING_REG 0x03
#define MODBUS_FUNC_CODE_INPUT_REG 0x04
#define MODBUS_FUNC_CODE_FORCE_SINGLE_COIL 0x05
#define MODBUS_FUNC_CODE_PRESET_SINGLE_REG 0x06
#define MODBUS_FUNC_CODE_FORCE_MULTIPLE_COIL 0x0F
#define MODBUS_FUNC_CODE_PRESET_MULTIPLE_REG 0x10

#define MODBUS_SPEEDS_SUPPORTED_MAX 8
#define MODBUS_PARITY_SUPPORTED_MAX 4

#define MODBUS_ERR_NONE 0x00
#define MODBUS_ERR_EXCEPTION_INVALID_DATA_VAL 0x03
#define MODBUS_ERR_EXCEPTION_SLAVE_BUSY 0x06

#define MODBUS_ERR_CRC 0xFF
#define MODBUS_ERR_TIMEOUT 0xFE
#define MODBUS_ERR_INVALID_REG_ADDRESS 0xFD
#define MODBUS_ERR_INVALID_RESPONSE_LEN 0xFC
#define MODBUS_ERR_INVALID_MODBUS_COMMAND 0xFB
#define MODBUS_ERR_VALUE_OUT_OF_RANGE 0xFA
#define MODBUS_ERR_INVALID_FUNC 0xF9
#define MODBUS_ERR_INVALID_COMMAND 0xF8

typedef struct
{
    uint8_t ui8DeviceID = 7; // 7
    uint8_t ui8Baudrate = 4; // 9600
    uint8_t ui8Parity = 0;   // Parity - None
    uint8_t ui8Stopbit = 1;
    uint8_t ui8Databit = 8;
    uint8_t aui8RxBuffer[MODBUS_RX_BUFFER_SIZE];
    uint8_t aui8TxBuffer[MODBUS_TX_BUTTER_SIZE];
} MODBUS_PARAMETER;

class Modbus
{
public:
    MODBUS_PARAMETER tdfModbus;
    Modbus();
    void Start(void);
    void Pin_Control(uint8_t ui8Pin, uint8_t ui8Control);
    uint32_t Get_Baudrate(uint8_t ui8Index);
    uint8_t Get_Parity(uint8_t ui8Index);
    uint8_t Get_TimePerByte(uint8_t ui8Index);
    uint32_t Get_UART_Config(uint8_t ui8Parity, uint8_t ui8Databits, uint8_t ui8Stopbit);
    uint16_t CalculateCRC16(uint8_t *MsgStartAddr, uint8_t MsgLength);
    uint8_t Verify_FunctionCode(uint8_t ui8ReceivedByte);
    // char ASCII_LRC(uint8 *MsgSourceAddr, uint8 MsgLength);
    void Test(void);
};

#if !defined(NO_GLOBAL_INSTANCES) && !defined(NO_GLOBAL_MODBUS)
extern Modbus MBus;
#endif
#endif