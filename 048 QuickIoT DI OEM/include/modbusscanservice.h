#ifndef _MODBUS_SCAN_SERVICE_H_INCLUDED_
#define _MODBUS_SCAN_SERVICE_H_INCLUDED_ 1

#include <string>
#include <dataservice.h>
#include <cthread.h>
#include <mbrconfig.h>

#define MAX_MBR_CP_ALLOWED 32

using namespace std;

class ModbusScanService : public DataService
{
public:
	void Start();
	void Stop();
	// DataSet* GetData();
	void StateMachine();

	void Initialize(uint8 p_iMBRCount, MBRConfig *p_pMBR, Logger *p_pLogger);
	static ModbusScanService *GetInstance();
	~ModbusScanService();
	string ToString();

private:
	ModbusScanService();

	uint8 m_iMBRCount;
	MBRConfig *m_pMbrConfig;

	CThread m_oStateMachineThread;

	uint8 m_iLastScannedMbrIdx;
	bool m_bRTUScanInProgress;

	static ModbusScanService *m_pSingleInstance;
	static void ThreadStateMachine(void *p_pArgs);
};

#endif
