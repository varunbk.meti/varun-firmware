#ifndef _BLINK_TYPE_H_INCLUDED_
#define _BLINK_TYPE_H_INCLUDED_ 1

#include<defs.h>

using namespace std;

class BlinkType
{
public:
    enum Value : uint8
    {
        Unknown = 0,
        Fast = 1,
        Once = 2,
        Twice = 3,
        BeforeFactoryReset = 4,
        BeforeMbrReset = 5,
        Slow = 6,
        OnceFor300Ms = 7,
        BuzzOnce = 8,
        BuzzTwice = 9
    };
    
    static uint8 GetSize() { return 10; }

    BlinkType() = default;
    BlinkType(Value code) : m_iCode(code) { }

    operator Value() const { return m_iCode; }
    explicit operator bool() = delete;
    constexpr bool operator==(BlinkType a) const { return m_iCode == a.m_iCode; }
    constexpr bool operator!=(BlinkType a) const { return m_iCode != a.m_iCode; }
    uint8 GetCode();
    string GetLabel();
    string ToString();

    static BlinkType Parse(int code);

private:
    Value m_iCode;
};

#endif
