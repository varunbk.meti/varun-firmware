#ifndef _HEADER_LED_H_
#define _HEADER_LED_H_
#include <Arduino.h>

//Constatnts
#define MAX_LEDS 3

//led index
#define LED_RED 0
#define LED_GREEN 1
#define LED_BLUE 2

//led physical pins
#define LED_RED_PIN GPIO_NUM_5
#define LED_GREEN_PIN GPIO_NUM_4
#define LED_BLUE_PIN GPIO_NUM_27

//led modes
#define LED_MODE_OFF 0
#define LED_MODE_ON 1
#define LED_MODE_BLINK 2
#define LED_MODE_FAST_BLINK 3

// pin state
#define PIN_LOW 0
#define PIN_HIGH 1

#define LED_BLINK_TIME 20
#define LED_FAST_BLINK_TIME 1

//Variables
struct ledControl
{
    int mode;
    int pin_state;
    int dc;
};
extern struct ledControl leds[MAX_LEDS];

//Functions
void led_setup(void);
void led_set_mode(uint8_t led, uint8_t mode);
void led_timer(void);
void led_apply(void);

#endif