#include<string>
#include<modbuscommerror.h>
using namespace std;

ModbusCommError ModbusCommError::Parse(int code)
{
    if (code == ErrorCRC) {
        return ErrorCRC;
    }
    else if (code == ErrorTimeout) {
        return ErrorTimeout;
    }
    else if (code == InvalidRegisterAddress) {
        return InvalidRegisterAddress;
    }
    else if (code == InvalidResponseLength) {
        return InvalidResponseLength;
    }
    else if (code == InvalidModbusCommand) {
        return InvalidModbusCommand;
    }
    else if (code == InvalidFunction) {
        return InvalidFunction;
    }
    else if (code == InvalidDataValue) {
        return InvalidDataValue;
    }
    else if (code == GatewayFailed) {
        return GatewayFailed;
    }
    else if (code == MetiNoNewData) {
        return MetiNoNewData;
    }
    else if (code == RtuErrorUnknown) {
        return RtuErrorUnknown;
    }

    return DaqErrorNone;
}

string ModbusCommError::ToString()
{
    return this->GetLabel();
}

uint8 ModbusCommError::GetCode()
{
    return m_iCode;
}

string ModbusCommError::GetLabel()
{
    if (m_iCode == DaqErrorNone) {
        return "DaqErrorNone";
    }
    else if (m_iCode == ErrorCRC) {
        return "ErrorCRC";
    }
    else if (m_iCode == ErrorTimeout) {
        return "ErrorTimeout";
    }
    else if (m_iCode == InvalidRegisterAddress) {
        return "InvalidRegisterAddress";
    }
    else if (m_iCode == InvalidResponseLength) {
        return "InvalidResponseLength";
    }
    else if (m_iCode == InvalidModbusCommand) {
        return "InvalidModbusCommand";
    }
    else if (m_iCode == InvalidFunction) {
        return "InvalidFunction";
    }
    else if (m_iCode == InvalidDataValue) {
        return "InvalidDataValue";
    }
    else if (m_iCode == GatewayFailed) {
        return "GatewayFailed";
    }
    else if (m_iCode == MetiNoNewData) {
        return "MetiNoNewData";
    }
    else if (m_iCode == RtuErrorUnknown) {
        return "RtuErrorUnknown";
    }

    return "DaqErrorNone";
}
