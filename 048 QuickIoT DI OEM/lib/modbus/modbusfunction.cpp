#include<string>
#include<modbusfunction.h>
using namespace std;

ModbusFunction ModbusFunction::Parse(int code)
{
	if (code == ReadCoilRegister) {
		return ReadCoilRegister;
	}
	else if (code == ReadInputStatusRegister) {
		return ReadInputStatusRegister;
	}
	else if (code == ReadHoldingRegister) {
		return ReadHoldingRegister;
	}
	else if (code == ReadInputRegister) {
		return ReadInputRegister;
	}
	else if (code == WriteSingleCoil) {
		return WriteSingleCoil;
	}
	else if (code == WriteSingleRegister) {
		return WriteSingleRegister;
	}
	else if (code == WriteMultipleCoils) {
		return WriteMultipleCoils;
	}
	else if (code == WriteMultipleRegisters) {
		return WriteMultipleRegisters;
	}

	return Unknown;
}

uint8 ModbusFunction::GetExpectedBytes(uint8 p_iRegisterCount)
{
	uint8 l_iRetVal = 3;

	if (m_iCode == ReadCoilRegister) {
		l_iRetVal += ((p_iRegisterCount - 1) >> 3) + 1;
	}
	else if (m_iCode == ReadInputStatusRegister) {
		l_iRetVal += ((p_iRegisterCount - 1) >> 3) + 1;
	}
	else if (m_iCode == ReadHoldingRegister) {
		l_iRetVal += p_iRegisterCount * 2;
	}
	else if (m_iCode == ReadInputRegister) {
		l_iRetVal += p_iRegisterCount * 2;
	}
	else if (m_iCode == WriteSingleCoil) {
		l_iRetVal += 5;
	}
	else if (m_iCode == WriteSingleRegister) {
		l_iRetVal += 5;
	}
	else if (m_iCode == WriteMultipleCoils) {
		l_iRetVal += 5;
	}
	else if (m_iCode == WriteMultipleRegisters) {
		l_iRetVal += 5;
	}
	else {
		l_iRetVal += 5;
	}

	return l_iRetVal;
}

string ModbusFunction::ToString()
{
    return this->GetLabel();
}

uint8 ModbusFunction::GetCode()
{
    return m_iCode;
}

string ModbusFunction::GetLabel()
{
	if (m_iCode == Unknown) {
		return "Unknown";
	}
	else if (m_iCode == ReadCoilRegister) {
		return "ReadCoilRegister";
	}
	else if (m_iCode == ReadInputStatusRegister) {
		return "ReadInputStatusRegister";
	}
	else if (m_iCode == ReadHoldingRegister) {
		return "ReadHoldingRegister";
	}
	else if (m_iCode == ReadInputRegister) {
		return "ReadInputRegister";
	}
	else if (m_iCode == WriteSingleCoil) {
		return "WriteSingleCoil";
	}
	else if (m_iCode == WriteSingleRegister) {
		return "WriteSingleRegister";
	}
	else if (m_iCode == WriteMultipleCoils) {
		return "WriteMultipleCoils";
	}
	else if (m_iCode == WriteMultipleRegisters) {
		return "WriteMultipleRegisters";
	}

	return "Unknown";
}
