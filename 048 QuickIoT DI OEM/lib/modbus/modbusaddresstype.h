#ifndef _MODBUS_ADDRESS_TYPE_H_INCLUDED_
#define _MODBUS_ADDRESS_TYPE_H_INCLUDED_ 1

#include<string>
#include<defs.h>

using namespace std;

class ModbusAddressType
{
public:
    enum Value : uint8
    {
        ModbusRTU0 = 0,
        ModbusRTU1 = 1,
        ModbusASCII = 2,
        ModbusTCPV4 = 3
    };

    ModbusAddressType() = default;
    ModbusAddressType(Value code) : m_iCode(code) { }

    operator Value() const { return m_iCode; }
    explicit operator bool() = delete;
    constexpr bool operator==(ModbusAddressType a) const { return m_iCode == a.m_iCode; }
    constexpr bool operator!=(ModbusAddressType a) const { return m_iCode != a.m_iCode; }
    uint8 GetCode();
    string GetLabel();
    string ToString();

    static ModbusAddressType Parse(int code);

private:
    Value m_iCode;
};

#endif
