#include <Arduino.h>
#include <stdio.h>
#include <string.h>
#include <cdate.h>
#include <serialport.h>
#include <indicator.h>
#include <blinkconfig.h>
// #include <blinktype.h>
#include <modbusmasterrtu.h>
#include <..\..\include\meti_modbus.h>

using namespace std;

extern Indicator *g_pIndicators;
extern BlinkConfig *g_pBlinkConfig;

extern Logger *g_pLogger;

#ifdef ENABLE_MODBUS_MASTER
//*************************************************************************************************
//*** CRC look-up table
//*************************************************************************************************
const int TabCRCHi[] =
	{
		0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40,
		0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41,
		0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41,
		0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40,
		0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41,
		0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40,
		0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40,
		0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41,
		0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41,
		0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40,
		0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40,
		0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41,
		0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40,
		0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41,
		0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41,
		0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40};

const int TabCRCLo[] =
	{
		0x00, 0xC0, 0xC1, 0x01, 0xC3, 0x03, 0x02, 0xC2, 0xC6, 0x06, 0x07, 0xC7, 0x05, 0xC5, 0xC4, 0x04,
		0xCC, 0x0C, 0x0D, 0xCD, 0x0F, 0xCF, 0xCE, 0x0E, 0x0A, 0xCA, 0xCB, 0x0B, 0xC9, 0x09, 0x08, 0xC8,
		0xD8, 0x18, 0x19, 0xD9, 0x1B, 0xDB, 0xDA, 0x1A, 0x1E, 0xDE, 0xDF, 0x1F, 0xDD, 0x1D, 0x1C, 0xDC,
		0x14, 0xD4, 0xD5, 0x15, 0xD7, 0x17, 0x16, 0xD6, 0xD2, 0x12, 0x13, 0xD3, 0x11, 0xD1, 0xD0, 0x10,
		0xF0, 0x30, 0x31, 0xF1, 0x33, 0xF3, 0xF2, 0x32, 0x36, 0xF6, 0xF7, 0x37, 0xF5, 0x35, 0x34, 0xF4,
		0x3C, 0xFC, 0xFD, 0x3D, 0xFF, 0x3F, 0x3E, 0xFE, 0xFA, 0x3A, 0x3B, 0xFB, 0x39, 0xF9, 0xF8, 0x38,
		0x28, 0xE8, 0xE9, 0x29, 0xEB, 0x2B, 0x2A, 0xEA, 0xEE, 0x2E, 0x2F, 0xEF, 0x2D, 0xED, 0xEC, 0x2C,
		0xE4, 0x24, 0x25, 0xE5, 0x27, 0xE7, 0xE6, 0x26, 0x22, 0xE2, 0xE3, 0x23, 0xE1, 0x21, 0x20, 0xE0,
		0xA0, 0x60, 0x61, 0xA1, 0x63, 0xA3, 0xA2, 0x62, 0x66, 0xA6, 0xA7, 0x67, 0xA5, 0x65, 0x64, 0xA4,
		0x6C, 0xAC, 0xAD, 0x6D, 0xAF, 0x6F, 0x6E, 0xAE, 0xAA, 0x6A, 0x6B, 0xAB, 0x69, 0xA9, 0xA8, 0x68,
		0x78, 0xB8, 0xB9, 0x79, 0xBB, 0x7B, 0x7A, 0xBA, 0xBE, 0x7E, 0x7F, 0xBF, 0x7D, 0xBD, 0xBC, 0x7C,
		0xB4, 0x74, 0x75, 0xB5, 0x77, 0xB7, 0xB6, 0x76, 0x72, 0xB2, 0xB3, 0x73, 0xB1, 0x71, 0x70, 0xB0,
		0x50, 0x90, 0x91, 0x51, 0x93, 0x53, 0x52, 0x92, 0x96, 0x56, 0x57, 0x97, 0x55, 0x95, 0x94, 0x54,
		0x9C, 0x5C, 0x5D, 0x9D, 0x5F, 0x9F, 0x9E, 0x5E, 0x5A, 0x9A, 0x9B, 0x5B, 0x99, 0x59, 0x58, 0x98,
		0x88, 0x48, 0x49, 0x89, 0x4B, 0x8B, 0x8A, 0x4A, 0x4E, 0x8E, 0x8F, 0x4F, 0x8D, 0x4D, 0x4C, 0x8C,
		0x44, 0x84, 0x85, 0x45, 0x87, 0x47, 0x46, 0x86, 0x82, 0x42, 0x43, 0x83, 0x41, 0x81, 0x80, 0x40};

void ModbusMasterRTU::ExecuteCommand()
{
	this->CreateCommand();
	g_pLogger->Write(LogLevel::Info, "ModbusMasterRTU", "Connecting through %s,%s,%s", "Serial2", this->m_oSpeed.GetLabel().c_str(), get_uart_config_label(g_modbusSerialConfig).c_str());

	g_modbusSerialConfig = get_uart_config(this->m_oParity.GetCode(), 1);
	ModbusSerial.begin(this->m_oSpeed.GetSpeed(), g_modbusSerialConfig, PIN_MODBUS_RX, PIN_MODBUS_TX);

	int l_iTotalBits = 9; // start bit + 8 bit data
	if (ParityType::None != this->m_oParity)
	{
		l_iTotalBits++;
	}
	l_iTotalBits += this->m_iStopBits;

	long l_iSleepTime = l_iTotalBits * this->m_oSpeed.GetTxTime();
	l_iSleepTime *= this->m_iCommandSize;

	g_pLogger->Write(LogLevel::Trace, "ModbusMasterRTU", "Writing %d bytes. Wait time:%d microSec", this->m_iCommandSize, l_iSleepTime);
	m_pEnablePin->SwitchOn();
	ModbusSerial.write(this->m_cCommandBytes, this->m_iCommandSize);
	usleep(l_iSleepTime);

	m_pEnablePin->SwitchOff();

	ModbusSerial.flush();
	// ModbusSerial.begin(this->m_oSpeed.GetSpeed(), ui32UartConfigRx, PIN_MODBUS_RX, PIN_MODBUS_TX);

	this->m_iResponseSize = 0;
	CDate l_oRxStartTime;
	l_oRxStartTime = CDate::Now();
	do
	{
		while (ModbusSerial.available())
		{
			this->m_cResponseBytes[this->m_iResponseSize++] = ModbusSerial.read();
		}
		if (this->m_iReadTimeOut <= l_oRxStartTime.TimeElapsed(CDate::Now(), IntervalType::MilliSecond))
		{
			g_pLogger->Write(LogLevel::Trace, "ModbusMasterRTU", "Timeout break...");
			break;
		}
	} while (this->m_iResponseSize < (this->m_iExpectedBytes + MODBUS_RTU_CRC_SIZE));

	while (ModbusSerial.available())
	{
		this->m_cResponseBytes[this->m_iResponseSize++] = ModbusSerial.read();
	}

	g_pLogger->Write(LogLevel::Trace, "ModbusMasterRTU", "Response bytes: %d", this->m_iResponseSize);

	if (this->m_iResponseSize < MODBUS_MINIMUM_RESPONSE_SIZE + MODBUS_RTU_CRC_SIZE)
	{
		g_pLogger->Write(LogLevel::Trace, "ModbusMasterRTU", "CP 1 : %d < %d", this->m_iResponseSize, MODBUS_MINIMUM_RESPONSE_SIZE + MODBUS_RTU_CRC_SIZE);
		this->m_oCommError = ModbusCommError::ErrorTimeout;
		return;
	}

	g_pLogger->Write(LogLevel::Trace, "ModbusMasterRTU", "CRC Validation");
	if (!this->CalculateCRC16((uint8 *)this->m_cResponseBytes, this->m_iResponseSize - MODBUS_RTU_CRC_SIZE, true))
	{
		g_pLogger->Write(LogLevel::Error, "ModbusMasterRTU", "CRC mismatch!!!");
		this->m_oCommError = ModbusCommError::ErrorCRC;
		return;
	}

	this->m_oCommError = ModbusCommError::DaqErrorNone;
}

void ModbusMasterRTU::CreateCommand()
{
	this->ClearCommand();
	ModbusMaster::CreateCommand();
	this->CalculateCRC16(this->m_cCommandBytes, this->m_iCommandSize);
	this->m_iCommandSize += 2;
}

bool ModbusMasterRTU::CalculateCRC16(uint8 *p_pMessage, uint8 p_iLength)
{
	return this->CalculateCRC16(p_pMessage, p_iLength, false);
}

bool ModbusMasterRTU::CalculateCRC16(uint8 *p_pMessage, uint8 p_iLength, bool p_bValidateOnly)
{
	uint8 lookupIndex = 0;
	uint8 loopIndex = 0;

	uint8 crc[2];

	crc[IDX_CRC_LOW] = 0xFF; // Initialize CRC register to 0xFFFF
	crc[IDX_CRC_HIGH] = 0xFF;

	for (loopIndex = 0; loopIndex < p_iLength; loopIndex++)
	{
		lookupIndex = (crc[IDX_CRC_LOW] ^ p_pMessage[loopIndex]) & 0xFF;
		crc[IDX_CRC_LOW] = (crc[IDX_CRC_HIGH] ^ TabCRCHi[lookupIndex]) & 0xFF;
		crc[IDX_CRC_HIGH] = TabCRCLo[lookupIndex];
	}

	if (p_bValidateOnly)
	{
		if (p_pMessage[p_iLength] == crc[IDX_CRC_LOW])
		{
			if (p_pMessage[p_iLength + 1] == crc[IDX_CRC_HIGH])
			{
				return true;
			}
		}
		return false;
	}
	p_pMessage[p_iLength++] = crc[IDX_CRC_LOW];
	p_pMessage[p_iLength++] = crc[IDX_CRC_HIGH];

	return true;
}

void ModbusMasterRTU::Clear()
{
	ModbusMaster::Clear();
	this->m_oSpeed = SpeedType::Speed_9600;
	this->m_iDataBits = 8;
	this->m_oParity = ParityType::None;
	this->m_iStopBits = 1;
}

ModbusMasterRTU::ModbusMasterRTU()
{
	this->Clear();
}

ModbusMasterRTU::~ModbusMasterRTU()
{
	this->Clear();
}

SpeedType ModbusMasterRTU::GetSpeed()
{
	return this->m_oSpeed;
}
void ModbusMasterRTU::SetSpeed(SpeedType value)
{
	this->m_oSpeed = value;
}

ParityType ModbusMasterRTU::GetParity()
{
	return this->m_oParity;
}
void ModbusMasterRTU::SetParity(ParityType value)
{
	this->m_oParity = value;
}

uint8 ModbusMasterRTU::GetDataBits()
{
	return this->m_iDataBits;
}
void ModbusMasterRTU::SetDataBits(uint8 value)
{
	this->m_iDataBits = value;
}

uint8 ModbusMasterRTU::GetStopBits()
{
	return this->m_iStopBits;
}
void ModbusMasterRTU::SetStopBits(uint8 value)
{
	this->m_iStopBits = value;
}

void ModbusMasterRTU::SetEnablePin(Indicator *value)
{
	this->m_pEnablePin = value;
}

#endif