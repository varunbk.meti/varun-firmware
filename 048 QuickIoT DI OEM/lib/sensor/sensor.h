#ifndef _SENSOR_H_INCLUDED_
#define _SENSOR_H_INCLUDED_ 1

#include<Arduino.h>
#include<ArduinoJson.h>

#include<defs.h>
#include<cdate.h>
#include<byteordertype.h>
#include<datatype.h>
#include<modbusmaster.h>

using namespace std;

class Sensor
{
public:
    void Parse( JsonObject src);
    Sensor();
    ~Sensor();

    bool ExtractData( CDate time, ModbusMaster* p_pModbus);
    bool ExtractData( ModbusMaster* p_pModbus);

    CDate GetLastDataTime();
    ModbusCommError GetLastDataError();
    double GetLastDataValue();
    string GetLastDataValueString();

    void SetID( long value);
    void SetKey( string value);
    void SetName( string value);
    void SetByteOrder( ByteOrderType value);
    void SetDataType( DataType value);
    void SetStartRegisterAddress( uint16 value);
    void SetRegisterCount( uint8 value);
    void SetScalingNumerator( long value);
    void SetScalingDenominator( long value);
    void SetOffsetValue( double value);
    void SetDecimalPresision( uint8 value);

    long GetID();
    string GetKey();
    string GetName();
    ByteOrderType GetByteOrder();
    DataType GetDataType();
    uint16 GetStartRegisterAddress();
    uint8 GetRegisterCount();
    long GetScalingNumerator();
    long GetScalingDenominator();
    double GetOffsetValue();
    uint8 GetDecimalPresision();

protected:

    long PackData( uint8* p_pOrderedData, uint8 p_iSize);
    long PackData( uint8* p_pOrderedData, uint8 p_iSize, bool signedData);

    double PackDouble( uint8* p_pOrderedData, uint8 p_iSize);

    long m_lID;
    string m_sKey;
    string m_sName;
    string m_sUnitOfMeasure;
    uint8 m_iReportGroupID;
    float m_iDisplayOrder;
    ByteOrderType m_oByteOrder;
    DataType m_oDataType;
    uint16 m_iStartRegisterAddress;
    uint8 m_iRegisterCount;
	long m_lScalingNumerator;
	long m_lScalingDenominator;
	double m_eOffsetValue;
	uint8 m_iDecimalPresision;

    CDate m_oLastDataTime;
    ModbusCommError m_oLastDataError;
    double m_eLastDataValue;
    string m_sLastDataValue;

};

#endif