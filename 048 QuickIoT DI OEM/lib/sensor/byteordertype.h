#ifndef _BYTE_ORDER_TYPE_H_INCLUDED_
#define _BYTE_ORDER_TYPE_H_INCLUDED_ 1

#include<string>
#include<defs.h>

using namespace std;

class ByteOrderType
{
public:
    enum Value : uint8
    {
        BO_0123_3210 = 0,
        BO_0123_2301 = 1,
        BO_0123_0123 = 2,
        BO_0123_1032 = 3,
        BO_01_10 = 4,
        BO_01_01 = 5,
        BO_0_0 = 6,
        BO_1_0 = 7,
        BO_01234567_76543210 = 8,
        BO_01234567_67452301 = 9,
        BO_01234567_01234567 = 10,
        BO_01234567_10325476 = 11,
        BO_01234567_32107654 = 12,
        BO_01234567_23016745 = 13,
        BO_01234567_45670123 = 14,
        BO_01234567_54761032 = 15
    };

    ByteOrderType() = default;
    ByteOrderType(Value code) : m_iCode(code) { }

    operator Value() const { return m_iCode; }
    explicit operator bool() = delete;
    constexpr bool operator==(ByteOrderType a) const { return m_iCode == a.m_iCode; }
    constexpr bool operator!=(ByteOrderType a) const { return m_iCode != a.m_iCode; }
    uint8 GetCode();
    string GetLabel();
    string ToString();

    static ByteOrderType Parse(int code);
    uint8 GetArray( uint8* dest);
private:
    Value m_iCode;
};

#endif
