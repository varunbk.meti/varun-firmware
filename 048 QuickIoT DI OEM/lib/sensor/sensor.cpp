#include <Arduino.h>
#include <utils.h>
#include <sensor.h>
#include <logger.h>
#include <string.h>
#include <../../include/config.h>
#include <../../include/config_reader.h>

extern uint16_t prevRawValue;

// Narayan temporary
extern Logger *g_pLogger;
extern DynamicJsonDocument sensorScaling_doc;

bool Sensor::ExtractData(ModbusMaster *p_pModbus)
{
    return this->ExtractData(CDate::Now(), p_pModbus);
}

bool Sensor::ExtractData(CDate time, ModbusMaster *p_pModbus)
{
    uint8 l_iDataSize = 0;
    uint8 l_iByteOrderSize = 0;
    uint8 l_pRawData[8];
    uint8 l_pByteOrder[8];
    uint8 l_pOrderedData[8];
    memset(l_pRawData, 0x00, 8);
    memset(l_pByteOrder, 0x00, 8);
    memset(l_pOrderedData, 0x00, 8);

    this->m_oLastDataTime = time; // CDate::Now();
    this->m_oLastDataError = p_pModbus->GetCommError();
    if (ModbusCommError::DaqErrorNone != this->m_oLastDataError)
    {
        // this->m_eLastDataValue = 0.0;
        return false;
    }

    l_iDataSize = this->m_oDataType.GetSize();
    l_iByteOrderSize = this->m_oByteOrder.GetArray(l_pByteOrder);

    // Serial.printf( "\nExtractData::Name:%s, Start:%d, DT:%s\n", this->m_sName.c_str(), this->m_iStartRegisterAddress, this->m_oDataType.GetLabel().c_str());

    if (!p_pModbus->GetRegisterData(this->m_iStartRegisterAddress, l_iDataSize, l_pRawData))
    {
        // Serial.printf( "%s Value: GetRegisterData failed!!!!\n", this->m_sName.c_str());
        return false;
    }

    for (uint8 l_iIdx = 0; l_iIdx < l_iByteOrderSize; l_iIdx++)
    {
        l_pOrderedData[l_iIdx] = l_pRawData[l_pByteOrder[l_iIdx]];
    }

    /*
    Serial.printf( "RawData:");
	for ( uint8 l_iIdx = 0; l_iIdx < 8; l_iIdx++) {
		Serial.printf( " %02X", l_pRawData[l_iIdx]);
	}
	Serial.printf( "\n");

    Serial.printf( "ByteOrder:%s:", this->m_oByteOrder.GetLabel().c_str());
    for( uint8 l_iIdx = 0; l_iIdx < l_iByteOrderSize; l_iIdx++) {
        Serial.printf( " %02X", l_pByteOrder[l_iIdx]);
    }
	Serial.printf( "\n");

	Serial.printf( "Ordered Data:");
    for( uint8 l_iIdx = 0; l_iIdx < l_iByteOrderSize; l_iIdx++) {
        Serial.printf( " %02X", l_pOrderedData[l_iIdx]);
    }
    Serial.printf( "\n");
    */

    double l_ePrescaledValue = 0.0;
    double l_eFinalValue = 0.0;
    //long l_lBuff = 0x00;

    uint8 l_iDestIdx = 0;
    memset(l_pOrderedData, 0x00, 8);

    switch (this->m_oDataType)
    {
    case DataType::DT_INT32:
    case DataType::DT_INT64:
        l_pOrderedData[l_iDestIdx++] = l_pRawData[l_pByteOrder[0]];
        l_pOrderedData[l_iDestIdx++] = l_pRawData[l_pByteOrder[1]];
        l_pOrderedData[l_iDestIdx++] = l_pRawData[l_pByteOrder[2]];
        l_pOrderedData[l_iDestIdx++] = l_pRawData[l_pByteOrder[3]];
        l_ePrescaledValue = this->PackData(l_pOrderedData, l_iDestIdx, true);
        // Serial.printf( "CP 1. l_ePrescaledValue:%f\n", l_ePrescaledValue);
        break;

    case DataType::DT_UINT32:
    case DataType::DT_UINT64:
        l_pOrderedData[l_iDestIdx++] = l_pRawData[l_pByteOrder[0]];
        l_pOrderedData[l_iDestIdx++] = l_pRawData[l_pByteOrder[1]];
        l_pOrderedData[l_iDestIdx++] = l_pRawData[l_pByteOrder[2]];
        l_pOrderedData[l_iDestIdx++] = l_pRawData[l_pByteOrder[3]];
        l_ePrescaledValue = this->PackData(l_pOrderedData, l_iDestIdx);
        // Serial.printf( "CP 2. l_ePrescaledValue:%f\n", l_ePrescaledValue);
        break;

    case DataType::DT_UINT16:
        l_pOrderedData[l_iDestIdx++] = l_pRawData[l_pByteOrder[2]];
        l_pOrderedData[l_iDestIdx++] = l_pRawData[l_pByteOrder[3]];
        l_ePrescaledValue = this->PackData(l_pOrderedData, l_iDestIdx);
        // Serial.printf( "CP 3. l_ePrescaledValue:%f\n", l_ePrescaledValue);
        break;

    case DataType::DT_INT16:
        l_pOrderedData[l_iDestIdx++] = l_pRawData[l_pByteOrder[2]];
        l_pOrderedData[l_iDestIdx++] = l_pRawData[l_pByteOrder[3]];
        l_ePrescaledValue = this->PackData(l_pOrderedData, l_iDestIdx, true);
        // Serial.printf( "CP 4. l_ePrescaledValue:%f\n", l_ePrescaledValue);
        break;

    case DataType::DT_UINT8:
        l_pOrderedData[l_iDestIdx++] = l_pRawData[l_pByteOrder[3]];
        l_ePrescaledValue = this->PackData(l_pOrderedData, l_iDestIdx);
        // Serial.printf( "CP 5. l_ePrescaledValue:%f\n", l_ePrescaledValue);
        break;

    case DataType::DT_FLOAT:
    case DataType::DT_DOUBLE:
        l_pOrderedData[l_iDestIdx++] = l_pRawData[l_pByteOrder[0]];
        l_pOrderedData[l_iDestIdx++] = l_pRawData[l_pByteOrder[1]];
        l_pOrderedData[l_iDestIdx++] = l_pRawData[l_pByteOrder[2]];
        l_pOrderedData[l_iDestIdx++] = l_pRawData[l_pByteOrder[3]];
        l_ePrescaledValue = this->PackDouble(l_pOrderedData, l_iDestIdx);
        // Serial.printf( "CP 6. l_ePrescaledValue:%f\n", l_ePrescaledValue);
        break;

    case DataType::DT_STREAM_4_BYTES:
        l_pOrderedData[l_iDestIdx++] = l_pRawData[l_pByteOrder[0]];
        l_pOrderedData[l_iDestIdx++] = l_pRawData[l_pByteOrder[1]];
        l_pOrderedData[l_iDestIdx++] = l_pRawData[l_pByteOrder[2]];
        l_pOrderedData[l_iDestIdx++] = l_pRawData[l_pByteOrder[3]];
        l_ePrescaledValue = this->PackData(l_pOrderedData, l_iDestIdx);
        // Serial.printf( "CP 7. l_ePrescaledValue:%f\n", l_ePrescaledValue);
        break;

    case DataType::DT_BIT0:
    case DataType::DT_BIT1:
    case DataType::DT_BIT2:
    case DataType::DT_BIT3:
    case DataType::DT_BIT4:
    case DataType::DT_BIT5:
    case DataType::DT_BIT6:
    case DataType::DT_BIT7:
        l_pOrderedData[l_iDestIdx] = l_pRawData[l_pByteOrder[3]];
        /*
        Serial.printf( "CP 8. l_pByteOrder[3]:%d\n", l_pByteOrder[3]);
        Serial.printf( "CP 8. l_pRawData[l_pByteOrder[3]]:%d\n", l_pRawData[l_pByteOrder[3]]);
        Serial.printf( "CP 8. l_pOrderedData[l_iDestIdx]:%d\n", l_pOrderedData[l_iDestIdx]);
        Serial.printf( "CP 8. %02x & %02X\n", l_pOrderedData[l_iDestIdx], this->m_oDataType.GetBitMask());
        */
        if (l_pOrderedData[l_iDestIdx] & this->m_oDataType.GetBitMask())
        {
            l_ePrescaledValue = 1.0;
        }
        else
        {
            l_ePrescaledValue = 0.0;
        }
        // Serial.printf( "CP 8. l_ePrescaledValue:%f\n", l_ePrescaledValue);
        break;

    case DataType::DT_ASCII:
    default:
        // Serial.printf( "CP 9. l_ePrescaledValue:%f\n", l_ePrescaledValue);
        break;
    }

    double sensorValue_ma = 0;
    double sensorValue_level = 0;
    double sensorValue_volume = 0;

    // Serial.printf( "CP 9. l_ePrescaledValue:%f\n", l_ePrescaledValue);

    l_eFinalValue = l_ePrescaledValue;

    prevRawValue = l_eFinalValue; //This will be used in calibration when needed

    // 4-20mA processing
    int32_t sourceZero = sensorScaling_doc["calibratedZero"].as<int32_t>();
    int32_t sourceSpan = sensorScaling_doc["calibratedSpan"].as<int32_t>();
    int32_t destZero = sensorScaling_doc["zero"].as<int32_t>();
    int32_t destSpan = sensorScaling_doc["span"].as<int32_t>();
    //int8_t decimals = sensorScaling_doc["decimals"].as<int8_t>();

    // 4-20mA current scaling - with error correction (not used in calculations, only for reporting)
    int32_t scaledCurrent = (int32_t)scaleValue(l_eFinalValue, sourceZero, sourceSpan, 400, 2000);
    sensorValue_ma = scaledCurrent;    

    int32_t scaledSource = (int32_t)scaleValue(l_eFinalValue, sourceZero, sourceSpan, destZero, destSpan);

    sensorValue_level = scaledSource;

    // The following is to ensure that scaledFinal has the final value even if nl is not enabled..
    int32_t scaledFinal = scaledSource;

    // If non linear is enabled....
    if (g_nl_enabled == 1)
    {
        // if index is matching just fetch the volume data
        // int16_t index = array_getIndex(scaling_nl_value, 0, 0, scaling_nl_max_rows_set - 1, scaledSource);

        int16_t index = 0;
        // If index is not matching, do a interpolation/scaling....
        if (index == -1)
        {
            // get the least greater index
            // index = array_getLeastGreaterIndex(&scaling_nl_value[0], 1, 0, scaling_nl_max_rows_set - 1, scaledSource);

            // Zero values will be at index -1, Span will be at index
            sourceZero = scaling_nl_value[index - 1];
            sourceSpan = scaling_nl_value[index];
            destZero = scaling_nl_volume[index - 1];
            destSpan = scaling_nl_volume[index];

            // Scale the input
            scaledFinal = (int32_t)scaleValue(scaledSource, sourceZero, sourceSpan, destZero, destSpan);
            // g_pLogger->Write(LogLevel::Debug, "NonLinear - Scaled", "index: %d, scaling_nl_max_rows_set:%d, scaledSource: %d, scaledFinal: %d", index, scaling_nl_max_rows_set, scaledSource, scaledFinal);
        }
        else
        {
            scaledFinal = scaling_nl_volume[index];
            // g_pLogger->Write(LogLevel::Debug, "NonLinear - Fetched", "index:%d, scaledSource: %d, scaledFinal : %d", index, scaledSource, scaledFinal);
        }
        sensorValue_volume = scaledFinal;
    }
    
    if (strcmp(this->GetKey().c_str(),"sensor_100") == 0) {
        l_eFinalValue = sensorValue_ma;
    } else if (strcmp(this->GetKey().c_str(),"sensor_101") == 0) {
        l_eFinalValue = sensorValue_level;
    } else if (strcmp(this->GetKey().c_str(),"sensor_102") == 0) {
        l_eFinalValue = sensorValue_volume;
    }

    l_eFinalValue = l_eFinalValue / pow(10,this->m_iDecimalPresision);

    this->m_eLastDataValue = l_eFinalValue;
    char l_cBuff[32];
    sprintf(l_cBuff, "%.*f", this->m_iDecimalPresision, l_eFinalValue);
    this->m_sLastDataValue = "";
    this->m_sLastDataValue.append(l_cBuff);

    return true;
}

double Sensor::PackDouble(uint8 *p_pOrderedData, uint8 p_iSize)
{
    long l_lBuff = this->PackData(p_pOrderedData, p_iSize, false);
    long l_lBase = (l_lBuff & ((1 << 23) - 1)) + (1 << 23) * (l_lBuff >> 31 | 1);

    long l_lExponent = (l_lBuff >> 23 & 0xFF) - 127;

    return l_lBase * pow(2, l_lExponent - 23);
}

long Sensor::PackData(uint8 *p_pOrderedData, uint8 p_iSize)
{
    return this->PackData(p_pOrderedData, p_iSize, false);
}

long Sensor::PackData(uint8 *p_pOrderedData, uint8 p_iSize, bool signedData)
{
    long l_lValueBuff = 0;

    uint8 l_iIdx1 = 0;
    uint8 l_iIdx2 = p_iSize - 1;

    for (l_iIdx1 = 0, l_iIdx2 = p_iSize - 1; l_iIdx1 < p_iSize; l_iIdx1++, l_iIdx2--)
    {
        l_lValueBuff |= p_pOrderedData[l_iIdx2] << (8 * l_iIdx1);
    }

    if (signedData)
    {
        uint8 l_iLongSize = sizeof(long);
        l_iIdx2 = l_iLongSize - p_iSize - 1;
        if (p_pOrderedData[p_iSize - 2] & 0x80)
        {
            for (; l_iIdx1 < l_iLongSize; l_iIdx1++, l_iIdx2--)
            {
                l_lValueBuff |= 0xFF << (8 * l_iIdx1);
            }
        }
    }

    return l_lValueBuff;
}

void Sensor::Parse(JsonObject src)
{
    // const size_t capacity = JSON_OBJECT_SIZE(2) + JSON_OBJECT_SIZE(8) + 8*JSON_OBJECT_SIZE(13) + JSON_OBJECT_SIZE(15);
    // DynamicJsonDocument doc(capacity);

    this->m_lID = src["id"].as<long>();
    this->m_sName = src["nm"].as<string>();
    this->m_sUnitOfMeasure = src["un"].as<string>();
    this->m_iReportGroupID = src["rg"].as<uint>();
    this->m_iDisplayOrder = src["do"].as<float>();
    this->m_iStartRegisterAddress = src["ra"].as<uint>();
    this->m_iRegisterCount = 1;
    this->m_oDataType = DataType::Parse(src["dt"].as<int>());
    this->m_oByteOrder = ByteOrderType::Parse(src["bo"].as<int>());
    this->m_lScalingNumerator = src["sn"].as<long>();
    this->m_lScalingDenominator = src["sd"].as<long>();
    this->m_iDecimalPresision = src["dp"].as<uint>();
    this->m_eOffsetValue = src["os"].as<double>();
}

Sensor::Sensor()
{
    this->m_lID = 0;
    this->m_sName = "";
    this->m_oByteOrder = ByteOrderType::BO_0_0;
    this->m_oDataType = DataType::DT_UINT32;
    this->m_iStartRegisterAddress = 0;
    this->m_lScalingNumerator = 0;
    this->m_lScalingDenominator = 0;
    this->m_eOffsetValue = 0.0;
    this->m_iDecimalPresision = 2;
}

Sensor::~Sensor()
{
}

long Sensor::GetID()
{
    return this->m_lID;
}
string Sensor::GetKey()
{
    return this->m_sKey;
}
string Sensor::GetName()
{
    return this->m_sName;
}
ByteOrderType Sensor::GetByteOrder()
{
    return this->m_oByteOrder;
}
DataType Sensor::GetDataType()
{
    return this->m_oDataType;
}
uint16 Sensor::GetStartRegisterAddress()
{
    return this->m_iStartRegisterAddress;
}
uint8 Sensor::GetRegisterCount()
{
    return this->m_iRegisterCount;
}
long Sensor::GetScalingNumerator()
{
    return this->m_lScalingNumerator;
}
long Sensor::GetScalingDenominator()
{
    return this->m_lScalingDenominator;
}
double Sensor::GetOffsetValue()
{
    return this->m_eOffsetValue;
}
uint8 Sensor::GetDecimalPresision()
{
    return this->m_iDecimalPresision;
}

void Sensor::SetID(long value)
{
    this->m_lID = value;
}
void Sensor::SetKey(string value)
{
    this->m_sKey = value;
}
void Sensor::SetName(string value)
{
    this->m_sName = value;
}
void Sensor::SetByteOrder(ByteOrderType value)
{
    this->m_oByteOrder = value;
}
void Sensor::SetDataType(DataType value)
{
    this->m_oDataType = value;
}
void Sensor::SetStartRegisterAddress(uint16 value)
{
    this->m_iStartRegisterAddress = value;
}
void Sensor::SetRegisterCount(uint8 value)
{
    this->m_iRegisterCount = value;
}
void Sensor::SetScalingNumerator(long value)
{
    this->m_lScalingNumerator = value;
}
void Sensor::SetScalingDenominator(long value)
{
    this->m_lScalingDenominator = value;
}
void Sensor::SetOffsetValue(double value)
{
    this->m_eOffsetValue = value;
}
void Sensor::SetDecimalPresision(uint8 value)
{
    this->m_iDecimalPresision = value;
}

CDate Sensor::GetLastDataTime()
{
    return this->m_oLastDataTime;
}

ModbusCommError Sensor::GetLastDataError()
{
    return this->m_oLastDataError;
}

double Sensor::GetLastDataValue()
{
    return this->m_eLastDataValue;
}

string Sensor::GetLastDataValueString()
{
    return this->m_sLastDataValue;
}
