#ifndef _SERIAL_H_INCLUDED_1_
#define _SERIAL_H_INCLUDED_1_

#include <string>
#include <sys/types.h>
#include <sys/time.h>
#include <stdio.h>
#include <unistd.h>

#include<logger.h>
#include<speedtype.h>
#include<paritytype.h>

#define MAX_SERIAL_RX_BUFFER_SIZE 1024

class SerialPort {
public:

    bool Open( SpeedType p_oSpeed, uint8 p_iDataBits, ParityType p_oParity, uint8 p_iStopBits);
    bool Send( const char* p_pData, int p_iLength);
	bool Send( string Data);
	string Receive();
    string Receive( uint16 p_iExpectedBytes, long p_iTimeoutMillis);
    void Close();

	string GetPortName();
	void SetPortName(string value);

	void Init();
	~SerialPort();
	static SerialPort* GetInstance();
	static SerialPort* GetInstance( string p_sPortName);
	static void Initialize( uint8 p_iPortCount);
	static void Release();
private:
	SerialPort();

	string m_sPortName;

	long m_lFileHandle;

	struct timeval m_iWaitTimer;

	int m_iLastError;

	char m_cInputBuffer[MAX_SERIAL_RX_BUFFER_SIZE];

	Logger m_oLog;

	static uint8 m_iTotalPorts;
	static SerialPort** m_pPorts;
};



#endif