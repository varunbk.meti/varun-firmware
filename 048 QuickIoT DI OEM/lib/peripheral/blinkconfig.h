#ifndef _BLINK_CONFIG_H_INCLUDED
#define _BLINK_CONFIG_H_INCLUDED

#include<defs.h>

class BlinkConfig {
public:
	void Set(bool p_bContinuous, uint16 p_iOnTime, uint16 p_iOffTime, uint8 p_iBlinkCount, uint8 p_iCycleCount, uint16 p_iSleepInterval);

	BlinkConfig();
	~BlinkConfig();

	uint8	GetBlinkCount();
	void	SetBlinkCount(uint8 value);
	uint16	GetOnTime();
	void	SetOnTime(uint16 value);
	uint16	GetOffTime();
	void	SetOffTime(uint16 value);
	uint8	GetCycleCount();
	void	SetCycleCount(uint8 value);
	uint16	GetSleepInterval();
	void	SetSleepInterval(uint16 value);
	bool	IsContinuous();
	void	EnableContinuous();
	void	DisableContinuous();

private:
	uint8	m_iBlinkCount;
	uint16	m_iOnTime;
	uint16	m_iOffTime;
	uint8	m_iCycleCount;
	uint16	m_iSleepInterval;
	bool	m_bContinuous;
};


#endif
