#ifndef _MODBUS_SPEED_TYPE_H_INCLUDED_
#define _MODBUS_SPEED_TYPE_H_INCLUDED_ 1

#include<string>
#include<defs.h>

using namespace std;

class SpeedType
{
public:
    enum Value : uint8
    {
        Speed_115200 = 0,
        Speed_57600 = 1,
        Speed_38400 = 2,
        Speed_19200 = 3,
        Speed_9600 = 4,
        Speed_4800 = 5,
        Speed_2400 = 6,
        Speed_1200 = 7,
        Speed_Disabled = 8
    };

    SpeedType() = default;
    SpeedType(Value code) : m_iCode(code) { }

    operator Value() const { return m_iCode; }
    explicit operator bool() = delete;
    constexpr bool operator==(SpeedType a) const { return m_iCode == a.m_iCode; }
    constexpr bool operator!=(SpeedType a) const { return m_iCode != a.m_iCode; }
    uint8 GetCode();
    string GetLabel();
    string ToString();
    int GetSpeed();
    int GetTxTime();

    static SpeedType Parse(int code);

private:
    Value m_iCode;
};

#endif
