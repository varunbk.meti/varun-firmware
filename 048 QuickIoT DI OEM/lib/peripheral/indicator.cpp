#include<Arduino.h>
#include<indicator.h>

CThread Indicator::m_oStateThread;
uint8 Indicator::m_iTotalIndicators = 0;
Indicator* Indicator::m_pIndicators = NULL;

void Indicator::StateMachine()
{
	switch (this->m_oState) {
	case BlinkState::Start:
		// Serial.printf( "BlinkState::Start\n");
		this->m_iElapsedBlinkCount = 0;
		this->m_iElapsedCycleCount = 0;
		digitalWrite(this->m_iPinNumber, HIGH);
		this->SetState(BlinkState::On);
		break;

	case BlinkState::On:
		if (this->IsStateTimedOut(this->m_oBlinkConfig.GetOnTime()) ) {
			// Serial.printf( "BlinkState::On Timedout\n");
			digitalWrite(this->m_iPinNumber, LOW);
			this->SetState(BlinkState::Off);
		}
		break;

	case BlinkState::Off:
		if (this->IsStateTimedOut(this->m_oBlinkConfig.GetOffTime())) {
			// Serial.printf( "BlinkState::Off Timedout\n");
			this->m_iElapsedBlinkCount++;

			if (this->m_iElapsedBlinkCount < this->m_oBlinkConfig.GetBlinkCount()) {
				// Serial.printf( "BlinkState::Off CP 1\n");
				digitalWrite(this->m_iPinNumber, HIGH);
				this->SetState(BlinkState::On);
			}
			else {
				if (this->m_oBlinkConfig.GetSleepInterval() > 0) {
					// Serial.printf( "BlinkState::Off CP 2\n");
					this->SetState(BlinkState::CycleSleep);
				}
				else {
					// Serial.printf( "BlinkState::Off CP 3\n");
					this->SetState(BlinkState::CycleEnd);
				}
			}
		}
		break;

	case BlinkState::CycleSleep:
		if (this->IsStateTimedOut(this->m_oBlinkConfig.GetSleepInterval())) {
			// Serial.printf( "BlinkState::CycleSleep Sleep timedout: %d\n", this->m_oBlinkConfig.GetSleepInterval());
			this->SetState(BlinkState::CycleEnd);
		}
		break;

	case BlinkState::CycleEnd:
		this->m_iElapsedCycleCount++;
		if (this->m_oBlinkConfig.IsContinuous() ) {
			// Serial.printf( "BlinkState::CycleEnd CP 1\n");
			digitalWrite(this->m_iPinNumber, HIGH);
			this->SetState(BlinkState::On);
		} else if( this->m_iElapsedCycleCount < this->m_oBlinkConfig.GetCycleCount()) {
			// Serial.printf( "BlinkState::CycleEnd CP 2\n");
			this->m_iElapsedCycleCount = 0;
			digitalWrite(this->m_iPinNumber, HIGH);
			this->SetState(BlinkState::On);
		}
		else {
			// Serial.printf( "BlinkState::CycleEnd CP 3\n");
			this->SetState(BlinkState::Stop);
		}
		break;

	case BlinkState::Stop:
		// Serial.printf( "BlinkState::Stop CP 1\n");
		digitalWrite(this->m_iPinNumber, LOW);
		this->SetState(BlinkState::Idle);
		break;

	case BlinkState::Idle:
	default:
		break;
	}
}

void Indicator::SetState(BlinkState state)
{
	this->m_oState = state;
	this->m_oStateTime = CDate::Now();
}

bool Indicator::IsStateTimedOut(uint16 p_iInterval)
{
	return this->IsStateTimedOut(CDate::Now(), p_iInterval);
}

bool Indicator::IsStateTimedOut(CDate p_oCurrTime, uint16 p_iInterval)
{
	if (this->m_oStateTime.TimeElapsed(p_oCurrTime, IntervalType::MilliSecond) >= p_iInterval) {
		return true;
	}

	return false;
}

void Indicator::GlobalStateMachine( void*)
{
	for (uint8 l_iIdx = 0; l_iIdx < m_iTotalIndicators; l_iIdx++) {
		m_pIndicators[l_iIdx].StateMachine();
	}
}

void Indicator::Initialize(uint8 p_iIndicatorCount)
{
	m_iTotalIndicators = p_iIndicatorCount;
	m_pIndicators = new Indicator[m_iTotalIndicators];
	m_oStateThread.Initialize(Indicator::GlobalStateMachine, NULL);
	m_oStateThread.Run();
}

void Indicator::Release()
{
	m_oStateThread.Join();
	if (m_iTotalIndicators > 0) {
		if (m_pIndicators != NULL) {
			delete[] m_pIndicators;
		}
	}
	m_iTotalIndicators = 0;
	m_pIndicators = NULL;
}


void Indicator::Blink(BlinkConfig value)
{
	this->m_oBlinkConfig = value;
	this->SetState(BlinkState::Start);
}

void Indicator::SetPin(int pin)
{
	this->SetPin(pin, false);
}

void Indicator::SetPin(int pin, bool keepOn)
{
	this->m_iPinNumber = pin;
	pinMode(this->m_iPinNumber, OUTPUT);
	if (keepOn) {
		this->SwitchOn();
	}
	else {
		this->SwitchOff();
	}
	this->m_bEnabled = true;
}

void Indicator::SwitchOff()
{
	digitalWrite(this->m_iPinNumber, LOW);
	this->SetState(BlinkState::KeepOFF);
}

void Indicator::SwitchOn()
{
	digitalWrite(this->m_iPinNumber, HIGH);
	this->SetState(BlinkState::KeepON);
}

void Indicator::Toggle()
{
	if (BlinkState::KeepON == this->m_oState) {
		this->SwitchOff();
	}
	else {
		this->SwitchOn();
	}
}

Indicator* Indicator::GetInstance()
{
	return m_pIndicators;
}

Indicator::Indicator()
{
	this->m_iPinNumber = 0;
	this->m_bEnabled = false;
	this->m_iElapsedBlinkCount = 0;
	this->m_iElapsedCycleCount = 0;
	this->m_oState = BlinkState::Idle;
}

Indicator::~Indicator()
{
	this->SwitchOff();
}


