#include <serialport.h>
#include <termios.h>
#include <unistd.h>
#include <stdio.h>
#include <sys/time.h>
#include <string.h>
#include <stdlib.h>
#include <fcntl.h>
#include <pthread.h>

uint8 SerialPort::m_iTotalPorts = 0;
SerialPort** SerialPort::m_pPorts = NULL;

bool SerialPort::Open( SpeedType p_oSpeed, uint8 p_iDataBits, ParityType p_oParity, uint8 p_iStopBits)
{
	this->Close();
	this->m_lFileHandle = open( this->m_sPortName.c_str(), O_RDWR);

	// Create new termios struc, we call it 'tty' for convention
	struct termios tty;
	memset( &tty, 0, sizeof(tty));

	// Read in existing settings, and handle any error
	if( tcgetattr( this->m_lFileHandle, &tty) != 0) {
		printf("Error %i from tcgetattr: %s\n", errno, strerror(errno));
		this->Close();
		return false;
	}

	cfsetospeed( &tty, p_oSpeed.GetSpeed());

	tty.c_cflag &=  ~CSIZE;
	
	switch( p_iDataBits) {
	case 5:
		tty.c_cflag |= CS5;
		break;
	
	case 6:
		tty.c_cflag |= CS6;
		break;

	case 7:
		tty.c_cflag |= CS7;
		break;

	case 8:
	default:
		tty.c_cflag |= CS8; // 8 bits per byte (most common)
		break;
	}

	if ( ParityType::None == p_oParity ) {
		tty.c_cflag &= ~PARENB; // Clear parity bit, disabling parity (most common)
	} else if ( ParityType::Odd == p_oParity ) {
		tty.c_cflag |= PARENB;
		tty.c_cflag |= PARODD;
	} else if ( ParityType::Even == p_oParity ) {
		tty.c_cflag |= PARENB;
		tty.c_cflag &= ~PARODD;
	} else if ( ParityType::MarkSpace == p_oParity ) {
		tty.c_cflag &= ~PARENB; // Clear parity bit, disabling parity (most common)
	}

	if ( p_iStopBits == 2 ) {
		tty.c_cflag |= CSTOPB;
	} else {
		tty.c_cflag &= ~CSTOPB; // Clear stop field, only one stop bit used in communication (most common)
	}
	
	// tty.c_cflag &= ~CRTSCTS; // Disable RTS/CTS hardware flow control (most common)
	tty.c_cflag |= CREAD | CLOCAL; // Turn on READ & ignore ctrl lines (CLOCAL = 1)

	tty.c_lflag &= ~ICANON;
	tty.c_lflag &= ~ECHO; // Disable echo
	tty.c_lflag &= ~ECHOE; // Disable erasure
	tty.c_lflag &= ~ECHONL; // Disable new-line echo
	tty.c_lflag &= ~ISIG; // Disable interpretation of INTR, QUIT and SUSP
	tty.c_iflag &= ~(IXON | IXOFF | IXANY); // Turn off s/w flow ctrl
	tty.c_iflag &= ~(IGNBRK|BRKINT|PARMRK|ISTRIP|INLCR|IGNCR|ICRNL); // Disable any special handling of received bytes

	tty.c_oflag &= ~OPOST; // Prevent special interpretation of output bytes (e.g. newline chars)
	tty.c_oflag &= ~ONLCR; // Prevent conversion of newline to carriage return/line feed
	// tty.c_oflag &= ~OXTABS; // Prevent conversion of tabs to spaces (NOT PRESENT ON LINUX)
	tty.c_cc[VTIME] = 0;    // Wait for up to 1s (10 deciseconds), returning as soon as any data is received.
	tty.c_cc[VMIN] = 0;

	// Save tty settings, also checking for error
	if ( tcsetattr( this->m_lFileHandle, TCSANOW, &tty) != 0 ) {
		printf("Error %i from tcsetattr: %s\n", errno, strerror(errno));
		this->Close();
		return false;
	}

	return true;
}

void SerialPort::Close( void)
{
	if ( this->m_lFileHandle > 0 ) {
		close( this->m_lFileHandle);
	}
	this->m_lFileHandle = 0;
}

bool SerialPort::Send( string Data)
{
	return this->Send( Data.c_str(), Data.length());
}

bool SerialPort::Send( const char* Data, int Length)
{
	tcflush( this->m_lFileHandle, TCIOFLUSH);
	int l_iResult = write( this->m_lFileHandle, Data, Length);
	if ( l_iResult <= 0 )
	{
		this->Close();

		return false;
	}
	return true;
}

string SerialPort::Receive() {
	return this->Receive( (uint8)1, (long)1);
}

string SerialPort::Receive( uint16 p_iExpectedBytes, long p_iTimeOutMSec)
{
	int l_iRet = 0;

	if ( p_iTimeOutMSec < 100 ) {
		p_iTimeOutMSec = 100;
	}

	// Serial.printf( "Waiting for %d bytes or %d decisecs\n", p_iExpectedBytes, p_iTimeOutMSec);
	struct timeval l_oBeginTime;
	gettimeofday( &l_oBeginTime, NULL);
	struct timeval l_oEndTime;
	long seconds = 0;
	long micros = 0;
	do {
		/* Vinod
		if( serialDataAvail( this->m_lFileHandle) >= p_iExpectedBytes ) {
			break;
		}
		*/
		usleep( 1);
		gettimeofday( &l_oEndTime, NULL);
		seconds = (l_oEndTime.tv_sec - l_oBeginTime.tv_sec);
		micros = ((seconds * 1000000) + l_oEndTime.tv_usec) - (l_oBeginTime.tv_usec);
	} while(micros < (p_iTimeOutMSec * 1000));

	// printf( "Micros:%d, Seconds:%d\n", micros, seconds);

/* Vinod
	if( ! serialDataAvail( this->m_lFileHandle) ) {
		return "";
	}
	*/

	memset( this->m_cInputBuffer, 0, sizeof( MAX_SERIAL_RX_BUFFER_SIZE));

	l_iRet = read( this->m_lFileHandle, this->m_cInputBuffer, p_iExpectedBytes);

	if ( l_iRet <= 0) {
		this->Close();
		return "";
	}
	string l_sBuff = "";
	l_sBuff.append( this->m_cInputBuffer, l_iRet);
	return l_sBuff;
}

SerialPort::SerialPort()
{
	this->Init();
}

void SerialPort::Init()
{
	this->m_lFileHandle = 0;

	this->m_iLastError = 0;

	memset( this->m_cInputBuffer, 0, MAX_SERIAL_RX_BUFFER_SIZE);
}

SerialPort::~SerialPort()
{
	this->Close();

	sleep( 1);

	this->Init();
}

SerialPort* SerialPort::GetInstance()
{
	if ( m_iTotalPorts > 0 ) {
		return m_pPorts[0];
	}

	return NULL;
}

string SerialPort::GetPortName()
{
	return this->m_sPortName;
}

void SerialPort::SetPortName(string value)
{
	this->m_sPortName = value; 
}

void SerialPort::Initialize( uint8 p_iPortCount)
{
	m_iTotalPorts = p_iPortCount;
	m_pPorts = new SerialPort*[SerialPort::m_iTotalPorts];
	for( uint8 l_iIdx = 0; l_iIdx < m_iTotalPorts; l_iIdx++) {
		m_pPorts[l_iIdx] = new SerialPort();
	}
}

void SerialPort::Release()
{
	for( uint8 l_iIdx = 0; l_iIdx < m_iTotalPorts; l_iIdx++) {
		if ( m_pPorts[l_iIdx] != NULL ) {
			delete m_pPorts[l_iIdx];
			m_pPorts[l_iIdx] = NULL;
		}
	}

	m_iTotalPorts = 0;
}
