#ifndef _MODBUS_PARITY_TYPE_H_INCLUDED_
#define _MODBUS_PARITY_TYPE_H_INCLUDED_ 1

#include<string>
#include<defs.h>

using namespace std;

class ParityType
{
public:
    enum Value : uint8
    {
        None = 0,
        Even = 1,
        Odd = 2,
        MarkSpace = 3
    };

    ParityType() = default;
    ParityType(Value code) : m_iCode(code) { }

    operator Value() const { return m_iCode; }
    explicit operator bool() = delete;
    constexpr bool operator==(ParityType a) const { return m_iCode == a.m_iCode; }
    constexpr bool operator!=(ParityType a) const { return m_iCode != a.m_iCode; }
    uint8 GetCode();
    string GetLabel();
    string ToString();

    static ParityType Parse(int code);

private:
    Value m_iCode;
};

#endif
