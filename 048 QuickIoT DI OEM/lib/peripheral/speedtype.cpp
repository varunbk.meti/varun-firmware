#include <string>
#include <speedtype.h>

using namespace std;

SpeedType SpeedType::Parse(int code)
{
    if (code == Speed_57600)
    {
        return Speed_57600;
    }
    else if (code == Speed_38400)
    {
        return Speed_38400;
    }
    else if (code == Speed_19200)
    {
        return Speed_19200;
    }
    else if (code == Speed_9600)
    {
        return Speed_9600;
    }
    else if (code == Speed_4800)
    {
        return Speed_4800;
    }
    else if (code == Speed_2400)
    {
        return Speed_2400;
    }
    else if (code == Speed_1200)
    {
        return Speed_1200;
    }
    else if (code == Speed_Disabled)
    {
        return Speed_Disabled;
    }

    return Speed_115200;
}

string SpeedType::ToString()
{
    return this->GetLabel();
}

uint8 SpeedType::GetCode()
{
    return m_iCode;
}

string SpeedType::GetLabel()
{
    if (m_iCode == Speed_57600)
    {
        return "57600";
    }
    else if (m_iCode == Speed_38400)
    {
        return "38400";
    }
    else if (m_iCode == Speed_19200)
    {
        return "19200";
    }
    else if (m_iCode == Speed_9600)
    {
        return "9600";
    }
    else if (m_iCode == Speed_4800)
    {
        return "4800";
    }
    else if (m_iCode == Speed_2400)
    {
        return "2400";
    }
    else if (m_iCode == Speed_1200)
    {
        return "1200";
    }
    else if (m_iCode == Speed_Disabled)
    {
        return "Disabled";
    }

    return "115200";
}

int SpeedType::GetSpeed()
{
    if (m_iCode == Speed_57600)
    {
        return 57600;
    }
    else if (m_iCode == Speed_38400)
    {
        return 38400;
    }
    else if (m_iCode == Speed_19200)
    {
        return 19200;
    }
    else if (m_iCode == Speed_9600)
    {
        return 9600;
    }
    else if (m_iCode == Speed_4800)
    {
        return 4800;
    }
    else if (m_iCode == Speed_2400)
    {
        return 2400;
    }
    else if (m_iCode == Speed_1200)
    {
        return 1200;
    }
    else if (m_iCode == Speed_Disabled)
    {
        return 0;
    }

    return 115200;
}

int SpeedType::GetTxTime()
{
    if (m_iCode == Speed_57600)
    {
        return 18;
    }
    else if (m_iCode == Speed_38400)
    {
        return 26;
    }
    else if (m_iCode == Speed_19200)
    {
        return 53;
    }
    else if (m_iCode == Speed_9600)
    {
        return 104;
    }
    else if (m_iCode == Speed_4800)
    {
        return 208;
    }
    else if (m_iCode == Speed_2400)
    {
        return 417;
    }
    else if (m_iCode == Speed_1200)
    {
        return 833;
    }
    else if (m_iCode == Speed_Disabled)
    {
        return 0;
    }

    return 9;
}