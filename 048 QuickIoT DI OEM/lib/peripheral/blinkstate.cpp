#include<string>
#include<blinkstate.h>

using namespace std;

BlinkState BlinkState::Parse(int code)
{
    if (code == Idle) {
        return Idle;
    }
    else if (code == Start) {
        return Start;
    }
    else if (code == On) {
        return On;
    }
    else if (code == Off) {
        return Off;
    }
    else if (code == CycleSleep) {
        return CycleSleep;
    }
    else if (code == CycleEnd) {
        return CycleEnd;
    }
    else if (code == Stop) {
        return Stop;
    }
    else if (code == KeepOFF) {
        return KeepOFF;
    }
    else if (code == KeepON) {
        return KeepON;
    }
    

    return Idle;
}

string BlinkState::GetLabel()
{
    if (m_iCode == Idle) {
        return "Idle";
    }
    else if (m_iCode == Start) {
        return "Start";
    }
    else if (m_iCode == On) {
        return "On";
    }
    else if (m_iCode == Off) {
        return "Off";
    }
    else if (m_iCode == CycleSleep) {
        return "CycleSleep";
    }
    else if (m_iCode == CycleEnd) {
        return "CycleEnd";
    }
    else if (m_iCode == Stop) {
        return "Stop";
    }
    else if (m_iCode == KeepOFF) {
        return "KeepOFF";
    }
    else if (m_iCode == KeepON) {
        return "KeepON";
    }
    

    return "Idle";
}

string BlinkState::ToString()
{
    return this->GetLabel();
}

uint8 BlinkState::GetCode()
{
    return m_iCode;
}

