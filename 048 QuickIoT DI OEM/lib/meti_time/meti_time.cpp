#include <Arduino.h>
#include "lwip/apps/sntp.h"
#include <meti_time.h>
#include <logger.h>

const char *ntpServer1 = "0.in.pool.ntp.org";
const char *ntpServer2 = "time.google.com";
const char *ntpServer3 = "pool.ntp.org";

const long gmtOffset_sec = 3600 * 5.5;
const int daylightOffset_sec = 0;

extern Logger *g_pLogger;

void mtime_init(void)
{
    //init and get the time
    configTime(gmtOffset_sec, daylightOffset_sec, ntpServer1, ntpServer2, ntpServer3);
    mtime_print_local_time();
}

void mtime_print_local_time(void)
{
    struct tm timeinfo;
    if (!getLocalTime(&timeinfo))
    {
        g_pLogger->Write(LogLevel::Error, "mtime_handle_npt", "Failed to obtain time...");
    } else {
        g_pLogger->Write(LogLevel::Info, "mtime_handle_npt", "Valid local time...");
    }
}

uint32_t mtime_get_curr_timestamp(void)
{
    time_t now;
    time(&now);
    Serial.println(now);
    return now;
}

void mtime_handle_npt(void)
{
    static uint16_t time_ctr = 0;

    if (time_ctr++ >= METI_TIME_RESYNC_INTERVAL)
    {
        time_ctr = 0;
        sntp_stop();
        delay(5);
        g_pLogger->Write(LogLevel::Trace, "mtime_handle_npt", "Syncing time...");
        sntp_init();
    }
}
