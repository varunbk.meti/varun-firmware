#ifndef _METI_TIME_H
#define _METI_TIME_H

#include <Arduino.h>

#define METI_TIME_RESYNC_INTERVAL 180

extern const char *ntpServer1;
extern const char *ntpServer2;
extern const char *ntpServer3;

extern const long gmtOffset_sec;
extern const int daylightOffset_sec;
void mtime_init(void);
void mtime_print_local_time(void);
uint32_t mtime_get_curr_timestamp(void);
void mtime_handle_npt(void);

#endif