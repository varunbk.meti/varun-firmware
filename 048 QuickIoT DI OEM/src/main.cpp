/*******************************************************************************
 * Project Name: METI QuickIoT Modbus
 *
 * Version: 1.1
 *
 * Description:
 * In this project ESP32 handles web pages using SPIFFS
 * Owner: METI M2M India Pvt Ltd.,
 *
 *
 ********************************************************************************
 * Copyright (2020-21) , METI
 *******************************************************************************/

/* include headers */
#include <Arduino.h>
#include <WiFi.h>
#include <AsyncTCP.h>
#include <ESPAsyncWebServer.h>
#include <SPIFFS.h>
#include <ArduinoJson.h>
#include <config_reader.h>
#include <main.h>
#include <meti_wifi.h>
#include <meti_server.h>
#include <meti_utils.h>
#include <timers.h>
#include <led.h>
#include <stdio.h>
#include <logger.h>
#include <meti_time.h>
#include <esp_event_legacy.h>
#include <indicator.h>
#include <indicatortype.h>
#include <blinktype.h>
#include <meti_net_led.h>
#include <meti_modbus_led.h>
#include <meti_mqtt.h>
#include <meti_nvs.h>
#include <serialport.h>
#include <modbusmasterrtu.h>
#include <node.h>
#include <meti_wifi.h>
#include <modbusscanservice.h>
#include <meti_modbus.h>
#include <MD5Builder.h>
#include <esp_task_wdt.h>
#include <config.h>
#include <I2CMaster.h>
#include <display.h>
#include <Modbus.h>
#include <ModbusRTUSlave.h>
#include <METI_DI_OEM.h>

#define WDT_TIMEOUT 10

Logger *g_pLogger = NULL;
BlinkConfig *g_pBlinkConfig = NULL;
Indicator *g_pIndicators = NULL;

ModbusScanService *g_pScanner = NULL;
Node *g_pNode = NULL;

uint32_t active_driver_id = 0;

bool node_initialized = false;
bool mbr_initialized = false;
bool g_valid_subscription = false;

// Narayan - revisit the following two lines...
string g_sDeviceID = "";
string g_sSiliconID = "";

MD5Builder _md5;
String g_dev_md5 = "";

uint8_t calibrateId = 0;

// This will be used in calibration when needed
uint16_t prevRawValue = 0;

uint8_t hardwareType = HARDWARE_TYPE_OEM_DI;

double gSensorValueArray[SENSOR_VALUE_4_20MA_MAX];

// DI Water Lvl Structure variable
DI_Wtr_Lvl Wtr_Lvl;

/* webserver object on port 80 */
AsyncWebServer server(80);

void setup()
{
	pinMode(PIN_LED_RED, OUTPUT);
	digitalWrite(PIN_LED_RED, HIGH);

#ifdef ENABLE_5110_DISPLAY
	display_setup();
	display_logo();
#endif

	delay(100);

	Serial.end();
	Serial.begin(115200);
	delay(500);

	// Use LogLevel::Info in production....Trace/Debug in testing
	LogLevel logLev = LogLevel::Debug;
	if (logLev.GetCode() <= LogLevel::Debug)
	{
		delay(1000);
	}

	// test_routine();

	g_pLogger = new Logger(logLev);
	g_pLogger->Write(LogLevel::Info, "setup", "METI QuickIoT - Start...");

	InitializeBlinkProcess();
	InitializeIndicators();
	net_led_initialize();
	net_led_set_state(MULTI_LED_STATE_RED_ON);

	start_file_storage();

	read_device_file();

#ifdef ENABLE_SCALING
	read_sensor_scaling_file(); // THIS IS USED IN MULTIPLE MODES
#endif

#ifdef ENABLE_NON_LINEAR_SCALING
	read_sensor_scaling_nl_file();
#endif

#ifdef ENABLE_OUTPUT_CONTROL
	read_outCtrl_file();
#endif

#ifdef ENABLE_NVS
	meti_nvs_initialize();
	meti_nvs_read_all("params");
#endif

	setup_device_params();

#ifdef ENABLE_WIFI
	// NARAYAN - port fix (externally pulled up)
	pinMode(PIN_FORCED_AP_MODE_SWITCH, INPUT);

	g_pLogger->Write(LogLevel::Info, "setup", "Wifi start");

	read_wifi_file();
	meti_wifi_start();
	setup_server();

#endif

#ifdef Enable_Water_Lvl_Ctrl
	// data/drivers/mmd_1.json
	active_driver_id = 1;
	read_modbus_driver_file();
	DI_Init();
#endif

#ifdef ENABLE_I2C_MASTER
	// This will be the driver file for this fixed function i2c based device.
	// data/drivers/mmd_1.json
	active_driver_id = 1;
	read_modbus_driver_file(); // THIS IS USED IN MULTIPLE MODES
	I2CM.Start();
#endif

#ifdef ENABLE_MODBUS_SLAVE
	// modbus_led_initialize();
	MBus.Start();
#endif

#ifdef ENABLE_MODBUS_MASTER
	modbus_led_initialize();
	read_modbus_driver_file();

	// Initialize Modbus
	node_initialized = InitializeNode();
	g_pLogger->Write(LogLevel::Trace, "main", "node_initialized : %d", node_initialized);

	if (node_initialized == true)
	{
		InitializeMBRConfig();
	}
	else
	{
		g_pLogger->Write(LogLevel::Info, "main", "Node initialization failed");
		// Narayan check this...
		g_pIndicators[IndicatorType::LedModbusErr].Blink(g_pBlinkConfig[BlinkType::Fast]);
	}
#endif

#ifdef ENABLE_WIFI
#ifdef ENABLE_MQTT
	read_mqtt_file();
	// MQTT CONNECT will happen after WIFI connection is established...
	mqtt_setup();
	mqtt_setup_device_params();
	mqtt_setup_sensor_params();
#endif
#endif

#ifdef ENABLE_DEVICE_SUBSCRIPTION_CHECK
	validateDeviceSubscription();
#endif

	// Enable watch dog
	esp_task_wdt_init(WDT_TIMEOUT, true); // enable panic so ESP32 restarts
	esp_task_wdt_add(NULL);				  // add current thread to WDT watch

	// Time setup
	// mtime_init() will be call after wifi is initialized and IP received..

	timer_start();

#ifdef ENABLE_OUTPUT_CONTROL
	process_output_setup();
#endif

	g_pLogger->Write(LogLevel::Debug, "main", "Driver json size: %d", modbusDriverSize);
}

// END OF SETUP FUNCTIONS

void loop()
{
	// put your main code here, to run repeatedly:
	timer_check();
	esp_task_wdt_reset();
}

void start_file_storage(void)
{
	/* Initialize SPIFFS */
	if (!SPIFFS.begin())
	{
		g_pLogger->Write(LogLevel::Fatal, "start_file_storage", "An Error has occurred while mounting SPIFFS !!");

		while (1)
			; // stop
	}
}

void timer_timeout_1ms(void)
{
	// 1ms Tasks
}

void timer_timeout_10ms(void)
{
	// 10ms Tasks
}

void timer_timeout_50ms(void)
{
	// 50ms Tasks
	// led_timer();

#ifdef ENABLE_MODBUS_SLAVE
	MRTUS.Update();
#endif
}

void timer_timeout_100ms(void)
{
	// 100ms Tasks
#ifdef ENABLE_WIFI
	wifi_test_mode();
#endif

#ifndef ENABLE_DEVICE_SUBSCRIPTION_CHECK
	g_valid_subscription = true;
#endif

	if (g_valid_subscription == true)
	{
#ifdef ENABLE_I2C_MASTER
		if (calibrate_sensor == true)
		{
			calibrate_sensor = false;
			I2CM.Calibrate_IDAC();
		}
		else
		{
			I2CM.Read_From_Device();
			i2cm_update_sensor_data();
		}
#endif // ENABLE_I2C_MASTER

#ifdef ENABLE_OUTPUT_CONTROL
		process_output_control();
#endif

#ifdef ENABLE_5110_DISPLAY
		display_update_params();
		display_all();
#endif

#ifdef Enable_Water_Lvl_Ctrl
		DI_Watrer_Lvl_Read();
		update_DI_data();
#endif
	}
}

void timer_timeout_500ms(void)
{
// 500ms Tasks
#ifdef ENABLE_MODBUS_MASTER
	if (node_initialized == true)
	{
		if (file_send_inprogress == false)
		{
			g_pScanner->StateMachine();
		}
	}
#endif
}

// int ctr = 0;
void timer_timeout_1000ms(void)
{
	// 1 second tasks
	//  g_pLogger->Write(LogLevel::Trace, "1-s", "Mode: %s, Status: %s, got_ip: %d, ip: %s", meti_wifi_get_mode_text().c_str(), meti_wifi_get_status_text().c_str(), wifi_got_ip, WiFi.localIP().toString().c_str());

	// Remove the following freeheap printing line in production...
	// g_pLogger->Write(LogLevel::Info, "timer_timeout_1000ms", "FreeHeap: %d", ESP.getFreeHeap());

#ifdef ENABLE_WIFI
	wifi_station_display_once();
	update_device_wifi_params();

	if (wifi_test_mode_status == WIFI_TEST_MODE_OFF)
	{
		check_forced_ap_mode_entry();
		forced_ap_mode_timeout();
		store_sta_ip_address();
	}

	if (WiFi.getMode() == WIFI_STA)
	{
		wifi_check_status();

		// wifi_config_npt will be set once wifi gets the IP address from external router
		if (wifi_config_npt == true)
		{
			wifi_config_npt = false;
			mtime_init();
		}

		if (wifi_got_ip == true)
		{
			mtime_handle_npt();
		}

#ifdef ENABLE_MQTT
		if (g_valid_subscription == true)
		{
			// mqtt_enabled = true;
			if (mqtt_enabled == true)
			{
				mqtt_check_comm_status();
				if (file_send_inprogress == false)
				{
					mqtt_publish_timeout();
				}
			}
		}
#endif

		if (wifi_internet_ok == true)
		{
			set_rssi_led();
		}
	}
#endif
}

void setup_device_params(void)
{
	// remove colon and replace with -

	String dev_id;
	String mac = WiFi.macAddress();

	// Remove all colons in device id and add MQ1....
	dev_id = "MQI1-" + mac;
	dev_id.replace(":", "-");

	device_doc["dev_mac_addr"] = mac;
	device_doc["dev_id"] = dev_id;
	device_doc["dev_vdd"] = 3.3;

	g_dev_md5 = md5(dev_id + "quickiot dev_id");
	g_pLogger->Write(LogLevel::Info, "setup_device_params", "Device ID: %s", mac.c_str());
}

void InitializeIndicators(void)
{
	Indicator::Initialize(IndicatorType::GetSize());
	g_pIndicators = Indicator::GetInstance();

	g_pIndicators[IndicatorType::ModbusTxEnable].SetPin(MODBUS_TX_ENABLE_PIN);
}

void InitializeBlinkProcess(void)
{
	g_pBlinkConfig = new BlinkConfig[BlinkType::GetSize()];

	g_pBlinkConfig[BlinkType::Fast].Set(true, 50, 50, 1, 0, 0);
	g_pBlinkConfig[BlinkType::Once].Set(false, 200, 200, 1, 1, 0);
	g_pBlinkConfig[BlinkType::Twice].Set(false, 50, 200, 2, 1, 0);
	g_pBlinkConfig[BlinkType::BeforeFactoryReset].Set(false, 300, 200, 5, 1, 500);
	g_pBlinkConfig[BlinkType::BeforeMbrReset].Set(false, 300, 200, 3, 1, 1500);
	g_pBlinkConfig[BlinkType::Slow].Set(true, 500, 500, 1, 0, 0);
	g_pBlinkConfig[BlinkType::OnceFor300Ms].Set(false, 300, 300, 1, 1, 0);
	g_pBlinkConfig[BlinkType::BuzzOnce].Set(false, 100, 100, 1, 1, 0);
	g_pBlinkConfig[BlinkType::BuzzTwice].Set(false, 100, 100, 2, 1, 0);
}

void meti_device_restart(void)
{
	net_led_set_state(MULTI_LED_STATE_RED_ON);
	delay(100);
	ESP.restart();
}

#ifdef ENABLE_MODBUS_MASTER
void eventDataReceived(DynamicJsonDocument doc)
{
	String str;
	serializeJson(doc, str);

	g_pLogger->Write(LogLevel::Info, "main", "SensorData :%s", str.c_str());
}

void InitializeMBRConfig()
{
	g_pLogger->Write(LogLevel::Info, "main", "Initializing scanner...");
	MBRConfig::SetCallBack(eventDataReceived);
	MBRConfig *l_pConfig = g_pNode->GetMBRConfig();
	uint8 l_iMbrCount = g_pNode->GetMBRCount();
	g_pScanner = ModbusScanService::GetInstance();
	g_pScanner->Initialize(l_iMbrCount, l_pConfig, g_pLogger);
}

bool InitializeNode(void)
{
	bool retVal = false;
	g_pLogger->Write(LogLevel::Info, "main", "Initializing node...");

	DynamicJsonDocument driver_doc(modbusDriverSize);

	File root = SPIFFS.open("/drivers");
	File file = root.openNextFile();

	String fName;

	while (file)
	{
		fName = file.name();
		if (fName.substring(0, 13) == "/drivers/mmd_")
		{
			deserializeJson(driver_doc, file);
			file.close();

			if (driver_doc.containsKey("driver_id") && driver_doc.containsKey("dev_key"))
			{
				if (driver_doc["dev_key"] == g_dev_md5)
				{
					if (driver_doc["active"] == true)
					{
						g_pLogger->Write(LogLevel::Debug, "InitializeNode", "Using driver file: %s", fName.c_str());
						g_pNode = Node::GetInstance(driver_doc.as<JsonObject>());

						if (g_pNode == NULL)
						{
							g_pLogger->Write(LogLevel::Debug, "InitializeNode", "Invalid node configuration..");
							// fprintf(stderr, "Invalid node configuration.....");
							break;
						}
						else
						{
							retVal = true;
							// g_pNode->Initialize(g_pLogger);
							active_driver_id = driver_doc["driver_id"];
							g_sSiliconID = g_pNode->GetSiliconID();
							g_pLogger->Write(LogLevel::Info, "InitializeNode", "Configured Silicon:%s", g_sSiliconID.c_str());
							// Only one mbr allowed...
							break;
						}
					}
					else
					{
						g_pLogger->Write(LogLevel::Debug, "InitializeNode", "Modbus Driver Inactive : %s", fName.c_str());
					}
				}
				else
				{
					g_pLogger->Write(LogLevel::Debug, "InitializeNode", "Modbus Driver Invalid dev_key : %s", fName.c_str());
				}
			}
			else
			{
				g_pLogger->Write(LogLevel::Debug, "InitializeNode", "Modbus Driver Invalid : %s", fName.c_str());
			}
		}
		file = root.openNextFile();
	}

	file.close();
	root.close();
	return retVal;
}
#endif

String md5(String str)
{
	_md5.begin();
	_md5.add(String(str));
	_md5.calculate();
	return _md5.toString();
}

// void i2cm_update_sensor_data(void)
// {
// 	JsonObject root = sensorValues_doc.to<JsonObject>();
// 	JsonObject sensorSrc = modbusDriver_doc["command"]["responseParser"].as<JsonObject>();

// 	I2CM.Update_ScaledValues();

// 	root["time"] = "ABCD";

// 	for (JsonObject::iterator itr = sensorSrc.begin(); itr != sensorSrc.end(); ++itr)
// 	{
// 		String key = itr->key().c_str();
// 		uint8_t decimals = itr->value()["dp"];
// 		double finalVal = 0;
// 		char l_cBuff[32];

// 		JsonObject obj = root.createNestedObject(key);
// 		obj["error"] = 0;

// 		if (key.equals("sensor_100"))
// 		{
// 			finalVal = I2CM.Get_Resistance_Value();
// 			// g_pLogger->Write(LogLevel::Info, "update_sensor_data", "ADC Count: %d", I2CM.Get_ADCRawCount());
// 		}
// 		else if (key.equals("sensor_101"))
// 		{
// 			finalVal = I2CM.Get_ScaledValue(decimals);
// 			// update display process_val
// 			display_vals.process_val = finalVal;
// 			modbus_registers.process_val = I2CM.Get_ScaledValue(0);
// 		}
// 		else if (key.equals("sensor_102"))
// 		{
// 			finalVal = I2CM.Get_FinalValue(1, decimals);
// 			modbus_registers.value1 = I2CM.Get_FinalValue(1, 0);
// 		}
// 		else if (key.equals("sensor_103"))
// 		{
// 			finalVal = I2CM.Get_FinalValue(2, decimals);
// 			modbus_registers.value2 = I2CM.Get_FinalValue(2, 0);
// 		}
// 		else if (key.equals("sensor_104"))
// 		{
// 			finalVal = I2CM.Get_FinalValue(3, decimals);
// 			modbus_registers.value3 = I2CM.Get_FinalValue(3, 0);
// 		}
// 		else if (key.equals("sensor_105"))
// 		{
// 			finalVal = I2CM.Get_FinalValue(4, decimals);
// 			modbus_registers.value4 = I2CM.Get_FinalValue(4, 0);
// 		}
// 		else if (key.equals("sensor_106"))
// 		{
// 			finalVal = I2CM.Get_FinalValue(5, decimals);
// 			modbus_registers.value5 = I2CM.Get_FinalValue(5, 0);
// 		}

// 		sprintf(l_cBuff, "%.*f", decimals, finalVal);
// 		obj["value"] = l_cBuff;
// 		itr->value()["cv"] = l_cBuff;
// 	}
// }

#ifdef ENABLE_OUTPUT_CONTROL
void process_output_setup(void)
{
	pinMode(PIN_RELAY_1, OUTPUT);
}

void process_output_control(void)
{
	bool outCtrl_enabled = outCtrl_doc["output_1"]["enabled"].as<bool>(); // true

	if (outCtrl_enabled == true)
	{
		int16_t set_point_on;
		int16_t set_point_off;
		int16_t delay_on;
		int16_t delay_off;
		static bool relay_status = LOW;
		bool drive_relay = false;
		bool inverted = false;

		set_point_on = outCtrl_doc["output_1"]["set_point_on"].as<int>();
		set_point_off = outCtrl_doc["output_1"]["set_point_off"].as<int>();
		delay_on = outCtrl_doc["output_1"]["delay_on"].as<int>();
		delay_off = outCtrl_doc["output_1"]["delay_off"].as<int>();

		// If on is greater than off, then inverted logic
		if (set_point_on > set_point_off)
		{
			inverted = true;
		}

		int16_t scaledValue = I2CM.Get_ScaledValue(0);
		if (scaledValue == SENSOR_OPEN_STATE) // If sensor Open, switch off Relay...
		{
			relay_status = LOW;
			drive_relay = true;
		}
		else
		{
			if (inverted == false)
			{
				if (scaledValue <= set_point_on)
				{
					// relay on
					relay_status = HIGH;
					drive_relay = true;
				}

				if (scaledValue >= set_point_off)
				{
					// relay off
					relay_status = LOW;
					drive_relay = true;
				}
			}
			else
			{
				if (scaledValue >= set_point_on)
				{
					relay_status = HIGH;
					drive_relay = true;
				}

				if (scaledValue <= set_point_off)
				{
					relay_status = LOW;
					drive_relay = true;
				}
			}
		}

		// g_pLogger->Write(LogLevel::Debug, "poc", "set_point_on:%d, set_point_off:%d, scaledValue:%d, inverted:%d, relay_status:%d", set_point_on, set_point_off, scaledValue, inverted, relay_status);

		if (drive_relay == true)
		{
			digitalWrite(PIN_RELAY_1, relay_status);
			// update display relay status
			display_vals.relay_on = relay_status;
			modbus_registers.relay_on = relay_status;
		}
	}
}
#endif

void validateDeviceSubscription(void)
{
	g_valid_subscription = false;

	if (modbusDriver_doc.containsKey("driver_id") && modbusDriver_doc.containsKey("dev_key"))
	{
		if (modbusDriver_doc["dev_key"] == g_dev_md5)
		{
			if (modbusDriver_doc["active"] == true)
			{
				g_valid_subscription = true;
			}
		}
	}

	if (g_valid_subscription == false)
	{
		g_pLogger->Write(LogLevel::Error, "validateDeviceSubscription", "Device Registration Failed!!", modbusDriverSize);
		display_error();
	}
}

void DI_Watrer_Lvl_Read(void)
{
	uint8_t *DI_Ptr;
	DI_Ptr = Read_and_Process_Digital_Inputs();
	Wtr_Lvl.OHT_Level = DI_Ptr[0];
	Wtr_Lvl.Pump_1 = DI_Ptr[1];
	Wtr_Lvl.SUMP_Level = DI_Ptr[2];
	Wtr_Lvl.Pump_2 = DI_Ptr[3];
}

void update_DI_data(void)
{
	JsonObject root = sensorValues_doc.to<JsonObject>();
	JsonObject sensorSrc = modbusDriver_doc["command"]["responseParser"].as<JsonObject>();

	// I2CM.Update_ScaledValues();

	root["time"] = "ABCD";

	for (JsonObject::iterator itr = sensorSrc.begin(); itr != sensorSrc.end(); ++itr)
	{
		String key = itr->key().c_str();
		uint8_t decimals = itr->value()["dp"];
		double finalVal = 0;
		char l_cBuff[32];

		JsonObject obj = root.createNestedObject(key);
		obj["error"] = 0;

		if (key.equals("sensor_100"))
		{
			finalVal = Wtr_Lvl.OHT_Level;
			modbus_registers.val1 = Wtr_Lvl.OHT_Level;
		}
		else if (key.equals("sensor_101"))
		{
			finalVal = Wtr_Lvl.Pump_1;
			modbus_registers.val2 = Wtr_Lvl.Pump_1;
		}
		else if (key.equals("sensor_102"))
		{
			finalVal = Wtr_Lvl.SUMP_Level;
			modbus_registers.val3 = Wtr_Lvl.SUMP_Level;
		}
		else if (key.equals("sensor_103"))
		{
			finalVal = Wtr_Lvl.Pump_2;
			modbus_registers.val4 = Wtr_Lvl.Pump_2;
		}
		// else if (key.equals("sensor_104"))
		// {
		// 	finalVal = Wtr_Lvl.OHT_Prcnt_25;
		// 	modbus_registers.val5 = Wtr_Lvl.OHT_Prcnt_25;
		// }
		// else if (key.equals("sensor_105"))
		// {
		// 	finalVal = Wtr_Lvl.Pump_2;
		// 	modbus_registers.val6 = Wtr_Lvl.Pump_2;
		// }
		// else if (key.equals("sensor_106"))
		// {
		// 	finalVal = 0;
		// 	modbus_registers.val7 = Wtr_Lvl.SUMP_Prcnt_50;
		// }

		sprintf(l_cBuff, "%.*f", decimals, finalVal);
		obj["value"] = l_cBuff;
		itr->value()["cv"] = l_cBuff;
	}
}