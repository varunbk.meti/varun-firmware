#include <Arduino.h>
#include <display.h>
#include <utils.h>
#include <WiFi.h>
#include <config_reader.h>
#include <I2CMaster.h>
#include <logger.h>

/* Declare LCD object for SPI
 Adafruit_PCD8544(CLK,DIN,D/C,CE,RST); */
Adafruit_PCD8544 display = Adafruit_PCD8544(18, 23, 14, 5, 13);

display_struct display_vals;
extern Logger *g_pLogger;

void display_setup(void)
{
    display_vals.timeout = false;
    display_vals.relay_on = false;
    display_vals.wifi_rssi = 0;
    display_vals.wifi_connected_devices = 0;
    display_vals.process_val = 0;

    memset(display_vals.message_arr[IDX_DISP_MSG_SSID], 0, DISP_MAX_LEN);
    memset(display_vals.message_arr[IDX_DISP_MSG_INT_ERROR], 0, DISP_MAX_LEN);
    memset(display_vals.message_arr[IDX_DISP_MSG_MQTT_ERROR], 0, DISP_MAX_LEN);
    memset(display_vals.message_arr[IDX_DISP_MSG_OTH_ERROR2], 0, DISP_MAX_LEN);

    // NARAYAN - testing
    // strncpy(display_vals.message_arr[IDX_DISP_MSG_INT_ERROR], "Internet Err", DISP_MAX_LEN);
    // strncpy(display_vals.message_arr[IDX_DISP_MSG_MQTT_ERROR], "MQTT Error", DISP_MAX_LEN);

    display_vals.message_idx = 0;

    /* Initialize the Display*/
    display.begin();

    /* Change the contrast using the following API*/
    display.setContrast(DISP_CONTRAST);

    /* Clear the buffer */
    display.clearDisplay();
    display.display();
}

void display_all(void)
{
    display.clearDisplay();

    display_top_line();
    display_process_value();
    display_message_line();

    display.display();
}

void display_top_line(void)
{
    char line[DISP_ARRAY_LEN];
    char relay_char = ' ';
    uint8_t final_val = 0;
    char percent_char = '%';

    memset(line, 0, DISP_ARRAY_LEN);

    // g_pLogger->Write(LogLevel::Debug, "display_top_line", "Relay:%d Rssi:%d Conn:%d", display_vals.relay_on, display_vals.wifi_rssi, display_vals.wifi_connected_devices);

    if (display_vals.relay_on == true)
    {
        relay_char = 'R';
    }

    // if ap mode
    if (display_vals.wifi_rssi == -1)
    {
        final_val = display_vals.wifi_connected_devices;
        percent_char = ' ';
    }
    else
    {
        final_val = display_vals.wifi_rssi;
    }

    sprintf(line, "%c          %2d%c", relay_char, final_val, percent_char);
    display.setTextSize(1);
    display.setCursor(0, 0);
    display.println(line);
}

void display_process_value(void)
{
    char line[DISP_ARRAY_LEN];
    uint8_t dp_val = 0;
    memset(line, 0, DISP_ARRAY_LEN);

    display.setTextColor(BLACK, WHITE);

    if (display_vals.process_val == SENSOR_OPEN_STATE)
    {
        display.setTextSize(3);
        strcpy(line, "OPEN");
        display.setCursor(6, 10);
        display.println(line);
    }
    else
    {
        display.setTextSize(3);
        sprintf(line, "%3d", (uint16_t)display_vals.process_val);
        display.setTextSize(3);
        display.setCursor(0, 10);
        display.println(line);

        display.setTextSize(2);
        dp_val = (display_vals.process_val * 10) - ((uint16_t)display_vals.process_val * 10);
        sprintf(line, ".%d", dp_val);
        display.setCursor(51, 16);
        display.println(line);

        display.setTextSize(1);
        display.setCursor(75, 23);
        display.println("%");
    }
}

void display_message_line(void)
{
    char line[DISP_ARRAY_LEN];

    memset(line, 0, DISP_ARRAY_LEN);
    strncpy(line, display_vals.message_arr[display_vals.message_idx], DISP_MAX_LEN);

    display.setCursor(0, 40);
    display.setTextSize(1);
    display.println(line);
}

void display_logo(void)
{
    display.drawBitmap(21, 0, meti_logo1, 39, 35, BLACK);
    display.drawBitmap(15, 36, meti_logo2, 53, 12, BLACK);
    display.display();
}

void display_update_params(void)
{
    static uint16_t ctr = DISP_MESSAGE_TIMEOUT;
    ctr--;
    if (ctr == 0)
    {
        ctr = DISP_MESSAGE_TIMEOUT;
        display_vals.message_idx++;
        if (display_vals.message_idx >= DISP_MESSAGE_MAX)
        {
            display_vals.message_idx = 0;
        }

        // Check if next message has some content
        while (display_vals.message_arr[display_vals.message_idx][0] == 0)
        {
            display_vals.message_idx++;
            if (display_vals.message_idx >= DISP_MESSAGE_MAX)
            {
                display_vals.message_idx = 0;
                break;
            }
        }
    }

    memset(display_vals.message_arr[IDX_DISP_MSG_SSID], 0, DISP_MAX_LEN);

    // Check wifi mode station or ap mode
    if (WiFi.getMode() == WIFI_STA)
    {
        //SSID
        strncpy(display_vals.message_arr[IDX_DISP_MSG_SSID], device_doc["wf_conn_ssid"], DISP_MAX_LEN);

        //Wifi RSSI
        display_vals.wifi_rssi = device_doc["wf_rssi"];
        //if above 99 set to 99, 100% will not be displayed...
        if (display_vals.wifi_rssi > 99)
        {
            display_vals.wifi_rssi = 99;
        }
        //Wifi connected devices (ap mode) - will be set to zero
        display_vals.wifi_connected_devices = 0;
    }
    else //AP mode
    {
        //SSID will be set as local
        strcpy(display_vals.message_arr[IDX_DISP_MSG_SSID], "Local");
        //Wifi RSSI will be set to -1;
        display_vals.wifi_rssi = -1;
        //Wifi connected devices
        display_vals.wifi_connected_devices = device_doc["wf_conn_devs"];
    }
}

void display_error(void)
{
    char line[DISP_ARRAY_LEN];

    display.clearDisplay();

    display.setTextSize(1);
    strcpy(line, "Dev ID Error!");
    display.setCursor(2, 5);
    display.println(line);

    strcpy(line, "Call Vendor");
    display.setCursor(2, 30);
    display.println(line);

    display.display();
}