#include<string>
#include<indicatortype.h>

using namespace std;

IndicatorType IndicatorType::Parse(int code)
{
    if (code == LedRed) {
        return LedRed;
    }
    else if (code == LedGreen) {
        return LedGreen;
    }
    else if (code == LedBlue) {
        return LedBlue;
    }
    else if (code == ModbusTxEnable) {
        return ModbusTxEnable;
    }
    else if (code == LedModbusTx) {
        return LedModbusTx;
    }
    else if (code == LedModbusRx) {
        return LedModbusRx;
    }
    else if (code == LedModbusErr) {
        return LedModbusErr;
    }

    return LedRed;
}

string IndicatorType::GetLabel()
{
    if (m_iCode == LedRed) {
        return "LedRed";
    }
    else if (m_iCode == LedGreen) {
        return "LedGreen";
    }
    else if (m_iCode == LedBlue) {
        return "LedBlue";
    }
    else if (m_iCode == ModbusTxEnable) {
        return "ModbusTxEnable";
    }
    else if (m_iCode == LedModbusTx) {
        return "LedModbusTx";
    }
    else if (m_iCode == LedModbusRx) {
        return "LedModbusRx";
    }
    else if (m_iCode == LedModbusErr) {
        return "LedModbusErr";
    }

    return "";
}

string IndicatorType::ToString()
{
    return this->GetLabel();
}

uint8 IndicatorType::GetCode()
{
    return m_iCode;
}

