/*******************************************************************************
*File Name: Modbus.cpp
*
* Version: 1.0
*
* Description:
* In this source file for modbus application
*
*
* Owner:
* Yogesh M Iggalore
*
********************************************************************************
* Copyright (2020-21)
*******************************************************************************/

#include <Arduino.h>
#include <Modbus.h>
#include <ModbusRTUSlave.h>
#include <WiFi.h>
#include <logger.h>

/* Modbus speed 0 - 115200, 1 - 57600, 2 - 38400 , 3 - 19200 , 4 - 9600, 5 - 4800 , 6 - 2400 7-1200*/
uint32_t aui32ModbusActualBaudRate[MODBUS_SPEEDS_SUPPORTED_MAX] = {115200, 57600, 38400, 19200, 9600, 4800, 2400, 1200};
//Time per byte in milliseconds
//Index-Baudrate : 0 - 115200, 1 - 57600, 2 - 38400 , 3 - 19200 , 4 - 9600, 5 - 4800 , 6 - 2400, 7-1200
uint8_t aui8ModbusTimePerByte[MODBUS_SPEEDS_SUPPORTED_MAX] = {1, 1, 1, 1, 2, 2, 4, 8};

// 0 - None parity, 1 - Even Parity , 2 - Odd Parity , 3 - Mark Space
uint8_t aui8ModbusParity[MODBUS_PARITY_SUPPORTED_MAX] = {0, 1, 2, 3};

//*************************************************************************************************
//*** CRC look-up table
//*************************************************************************************************
const char TabCRCHi[] = {
    0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40,
    0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41,
    0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41,
    0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40,
    0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41,
    0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40,
    0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40,
    0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41,
    0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41,
    0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40,
    0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40,
    0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41,
    0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40,
    0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41,
    0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41,
    0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40};

const char TabCRCLo[] = {
    0x00, 0xC0, 0xC1, 0x01, 0xC3, 0x03, 0x02, 0xC2, 0xC6, 0x06, 0x07, 0xC7, 0x05, 0xC5, 0xC4, 0x04,
    0xCC, 0x0C, 0x0D, 0xCD, 0x0F, 0xCF, 0xCE, 0x0E, 0x0A, 0xCA, 0xCB, 0x0B, 0xC9, 0x09, 0x08, 0xC8,
    0xD8, 0x18, 0x19, 0xD9, 0x1B, 0xDB, 0xDA, 0x1A, 0x1E, 0xDE, 0xDF, 0x1F, 0xDD, 0x1D, 0x1C, 0xDC,
    0x14, 0xD4, 0xD5, 0x15, 0xD7, 0x17, 0x16, 0xD6, 0xD2, 0x12, 0x13, 0xD3, 0x11, 0xD1, 0xD0, 0x10,
    0xF0, 0x30, 0x31, 0xF1, 0x33, 0xF3, 0xF2, 0x32, 0x36, 0xF6, 0xF7, 0x37, 0xF5, 0x35, 0x34, 0xF4,
    0x3C, 0xFC, 0xFD, 0x3D, 0xFF, 0x3F, 0x3E, 0xFE, 0xFA, 0x3A, 0x3B, 0xFB, 0x39, 0xF9, 0xF8, 0x38,
    0x28, 0xE8, 0xE9, 0x29, 0xEB, 0x2B, 0x2A, 0xEA, 0xEE, 0x2E, 0x2F, 0xEF, 0x2D, 0xED, 0xEC, 0x2C,
    0xE4, 0x24, 0x25, 0xE5, 0x27, 0xE7, 0xE6, 0x26, 0x22, 0xE2, 0xE3, 0x23, 0xE1, 0x21, 0x20, 0xE0,
    0xA0, 0x60, 0x61, 0xA1, 0x63, 0xA3, 0xA2, 0x62, 0x66, 0xA6, 0xA7, 0x67, 0xA5, 0x65, 0x64, 0xA4,
    0x6C, 0xAC, 0xAD, 0x6D, 0xAF, 0x6F, 0x6E, 0xAE, 0xAA, 0x6A, 0x6B, 0xAB, 0x69, 0xA9, 0xA8, 0x68,
    0x78, 0xB8, 0xB9, 0x79, 0xBB, 0x7B, 0x7A, 0xBA, 0xBE, 0x7E, 0x7F, 0xBF, 0x7D, 0xBD, 0xBC, 0x7C,
    0xB4, 0x74, 0x75, 0xB5, 0x77, 0xB7, 0xB6, 0x76, 0x72, 0xB2, 0xB3, 0x73, 0xB1, 0x71, 0x70, 0xB0,
    0x50, 0x90, 0x91, 0x51, 0x93, 0x53, 0x52, 0x92, 0x96, 0x56, 0x57, 0x97, 0x55, 0x95, 0x94, 0x54,
    0x9C, 0x5C, 0x5D, 0x9D, 0x5F, 0x9F, 0x9E, 0x5E, 0x5A, 0x9A, 0x9B, 0x5B, 0x99, 0x59, 0x58, 0x98,
    0x88, 0x48, 0x49, 0x89, 0x4B, 0x8B, 0x8A, 0x4A, 0x4E, 0x8E, 0x8F, 0x4F, 0x8D, 0x4D, 0x4C, 0x8C,
    0x44, 0x84, 0x85, 0x45, 0x87, 0x47, 0x46, 0x86, 0x82, 0x42, 0x43, 0x83, 0x41, 0x81, 0x80, 0x40};

union
{
    unsigned char Array[2];

    struct
    {
        unsigned char LL : 4;
        unsigned char LH : 4;
        unsigned char HL : 4;
        unsigned char HH : 4;
    } NB4;

    struct
    {                     // Nibbles x 16 bits
        unsigned char Lo; // IntL
        unsigned char Hi; // IntH
    } Byte;

    struct
    { // BITs
        unsigned b00 : 1;
        unsigned b01 : 1;
        unsigned b02 : 1;
        unsigned b03 : 1;
        unsigned b04 : 1;
        unsigned b05 : 1;
        unsigned b06 : 1;
        unsigned b07 : 1;
        unsigned b08 : 1;
        unsigned b09 : 1;
        unsigned b10 : 1;
        unsigned b11 : 1;
        unsigned b12 : 1;
        unsigned b13 : 1;
        unsigned b14 : 1;
        unsigned b15 : 1;
    } Bit;

    signed int INTs;
    unsigned int INTu;
} CRC;

extern Logger *g_pLogger;

Modbus::Modbus() {}

void Modbus::Start(void)
{
    // start modbus hardware here
    uint32_t ui32Baudrate;
    pinMode(MODBUS_TX_ENABLE_PIN, OUTPUT);
    Pin_Control(MODBUS_ENABLE, LOW); //Varun Changed to LOW

#ifdef ENABLE_MODBUS_LED    
    pinMode(MODBUS_TX_LED_PIN, OUTPUT);
    pinMode(MODBUS_RX_LED_PIN, OUTPUT);
    pinMode(MODBUS_SPARE_LED_PIN, OUTPUT);

    Pin_Control(MODBUS_RX_LED, LOW);
    Pin_Control(MODBUS_TX_LED, LOW);
    Pin_Control(MODBUS_SPARE_LED, LOW);
#endif

    ui32Baudrate = aui32ModbusActualBaudRate[tdfModbus.ui8Baudrate];
    MODBUS_SERIAL.begin(ui32Baudrate, Get_UART_Config(tdfModbus.ui8Parity, tdfModbus.ui8Databit, tdfModbus.ui8Stopbit), MODBUS_RX_PIN, MODBUS_TX_PIN);

    g_pLogger->Write(LogLevel::Info, "Modbus_Start", "baudrate: %d nParity: %d Stopbit: %d Databit: %d DeviceId: %d ", ui32Baudrate, tdfModbus.ui8Parity, tdfModbus.ui8Stopbit, tdfModbus.ui8Databit, tdfModbus.ui8DeviceID);

    // DEBUG_SERIAL.println("\r\nModbus UART Start");
    // DEBUG_SERIAL.printf("baudrate: %d\r\nParity: %d\r\nStopbit: %d\r\nDatabit: %d\r\nDeviceId: %d\r\n",

    memset(MRTUS.input_data, 0, INPUT_BUFFER_SIZE);
    memset(MRTUS.modbus_cmd, 0, INPUT_BUFFER_SIZE);
}

void Modbus::Pin_Control(uint8_t ui8Pin, uint8_t ui8Control)
{
    if (ui8Pin == MODBUS_ENABLE)
    {
        digitalWrite(MODBUS_TX_ENABLE_PIN, ui8Control);
    }

#ifdef ENABLE_MODBUS_LED
    if (ui8Pin == MODBUS_RX_LED)
    {
        digitalWrite(MODBUS_RX_LED_PIN, ui8Control);
    }

    if (ui8Pin == MODBUS_TX_LED)
    {
        digitalWrite(MODBUS_TX_LED_PIN, ui8Control);
    }

    if (ui8Pin == MODBUS_SPARE_LED)
    {
        digitalWrite(MODBUS_SPARE_LED_PIN, ui8Control);
    }
#endif

}

uint32_t Modbus::Get_Baudrate(uint8_t ui8Index)
{
    return aui32ModbusActualBaudRate[ui8Index];
}

uint8_t Modbus::Get_Parity(uint8_t ui8Index)
{
    return aui8ModbusParity[ui8Index];
}

uint8_t Modbus::Get_TimePerByte(uint8_t ui8Index)
{
    return aui8ModbusTimePerByte[ui8Index];
}

uint32_t Modbus::Get_UART_Config(uint8_t ui8Parity, uint8_t ui8Databits, uint8_t ui8Stopbit)
{

    if ((ui8Databits == MODBUS_DATA_BITS_5) & (ui8Parity == MODBUS_PARITY_NONE) & (ui8Stopbit == MODBUS_STOP_BIT_1))
    {
        return SERIAL_5N1;
    }
    else if ((ui8Databits == MODBUS_DATA_BITS_6) & (ui8Parity == MODBUS_PARITY_NONE) & (ui8Stopbit == MODBUS_STOP_BIT_1))
    {
        return SERIAL_6N1;
    }
    else if ((ui8Databits == MODBUS_DATA_BITS_7) & (ui8Parity == MODBUS_PARITY_NONE) & (ui8Stopbit == MODBUS_STOP_BIT_1))
    {
        return SERIAL_7N1;
    }
    else if ((ui8Databits == MODBUS_DATA_BITS_8) & (ui8Parity == MODBUS_PARITY_NONE) & (ui8Stopbit == MODBUS_STOP_BIT_1))
    {
        return SERIAL_8N1;
    }
    else if ((ui8Databits == MODBUS_DATA_BITS_5) & (ui8Parity == MODBUS_PARITY_NONE) & (ui8Stopbit == MODBUS_STOP_BIT_2))
    {
        return SERIAL_5N2;
    }
    else if ((ui8Databits == MODBUS_DATA_BITS_6) & (ui8Parity == MODBUS_PARITY_NONE) & (ui8Stopbit == MODBUS_STOP_BIT_2))
    {
        return SERIAL_6N2;
    }
    else if ((ui8Databits == MODBUS_DATA_BITS_7) & (ui8Parity == MODBUS_PARITY_NONE) & (ui8Stopbit == MODBUS_STOP_BIT_2))
    {
        return SERIAL_7N2;
    }
    else if ((ui8Databits == MODBUS_DATA_BITS_8) & (ui8Parity == MODBUS_PARITY_NONE) & (ui8Stopbit == MODBUS_STOP_BIT_2))
    {
        return SERIAL_8N2;
    }
    else if ((ui8Databits == MODBUS_DATA_BITS_5) & (ui8Parity == MODBUS_PARITY_EVEN) & (ui8Stopbit == MODBUS_STOP_BIT_1))
    {
        return SERIAL_5E1;
    }
    else if ((ui8Databits == MODBUS_DATA_BITS_6) & (ui8Parity == MODBUS_PARITY_EVEN) & (ui8Stopbit == MODBUS_STOP_BIT_1))
    {
        return SERIAL_6E1;
    }
    else if ((ui8Databits == MODBUS_DATA_BITS_7) & (ui8Parity == MODBUS_PARITY_EVEN) & (ui8Stopbit == MODBUS_STOP_BIT_1))
    {
        return SERIAL_7E1;
    }
    else if ((ui8Databits == MODBUS_DATA_BITS_8) & (ui8Parity == MODBUS_PARITY_EVEN) & (ui8Stopbit == MODBUS_STOP_BIT_1))
    {
        return SERIAL_8E1;
    }
    else if ((ui8Databits == MODBUS_DATA_BITS_5) & (ui8Parity == MODBUS_PARITY_EVEN) & (ui8Stopbit == MODBUS_STOP_BIT_2))
    {
        return SERIAL_5E2;
    }
    else if ((ui8Databits == MODBUS_DATA_BITS_6) & (ui8Parity == MODBUS_PARITY_EVEN) & (ui8Stopbit == MODBUS_STOP_BIT_2))
    {
        return SERIAL_6E2;
    }
    else if ((ui8Databits == MODBUS_DATA_BITS_7) & (ui8Parity == MODBUS_PARITY_EVEN) & (ui8Stopbit == MODBUS_STOP_BIT_2))
    {
        return SERIAL_7E2;
    }
    else if ((ui8Databits == MODBUS_DATA_BITS_8) & (ui8Parity == MODBUS_PARITY_EVEN) & (ui8Stopbit == MODBUS_STOP_BIT_2))
    {
        return SERIAL_8E2;
    }
    else if ((ui8Databits == MODBUS_DATA_BITS_5) & (ui8Parity == MODBUS_PARITY_ODD) & (ui8Stopbit == MODBUS_STOP_BIT_1))
    {
        return SERIAL_5O1;
    }
    else if ((ui8Databits == MODBUS_DATA_BITS_6) & (ui8Parity == MODBUS_PARITY_ODD) & (ui8Stopbit == MODBUS_STOP_BIT_1))
    {
        return SERIAL_6O1;
    }
    else if ((ui8Databits == MODBUS_DATA_BITS_7) & (ui8Parity == MODBUS_PARITY_ODD) & (ui8Stopbit == MODBUS_STOP_BIT_1))
    {
        return SERIAL_7O1;
    }
    else if ((ui8Databits == MODBUS_DATA_BITS_8) & (ui8Parity == MODBUS_PARITY_ODD) & (ui8Stopbit == MODBUS_STOP_BIT_1))
    {
        return SERIAL_8O1;
    }
    else if ((ui8Databits == MODBUS_DATA_BITS_5) & (ui8Parity == MODBUS_PARITY_ODD) & (ui8Stopbit == MODBUS_STOP_BIT_2))
    {
        return SERIAL_5O2;
    }
    else if ((ui8Databits == MODBUS_DATA_BITS_6) & (ui8Parity == MODBUS_PARITY_ODD) & (ui8Stopbit == MODBUS_STOP_BIT_2))
    {
        return SERIAL_6O2;
    }
    else if ((ui8Databits == MODBUS_DATA_BITS_7) & (ui8Parity == MODBUS_PARITY_ODD) & (ui8Stopbit == MODBUS_STOP_BIT_2))
    {
        return SERIAL_7O2;
    }
    else if ((ui8Databits == MODBUS_DATA_BITS_8) & (ui8Parity == MODBUS_PARITY_ODD) & (ui8Stopbit == MODBUS_STOP_BIT_2))
    {
        return SERIAL_8O2;
    }
    else
    {
        return SERIAL_8N1;
    }
}

uint16_t Modbus::CalculateCRC16(uint8_t *MsgStartAddr, uint8_t MsgLength)
{
    uint16_t ui16Response = 0;
    uint8_t Index;

    uint8_t CRCtmpHi = 0xFF; // Initialize CRC register to 0xFFFF
    uint8_t CRCtmpLo = 0xFF;

    while (MsgLength--) // Find CRC for each byte of the string
    {
        Index = CRCtmpLo ^ *MsgStartAddr++;
        CRCtmpLo = CRCtmpHi ^ TabCRCHi[Index];
        CRCtmpHi = TabCRCLo[Index];
    }

    /* Actual
    ui16Response = CRCtmpHi;
    ui16Response = ui16Response << 8;
    ui16Response = ui16Response | CRCtmpLo;
    CRC.Byte.Hi = CRCtmpHi;                             // Update the CRC registers
    CRC.Byte.Lo = CRCtmpLo;
    */
    /* changed */

    ui16Response = CRCtmpLo;
    ui16Response = ui16Response << 8;
    ui16Response = ui16Response | CRCtmpHi;
    CRC.Byte.Hi = CRCtmpHi; // Update the CRC registers
    CRC.Byte.Lo = CRCtmpLo;

    return ui16Response;
}

uint8_t Modbus::Verify_FunctionCode(uint8_t ui8ReceivedByte)
{
    uint8_t ui8Response = 0;
    uint8_t ui8FunCode = 0;

    ui8FunCode = ui8ReceivedByte;
    ui8ReceivedByte = ui8ReceivedByte & 0x1F;

    switch (ui8ReceivedByte)
    {
    case MODBUS_FUNC_CODE_COIL_REG:
        ui8Response = 1;
        break;
    case MODBUS_FUNC_CODE_INPUT_STATUS:
        ui8Response = 1;
        break;
    case MODBUS_FUNC_CODE_HOLDING_REG:
        ui8Response = 1;
        break;
    case MODBUS_FUNC_CODE_INPUT_REG:
        ui8Response = 1;
        break;
    case MODBUS_FUNC_CODE_FORCE_SINGLE_COIL:
        ui8Response = 1;
        break;
    case MODBUS_FUNC_CODE_PRESET_SINGLE_REG:
        ui8Response = 1;
        break;
    case MODBUS_FUNC_CODE_FORCE_MULTIPLE_COIL:
        ui8Response = 1;
        break;
    case MODBUS_FUNC_CODE_PRESET_MULTIPLE_REG:
        ui8Response = 1;
        break;
    default:
        ui8Response = 0;
        break;
    }

    /* proper funcode error */
    if (ui8Response == 1)
    {
        if ((ui8FunCode >> 4) == 0x8)
        {
            ui8Response = 2;
        }
        else if ((ui8FunCode >> 4) == 0x9)
        {
            ui8Response = 2;
        }
        else if ((ui8FunCode >> 4) == 0x1)
        {
            ui8Response = 1;
        }
        else
        {
            if ((ui8FunCode >> 4) != 0x0)
            {
                ui8Response = 0;
            }
        }
    }

    return ui8Response;
}

/****************************************************************************** 
* Function Name: Modbus_ASCII_LRC
*******************************************************************************
*
* Summary:
*  This function calculates LRC
*
* Parameters:  
*  MsgSourceAddr, MsgLength
*
* Return: 
*  cLRC
*
*******************************************************************************/
// char Modbus::ASCII_LRC(uint8 *MsgSourceAddr, uint8 MsgLength){
// 	uint8 cLRC = 0;

// 	while (MsgLength != 0){
// 		cLRC += *MsgSourceAddr++;
// 		MsgLength--;
// 	}
//     cLRC = 0xFF - cLRC;
//     cLRC = cLRC + 1;

// 	return cLRC;
// }

void Modbus::Test(void)
{
    MODBUS_SERIAL.println("testing");
}

// Preinstantiate Objects //////////////////////////////////////////////////////
#if !defined(NO_GLOBAL_INSTANCES) && !defined(NO_GLOBAL_MODBUS)
Modbus MBus;
#endif
