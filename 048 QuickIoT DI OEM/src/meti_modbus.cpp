#include <Arduino.h>
#include <meti_modbus.h>

#ifdef ENABLE_MODBUS_MASTER
uint32_t g_modbusSerialConfig = SERIAL_8N1;

uint32_t get_uart_config(uint8_t ui8Parity, uint8_t ui8Stopbit)
{
    uint32_t retVal = SERIAL_8N1;

    switch (ui8Stopbit)
    {
    case MODBUS_STOP_BIT_2:
        switch (ui8Parity)
        {
        case MODBUS_PARITY_EVEN:
            retVal = SERIAL_8E2;
            break;

        case MODBUS_PARITY_ODD:
            retVal = SERIAL_8O2;
            break;

        case MODBUS_PARITY_NONE:
            retVal = SERIAL_8N2;
            break;
        }
        break;
    case MODBUS_STOP_BIT_1:
        switch (ui8Parity)
        {
        case MODBUS_PARITY_EVEN:
            retVal = SERIAL_8E1;
            break;

        case MODBUS_PARITY_ODD:
            retVal = SERIAL_8O1;
            break;

        case MODBUS_PARITY_NONE:
            retVal = SERIAL_8N1;
            break;
        }
        break;
    }

    return retVal;
}

String get_uart_config_label(uint32_t config_val)
{

    switch (config_val)
    {
    case SERIAL_8N2:
        return "SERIAL_8N2";
        break;
    case SERIAL_8E2:
        return "SERIAL_8E2";
        break;
    case SERIAL_8O2:
        return "SERIAL_8O2";
        break;
    case SERIAL_8N1:
        return "SERIAL_8N1";
        break;
    case SERIAL_8E1:
        return "SERIAL_8E1";
        break;
    case SERIAL_8O1:
        return "SERIAL_8O1";
        break;
    default:
        return "SERIAL_UNCONFIGURED";
        break;
    }
}

#endif