#include <string.h>
#include <modbusscanservice.h>
#include <modbusmasterrtu.h>
#include <indicator.h>
#include <indicatortype.h>
#include <blinkconfig.h>
#include <blinktype.h>

using namespace std;

extern Indicator *g_pIndicators;
extern BlinkConfig *g_pBlinkConfig;
extern Logger *g_pLogger;

ModbusScanService *ModbusScanService::m_pSingleInstance = NULL;

void ModbusScanService::Start()
{
	this->m_oStateMachineThread.Initialize(ModbusScanService::ThreadStateMachine, this);
	this->m_oStateMachineThread.Run();
}

void ModbusScanService::Stop()
{
	this->m_oStateMachineThread.Join();
}

/*
DataSet* ModbusScanService::GetData()
{
	DataSet* l_pRetVal = new DataSet();
	
	for (uint8 l_iIdx = 0; l_iIdx < this->m_iMBRCount; l_iIdx++) {
		l_pRetVal->Add(this->m_pMbrConfig[l_iIdx].GetData());
	}

	if (l_pRetVal->GetSize() == 0) {
		delete l_pRetVal;
		l_pRetVal = NULL;
	}

	return l_pRetVal;
}
*/

void ModbusScanService::ThreadStateMachine(void *p_pArgs)
{
	ModbusScanService *l_pInstance = (ModbusScanService *)p_pArgs;

	// g_pLogger->Write( LogLevel::Trace, "ModbusScanService::ThreadStateMachine", "Start");

	if (l_pInstance->m_bRTUScanInProgress)
	{
		if (l_pInstance->m_pMbrConfig[l_pInstance->m_iLastScannedMbrIdx].IsScanActive())
		{
			return;
		}
		else
		{
			l_pInstance->m_bRTUScanInProgress = false;
		}
	}

	uint8 l_iTriggeredIndex = l_pInstance->m_iMBRCount;
	CDate l_oCurrTime = CDate::Now();
	for (uint8 l_iIdx = l_pInstance->m_iLastScannedMbrIdx + 1; l_iIdx < l_pInstance->m_iMBRCount; l_iIdx++)
	{
		if (l_pInstance->m_pMbrConfig[l_iIdx].IsScanTriggered(l_oCurrTime))
		{
			if (l_iTriggeredIndex == l_pInstance->m_iMBRCount)
			{
				l_iTriggeredIndex = l_iIdx;
			}
		}
	}

	for (uint8 l_iIdx = 0; l_iIdx <= l_pInstance->m_iLastScannedMbrIdx; l_iIdx++)
	{
		if (l_pInstance->m_pMbrConfig[l_iIdx].IsScanTriggered(l_oCurrTime))
		{
			if (l_iTriggeredIndex == l_pInstance->m_iMBRCount)
			{
				l_iTriggeredIndex = l_iIdx;
			}
		}
	}

	if (l_iTriggeredIndex != l_pInstance->m_iMBRCount)
	{
		g_pLogger->Write(LogLevel::Trace, "ModbusScanService::ThreadStateMachine", "Scan triggered for %d:%s", l_iTriggeredIndex, l_pInstance->m_pMbrConfig[l_iTriggeredIndex].GetName().c_str());
		l_pInstance->m_iLastScannedMbrIdx = l_iTriggeredIndex;
		l_pInstance->m_bRTUScanInProgress = true;
		// g_pIndicators[IndicatorType::Scan].Blink(g_pBlinkConfig[BlinkType::OnceFor300Ms]);
		l_pInstance->m_pMbrConfig[l_iTriggeredIndex].RunScan();
		// g_pIndicators[IndicatorType::LedModbusTx].Blink(g_pBlinkConfig[BlinkType::Once]);
	}
}

void ModbusScanService::StateMachine()
{
	ThreadStateMachine(this);
}

void ModbusScanService::Initialize(uint8 p_iMBRCount, MBRConfig *p_pMBR, Logger *p_pLogger)
{
	DataService::Initialize(p_pLogger);
	ModbusMaster::Initialize(p_pLogger, NULL);
	if (p_iMBRCount == 0)
		return;

	this->m_iMBRCount = p_iMBRCount;
	this->m_iLastScannedMbrIdx = this->m_iMBRCount - 1;
	this->m_pMbrConfig = p_pMBR;
}

ModbusScanService *ModbusScanService::GetInstance()
{
	if (m_pSingleInstance == NULL)
	{
		m_pSingleInstance = new ModbusScanService();
	}

	return m_pSingleInstance;
}

ModbusScanService::ModbusScanService()
{
	this->m_iMBRCount = 0;
	this->m_iLastScannedMbrIdx = 0;
	this->m_bRTUScanInProgress = false;
	this->m_pMbrConfig = NULL;
}

ModbusScanService::~ModbusScanService()
{
	ModbusMaster::Release();
	this->m_pMbrConfig = NULL;
	this->m_bRTUScanInProgress = false;
	this->m_iMBRCount = 0;
}

string ModbusScanService::ToString()
{
	return "ModbusScanService";
}
