#include <Arduino.h>
#include <WiFi.h>
#include <meti_mqtt.h>
#include <main.h>
#include <config_reader.h>
#include <meti_wifi.h>
#include <meti_nvs.h>
#include <meti_net_led.h>

uint16_t mqtt_device_params_id;
uint16_t mqtt_sensor_params_id;
uint16_t mqtt_sensor_values_id;

char buff[20];
const size_t mqtt_topic_capacity = JSON_OBJECT_SIZE(64) + 1000;
DynamicJsonDocument mqtt_topic_doc(mqtt_topic_capacity);

AsyncMqttClient mqttClient;

/* cleintid should be unique */
String sClientId;

/* will messege on client disconnection and connection to other client */
String sMqttWillTopic;

String sMqttDeviceParamsTopic;
String sMqttSensorParamsTopic;
String sMqttSensorValuesTopic;

bool mqtt_enabled = false;
bool mqtt_status = false;
bool mqtt_start = true;

bool mqtt_first_pass = true;

int mqtt_publish_interval_ctr = PUBLISH_INTERVAL_MIN; //this will be overwritten by configured value on start
int mqtt_reconnect_ctr = RECONNECT_INTERVAL;

StaticJsonDocument<MQTT_SP_DOC_SIZE> mqtt_sp_doc;
StaticJsonDocument<MQTT_DP_DOC_SIZE> mqtt_dp_doc;
StaticJsonDocument<MQTT_SV_DOC_SIZE> mqtt_sv_doc;

void mqtt_setup(void)
{
    mqtt_enabled = mqtt_doc["enabled"];

    if (mqtt_enabled == false)
    {
        return;
    }

    mqtt_setup_topics();

    mqttClient.onConnect(mqtt_on_connect);
    mqttClient.onDisconnect(mqtt_on_disconnect);
    mqttClient.onSubscribe(mqtt_on_subscribe_ack);
    mqttClient.onUnsubscribe(mqtt_on_unsubscribe_ack);
    mqttClient.onMessage(mqtt_on_message);
    mqttClient.onPublish(mqtt_on_publish_ack);

    //Initial reconnect will be immediate...
    mqtt_reconnect_ctr = 1;
    mqtt_first_pass = true;
}

void mqtt_setup_topics_old(void)
{
    g_pLogger->Write(LogLevel::Debug, "mqtt", "Setup topics");

    const char *dev_id = device_doc["dev_id"]; // "ab.mn.xy.ab"

    sClientId.concat(dev_id);

    /* will message topic  */
    sMqttWillTopic = sClientId + "/status";

    JsonObject sensorSrc = modbusDriver_doc["command"]["responseParser"].as<JsonObject>();

    for (JsonObject::iterator itr = sensorSrc.begin(); itr != sensorSrc.end(); ++itr)
    {
        String sname = itr->value()["nm"];
        sname.replace(" ", "_");
        sname.replace("/", "_");
        sname.replace("+", "_");
        sname.replace("#", "_");

        mqtt_topic_doc[itr->key().c_str()] = sClientId + "/get/" + sname;

        g_pLogger->Write(LogLevel::Debug, "mqtt", "MQTT Topic:%s -> %s", itr->key().c_str(), sname.c_str());
    }

    String str;
    serializeJson(mqtt_topic_doc, str);
    g_pLogger->Write(LogLevel::Debug, "mqtt", "MQTT Topics:%s", str.c_str());
}

void mqtt_setup_topics(void)
{
    g_pLogger->Write(LogLevel::Debug, "mqtt", "Setup topics");

    const char *dev_id = device_doc["dev_id"]; // "ab.mn.xy.ab"

    sClientId.concat(dev_id);

    /* will message topic  */
    sMqttWillTopic = sClientId + "/status";

    sMqttDeviceParamsTopic = sClientId + "/device/params";
    sMqttSensorParamsTopic = sClientId + "/mbr0/sensor_params";
    sMqttSensorValuesTopic = sClientId + "/mbr0/sensor_values";
}

void mqtt_connect(void)
{
    String mqtt_str;
    serializeJson(mqtt_doc, mqtt_str);
    g_pLogger->Write(LogLevel::Info, "mqtt", "Connecting to MQTT... %s\n", mqtt_str.c_str());

    // bool mqtt_enabled = mqtt_doc["enabled"].as<bool>(); // true
    const char *mqtt_server = mqtt_doc["server"]; // "silicosmos.in"
    int mqtt_port = mqtt_doc["port"];             // 1883
    int mqtt_ka = mqtt_doc["ka"];                 // 15
    const char *mqtt_un = mqtt_doc["un"];         // "yogesh"
    const char *mqtt_pw = mqtt_doc["pw"];         // "yogesh"
    int mqtt_qos = mqtt_doc["qos"];               // 2
    // const char *mqtt_t_pw = mqtt_doc["t_pw"];     // not used now
    int mqtt_pi = mqtt_doc["pi"]; // 10

    /* set mqtt client id */
    mqttClient.setClientId(sClientId.c_str());

    /* set username and password */
    mqttClient.setCredentials(mqtt_un, mqtt_pw);

    /* set server and port */
    mqttClient.setServer(mqtt_server, mqtt_port);

    /* set keepalive value */
    mqttClient.setKeepAlive(mqtt_ka);

    /* set will message */
    // Narayan ... will topic is removed in this...
    // mqttClient.setWill(sMqttWillTopic.c_str(), mqtt_qos, MQTT_RETAIN, "offline", strlen("offline"));

    /* connect to mqtt */
    mqttClient.connect();

    mqtt_publish_interval_ctr = mqtt_pi;
    mqtt_reconnect_ctr = RECONNECT_INTERVAL;
}

void mqtt_on_connect(bool sessionPresent)
{
    mqtt_status = true;
    g_pLogger->Write(LogLevel::Info, "mqtt", "Connected to MQTT... Session: %d", sessionPresent);

    /* publish will topic to server */
    // Narayan - will topic is removed in this...
    // mqttClient.publish(sMqttWillTopic.c_str(), MQTT_QOS_1, MQTT_RETAIN, "online");
    // g_pLogger->Write(LogLevel::Info, "mqtt", "Publishing will topic..");

    if (mqtt_first_pass == true)
    {
        mqtt_first_pass = false;
        mqtt_publish_retain();
        mqtt_publish_sensor_values();
    }
}

// void mqtt_publish_sensor_values(void)
// {
//     if (mqtt_status == true)
//     {

//     }
// }

void mqtt_on_disconnect(AsyncMqttClientDisconnectReason reason)
{
    // led_set_mode(LED_MQTT, LED_MODE_FAST_BLINK);

    String dc_reason = mqtt_disconnect_reason(reason);
    g_pLogger->Write(LogLevel::Info, "mqtt", "Disconnected from MQTT Reason: %s", dc_reason.c_str());

    //Narayan check if wifi status has to be checked...
    mqtt_status = false;
    mqtt_start = true;
}

void mqtt_on_subscribe_ack(uint16_t packetId, uint8_t qos)
{
    g_pLogger->Write(LogLevel::Info, "mqtt", "Subscribe acknowledged QoS %d, packetId: %d", qos, packetId);
}

void mqtt_on_unsubscribe_ack(uint16_t packetId)
{
    g_pLogger->Write(LogLevel::Info, "mqtt", "Unsubscribe acknowledged packetId: %d", packetId);
}

void mqtt_on_message(char *topic, char *payload, AsyncMqttClientMessageProperties properties, size_t len, size_t index, size_t total)
{
    // uint8_t ui8Payload;

    // Serial.printf("Publish received topic: %d\n", packetId);

    // Serial.println("Publish received.");
    // Serial.print("  topic: ");
    // Serial.println(topic);
    // Serial.print("  qos: ");
    // Serial.println(properties.qos);
    // Serial.print("  dup: ");
    // Serial.println(properties.dup);
    // Serial.print("  retain: ");
    // Serial.println(properties.retain);
    // Serial.print("  len: ");
    // Serial.println(len);
    // Serial.print("  index: ");
    // Serial.println(index);
    // Serial.print("  total: ");
    // Serial.println(total);
    // Serial.println(payload);

    // ui8Payload = String(payload).toInt();
    // Serial.print("Payload: ");
    // Serial.println(ui8Payload);

    // if(strcmp(topic,MqFile.sMqttSubRelayTopic.c_str()) == 0){
    //     if(ui8Payload){
    //         EApp.Application_Pin_Write(RELAY, HIGH);
    //     }else{
    //         EApp.Application_Pin_Write(RELAY, LOW);
    //     }
    //     Mqtt_Publish_All();
    // }
}

void mqtt_on_publish_ack(uint16_t packetId)
{
    // digitalWrite(LED_MQTT_PIN, HIGH);
    g_pLogger->Write(LogLevel::Info, "mqtt", "Publish acknowledged... packetId: %d", packetId);
    wifi_internet_ok = true;
}

void mqtt_publish_timeout(void)
{
    if (WiFi.getMode() == WIFI_STA)
    {
        if (mqtt_publish_interval_ctr-- <= 0)
        {
            int mqtt_pi = mqtt_doc["pi"]; // 10

            mqtt_publish_interval_ctr = mqtt_pi;
            // mqtt_publish_modbus_data();
            mqtt_publish_sensor_values();
        }
    }
}

void mqtt_publish_modbus_data(void)
{
    if (WiFi.getMode() == WIFI_STA)
    {
        if (mqtt_status == true)
        {
            JsonObject sensorSrc = sensorValues_doc.as<JsonObject>();

            for (JsonObject::iterator itr = sensorSrc.begin(); itr != sensorSrc.end(); ++itr)
            {
                const char *key = itr->key().c_str();

                if (mqtt_topic_doc.containsKey(key))
                {
                    const char *topic = mqtt_topic_doc[key];

                    JsonObject val = itr->value().as<JsonObject>();
                    if (val.getMember("error") > 0)
                    {
                        // if modbus error, do not publish...
                        g_pLogger->Write(LogLevel::Debug, "mqtt_publish", "Modbus Error!!! skiping topic:%s", topic);
                    }
                    else
                    {
                        // double final_val = val.getMember("value").as<float>();
                        String final_val = val.getMember("value");
                        //decimal places has to come dynamically
                        //Narayan decimal places not working here...
                        // snprintf(buff, sizeof(buff), "%.0*f", 2, final_val);
                        g_pLogger->Write(LogLevel::Trace, "mqtt_publish", "topic:%s = %s", topic, final_val.c_str());

                        // Narayan disabled for bulk head...
                        // mqtt_rrc_id = mqttClient.publish(topic, MQTT_QOS_1, MQTT_NO_RETAIN, final_val.c_str());
                    }
                }
            }
        }
    }
}

String mqtt_disconnect_reason(AsyncMqttClientDisconnectReason reason)
{
    String retVal = "Unknown";

    switch ((int8_t)reason)
    {
    case 0:
        retVal = "TCP_DISCONNECTED";
        break;

    case 1:
        retVal = "MQTT_UNACCEPTABLE_PROTOCOL_VERSION";
        break;

    case 2:
        retVal = "MQTT_IDENTIFIER_REJECTED";
        break;

    case 3:
        retVal = "MQTT_SERVER_UNAVAILABLE";
        break;

    case 4:
        retVal = "MQTT_MALFORMED_CREDENTIALS";
        break;

    case 5:
        retVal = "MQTT_NOT_AUTHORIZED";
        break;

    default:
        break;
    }

    return retVal;
}

void mqtt_check_comm_status(void)
{
    if (g_valid_subscription == true)
    {
        if (mqtt_status == false)
        {
            if (wifi_got_ip == true)
            {
                if (mqtt_start == true)
                {
                    if (mqtt_reconnect_ctr-- <= 0)
                    {
                        mqtt_start = false;
                        mqtt_connect();
                    }
                }
            }
        }
    }
}

void mqtt_setup_device_params(void)
{
    mqtt_dp_doc["0"] = "BulkHead1";
    mqtt_dp_doc["pc"] = "QuickIoT Bulk Head NLL";
    mqtt_dp_doc["hw"] = "1.0.0";
    mqtt_dp_doc["fw"] = "1.0.0";
    mqtt_dp_doc["dn"] = "Room2";
    mqtt_dp_doc["wi"] = "METI AIRTEL";
    mqtt_dp_doc["cn"] = "METI M2M India Pvt Ltd";
    // serializeJson(doc, output);
}

void mqtt_setup_sensor_params(void)
{
    String outputText;

    //Open the modbus driver and loop through all sensors....
    JsonObject sensorSrc = modbusDriver_doc["command"]["responseParser"].as<JsonObject>();

    //clear the sensor_params doc
    mqtt_sp_doc.clear();
    uint8_t sensor_index = 0;

    for (JsonObject::iterator itr = sensorSrc.begin(); itr != sensorSrc.end(); ++itr)
    {
        bool sensor_active = itr->value()["ac"].as<bool>();

        if (sensor_active == true)
        {
            bool sensor_tx = itr->value()["tx"].as<bool>();

            if (sensor_tx == true)
            {
                //add the sensor to sensor_params doc
                String nm = itr->value()["nm"];
                JsonObject val = mqtt_sp_doc.createNestedObject(nm);
                // val["si"] = sensor_index;
                val["um"] = itr->value()["un"];
                val["rg"] = itr->value()["rg"].as<int>();
                // val["rg"] = itr->value()["rg"];
            }
        }
        sensor_index++;
    }

    serializeJson(mqtt_sp_doc, outputText);
    g_pLogger->Write(LogLevel::Info, "mqtt_setup_sensor_params", "sensor_params:%s", outputText.c_str());
}

void mqtt_publish_retain(void)
{
    String outputText;
    String outputText1;

    serializeJson(mqtt_sp_doc, outputText);
    mqtt_sensor_params_id = mqttClient.publish(sMqttSensorParamsTopic.c_str(), MQTT_QOS_1, MQTT_RETAIN, outputText.c_str());

    serializeJson(mqtt_dp_doc, outputText1);
    mqtt_device_params_id = mqttClient.publish(sMqttDeviceParamsTopic.c_str(), MQTT_QOS_1, MQTT_RETAIN, outputText1.c_str());
}

void mqtt_publish_sensor_values(void)
{
    if (mqtt_status == true)
    {
        String outputText;

        //Open the modbus driver and loop through all sensors....
        JsonObject sensorSrc = modbusDriver_doc["command"]["responseParser"].as<JsonObject>();

        //clear the sensor_values doc
        mqtt_sv_doc.clear();
        mqtt_sv_doc["ts"] = 1614166449;

        int8_t sensor_index = 0;

        for (JsonObject::iterator itr = sensorSrc.begin(); itr != sensorSrc.end(); ++itr)
        {
            bool sensor_active = itr->value()["ac"].as<bool>();

            if (sensor_active == true)
            {
                bool sensor_tx = itr->value()["tx"].as<bool>();

                if (sensor_tx == true)
                {
                    //add the sensor to sensor_params doc
                    String nm = itr->value()["nm"];
                    JsonObject val = mqtt_sv_doc.createNestedObject(nm);
                    // val["si"] = sensor_index;
                    val["v"] = itr->value()["cv"].as<double>();
                    val["m"] = "";
                    val["e"] = "";
                    val["c"] = "#02343";
                }
            }
            sensor_index++;
        }

        serializeJson(mqtt_sv_doc, outputText);
        g_pLogger->Write(LogLevel::Debug, "mqtt_publish_sensor_values", "sensor_values:%s", outputText.c_str());
        mqtt_sensor_values_id = mqttClient.publish(sMqttSensorValuesTopic.c_str(), MQTT_QOS_1, MQTT_RETAIN, outputText.c_str());
    }
}