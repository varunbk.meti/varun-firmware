#include <Arduino.h>
#include <meti_modbus_led.h>
#include <meti_net_led.h>
#include <logger.h>
#include <indicator.h>
#include <indicatortype.h>
#include <blinktype.h>
#include <Modbus.h>

extern multi_led_state_t multi_led_state;

extern Logger *g_pLogger;
extern BlinkConfig *g_pBlinkConfig;
extern Indicator *g_pIndicators;

void modbus_led_initialize(void)
{
#ifdef ENABLE_MODBUS_LED	
	g_pIndicators[IndicatorType::LedModbusTx].SetPin(PIN_LED_MODBUS_TX);
	g_pIndicators[IndicatorType::LedModbusRx].SetPin(PIN_LED_MODBUS_RX);
	g_pIndicators[IndicatorType::LedModbusErr].SetPin(PIN_LED_MODBUS_ERR);
#endif
}

void modbus_led_set_state(uint8_t led_state)
{
	switch (led_state)
	{
	case MULTI_LED_STATE_ALL_OFF:
		g_pIndicators[IndicatorType::LedRed].SwitchOff();
		g_pIndicators[IndicatorType::LedBlue].SwitchOff();
		g_pIndicators[IndicatorType::LedGreen].SwitchOff();
		break;

	case MULTI_LED_STATE_GREEN_ON:
		g_pIndicators[IndicatorType::LedRed].SwitchOff();
		g_pIndicators[IndicatorType::LedBlue].SwitchOff();
		g_pIndicators[IndicatorType::LedGreen].SwitchOn();
		break;

	case MULTI_LED_STATE_GREEN_BLINK_SLOW:
		g_pIndicators[IndicatorType::LedRed].SwitchOff();
		g_pIndicators[IndicatorType::LedBlue].SwitchOff();
		g_pIndicators[IndicatorType::LedGreen].Blink(g_pBlinkConfig[BlinkType::Slow]);
		break;

	case MULTI_LED_STATE_GREEN_BLINK_FAST:
		g_pIndicators[IndicatorType::LedRed].SwitchOff();
		g_pIndicators[IndicatorType::LedBlue].SwitchOff();
		g_pIndicators[IndicatorType::LedGreen].Blink(g_pBlinkConfig[BlinkType::Fast]);
		break;

	case MULTI_LED_STATE_RED_ON:
		g_pIndicators[IndicatorType::LedRed].SwitchOn();
		g_pIndicators[IndicatorType::LedBlue].SwitchOff();
		g_pIndicators[IndicatorType::LedGreen].SwitchOff();
		break;

	case MULTI_LED_STATE_RED_BLINK_SLOW:
		g_pIndicators[IndicatorType::LedRed].Blink(g_pBlinkConfig[BlinkType::Slow]);
		g_pIndicators[IndicatorType::LedBlue].SwitchOff();
		g_pIndicators[IndicatorType::LedGreen].SwitchOff();
		break;

	case MULTI_LED_STATE_RED_BLINK_FAST:

		g_pIndicators[IndicatorType::LedRed].Blink(g_pBlinkConfig[BlinkType::Fast]);
		g_pIndicators[IndicatorType::LedBlue].SwitchOff();
		g_pIndicators[IndicatorType::LedGreen].SwitchOff();
		break;

	case MULTI_LED_STATE_BLUE_ON:
		g_pIndicators[IndicatorType::LedRed].SwitchOff();
		g_pIndicators[IndicatorType::LedBlue].SwitchOn();
		g_pIndicators[IndicatorType::LedGreen].SwitchOff();
		break;

	case MULTI_LED_STATE_BLUE_BLINK_SLOW:
		g_pIndicators[IndicatorType::LedRed].SwitchOff();
		g_pIndicators[IndicatorType::LedBlue].Blink(g_pBlinkConfig[BlinkType::Slow]);
		g_pIndicators[IndicatorType::LedGreen].SwitchOff();
		break;

	case MULTI_LED_STATE_BLUE_BLINK_FAST:
		g_pIndicators[IndicatorType::LedRed].SwitchOff();
		g_pIndicators[IndicatorType::LedBlue].Blink(g_pBlinkConfig[BlinkType::Fast]);
		g_pIndicators[IndicatorType::LedGreen].SwitchOff();
		break;
	}
}

void modbus_led_test(void)
{
	while (1)
	{
		g_pIndicators[IndicatorType::LedModbusTx].Blink(g_pBlinkConfig[BlinkType::Once]);
		delay(3000);
		g_pIndicators[IndicatorType::LedModbusRx].Blink(g_pBlinkConfig[BlinkType::Once]);
		delay(3000);
		g_pIndicators[IndicatorType::LedModbusErr].Blink(g_pBlinkConfig[BlinkType::Once]);
		delay(3000);
	}
}
