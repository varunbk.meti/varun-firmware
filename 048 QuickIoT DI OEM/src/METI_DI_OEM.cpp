#include <Arduino.h>
#include "METI_DI_OEM.h"

DI_Wtr_Lvl Neer_H;

void DI_Init(void)
{
    // Pump
    pinMode(Pump1, INPUT);
    pinMode(Pump2, INPUT);
    // OHT
    pinMode(OHT_Percent_100, INPUT);
    pinMode(OHT_Percent_75, INPUT);
    pinMode(OHT_Percent_50, INPUT);
    pinMode(OHT_Percent_25, INPUT);
    pinMode(OHT_Percent_100, INPUT);
    // SUMP
    pinMode(SUMP_Percent_100, INPUT);
    pinMode(SUMP_Percent_50, INPUT);
    pinMode(SUMP_Percent_25, INPUT);
}

uint8_t *Read_and_Process_Digital_Inputs(void)
{
    static uint8_t Wtr_Lvl_Arr[Total_DI_Data_Size];
    Neer_H.Pump_1 = digitalRead(Pump1);
    Neer_H.OHT_Prcnt_100 = digitalRead(OHT_Percent_100);
    Neer_H.OHT_Prcnt_75 = digitalRead(OHT_Percent_75);
    Neer_H.OHT_Prcnt_50 = digitalRead(OHT_Percent_50);
    Neer_H.OHT_Prcnt_25 = digitalRead(OHT_Percent_25);
    Neer_H.Pump_2 = digitalRead(Pump2);
    Neer_H.SUMP_Prcnt_100 = digitalRead(SUMP_Percent_100);
    Neer_H.SUMP_Prcnt_50 = digitalRead(SUMP_Percent_50);
    Neer_H.SUMP_Prcnt_25 = digitalRead(SUMP_Percent_25);

    // Process OHT Water Percentage using Level SensOR
    if (!Neer_H.OHT_Prcnt_100 && !Neer_H.OHT_Prcnt_75 && !Neer_H.OHT_Prcnt_50 && !Neer_H.OHT_Prcnt_25)
    {
        Wtr_Lvl_Arr[0] = 100;
    }
    else if ((!Neer_H.OHT_Prcnt_75 && !Neer_H.OHT_Prcnt_50 && !Neer_H.OHT_Prcnt_25) && ((!Neer_H.OHT_Prcnt_100) == 0))
    {
        Wtr_Lvl_Arr[0] = 75;
    }
    else if ((!Neer_H.OHT_Prcnt_50 && !Neer_H.OHT_Prcnt_25) && ((!Neer_H.OHT_Prcnt_100 && Neer_H.OHT_Prcnt_75) == 0))
    {
        Wtr_Lvl_Arr[0] = 50;
    }
    else if ((!Neer_H.OHT_Prcnt_25) && ((!Neer_H.OHT_Prcnt_100 && !Neer_H.OHT_Prcnt_75 && !Neer_H.OHT_Prcnt_50) == 0))
    {
        Wtr_Lvl_Arr[0] = 25;
    }
    else
    {
        Wtr_Lvl_Arr[0] = 0;
    }

    // Process Sumo Water Percentage using Level SensOR
    if (!Neer_H.SUMP_Prcnt_100 && !Neer_H.SUMP_Prcnt_50 && !Neer_H.SUMP_Prcnt_25)
    {
        Wtr_Lvl_Arr[2] = 100;
    }
    else if ((!Neer_H.SUMP_Prcnt_50 && !Neer_H.SUMP_Prcnt_25) && ((!Neer_H.SUMP_Prcnt_100) == 0))
    {
        Wtr_Lvl_Arr[2] = 50;
    }
    else if ((!Neer_H.SUMP_Prcnt_25) && ((!Neer_H.SUMP_Prcnt_100 && !Neer_H.SUMP_Prcnt_50) == 0))
    {
        Wtr_Lvl_Arr[2] = 25;
    }
    else
    {
        Wtr_Lvl_Arr[2] = 0;
    }

    Wtr_Lvl_Arr[1] = !Neer_H.Pump_1;
    Wtr_Lvl_Arr[3] = !Neer_H.Pump_2;

    return Wtr_Lvl_Arr;
}