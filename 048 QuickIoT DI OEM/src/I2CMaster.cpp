/*******************************************************************************
* File Name: I2CMaster.cpp
*
* Version: 1.0
*
* Description:
* This is source file for i2c master functions All the functions related to
* i2c master are here.
*
* Written By:
* Yogesh M Iggalore
*
*
********************************************************************************
* Copyright (2021-22) , METI
********************************************************************************/

#include <Arduino.h>
#include <I2CMaster.h>
#include <Wire.h>
#include <I2CCore.h>
#include <config_reader.h>
#include <utils.h>
#include <logger.h>

// Constructors ////////////////////////////////////////////////////////////////
I2CMaster::I2CMaster() {}

extern Logger *g_pLogger;

double g_final_val[SCALING_NL_MAX_COLS];

#define IIR_HISTORY_PERCENT 50

#define PREV_SAMPLE_MAX 8
uint16_t u16_prev_samples[PREV_SAMPLE_MAX];
uint8_t u8_prev_sample_ctr = 0;

uint16_t temp;

bool calibrate_sensor = false;

void I2CMaster::Start(void)
{
    I2CC.Start();
}

void I2CMaster::Read_From_Device(void)
{
    uint8_t aui8Buffer[8] = {0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF};
    uint16 u16_present_val;
    uint32 u32_present_val;
    static uint16 u16_previous_val = 0;
    static bool first_run = true;
    static uint8_t logger_ctr = 10;

    uint32_t u32_present_val_avg = 0;

    for (int8_t ctr = 0; ctr < 10; ctr++)
    {
        I2CC.Read_Multiple_Bytes(I2C_ADDRESS, 0, 8, aui8Buffer);

        u16_present_val = aui8Buffer[1];
        u16_present_val <<= 8;
        u16_present_val |= aui8Buffer[2];

        u32_present_val_avg += u16_present_val;
    }

    ui8_dacValue = aui8Buffer[3];
    // ui16_scaledValue = aui8Buffer[0];
    // ui16_scaledValue <<= 8;
    // ui16_scaledValue |= aui8Buffer[1];

    // u16_present_val = aui8Buffer[1];
    // u16_present_val <<= 8;
    // u16_present_val |= aui8Buffer[2];

    u16_present_val = u32_present_val_avg / 10;

    if (first_run == true)
    {
        first_run = false;
        u16_previous_val = u16_present_val;
    }

    bool value_stable = true;
    for (uint8_t i = 0; i < PREV_SAMPLE_MAX; i++)
    {
        if (u16_present_val != u16_prev_samples[i])
        {
            value_stable = false;
        }
    }

    //if last 4 samples are same, then consider value is stable and use present value
    if (value_stable == true)
    {
        ui16_voltage = u16_present_val;
    }
    else
    {
        //Apply IIR filter
        u32_present_val = (float)((u16_previous_val * IIR_HISTORY_PERCENT) + (u16_present_val * (100 - IIR_HISTORY_PERCENT))) / 100 + 0.5;
        ui16_voltage = u32_present_val;
    }
    u16_previous_val = ui16_voltage;

    i16_adcCount = aui8Buffer[6];
    i16_adcCount <<= 8;
    i16_adcCount |= aui8Buffer[7];

    u8_prev_sample_ctr++;
    if (u8_prev_sample_ctr >= PREV_SAMPLE_MAX)
    {
        u8_prev_sample_ctr = 0;
    }
    u16_prev_samples[u8_prev_sample_ctr] = u16_present_val;

    temp = u16_present_val;

    if (logger_ctr-- == 0)
    {
        logger_ctr = 10;
        g_pLogger->Write(LogLevel::Info, "Read I2C", "u16_present_val:%d dacVal:%d Resistance:%d", u16_present_val, ui8_dacValue, Get_Resistance_Value());
    }
}

int16_t I2CMaster::Get_ADCRawCount(void)
{
    return temp;
}

double I2CMaster::Get_milliVolts(void)
{
    return ui16_voltage;
}

double I2CMaster::Get_ScaledCurrent(uint8_t decimals)
{
    int32_t sourceZero = sensorScaling_doc["calibratedZero"].as<int32_t>();
    int32_t sourceSpan = sensorScaling_doc["calibratedSpan"].as<int32_t>();
    uint16_t destZero = 0;
    uint16_t destSpan = 2000;

    double scaledCurrent = (int32_t)scaleValue(ui16_voltage, sourceZero, sourceSpan, destZero, destSpan);

    // If scaledCurrent is less than zero, but
    // if (scaledCurrent < (sensorScaling_doc["calibratedZero"].as<int32_t>() - 50) )
    // {
    //     // sensor open condition
    //     scaledCurrent = SENSOR_OPEN_STATE;
    // }
    // else
    // {
    if (scaledCurrent < destZero)
    {
        // if sensor value less than calibrated value, set it to calibrated zero
        scaledCurrent = destZero;
    }
    else if (scaledCurrent > destSpan)
    {
        // if sensor value greater than calibrated value, set it to calibrated span
        scaledCurrent = destSpan;
    }

    scaledCurrent = scaledCurrent / pow(10, decimals);
    scaledCurrent = RoundOff(scaledCurrent, decimals);
    // }

    return scaledCurrent;
}

double I2CMaster::Get_ScaledValue(uint8_t decimals)
{
    int32_t sourceZero = 0;
    int32_t sourceSpan = 2000;
    int32_t destZero = LEVEL_MIN;
    int32_t destSpan = LEVEL_MAX;
    double scaledFinal = 0;

    double scaledCurrent = Get_ScaledCurrent(0);
    if (scaledCurrent == SENSOR_OPEN_STATE)
    {
        scaledFinal = SENSOR_OPEN_STATE;
    }
    else
    {
        scaledFinal = (double)scaleValue(scaledCurrent, sourceZero, sourceSpan, destZero, destSpan);
        scaledFinal = scaledFinal / pow(10, decimals);
        scaledFinal = RoundOff(scaledFinal, decimals);
    }

    return scaledFinal;
}

// double I2CMaster::Get_ScaledValue_NL(uint8_t decimals)
// {
//     int32_t sourceZero;
//     int32_t sourceSpan;
//     int32_t destZero;
//     int32_t destSpan;

//     double scaledFinal = 0;

//     uint8_t g_nl_enabled = 1; //Narayan - remove this...
//     int32_t scaledSource = I2CM.Get_ScaledValue(0);

//     if (scaledSource == SENSOR_OPEN_STATE)
//     {
//         scaledFinal = SENSOR_OPEN_VAL;
//     }
//     else
//     {
//         if (g_nl_enabled == 1)
//         {
//             // if index is matching just fetch the volume data
//             int16_t index = array_getIndex(&scaling_nl_value[0], 0, scaling_nl_max_rows_set - 1, scaledSource);

//             // If index is not matching, do a interpolation/scaling....
//             if (index == -1)
//             {
//                 // get the least greater index
//                 index = array_getLeastGreaterIndex(&scaling_nl_value[0], 0, scaling_nl_max_rows_set - 1, scaledSource);

//                 // Zero values will be at index -1, Span will be at index
//                 sourceZero = scaling_nl_value[index - 1];
//                 sourceSpan = scaling_nl_value[index];
//                 destZero = scaling_nl_volume[index - 1];
//                 destSpan = scaling_nl_volume[index];

//                 // Scale the input
//                 scaledFinal = (double)scaleValue(scaledSource, sourceZero, sourceSpan, destZero, destSpan);
//                 // g_pLogger->Write(LogLevel::Debug, "NonLinear - Scaled", "index: %d, scaling_nl_max_rows_set:%d, scaledSource: %d, scaledFinal: %d", index, scaling_nl_max_rows_set, scaledSource, scaledFinal);
//             }
//             else
//             {
//                 scaledFinal = (double)scaling_nl_volume[index];
//                 // g_pLogger->Write(LogLevel::Debug, "NonLinear - Fetched", "index:%d, scaledSource: %d, scaledFinal : %d", index, scaledSource, scaledFinal);
//             }

//             scaledFinal = scaledFinal / pow(10, decimals);
//             scaledFinal = RoundOff(scaledFinal, decimals);
//         }
//     }

//     return scaledFinal;
// }

void I2CMaster::Update_ScaledValues(void)
{
    uint16_t i;
    int32_t sourceZero;
    int32_t sourceSpan;
    int32_t destZero;
    int32_t destSpan;

    double scaledFinal = 0;

    int32_t scaledSource = I2CM.Get_ScaledValue(0);

    if (scaledSource == SENSOR_OPEN_STATE)
    {
        scaledFinal = SENSOR_OPEN_VAL;
    }
    else
    {
        // if index is matching just fetch the volume data
        int16_t index = array_getIndex(scaling_array, 0, scaling_nl_max_rows_set - 1, scaledSource);

        // If index is not matching, do a interpolation/scaling....
        if (index == -1)
        {
            // get the least greater index
            index = array_getLeastGreaterIndex(scaling_array, 0, scaling_nl_max_rows_set - 1, scaledSource);

            for (i = 1; i < 6; i++)
            {
                // Zero values will be at index -1, Span will be at index
                sourceZero = scaling_array[index - 1][0];
                sourceSpan = scaling_array[index][0];
                destZero = scaling_array[index - 1][i];
                destSpan = scaling_array[index][i];

                // Scale the input
                g_final_val[i] = (double)scaleValue(scaledSource, sourceZero, sourceSpan, destZero, destSpan);
                // g_pLogger->Write(LogLevel::Debug, "NonLinear - Scaled", "index: %d, scaling_nl_max_rows_set:%d, scaledSource: %d, scaledFinal: %d", index, scaling_nl_max_rows_set, scaledSource, scaledFinal);
            }
        }
        else
        {
            for (i = 1; i < 6; i++)
            {
                g_final_val[i] = (double)scaling_array[index][i];
            }
            // g_pLogger->Write(LogLevel::Debug, "NonLinear - Fetched", "index:%d, scaledSource: %d, scaledFinal : %d", index, scaledSource, scaledFinal);
        }

        // scaledFinal = scaledFinal / pow(10, decimals);
        // scaledFinal = RoundOff(scaledFinal, decimals);
    }

    // return scaledFinal;
}

double I2CMaster::Get_FinalValue(uint8_t value_idx, uint8_t decimals)
{
    double finalVal = g_final_val[value_idx];

    if (decimals > 0)
    {
        finalVal = finalVal / pow(10, decimals);
        finalVal = RoundOff(finalVal, decimals);
    }

    return finalVal;
}

void I2CMaster::Calibrate_IDAC(void)
{
    I2CC.Write_Single_Byte(I2C_ADDRESS, 0, 1);
}

double I2CMaster::Get_IDAC_Current(void)
{
    return (ui8_dacValue * IDAC_CURRENT_STEP_SIZE);
}

uint16_t I2CMaster::Get_Resistance_Value(void)
{
    double calc_res = 0;

    calc_res = ui16_voltage / Get_IDAC_Current() * 1000;
    return (uint16_t)calc_res;
}


// Preinstantiate Objects //////////////////////////////////////////////////////
#if !defined(NO_GLOBAL_INSTANCES) && !defined(NO_GLOBAL_I2CMASTER)
I2CMaster I2CM;
#endif
