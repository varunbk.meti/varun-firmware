#include<dataservice.h>

using namespace std;

void DataService::Initialize(Logger* p_pLogger)
{
	if (p_pLogger == NULL) {
		this->m_pLogger->DisableConsole();
	}
	else {
		if (this->m_bDeleteLogger) {
			delete this->m_pLogger;
			this->m_bDeleteLogger = false;
		}
		this->m_pLogger = p_pLogger;
	}
}

DataService::DataService()
{
	this->m_bDeleteLogger = true;
	this->m_pLogger = new Logger();
}

DataService::~DataService()
{
	if (this->m_bDeleteLogger) {
		delete this->m_pLogger;
	}
	this->m_bDeleteLogger = false;
	this->m_pLogger = NULL;
}

string DataService::ToString()
{
	return "DataService";
}

