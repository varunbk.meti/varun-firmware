/*******************************************************************************
 *File Name: ModbusSlave.cpp
 *
 * Version: 1.0
 *
 * Description:
 * In this source file for modbus slave application
 *
 *
 * Owner:
 * Yogesh M Iggalore
 *
 ********************************************************************************
 * Copyright (2020-21)
 *******************************************************************************/

#include <Arduino.h>
#include <ModbusRTUSlave.h>
#include <Modbus.h>
#include <logger.h>
#include <display.h>

modbus_struct modbus_registers;

extern Logger *g_pLogger;

// Constructors ////////////////////////////////////////////////////////////////
ModbusRTUSlave::ModbusRTUSlave() {}

void ModbusRTUSlave::uart_read(void)
{
    static uint8_t ctr = 0;
    if (MODBUS_SERIAL.available() > 0)
    {
        // MBus.Pin_Control(MODBUS_RX_LED, HIGH);
        while (MODBUS_SERIAL.available())
        {
            input_data[ctr++] = MODBUS_SERIAL.read();

            if (ctr >= INPUT_BUFFER_SIZE)
            {
                ctr = 1;
            }
        }
    }
    else
    {
        if (ctr > 0) // if (ctr > 7)
        {
            memcpy(modbus_cmd, input_data, INPUT_BUFFER_SIZE);
            memset(input_data, 0, INPUT_BUFFER_SIZE);

            // Serial.print("Modbus Rx ");
            // for (uint8_t i = 0; i < ctr; i++)
            // {
            //     Serial1.print(modbus_cmd[i], HEX);
            //     Serial1.print(" ");
            // }
            // Serial1.println("");

            modbus_rx_complete = true;
            modbus_process_cmd(ctr);
            // MBus.Pin_Control(MODBUS_RX_LED, LOW);
            ctr = 0;
        }
    }
}

void ModbusRTUSlave::modbus_process_cmd(uint8_t rx_len)
{
    uint8_t error_code = 0;
    uint16_t ui16CRC;
    uint8_t ui8CRCLow;
    uint8_t ui8CRCHigh;

    if (rx_len != 8)
    { // If invalid command length
        error_code = MODBUS_ERROR_NO_RESPONSE;
        // Serial1.println("MODBUS_ERROR_NO_RESPONSE");
    }
    else if (modbus_cmd[0] != MBus.tdfModbus.ui8DeviceID)
    { // If slave id does not match
        error_code = MODBUS_ERROR_NO_RESPONSE;
        // Serial1.println("MODBUS_ERROR_NO_RESPONSE");
    }
    else
    { // Calculate crd
        ui16CRC = MBus.CalculateCRC16(modbus_cmd, (rx_len - 2));
        ui8CRCHigh = ui16CRC;
        ui8CRCLow = ui16CRC >> 8;

        if (!(ui8CRCLow == modbus_cmd[rx_len - 2] && ui8CRCHigh == modbus_cmd[rx_len - 1]))
        { // Invalid CRC
            error_code = MODBUS_ERROR_NO_RESPONSE;
            // Serial1.println("MODBUS_ERROR_NO_RESPONSE");
        }
        else if (modbus_cmd[1] != 3)
        {
            error_code = MODBUS_ERR_INVALID_FUNC;
            // Serial1.println("MODBUS_ERR_INVALID_FUNC");
        }
    }

    if ((error_code == 0) || (error_code == MODBUS_ERR_INVALID_FUNC))
    {
        memcpy(MBus.tdfModbus.aui8RxBuffer, modbus_cmd, INPUT_BUFFER_SIZE);
        tdfRTUSlave.ui16RTURXCounter = rx_len;
        tdfRTUSlave.ui16ReadResult = RTU_MODBUSSLAVE_DATA_RECEIVED;
        tdfRTUSlave.ui16ReadResult = tdfRTUSlave.ui16ReadResult << 8;
        tdfRTUSlave.ui16ReadResult = tdfRTUSlave.ui16ReadResult | tdfRTUSlave.ui16RTURXCounter;
    }
    else
    {
        tdfRTUSlave.ui16ReadResult = RTU_MODBUSSLAVE_NO_DATA;
        tdfRTUSlave.ui16ReadResult = tdfRTUSlave.ui16ReadResult << 8;
        tdfRTUSlave.ui16ReadResult = tdfRTUSlave.ui16ReadResult | 0;
    }
}

/******************************************************************************
 * Function Name: Read
 *******************************************************************************
 *
 * Summary:
 *  This function call internally ModbusMap_Is_DataReceived function.
 *  Based on return values, sets ui16ModbusMapReadResult
 *
 *
 *
 * Parameters:
 *  None
 *
 * Return:
 *  None
 *
 *******************************************************************************/
void ModbusRTUSlave::Update(void)
{
    // Read();
    uart_read();
    Process();
    Write();
}

void ModbusRTUSlave::Read(void)
{
    uint8_t ui8Response;
    uint16_t ui16Result;

    ui16Result = Is_DataReceived();
    ui8Response = ui16Result >> 8;

    if (ui8Response == 1)
    {
        tdfRTUSlave.ui16ReadResult = RTU_MODBUSSLAVE_DATA_RECEIVED;
        tdfRTUSlave.ui16ReadResult = tdfRTUSlave.ui16ReadResult << 8;
        ui8Response = ui16Result;
        tdfRTUSlave.ui16ReadResult = tdfRTUSlave.ui16ReadResult | ui8Response;
    }
    else
    {
        tdfRTUSlave.ui16ReadResult = RTU_MODBUSSLAVE_NO_DATA;
        tdfRTUSlave.ui16ReadResult = tdfRTUSlave.ui16ReadResult << 8;
        ui8Response = 0;
        tdfRTUSlave.ui16ReadResult = tdfRTUSlave.ui16ReadResult | ui8Response;
    }
}

/******************************************************************************
 * Function Name: Process
 *******************************************************************************
 *
 * Summary:
 *  This function checks for APP_MODBUSMAP_DATA_RECEIVED and process the command.
 *  Based on return values, sets ui16ModbusMapProcessResult
 *
 *
 *
 * Parameters:
 *  None
 *
 * Return:
 *  None
 *
 *******************************************************************************/

void ModbusRTUSlave::Process(void)
{
    uint8_t ui8Response;
    uint8_t ui8BufferSize = 0;
    uint16_t ui16Response = 0;
    uint8_t ui8CRCFirst = 0;
    uint8_t ui8CRCSecond = 0;

    ui8Response = tdfRTUSlave.ui16ReadResult >> 8;
    if (ui8Response == RTU_MODBUSSLAVE_DATA_RECEIVED)
    {
        ui8BufferSize = tdfRTUSlave.ui16ReadResult; // get buffer size in lower 8 bits
        ui16Response = MBus.CalculateCRC16(MBus.tdfModbus.aui8RxBuffer, ui8BufferSize - 2);
        ui8CRCFirst = ui16Response >> 8;
        ui8CRCSecond = ui16Response;

        /* Check for CRC */
        if ((MBus.tdfModbus.aui8RxBuffer[ui8BufferSize - 2] == ui8CRCFirst) & ((MBus.tdfModbus.aui8RxBuffer[ui8BufferSize - 1] == ui8CRCSecond)))
        {
            Process_Modbus_Parameters();
        }
    }
}

/******************************************************************************
 * Function Name: ModbusMap_Write
 *******************************************************************************
 *
 * Summary:
 *  This function write the modbus map processed data.
 *  number of byte sends depends on variable ui16ModbusSlaveMapResult's lower byte
 *
 *
 *
 * Parameters:
 *  None
 *
 * Return:
 *  None
 *
 *******************************************************************************/

void ModbusRTUSlave::Write(void)
{
    uint8_t ui8ByteCount = 0;
    uint8_t ui8LoopCounter = 0;

    ui8ByteCount = tdfRTUSlave.ui16ProcessResult;
    if (ui8ByteCount > 0)
    { // more than 0 means send response
        // Enable modbusTXE, TXELED

        MBus.Pin_Control(MODBUS_ENABLE, HIGH); // Varun Changed to HIGH
        // MBus.Pin_Control(MODBUS_TX_LED, HIGH);

        // Serial1.print("Modbus Tx: ");
        for (ui8LoopCounter = 0; ui8LoopCounter < ui8ByteCount; ui8LoopCounter++)
        {
            MODBUS_SERIAL.write(MBus.tdfModbus.aui8TxBuffer[ui8LoopCounter]);
            while (MODBUS_SERIAL.availableForWrite() < 127)
                ;

            /* print debug */
            // Serial1.print(MBus.tdfModbus.aui8TxBuffer[ui8LoopCounter], HEX);
            // Serial1.print(" ");
        }

        // Serial1.println(" ");

        // clear all set values....
        tdfRTUSlave.ui16ReadResult = 0;
        tdfRTUSlave.ui16ProcessResult = 0;
        tdfRTUSlave.ui16WriteResult = 0;

        for (ui8LoopCounter = 0; ui8LoopCounter < MRTUS.tdfRTUSlave.ui16RTURXCounter; ui8LoopCounter++)
        {
            MBus.tdfModbus.aui8RxBuffer[ui8LoopCounter] = 0;
        }

        MRTUS.tdfRTUSlave.ui16RTURXCounter = 0;

        // Yogesh... Wait till all bytes and bits in the buffer are transmitted in Physcical layer..
        //  Narayan - check this....
        delay(10);

        MBus.Pin_Control(MODBUS_ENABLE, LOW); // Varun Changed to LOW
        // MBus.Pin_Control(MODBUS_TX_LED, LOW);
    }
}

void ModbusRTUSlave::Read_Data_From_Serial(void)
{
    uint8_t ui8BufferSize;
    uint8_t ui8ReadByte;

    ui8BufferSize = MODBUS_SERIAL.available();
    while (ui8BufferSize)
    {
        ui8ReadByte = MODBUS_SERIAL.read();
        MBus.tdfModbus.aui8RxBuffer[MRTUS.tdfRTUSlave.ui16RTURXCounter++] = ui8ReadByte;
        if (MRTUS.tdfRTUSlave.ui16RTURXCounter > MODBUS_RX_BUFFER_SIZE)
        {
            MRTUS.tdfRTUSlave.ui16RTURXCounter = 0;
        }

        // Serial1.println(ui8ReadByte, HEX);
        ui8BufferSize--;
        // Serial1.print("1 ");
        // Serial1.println(MBus.tdfModbus.aui8RxBuffer[MRTUS.tdfRTUSlave.ui16RTURXCounter -1],HEX);
    }
}

/******************************************************************************
 * Function Name: ModbusMap_Is_DataReceived
 *******************************************************************************
 *
 * Summary:
 *  This function read any query request from modbus master.
 *  The function checks for any date byte received in modbus_uart rx buffer
 *  sets ui16ModbusSlaveReadResult veriable
 *
 *
 * Parameters:
 *  None
 *
 * Return:
 *  Returns read result and number of bytes received
 *
 *******************************************************************************/

uint16_t ModbusRTUSlave::Is_DataReceived(void)
{
    uint8_t ui8Response = 0;
    uint16_t ui16Result = 0;
    uint8_t ui8ExpectedBytes = 0;
    uint16_t ui16DelayTime = 0;
    uint16_t ui16Response = 0;
    uint8_t ui8CRCFirst = 0;
    uint8_t ui8CRCSecond = 0;
    uint8_t ui8LoopCounter = 0;

    Read_Data_From_Serial();

    if (MRTUS.tdfRTUSlave.ui16RTURXCounter > 0)
    {
        /* check for device ID */
        if (MBus.tdfModbus.aui8RxBuffer[0] != MBus.tdfModbus.ui8DeviceID)
        {
            // Serial1.println("Slave id error");
            //  for (ui8LoopCounter = 0; ui8LoopCounter < MRTUS.tdfRTUSlave.ui16RTURXCounter; ui8LoopCounter++)
            //  {
            //      Serial1.print(MBus.tdfModbus.aui8RxBuffer[ui8LoopCounter], HEX);
            //      Serial1.print(" ");
            //  }
            //  Serial1.println(" ");
            for (ui8LoopCounter = 0; ui8LoopCounter < MRTUS.tdfRTUSlave.ui16RTURXCounter; ui8LoopCounter++)
            {
                MBus.tdfModbus.aui8RxBuffer[ui8LoopCounter] = 0;
            }
            MRTUS.tdfRTUSlave.ui16RTURXCounter = 0;
        }
        else
        {
            /* Check for bytes received */
            if (MRTUS.tdfRTUSlave.ui16RTURXCounter < 2)
            {
                /* wait for 2 more bytes */
                ui16DelayTime = MBus.Get_TimePerByte(MBus.tdfModbus.ui8Baudrate) * 4;
                delay(ui16DelayTime);
            }

            Read_Data_From_Serial();
            /* Check for function code */
            if (MRTUS.tdfRTUSlave.ui16RTURXCounter >= 2)
            {

                /* check for fun code errror */
                if (MBus.Verify_FunctionCode(MBus.tdfModbus.aui8RxBuffer[1]) == 0)
                {
                    // Serial1.println("fun error");
                    //  Serial1.print("Modbus Rx: ");
                    //  for (ui8LoopCounter = 0; ui8LoopCounter < MRTUS.tdfRTUSlave.ui16RTURXCounter; ui8LoopCounter++)
                    //  {
                    //      Serial1.print(MBus.tdfModbus.aui8RxBuffer[ui8LoopCounter], HEX);
                    //      Serial1.print(" ");
                    //  }
                    //  Serial1.println(" ");
                    for (ui8LoopCounter = 0; ui8LoopCounter < MRTUS.tdfRTUSlave.ui16RTURXCounter; ui8LoopCounter++)
                    {
                        MBus.tdfModbus.aui8RxBuffer[ui8LoopCounter] = 0;
                    }
                    MRTUS.tdfRTUSlave.ui16RTURXCounter = 0;
                }
                /* Check for invalid function code */
                else if (MBus.Verify_FunctionCode(MBus.tdfModbus.aui8RxBuffer[1]) == 2)
                {
                    if (MRTUS.tdfRTUSlave.ui16RTURXCounter < 5)
                    {
                        /* wait for 3 more bytes */
                        ui16DelayTime = MBus.Get_TimePerByte(MBus.tdfModbus.ui8Baudrate) * 6;
                        delay(ui16DelayTime);
                        Read_Data_From_Serial();
                    }

                    if (MRTUS.tdfRTUSlave.ui16RTURXCounter >= 5)
                    {
                        ui16Response = MBus.CalculateCRC16(MBus.tdfModbus.aui8RxBuffer, MRTUS.tdfRTUSlave.ui16RTURXCounter - 2);
                        ui8CRCFirst = ui16Response >> 8;
                        ui8CRCSecond = ui16Response;
                        // Serial1.println("invalid fun code:");
                        //  Serial1.print("Modbus Rx: ");
                        //  for (ui8LoopCounter = 0; ui8LoopCounter < MRTUS.tdfRTUSlave.ui16RTURXCounter; ui8LoopCounter++)
                        //  {
                        //      Serial1.print(MBus.tdfModbus.aui8RxBuffer[ui8LoopCounter], HEX);
                        //      Serial1.print(" ");
                        //  }
                        //  Serial1.println(" ");

                        if ((MBus.tdfModbus.aui8RxBuffer[3] == ui8CRCFirst) & ((MBus.tdfModbus.aui8RxBuffer[4] == ui8CRCSecond)))
                        {
                            ui8Response = true;
                            ui16Result = ui8Response;
                            ui16Result = ui16Result << 8;
                            ui16Result = ui16Result | MRTUS.tdfRTUSlave.ui16RTURXCounter;
                            return ui16Result;
                        }
                    }
                    else
                    {
                        // Serial1.println("less modbus bytes");
                        //  Serial1.print("Modbus Rx: ");
                        //  for (ui8LoopCounter = 0; ui8LoopCounter < MRTUS.tdfRTUSlave.ui16RTURXCounter; ui8LoopCounter++)
                        //  {
                        //      Serial1.print(MBus.tdfModbus.aui8RxBuffer[ui8LoopCounter], HEX);
                        //      Serial1.print(" ");
                        //  }
                        //  Serial1.println(" ");
                        for (ui8LoopCounter = 0; ui8LoopCounter < MRTUS.tdfRTUSlave.ui16RTURXCounter; ui8LoopCounter++)
                        {
                            MBus.tdfModbus.aui8RxBuffer[ui8LoopCounter] = 0;
                        }
                        MRTUS.tdfRTUSlave.ui16RTURXCounter = 0;
                    }
                } // Valid function code wait for remaining bytes
                else if (MBus.Verify_FunctionCode(MBus.tdfModbus.aui8RxBuffer[1]) == 1)
                {
                    if (MRTUS.tdfRTUSlave.ui16RTURXCounter < 8)
                    {
                        /* wait for minimum 4 more bytes */
                        ui16DelayTime = MBus.Get_TimePerByte(MBus.tdfModbus.ui8Baudrate) * 6;
                        delay(ui16DelayTime);
                        Read_Data_From_Serial();
                    }

                    bool bCheckForMoreBytes = 0;
                    if (MBus.tdfModbus.aui8RxBuffer[1] == MODBUS_FUNC_CODE_FORCE_MULTIPLE_COIL)
                    {
                        bCheckForMoreBytes = 1;
                    }

                    if (MBus.tdfModbus.aui8RxBuffer[1] == MODBUS_FUNC_CODE_PRESET_MULTIPLE_REG)
                    {
                        bCheckForMoreBytes = 1;
                    }

                    // multiple register and coil
                    if (bCheckForMoreBytes == 1)
                    {
                        ui8ExpectedBytes = MBus.tdfModbus.aui8RxBuffer[6] + 9 - MRTUS.tdfRTUSlave.ui16RTURXCounter;

                        ui16DelayTime = MBus.Get_TimePerByte(MBus.tdfModbus.ui8Baudrate) * (ui8ExpectedBytes + 2);
                        delay(ui16DelayTime);
                        Read_Data_From_Serial();
                    }

                    // Serial1.print("Modbus Rx: ");
                    // for (ui8LoopCounter = 0; ui8LoopCounter < MRTUS.tdfRTUSlave.ui16RTURXCounter; ui8LoopCounter++)
                    // {
                    //     Serial1.print(MBus.tdfModbus.aui8RxBuffer[ui8LoopCounter], HEX);
                    //     Serial1.print(" ");
                    // }
                    // Serial1.println(" ");

                    ui16Response = MBus.CalculateCRC16(MBus.tdfModbus.aui8RxBuffer, MRTUS.tdfRTUSlave.ui16RTURXCounter - 2);
                    ui8CRCSecond = ui16Response;
                    ui8CRCFirst = ui16Response >> 8;

                    if ((MBus.tdfModbus.aui8RxBuffer[MRTUS.tdfRTUSlave.ui16RTURXCounter - 2] == ui8CRCFirst) & ((MBus.tdfModbus.aui8RxBuffer[MRTUS.tdfRTUSlave.ui16RTURXCounter - 1] == ui8CRCSecond)))
                    {
                        ui8Response = true;
                        ui16Result = ui8Response;
                        ui16Result = ui16Result << 8;
                        ui16Result = ui16Result | MRTUS.tdfRTUSlave.ui16RTURXCounter;
                        return ui16Result;
                    }
                }
                else
                {
                    // Serial1.println("Dont know what to do");
                    // dont knw what to do...
                    for (ui8LoopCounter = 0; ui8LoopCounter < MRTUS.tdfRTUSlave.ui16RTURXCounter; ui8LoopCounter++)
                    {
                        MBus.tdfModbus.aui8RxBuffer[ui8LoopCounter] = 0;
                    }
                    MRTUS.tdfRTUSlave.ui16RTURXCounter = 0;
                }
            }
            else
            {
                // Serial1.println("Dont know what to do");
            }
        }
    }

    ui16Result = ui8Response;
    ui16Result = ui16Result << 8;
    ui16Result = ui16Result | MRTUS.tdfRTUSlave.ui16RTURXCounter;

    return ui16Result;
}

void ModbusRTUSlave::Test(void)
{
    uint16_t ui16CRC;

    MBus.tdfModbus.aui8TxBuffer[0] = 0x01;
    MBus.tdfModbus.aui8TxBuffer[1] = 0x03;
    MBus.tdfModbus.aui8TxBuffer[2] = 0x00;
    MBus.tdfModbus.aui8TxBuffer[3] = 0x00;
    MBus.tdfModbus.aui8TxBuffer[4] = 0x00;
    MBus.tdfModbus.aui8TxBuffer[5] = 0x0A;

    ui16CRC = MBus.CalculateCRC16(MBus.tdfModbus.aui8TxBuffer, 6);
    MBus.tdfModbus.aui8TxBuffer[6] = ui16CRC >> 8;
    MBus.tdfModbus.aui8TxBuffer[7] = ui16CRC;

    tdfRTUSlave.ui16ProcessResult = 8;

    Write();
}

/******************************************************************************
 * Function Name: ModbusMap_Exception_Handling
 *******************************************************************************
 *
 * Summary:
 *  This function prepare aui8ModbusMapTxBuffer based on exception code.
 *
 *
 *
 *
 * Parameters:
 *  ui8ErrorCode: based on error code contruct the array to be sent...
 *
 * Return:
 *  None
 *
 *******************************************************************************/

void ModbusRTUSlave::Exception_Handling(uint8_t ui8ModbusSlaveId, uint8_t ui8ErrorCode, uint8_t ui8ErrorValue)
{
    uint16_t ui16Result = 0;
    // uint8_t ui8Value = 0;
    uint8_t ui8LoopCounter = 0;

    for (ui8LoopCounter = 0; ui8LoopCounter < MRTUS.tdfRTUSlave.ui16RTURXCounter; ui8LoopCounter++)
    {
        MBus.tdfModbus.aui8TxBuffer[ui8LoopCounter] = 0;
    }

    MBus.tdfModbus.aui8TxBuffer[0] = ui8ModbusSlaveId;
    MBus.tdfModbus.aui8TxBuffer[1] = ui8ErrorCode;
    MBus.tdfModbus.aui8TxBuffer[2] = ui8ErrorValue;

    ui16Result = MBus.CalculateCRC16(MBus.tdfModbus.aui8TxBuffer, 3);
    MBus.tdfModbus.aui8TxBuffer[3] = ui16Result >> 8;
    MBus.tdfModbus.aui8TxBuffer[4] = ui16Result;
}

/******************************************************************************
 * Function Name: ModbusMap_Process_ErrorCode
 *******************************************************************************
 *
 * Summary:
 *  This function process the error code
 *
 *
 *
 *
 * Parameters:
 *  ui8FunctionError, ui8ErrorCode
 *
 * Return:
 *  None
 *
 *******************************************************************************/
void ModbusRTUSlave::Process_ErrorCode(uint8_t ui8ModbusSlaveId, uint8_t ui8FunctionError, uint8_t ui8ErrorCode)
{
    MRTUS.tdfRTUSlave.ui16ProcessResult = MODBUS_ERR_INVALID_RESPONSE_LEN;
    MRTUS.tdfRTUSlave.ui16ProcessResult = MRTUS.tdfRTUSlave.ui16ProcessResult << 8;
    MRTUS.tdfRTUSlave.ui16ProcessResult = MRTUS.tdfRTUSlave.ui16ProcessResult | 5; // number of bytes to send...
    Exception_Handling(ui8ModbusSlaveId, ui8FunctionError, ui8ErrorCode);
}

/******************************************************************************
 * Function Name: ModbusMap_Process_Modbus_Parameters
 *******************************************************************************
 *
 * Summary:
 *  This function process Modbus config parameters
 *
 *
 *
 * Parameters:
 *  None
 *
 * Return:
 *  None
 *
 *******************************************************************************/
void ModbusRTUSlave::Process_Modbus_Parameters(void)
{
    switch (MBus.tdfModbus.aui8RxBuffer[1])
    {
    case MODBUS_FUNC_CODE_COIL_REG:
        Process_Modbus_Coils();
        break;
    case MODBUS_FUNC_CODE_INPUT_STATUS:
        Process_Modbus_Inputs();
        break;
    case MODBUS_FUNC_CODE_HOLDING_REG:
        Process_Modbus_Holding_Reg();
        break;
    case MODBUS_FUNC_CODE_INPUT_REG:
        Process_Modbus_Input_Reg();
        break;
    case MODBUS_FUNC_CODE_FORCE_SINGLE_COIL:
        Process_Modbus_Force_Coil();
        break;
    case MODBUS_FUNC_CODE_PRESET_SINGLE_REG:
        Process_Modbus_Preset_Reg();
        break;
    case MODBUS_FUNC_CODE_FORCE_MULTIPLE_COIL:
        Process_Modbus_Force_Mul_Coils();
        break;
    case MODBUS_FUNC_CODE_PRESET_MULTIPLE_REG:
        Process_Modbus_Preset_Mul_Reg();
        break;
    default:
        break;
    }
}

/******************************************************************************
 * Function Name: ModbusMap_Process_Modbus_Coils
 *******************************************************************************
 *
 * Summary:
 *  This function process Modbus coils config parameters
 *
 *
 *
 * Parameters:
 *  None
 *
 * Return:
 *  None
 *
 *******************************************************************************/
void ModbusRTUSlave::Process_Modbus_Coils(void)
{
    Process_ErrorCode(MBus.tdfModbus.ui8DeviceID, 0x81, 0x01); // illegal function code
}

/******************************************************************************
 * Function Name: ModbusMap_Process_Modbus_Inputs
 *******************************************************************************
 *
 * Summary:
 *  This function process Modbus input config parameters
 *
 *
 *
 * Parameters:
 *  None
 *
 * Return:
 *  None
 *
 *******************************************************************************/
void ModbusRTUSlave::Process_Modbus_Inputs(void)
{
    Process_ErrorCode(MBus.tdfModbus.ui8DeviceID, 0x82, 0x01); // illegal function code
}

/******************************************************************************
 * Function Name: ModbusMap_Process_Modbus_Input_Reg
 *******************************************************************************
 *
 * Summary:
 *  This function process Modbus input reg config parameters
 *
 *
 *
 * Parameters:
 *  None
 *
 * Return:
 *  None
 *
 *******************************************************************************/
void ModbusRTUSlave::Process_Modbus_Input_Reg(void)
{
    Process_ErrorCode(MBus.tdfModbus.ui8DeviceID, 0x84, 0x01); // illegal function code
}

/******************************************************************************
 * Function Name: ModbusMap_Process_Modbus_Force_Coil
 *******************************************************************************
 *
 * Summary:
 *  This function process Modbus force coil config parameters
 *
 *
 *
 * Parameters:
 *  None
 *
 * Return:
 *  None
 *
 *******************************************************************************/
void ModbusRTUSlave::Process_Modbus_Force_Coil(void)
{
    Process_ErrorCode(MBus.tdfModbus.ui8DeviceID, 0x85, 0x01); // illegal function code
}

/******************************************************************************
 * Function Name: ModbusMap_Process_Modbus_Force_Coil
 *******************************************************************************
 *
 * Summary:
 *  This function process Modbus force coil config parameters
 *
 *
 *
 * Parameters:
 *  None
 *
 * Return:
 *  None
 *
 *******************************************************************************/
void ModbusRTUSlave::Process_Modbus_Force_Mul_Coils(void)
{
    Process_ErrorCode(MBus.tdfModbus.ui8DeviceID, 0x8F, 0x01); // illegal function code
}

/******************************************************************************
 * Function Name: ModbusMap_Process_Modbus_Preset_Mul_Reg
 *******************************************************************************
 *
 * Summary:
 *  This function process Modbus multiple reg config parameters
 *
 *
 *
 * Parameters:
 *  None
 *
 * Return:
 *  None
 *
 *******************************************************************************/
void ModbusRTUSlave::Process_Modbus_Preset_Mul_Reg(void)
{
    Process_ErrorCode(MBus.tdfModbus.ui8DeviceID, 0x90, 0x01); // illegal function code
}

/******************************************************************************
 * Function Name: ModbusMap_Process_Modbus_Holding_Reg
 *******************************************************************************
 *
 * Summary:
 *  This function process Modbus holding reg config parameters
 *
 *
 *
 * Parameters:
 *  None
 *
 * Return:
 *  None
 *
 *******************************************************************************/
void ModbusRTUSlave::Process_Modbus_Holding_Reg(void)
{
    uint16_t ui16StartRegister = 0;
    uint16_t ui16NumberofRegister = 0;

    ui16StartRegister = MBus.tdfModbus.aui8RxBuffer[2];
    ui16StartRegister = ui16StartRegister << 8;
    ui16StartRegister = ui16StartRegister | MBus.tdfModbus.aui8RxBuffer[3];

    ui16NumberofRegister = MBus.tdfModbus.aui8RxBuffer[4];
    ui16NumberofRegister = ui16NumberofRegister << 8;
    ui16NumberofRegister = ui16NumberofRegister | MBus.tdfModbus.aui8RxBuffer[5];

    if ((ui16NumberofRegister > MODBUSSLAVE_MAX_READ_SIZE) | (ui16NumberofRegister == 0))
    {
        Process_ErrorCode(MBus.tdfModbus.ui8DeviceID, 0x83, 0x2);
    }
    else
    {
        if (ui16StartRegister < MODBUSSLAVE_HOLDING_REG_END_INDEX)
        {
            Process_Value_Register(ui16StartRegister, ui16NumberofRegister);
        }
        else
        {
            Process_ErrorCode(MBus.tdfModbus.ui8DeviceID, 0x83, 0x2);
        }
    }
    // Process_ErrorCode(MBus.tdfModbus.ui8DeviceID, 0x83, 0x01); // illegal function code
}

/******************************************************************************
 * Function Name: ModbusMap_Process_Modbus_Preset_Reg
 *******************************************************************************
 *
 * Summary:
 *  This function process Modbus preset reg config parameters
 *
 *
 *
 * Parameters:
 *  None
 *
 * Return:
 *  None
 *
 *******************************************************************************/
void ModbusRTUSlave::Process_Modbus_Preset_Reg(void)
{
    /*uint16 ui16StartRegister=0;
    uint16 ui16RegisterValue=0;
    uint8  ui8LoopCounter=0;

    ui16StartRegister = MBus.tdfModbus.aui8RxBuffer[2];
    ui16StartRegister = ui16StartRegister  << 8;
    ui16StartRegister = ui16StartRegister | MBus.tdfModbus.aui8RxBuffer[3];

    ui16RegisterValue = MBus.tdfModbus.aui8RxBuffer[4];
    ui16RegisterValue = ui16RegisterValue  << 8;
    ui16RegisterValue = ui16RegisterValue | MBus.tdfModbus.aui8RxBuffer[5];

    if((ui16StartRegister < MODBUSMAP_CONFIG_REG_START_INDEX) & (ui16StartRegister > MODBUSMAP_CONFIG_REG_END_INDEX)){
        Process_ErrorCode(ui8ModbusAddress,0x86,0x2);
    }else{
        ui16StartRegister = ui16StartRegister - MODBUSMAP_CONFIG_REG_START_INDEX;

        for(ui8LoopCounter=0;ui8LoopCounter<8;ui8LoopCounter++){
            aui8ModbusMapTxBuffer[ui8LoopCounter] = aui8ModbusMapRxBuffer[ui8LoopCounter];
        }

        ui16ModbusMapProcessResult = DAQ_ERR_NONE;
        ui16ModbusMapProcessResult = ui16ModbusMapProcessResult << 8;
        ui16ModbusMapProcessResult = ui16ModbusMapProcessResult | 8;//number of bytes to send...
    }*/

    Process_ErrorCode(MBus.tdfModbus.ui8DeviceID, 0x86, 0x1);
}

/******************************************************************************
 * Function Name: ModbusMap_Process_Config_Register
 *******************************************************************************
 *
 * Summary:
 *  This function process Modbus config registers
 *
 *
 *
 * Parameters:
 *  ui16StartRegister,ui16NumberofRegister
 *
 * Return:
 *  None
 *
 *******************************************************************************/
/*void ModbusRTUSlave::Process_Config_Register(uint16 ui16StartRegister,uint16 ui16NumberofRegister){
    uint16 ui16LoopCounter=0;
    uint16 ui16LoopEndValue=0;
    uint8 ui8ArrayCounter=0;
    uint16 ui16Result=0;
    uint8 ui8Value;

    ui16LoopCounter = ui16StartRegister - MODBUSMAP_CONFIG_REG_START_INDEX;
    ui16LoopEndValue = ui16LoopCounter + ui16NumberofRegister;

    if( ui16NumberofRegister > PACKET_SIZE){
        Process_ErrorCode(ui8ModbusAddress,0x83,0x2);
    }else{
        if((ui16LoopCounter + ui16NumberofRegister) > PACKET_SIZE){
            Process_ErrorCode(ui8ModbusAddress,0x83,0x2);
        }else{
            aui8ModbusMapTxBuffer[ui8ArrayCounter++] = ui8ModbusAddress;
            aui8ModbusMapTxBuffer[ui8ArrayCounter++] = FUNC_READ_HOLDING_REG;
            aui8ModbusMapTxBuffer[ui8ArrayCounter++] = ui16NumberofRegister * 2;

            for(;ui16LoopCounter<ui16LoopEndValue;ui16LoopCounter++){
                aui8ModbusMapTxBuffer[ui8ArrayCounter++] = tdfConfigPacket.aui16DeviceMainPackets[ui16LoopCounter] >> 8;
                aui8ModbusMapTxBuffer[ui8ArrayCounter++] = tdfConfigPacket.aui16DeviceMainPackets[ui16LoopCounter];
            }

            ui16Result = Modbus_CalculateCRC16(aui8ModbusMapTxBuffer,ui8ArrayCounter);
            aui8ModbusMapTxBuffer[ui8ArrayCounter++] = ui16Result;
            ui8Value = ui16Result >> 8;
            aui8ModbusMapTxBuffer[ui8ArrayCounter++] = ui8Value;

            ui16ModbusMapProcessResult = DAQ_ERR_NONE;
            ui16ModbusMapProcessResult = ui16ModbusMapProcessResult << 8;
            ui16ModbusMapProcessResult = ui16ModbusMapProcessResult | ui8ArrayCounter;//number of bytes to send...
        }

    }

}*/

/******************************************************************************
 * Function Name: ModbusMap_Process_Value_Register
 *******************************************************************************
 *
 * Summary:
 *  This function process Modbus value registers
 *
 *
 *
 * Parameters:
 *  ui16StartRegister,ui16NumberofRegister
 *
 * Return:
 *  None
 *
 *******************************************************************************/
void ModbusRTUSlave::Process_Value_Register(uint16_t ui16StartRegister, uint16_t ui16NumberofRegister)
{
    uint8_t ui8LoopCounter = 0;
    uint8_t ui8ArrayCounter = 0;
    uint16_t ui16Result = 0;
    // uint8_t ui8Value = 0;

    if ((ui16StartRegister + ui16NumberofRegister) > MODBUSSLAVE_HOLDING_REG_END_INDEX)
    {
        Process_ErrorCode(MBus.tdfModbus.ui8DeviceID, 0x83, 0x2);
    }
    else
    {

        Update_Holding_Registers();

        MBus.tdfModbus.aui8TxBuffer[ui8ArrayCounter++] = MBus.tdfModbus.ui8DeviceID;
        MBus.tdfModbus.aui8TxBuffer[ui8ArrayCounter++] = MODBUS_FUNC_CODE_HOLDING_REG;
        MBus.tdfModbus.aui8TxBuffer[ui8ArrayCounter++] = ui16NumberofRegister * 2;

        for (ui8LoopCounter = ui16StartRegister; ui8LoopCounter < (ui16NumberofRegister + ui16StartRegister); ui8LoopCounter++)
        {
            MBus.tdfModbus.aui8TxBuffer[ui8ArrayCounter++] = tdfRTUSlave.aui16HoldingReg[ui8LoopCounter] >> 8;
            MBus.tdfModbus.aui8TxBuffer[ui8ArrayCounter++] = tdfRTUSlave.aui16HoldingReg[ui8LoopCounter];
        }

        ui16Result = MBus.CalculateCRC16(MBus.tdfModbus.aui8TxBuffer, ui8ArrayCounter);
        MBus.tdfModbus.aui8TxBuffer[ui8ArrayCounter++] = ui16Result >> 8;
        MBus.tdfModbus.aui8TxBuffer[ui8ArrayCounter++] = ui16Result;

        MRTUS.tdfRTUSlave.ui16ProcessResult = MODBUS_ERR_NONE;
        MRTUS.tdfRTUSlave.ui16ProcessResult = MRTUS.tdfRTUSlave.ui16ProcessResult << 8;
        MRTUS.tdfRTUSlave.ui16ProcessResult = MRTUS.tdfRTUSlave.ui16ProcessResult | ui8ArrayCounter; // number of bytes to send...
    }
}

void ModbusRTUSlave::Update_Holding_Registers(void)
{
    // tdfRTUSlave.aui16HoldingReg[0] = modbus_registers.process_val;
    // tdfRTUSlave.aui16HoldingReg[1] = modbus_registers.relay_on;

    // tdfRTUSlave.aui16HoldingReg[2] = (modbus_registers.value1 >> 16);
    // tdfRTUSlave.aui16HoldingReg[3] = (modbus_registers.value1 & 0x0000FFFF);

    // tdfRTUSlave.aui16HoldingReg[4] = (modbus_registers.value2 >> 16);
    // tdfRTUSlave.aui16HoldingReg[5] = (modbus_registers.value2 & 0x0000FFFF);

    // tdfRTUSlave.aui16HoldingReg[6] = (modbus_registers.value3 >> 16);
    // tdfRTUSlave.aui16HoldingReg[7] = (modbus_registers.value3 & 0x0000FFFF);

    // tdfRTUSlave.aui16HoldingReg[8] = (modbus_registers.value4 >> 16);
    // tdfRTUSlave.aui16HoldingReg[9] = (modbus_registers.value4 & 0x0000FFFF);

    // tdfRTUSlave.aui16HoldingReg[10] = (modbus_registers.value5 >> 16);
    // tdfRTUSlave.aui16HoldingReg[11] = (modbus_registers.value5 & 0x0000FFFF);

    tdfRTUSlave.aui16HoldingReg[0] = modbus_registers.val1;
    tdfRTUSlave.aui16HoldingReg[1] = modbus_registers.val2;

    tdfRTUSlave.aui16HoldingReg[2] = modbus_registers.val3;
    tdfRTUSlave.aui16HoldingReg[3] = modbus_registers.val4;

    tdfRTUSlave.aui16HoldingReg[4] = modbus_registers.val5;
    tdfRTUSlave.aui16HoldingReg[5] = modbus_registers.val6;

    tdfRTUSlave.aui16HoldingReg[6] = modbus_registers.val7;
    tdfRTUSlave.aui16HoldingReg[7] = modbus_registers.val8;

    tdfRTUSlave.aui16HoldingReg[8] = modbus_registers.val9;
}
// Preinstantiate Objects //////////////////////////////////////////////////////
#if !defined(NO_GLOBAL_INSTANCES) && !defined(NO_GLOBAL_MODBUSRTUSLAVE)
ModbusRTUSlave MRTUS;
#endif
