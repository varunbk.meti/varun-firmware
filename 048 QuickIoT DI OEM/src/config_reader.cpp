#include <Arduino.h>
#include <ArduinoJson.h>
#include <SPIFFS.h>
#include <config_reader.h>
#include <main.h>
#include <string.h>

DynamicJsonDocument mqtt_doc(mqtt_capacity + mqtt_capacity_add);
DynamicJsonDocument outCtrl_doc(outCtrl_capacity + outCtrl_capacity_add);
DynamicJsonDocument device_doc(device_capacity + device_capacity_add);
DynamicJsonDocument wifi_doc(wifi_capacity + wifi_capacity_add);
DynamicJsonDocument sensorScaling_doc(sensorScaling_capacity + sensorScaling_capacity_add);

DynamicJsonDocument params_doc(params_capacity + params_capacity_add);
DynamicJsonDocument sensorValues_doc(sensorValues_capacity + sensorValues_capacity_add);
DynamicJsonDocument modbusDriver_doc(modbusDriver_capacity + modbusDriver_capacity_add);

int32_t scaling_nl_value[SCALING_NL_MAX_ROWS];
int32_t scaling_nl_volume[SCALING_NL_MAX_ROWS];
int32_t scaling_array[SCALING_NL_MAX_ROWS][SCALING_NL_MAX_COLS];

int16_t scaling_nl_max_rows_set = 0;
uint8_t g_nl_enabled = 0;

void read_mqtt_file(void)
{
    String fileData;
    String path = "/config/mqtt.json";
    File configFile = SPIFFS.open(path, "r");

    if (!configFile)
    {
        g_pLogger->Write(LogLevel::Fatal, "read_mqtt_file", "Failed to open %s..!!!", path.c_str());
        return;
    }
    else
    {
        g_pLogger->Write(LogLevel::Debug, "read_mqtt_file", "Read file %s", path.c_str());
    }

    deserializeJson(mqtt_doc, configFile);
}

void read_device_file(void)
{
    String fileData;
    String path = "/config/device.json";
    File deviceFile = SPIFFS.open(path, "r");

    if (!deviceFile)
    {
        g_pLogger->Write(LogLevel::Fatal, "read_device_file", "Failed to open %s..!!!", path.c_str());
        return;
    }
    else
    {
        g_pLogger->Write(LogLevel::Debug, "read_device_file", "Read file %s", path.c_str());
    }

    deserializeJson(device_doc, deviceFile);
}

void read_wifi_file(void)
{
    String fileData;
    String path = "/config/wifi.json";
    File wifiFile = SPIFFS.open(path, "r");

    if (!wifiFile)
    {
        g_pLogger->Write(LogLevel::Fatal, "read_wifi_file", "Failed to open %s..!!!", path.c_str());
        return;
    }
    else
    {
        g_pLogger->Write(LogLevel::Debug, "read_wifi_file", "Read file %s", path.c_str());
    }

    deserializeJson(wifi_doc, wifiFile);
}

void read_sensor_scaling_file(void)
{
    String fileData;
    String path = "/config/sensorScaling.json";
    File sensorScalingFile = SPIFFS.open(path, "r");

    if (!sensorScalingFile)
    {
        g_pLogger->Write(LogLevel::Fatal, "read_sensorScaling_file", "Failed to open %s..!!!", path.c_str());
        return;
    }
    else
    {
        g_pLogger->Write(LogLevel::Debug, "read_sensorScaling_file", "Read file %s", path.c_str());
    }

    deserializeJson(sensorScaling_doc, sensorScalingFile);
}

void read_sensor_scaling_nl_file(void)
{
    String fileData;
    String line;
    String path = "/config/sensor_nl.csv";
    File sensorScaling_nlFile = SPIFFS.open(path, "r");
    // FILE sensorScaling_nlFile = fopen(path.c_str(), "r");

    if (!sensorScaling_nlFile)
    {
        g_pLogger->Write(LogLevel::Fatal, "read_sensorScaling_nl_file", "Failed to open %s..!!!", path.c_str());
        return;
    }
    else
    {
        g_pLogger->Write(LogLevel::Debug, "read_sensorScaling_nl_file", "Read file %s", path.c_str());
        // first line will contain, if nl is enabled or not
        if (sensorScaling_nlFile.available())
        {
            line = sensorScaling_nlFile.readStringUntil('\n');
        }

        int row = 0;
        // actual scaling data will be fromt he second line of csv
        while (sensorScaling_nlFile.available())
        {
            line = sensorScaling_nlFile.readStringUntil('\n');

            sscanf(line.c_str(), "%d,%d,%d,%d,%d,%d",
                   &scaling_array[row][0],
                   &scaling_array[row][1],
                   &scaling_array[row][2],
                   &scaling_array[row][3],
                   &scaling_array[row][4],
                   &scaling_array[row][5]);
            row++;
            if (row >= SCALING_NL_MAX_ROWS)
            {
                break;
            }
        }

        scaling_nl_max_rows_set = row;

        // Close the file
        sensorScaling_nlFile.close();

        // for (int i = 0; i < row; i++)
        // {
        //     printf("%d --- %d -> %d\n", i, scaling_nl_value[i], scaling_nl_volume[i]);
        // }
    }
}

// void read_params_file(void)
// {
//     String fileData;
//     String path = "/config/params.json";
//     File paramsFile = SPIFFS.open(path, "r");

//     if (!paramsFile)
//     {
//         g_pLogger->Write(LogLevel::Fatal, "read_params_file", "Failed to open %s..!!!", path.c_str());
//         return;
//     }
//     else
//     {
//         g_pLogger->Write(LogLevel::Debug, "read_params_file", "Read file %s", path.c_str());
//     }

//     deserializeJson(params_doc, paramsFile);
// }

void read_modbus_driver_file(void)
{
    String fileData;

    if (active_driver_id > 0)
    {
        String path = "/drivers/mmd_";
        path = path + active_driver_id;
        path = path + ".json";

        File driverFile = SPIFFS.open(path, "r");

        if (!driverFile)
        {
            g_pLogger->Write(LogLevel::Fatal, "read_modbus_driver", "Failed to open %s..!!!", path.c_str());
            return;
        }
        else
        {
            g_pLogger->Write(LogLevel::Debug, "read_modbus_driver", "Read file %s", path.c_str());
        }

        deserializeJson(modbusDriver_doc, driverFile);
    }
}

void read_outCtrl_file(void)
{
    String fileData;
    String path = "/config/outCtrl.json";
    File configFile = SPIFFS.open(path, "r");

    if (!configFile)
    {
        g_pLogger->Write(LogLevel::Fatal, "read_outCtrl_file", "Failed to open %s..!!!", path.c_str());
        return;
    }
    else
    {
        g_pLogger->Write(LogLevel::Debug, "read_outCtrl_file", "Read file %s", path.c_str());
    }

    deserializeJson(outCtrl_doc, configFile);
}