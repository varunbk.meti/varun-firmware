/*******************************************************************************
* File Name: SOL_8.h  
* Version 2.20
*
* Description:
*  This file contains the Alias definitions for Per-Pin APIs in cypins.h. 
*  Information on using these APIs can be found in the System Reference Guide.
*
* Note:
*
********************************************************************************
* Copyright 2008-2015, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_PINS_SOL_8_ALIASES_H) /* Pins SOL_8_ALIASES_H */
#define CY_PINS_SOL_8_ALIASES_H

#include "cytypes.h"
#include "cyfitter.h"
#include "cypins.h"


/***************************************
*              Constants        
***************************************/
#define SOL_8_0			(SOL_8__0__PC)
#define SOL_8_0_PS		(SOL_8__0__PS)
#define SOL_8_0_PC		(SOL_8__0__PC)
#define SOL_8_0_DR		(SOL_8__0__DR)
#define SOL_8_0_SHIFT	(SOL_8__0__SHIFT)
#define SOL_8_0_INTR	((uint16)((uint16)0x0003u << (SOL_8__0__SHIFT*2u)))

#define SOL_8_INTR_ALL	 ((uint16)(SOL_8_0_INTR))


#endif /* End Pins SOL_8_ALIASES_H */


/* [] END OF FILE */
