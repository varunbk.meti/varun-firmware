/*******************************************************************************
* File Name: HW_Select2.c  
* Version 2.20
*
* Description:
*  This file contains APIs to set up the Pins component for low power modes.
*
* Note:
*
********************************************************************************
* Copyright 2015, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#include "cytypes.h"
#include "HW_Select2.h"

static HW_Select2_BACKUP_STRUCT  HW_Select2_backup = {0u, 0u, 0u};


/*******************************************************************************
* Function Name: HW_Select2_Sleep
****************************************************************************//**
*
* \brief Stores the pin configuration and prepares the pin for entering chip 
*  deep-sleep/hibernate modes. This function applies only to SIO and USBIO pins.
*  It should not be called for GPIO or GPIO_OVT pins.
*
* <b>Note</b> This function is available in PSoC 4 only.
*
* \return 
*  None 
*  
* \sideeffect
*  For SIO pins, this function configures the pin input threshold to CMOS and
*  drive level to Vddio. This is needed for SIO pins when in device 
*  deep-sleep/hibernate modes.
*
* \funcusage
*  \snippet HW_Select2_SUT.c usage_HW_Select2_Sleep_Wakeup
*******************************************************************************/
void HW_Select2_Sleep(void)
{
    #if defined(HW_Select2__PC)
        HW_Select2_backup.pcState = HW_Select2_PC;
    #else
        #if (CY_PSOC4_4200L)
            /* Save the regulator state and put the PHY into suspend mode */
            HW_Select2_backup.usbState = HW_Select2_CR1_REG;
            HW_Select2_USB_POWER_REG |= HW_Select2_USBIO_ENTER_SLEEP;
            HW_Select2_CR1_REG &= HW_Select2_USBIO_CR1_OFF;
        #endif
    #endif
    #if defined(CYIPBLOCK_m0s8ioss_VERSION) && defined(HW_Select2__SIO)
        HW_Select2_backup.sioState = HW_Select2_SIO_REG;
        /* SIO requires unregulated output buffer and single ended input buffer */
        HW_Select2_SIO_REG &= (uint32)(~HW_Select2_SIO_LPM_MASK);
    #endif  
}


/*******************************************************************************
* Function Name: HW_Select2_Wakeup
****************************************************************************//**
*
* \brief Restores the pin configuration that was saved during Pin_Sleep(). This 
* function applies only to SIO and USBIO pins. It should not be called for
* GPIO or GPIO_OVT pins.
*
* For USBIO pins, the wakeup is only triggered for falling edge interrupts.
*
* <b>Note</b> This function is available in PSoC 4 only.
*
* \return 
*  None
*  
* \funcusage
*  Refer to HW_Select2_Sleep() for an example usage.
*******************************************************************************/
void HW_Select2_Wakeup(void)
{
    #if defined(HW_Select2__PC)
        HW_Select2_PC = HW_Select2_backup.pcState;
    #else
        #if (CY_PSOC4_4200L)
            /* Restore the regulator state and come out of suspend mode */
            HW_Select2_USB_POWER_REG &= HW_Select2_USBIO_EXIT_SLEEP_PH1;
            HW_Select2_CR1_REG = HW_Select2_backup.usbState;
            HW_Select2_USB_POWER_REG &= HW_Select2_USBIO_EXIT_SLEEP_PH2;
        #endif
    #endif
    #if defined(CYIPBLOCK_m0s8ioss_VERSION) && defined(HW_Select2__SIO)
        HW_Select2_SIO_REG = HW_Select2_backup.sioState;
    #endif
}


/* [] END OF FILE */
