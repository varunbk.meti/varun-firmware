/*******************************************************************************
* File Name: Router.h  
* Version 2.20
*
* Description:
*  This file contains Pin function prototypes and register defines
*
********************************************************************************
* Copyright 2008-2015, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_PINS_Router_H) /* Pins Router_H */
#define CY_PINS_Router_H

#include "cytypes.h"
#include "cyfitter.h"
#include "Router_aliases.h"


/***************************************
*     Data Struct Definitions
***************************************/

/**
* \addtogroup group_structures
* @{
*/
    
/* Structure for sleep mode support */
typedef struct
{
    uint32 pcState; /**< State of the port control register */
    uint32 sioState; /**< State of the SIO configuration */
    uint32 usbState; /**< State of the USBIO regulator */
} Router_BACKUP_STRUCT;

/** @} structures */


/***************************************
*        Function Prototypes             
***************************************/
/**
* \addtogroup group_general
* @{
*/
uint8   Router_Read(void);
void    Router_Write(uint8 value);
uint8   Router_ReadDataReg(void);
#if defined(Router__PC) || (CY_PSOC4_4200L) 
    void    Router_SetDriveMode(uint8 mode);
#endif
void    Router_SetInterruptMode(uint16 position, uint16 mode);
uint8   Router_ClearInterrupt(void);
/** @} general */

/**
* \addtogroup group_power
* @{
*/
void Router_Sleep(void); 
void Router_Wakeup(void);
/** @} power */


/***************************************
*           API Constants        
***************************************/
#if defined(Router__PC) || (CY_PSOC4_4200L) 
    /* Drive Modes */
    #define Router_DRIVE_MODE_BITS        (3)
    #define Router_DRIVE_MODE_IND_MASK    (0xFFFFFFFFu >> (32 - Router_DRIVE_MODE_BITS))

    /**
    * \addtogroup group_constants
    * @{
    */
        /** \addtogroup driveMode Drive mode constants
         * \brief Constants to be passed as "mode" parameter in the Router_SetDriveMode() function.
         *  @{
         */
        #define Router_DM_ALG_HIZ         (0x00u) /**< \brief High Impedance Analog   */
        #define Router_DM_DIG_HIZ         (0x01u) /**< \brief High Impedance Digital  */
        #define Router_DM_RES_UP          (0x02u) /**< \brief Resistive Pull Up       */
        #define Router_DM_RES_DWN         (0x03u) /**< \brief Resistive Pull Down     */
        #define Router_DM_OD_LO           (0x04u) /**< \brief Open Drain, Drives Low  */
        #define Router_DM_OD_HI           (0x05u) /**< \brief Open Drain, Drives High */
        #define Router_DM_STRONG          (0x06u) /**< \brief Strong Drive            */
        #define Router_DM_RES_UPDWN       (0x07u) /**< \brief Resistive Pull Up/Down  */
        /** @} driveMode */
    /** @} group_constants */
#endif

/* Digital Port Constants */
#define Router_MASK               Router__MASK
#define Router_SHIFT              Router__SHIFT
#define Router_WIDTH              1u

/**
* \addtogroup group_constants
* @{
*/
    /** \addtogroup intrMode Interrupt constants
     * \brief Constants to be passed as "mode" parameter in Router_SetInterruptMode() function.
     *  @{
     */
        #define Router_INTR_NONE      ((uint16)(0x0000u)) /**< \brief Disabled             */
        #define Router_INTR_RISING    ((uint16)(0x5555u)) /**< \brief Rising edge trigger  */
        #define Router_INTR_FALLING   ((uint16)(0xaaaau)) /**< \brief Falling edge trigger */
        #define Router_INTR_BOTH      ((uint16)(0xffffu)) /**< \brief Both edge trigger    */
    /** @} intrMode */
/** @} group_constants */

/* SIO LPM definition */
#if defined(Router__SIO)
    #define Router_SIO_LPM_MASK       (0x03u)
#endif

/* USBIO definitions */
#if !defined(Router__PC) && (CY_PSOC4_4200L)
    #define Router_USBIO_ENABLE               ((uint32)0x80000000u)
    #define Router_USBIO_DISABLE              ((uint32)(~Router_USBIO_ENABLE))
    #define Router_USBIO_SUSPEND_SHIFT        CYFLD_USBDEVv2_USB_SUSPEND__OFFSET
    #define Router_USBIO_SUSPEND_DEL_SHIFT    CYFLD_USBDEVv2_USB_SUSPEND_DEL__OFFSET
    #define Router_USBIO_ENTER_SLEEP          ((uint32)((1u << Router_USBIO_SUSPEND_SHIFT) \
                                                        | (1u << Router_USBIO_SUSPEND_DEL_SHIFT)))
    #define Router_USBIO_EXIT_SLEEP_PH1       ((uint32)~((uint32)(1u << Router_USBIO_SUSPEND_SHIFT)))
    #define Router_USBIO_EXIT_SLEEP_PH2       ((uint32)~((uint32)(1u << Router_USBIO_SUSPEND_DEL_SHIFT)))
    #define Router_USBIO_CR1_OFF              ((uint32)0xfffffffeu)
#endif


/***************************************
*             Registers        
***************************************/
/* Main Port Registers */
#if defined(Router__PC)
    /* Port Configuration */
    #define Router_PC                 (* (reg32 *) Router__PC)
#endif
/* Pin State */
#define Router_PS                     (* (reg32 *) Router__PS)
/* Data Register */
#define Router_DR                     (* (reg32 *) Router__DR)
/* Input Buffer Disable Override */
#define Router_INP_DIS                (* (reg32 *) Router__PC2)

/* Interrupt configuration Registers */
#define Router_INTCFG                 (* (reg32 *) Router__INTCFG)
#define Router_INTSTAT                (* (reg32 *) Router__INTSTAT)

/* "Interrupt cause" register for Combined Port Interrupt (AllPortInt) in GSRef component */
#if defined (CYREG_GPIO_INTR_CAUSE)
    #define Router_INTR_CAUSE         (* (reg32 *) CYREG_GPIO_INTR_CAUSE)
#endif

/* SIO register */
#if defined(Router__SIO)
    #define Router_SIO_REG            (* (reg32 *) Router__SIO)
#endif /* (Router__SIO_CFG) */

/* USBIO registers */
#if !defined(Router__PC) && (CY_PSOC4_4200L)
    #define Router_USB_POWER_REG       (* (reg32 *) CYREG_USBDEVv2_USB_POWER_CTRL)
    #define Router_CR1_REG             (* (reg32 *) CYREG_USBDEVv2_CR1)
    #define Router_USBIO_CTRL_REG      (* (reg32 *) CYREG_USBDEVv2_USB_USBIO_CTRL)
#endif    
    
    
/***************************************
* The following code is DEPRECATED and 
* must not be used in new designs.
***************************************/
/**
* \addtogroup group_deprecated
* @{
*/
#define Router_DRIVE_MODE_SHIFT       (0x00u)
#define Router_DRIVE_MODE_MASK        (0x07u << Router_DRIVE_MODE_SHIFT)
/** @} deprecated */

#endif /* End Pins Router_H */


/* [] END OF FILE */
