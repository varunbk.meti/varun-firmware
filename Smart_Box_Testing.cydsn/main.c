/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/
#include "project.h"
void LCD_Disp(void);
void main_board(void);
void extension_board(void);
void Extension_1(void);

int main(void)
{
    CyGlobalIntEnable; /* Enable global interrupts. */

    /* Place your initialization/startup code here (e.g. MyInst_Start()) */
    LCD_Start();
    CyDelay(10);
    for(;;)
    {
        /* Place your application code here. */
        if((HW_Select1_Read()==0) && (HW_Select2_Read()==0))
        {
            main_board();
        }
        
        if((HW_Select1_Read()==1) && (HW_Select2_Read()==0))
        {
            Extension_1();
        }
        if((HW_Select1_Read()==0) && (HW_Select2_Read()==1))
        {
            main_board();
        }
        
    }
}

void LCD_Disp(void)
{
   LCD_Position(0,3);
   LCD_PrintString("Smart_Box");
   CyDelay(100);
   LCD_Position(1,3);
   LCD_PrintString("Testing");
   CyDelay(100);
}

void main_board(void)
{
    Status_Write(1);
    CyDelay(100);
    Status_Write(0);
    CyDelay(100);


    Router_Write(1);
    CyDelay(100);
    Router_Write(0);
    CyDelay(100);

    FAN_Write(1);
    CyDelay(100);
    FAN_Write(0);
    CyDelay(100);

    if(SIC_1_Read()!=0)
    {
       SOL_1_Write(0);
    }
    else
    {
       SOL_1_Write(1); 
    }
    if(SIC_2_Read()!=0)
    {
       SOL_2_Write(0);
    }
    else
    {
       SOL_2_Write(1); 
    }
    if(SIC_3_Read()!=0)
    {
       SOL_3_Write(0);
    }
    else
    {
       SOL_3_Write(1); 
    }
    if(SIC_4_Read()!=0)
    {
       SOL_4_Write(0);
    }
    else
    {
       SOL_4_Write(1); 
    }
    if(SIC_5_Read()!=0)
    {
       SOL_5_Write(0);
    }
    else
    {
       SOL_5_Write(1); 
    }
    if(SIC_6_Read()!=0)
    {
       SOL_6_Write(0);
    }
    else
    {
       SOL_6_Write(1); 
    }
    if(SIC_7_Read()!=0)
    {
       SOL_7_Write(0);
    }
    else
    {
       SOL_7_Write(1); 
    }
    if(SIC_8_Read()!=0)
    {
       SOL_8_Write(0);
    }
    else
    {
       SOL_8_Write(1); 
    }
    LCD_Disp();
}
void Extension_1(void)
{
    Status_Write(1);
    CyDelay(100);
    Status_Write(0);
    CyDelay(100);

    if(SIC_1_Read()!=0)
    {
       SOL_8_Write(0);
    }
    else
    {
       SOL_8_Write(1); 
    }
    if(SIC_2_Read()!=0)
    {
       SOL_7_Write(0);
    }
    else
    {
       SOL_7_Write(1); 
    }
    if(SIC_3_Read()!=0)
    {
       SOL_6_Write(0);
    }
    else
    {
       SOL_6_Write(1); 
    }
    if(SIC_4_Read()!=0)
    {
       SOL_5_Write(0);
    }
    else
    {
       SOL_5_Write(1); 
    }
    if(SIC_5_Read()!=0)
    {
       SOL_4_Write(0);
    }
    else
    {
       SOL_4_Write(1); 
    }
    if(SIC_6_Read()!=0)
    {
       SOL_3_Write(0);
    }
    else
    {
       SOL_3_Write(1); 
    }
    if(SIC_7_Read()!=0)
    {
       SOL_2_Write(0);
    }
    else
    {
       SOL_2_Write(1); 
    }
    if(SIC_8_Read()!=0)
    {
       SOL_1_Write(0);
    }
    else
    {
       SOL_1_Write(1); 
    }
}
/* [] END OF FILE */
