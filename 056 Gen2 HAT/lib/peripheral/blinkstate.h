#ifndef _BLINK_STATE_H_INCLUDED_
#define _BLINK_STATE_H_INCLUDED_ 1

#include<string>
#include<defs.h>

using namespace std;

class BlinkState
{
public:
    enum Value : uint8
    {
        Idle = 0,
        Start = 1,
        On = 2,
        Off = 3,
        CycleSleep = 4,
        CycleEnd = 5,
        Stop = 6,
        KeepOFF = 7,
        KeepON = 8
    };

    BlinkState() = default;
    BlinkState(Value code) : m_iCode(code) { }

    operator Value() const { return m_iCode; }
    explicit operator bool() = delete;
    constexpr bool operator==(BlinkState a) const { return m_iCode == a.m_iCode; }
    constexpr bool operator!=(BlinkState a) const { return m_iCode != a.m_iCode; }
    uint8 GetCode();
    string GetLabel();
    string ToString();

    static BlinkState Parse(int code);

private:
    Value m_iCode;
};

#endif
