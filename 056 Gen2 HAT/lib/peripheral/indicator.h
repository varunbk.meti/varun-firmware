#ifndef _INDICATOR_H_INCLUDED
#define _INDICATOR_H_INCLUDED

#include<defs.h>
#include<blinkconfig.h>
#include<blinkstate.h>
#include<cthread.h>
#include<cdate.h>

#define CYCLE_COUNT_CONTIUOUS = 0;

class Indicator {
public:

	void SetPin(int p_iPinNo);
	void SetPin(int p_iPinNo, bool p_bKeepOn);
	void SwitchOff();
	void SwitchOn();
	void Toggle();
	void Blink(BlinkConfig value);

	~Indicator();

	static Indicator* GetInstance();
	static void GlobalStateMachine(void*);
	static void Initialize( uint8 p_iIndicatorCount);
	static void Release();
private:
	Indicator();
	uint8 m_iPinNumber;
	BlinkConfig m_oBlinkConfig;
	
	bool m_bEnabled;
	uint8 m_iElapsedBlinkCount;
	uint8 m_iElapsedCycleCount;
	BlinkState m_oState;
	CDate m_oStateTime;

	void StateMachine();
	void SetState(BlinkState state);
	bool IsStateTimedOut(uint16 p_iInterval);
	bool IsStateTimedOut(CDate p_oCurrTime, uint16 p_iInterval);

	static CThread m_oStateThread;
	static uint8 m_iTotalIndicators;
	static Indicator* m_pIndicators;
};


#endif
