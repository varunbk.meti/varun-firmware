/*******************************************************************************
* File Name: gen2wifi.h
*
* Version: 1.0
*
* Description:
* This is header file for gen2 wifi functions All the functions related to
* gen2wifi core are here.
*
* Written By:
* Yogesh M Iggalore
*
*
********************************************************************************
* Copyright (2022-23) , METI
********************************************************************************/

#ifndef gen2wifi_h
#define gen2wifi_h
#include <Arduino.h>    

#define GEN2_BUFFER_SIZE    2048
#define GEN2_START_OF_TEXT  0x02
#define GEN2_END_OF_TEXT    0x03

//#define GEN2_START_OF_TEXT  '$'
//#define GEN2_END_OF_TEXT    '#'

typedef enum{
    port0=0,
    port1,
    port2
}euartport;

typedef enum{
    databits5=5,
    databits6,
    databits7,
    databits8
}euartdatabits;

typedef enum{
    none=0,
    even,
    odd
}euartparity;

typedef enum{
    stopbits1=1,
    stopbits2
}euartstopbits;

typedef enum{
    cfg_fail = -1,
    err_none=0
}egen2error;

typedef enum{
    state_none=0,
    state_reading,
    state_process,
    state_internet,
    state_write
}egen2state;

struct uart_config {
	euartport  port;
    uint8_t  rxpin;
    uint8_t  txpin;
    uint32_t baudrate;
	euartparity parity;
	euartstopbits stop_bits;
	euartdatabits data_bits;
	uint8_t flow_ctrl;
    uint16_t  internlrxbufsize;
};

struct gen2_config {
    uart_config cfg_uart;
    uint8_t     statuspin;
    uint16_t  servertimeout;
};


class gen2wifi
{
public:
    gen2wifi();
    int  start(gen2_config cfg);
    int  read(void);
    int  process(void);
    int  write(void);
    int  send_packet_to_server(void);
    int  activity(uint8_t internetstatus);
    euartport  get_port(void);
    uint8_t  get_rxpin(void);
    uint8_t  get_txpin(void);
    uint32_t get_baudrate(void);
	euartparity get_parity(void);
	euartstopbits get_stop_bits(void);
	euartdatabits get_data_bits(void);
	uint8_t get_flow_ctrl(void);
    uint16_t  get_internlrxbufsize(void);
    uint16_t  get_servertimeout(void);
    uint8_t     get_statuspin(void);
    uint8_t     get_internetstatus(void);
    egen2error  get_cfg_error(void);
private:
    gen2_config m_cfg;
    uint8_t     m_rxbuffer[GEN2_BUFFER_SIZE];
    uint16_t    m_rxcntr;
    egen2state  m_state;
    uint16_t    m_timeout;
    uint8_t     m_internetstatus;
    egen2error  m_error;
    unsigned long    m_lastreadtime;

    void set_internet_status(uint8_t internetstatus);
    uint32_t get_serialconfig(euartdatabits databits, euartparity parity, euartstopbits stopbits);
    int  send_response(String spkt);
    void read_serial_data(euartport);
    uint16_t get_timeout(char asciihex[]);
    uint8_t convert_asciihex_to_hex(char asciihex[]);
    void    set_state(egen2state state);
    egen2state    get_state(void);
};

#if !defined(NO_GLOBAL_INSTANCES) && !defined(NO_GLOBAL_GEN2WIFI)
extern gen2wifi gen2;
#endif
#endif