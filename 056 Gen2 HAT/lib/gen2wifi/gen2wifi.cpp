/*******************************************************************************
 * File Name: gen2wifi.cpp
 *
 * Version: 1.0
 *
 * Description:
 * This is source file for gen2 wifi functions All the functions related to
 * gen2wifi core are here.
 *
 * Written By:
 * Yogesh M Iggalore
 *
 *
 ********************************************************************************
 * Copyright (2022-23) , METI
 ********************************************************************************/
#include <gen2wifi.h>
#include <Arduino.h>
#include <HTTPClient.h>
#include <WiFiClient.h>

// Constructors ////////////////////////////////////////////////////////////////
gen2wifi::gen2wifi() {}

int gen2wifi::start(gen2_config cfg)
{
    m_cfg = cfg;

    /* set internet status pin */
    pinMode(m_cfg.statuspin, OUTPUT);

    m_error = err_none;
    set_state(state_reading);

    /* set uart configuration */
    switch (m_cfg.cfg_uart.port)
    {
    case port0:
        Serial.begin(m_cfg.cfg_uart.baudrate,
                     get_serialconfig(m_cfg.cfg_uart.data_bits, m_cfg.cfg_uart.parity, m_cfg.cfg_uart.stop_bits),
                     m_cfg.cfg_uart.rxpin,
                     m_cfg.cfg_uart.txpin);
        Serial.setRxBufferSize(m_cfg.cfg_uart.internlrxbufsize);
        break;
    case port1:
        Serial1.begin(m_cfg.cfg_uart.baudrate,
                      get_serialconfig(m_cfg.cfg_uart.data_bits, m_cfg.cfg_uart.parity, m_cfg.cfg_uart.stop_bits),
                      m_cfg.cfg_uart.rxpin,
                      m_cfg.cfg_uart.txpin);
        Serial1.setRxBufferSize(m_cfg.cfg_uart.internlrxbufsize);
        break;
    case port2:
        Serial2.begin(m_cfg.cfg_uart.baudrate,
                      get_serialconfig(m_cfg.cfg_uart.data_bits, m_cfg.cfg_uart.parity, m_cfg.cfg_uart.stop_bits),
                      m_cfg.cfg_uart.rxpin,
                      m_cfg.cfg_uart.txpin);
        Serial2.setRxBufferSize(m_cfg.cfg_uart.internlrxbufsize);
        break;
    default:
        m_error = cfg_fail;
        set_state(state_none);
    }

    return m_error;
}

int gen2wifi::activity(uint8_t internetstatus)
{

    set_internet_status(internetstatus);
    if (internetstatus != 0)
    {
        read();
        process();
        send_packet_to_server();
    }

    return 0;
}

int gen2wifi::read(void)
{

    if (m_error != err_none)
    {
        return cfg_fail;
    }

    switch (m_cfg.cfg_uart.port)
    {
    case port0:
        read_serial_data(port0);
        break;
    case port1:
        read_serial_data(port1);
        break;
    case port2:
        read_serial_data(port2);
        break;
    default:
        // if break point comes here, this lib is useless
        break;
    }

    if(m_rxcntr > 0)
    {
        if (m_rxbuffer[0] != GEN2_START_OF_TEXT)
        {
            set_state(state_reading);
            return -1;
        }

        if (m_rxbuffer[m_rxcntr - 1] == GEN2_END_OF_TEXT)
        {
            set_state(state_process);
        }
        else
        {
            if(millis() > m_lastreadtime)
            {
                if(millis() - m_lastreadtime  > 100)
                {
                    set_state(state_reading);
                    return -1;
                }
            }
        }
    }
    
    return 0;
}

int gen2wifi::process(void)
{
    uint16_t loopcntr;
    char time[5];

    if (m_error != err_none)
    {
        return cfg_fail;
    }

    if (m_state != state_process)
    {
        return -1;
        ;
    }

    /* set timeout */
    for (loopcntr = 0; loopcntr < 4; loopcntr++)
    {
        time[loopcntr] = m_rxbuffer[loopcntr+1];
    }
    time[4] = '\0';

    m_timeout = get_timeout(time);

    for (loopcntr = 0; loopcntr < (m_rxcntr - 6); loopcntr++)
    {
        m_rxbuffer[loopcntr] = m_rxbuffer[loopcntr + 5];
    }
    m_rxbuffer[loopcntr] = '\0';

    if (m_internetstatus == 1)
    {
        set_state(state_internet);
    }
    else
    {
        send_response("$#");
        set_state(state_reading);
    }

    return 0;
}

int gen2wifi::send_packet_to_server(void)
{
    int httpcode;
    String response;
    String spkt;

    if (m_error != err_none)
    {
        return cfg_fail;
    }

    if (m_state != state_internet)
    {
        return -1;
    }

    HTTPClient http;
    WiFiClient wifiClient;

    http.setTimeout(m_timeout);
    http.begin(wifiClient, String((char *)m_rxbuffer));
    
    httpcode = http.GET();
    if (httpcode == HTTP_CODE_OK)
    {
        response = http.getString();
        spkt = "$" + response + "#";
    }
    else
    {
        spkt = "$#";
    }

    http.end();
    send_response(spkt);
    set_state(state_reading);

    return 0;
}

int gen2wifi::send_response(String spkt)
{
    switch (m_cfg.cfg_uart.port)
    {
    case port0:
        Serial.print(spkt);
        break;

    case port1:
        Serial1.print(spkt);
        break;

    case port2:
        Serial2.print(spkt);
        break;
    default:
        // if break point comes here through laptop and goto himalaya
        break;
    }

    return 0;
}

void gen2wifi::set_internet_status(uint8_t internetstatus)
{
    if (internetstatus == 0)
    {
        m_internetstatus = 0;
        digitalWrite(m_cfg.statuspin, 0);
    }
    else
    {
        m_internetstatus = 1;
        digitalWrite(m_cfg.statuspin, 1);
    }
}

void gen2wifi::read_serial_data(euartport port)
{
    switch (port)
    {
    case port0:
        if (Serial.available())
        {
            while (Serial.available())
            {
                if(m_rxcntr == 0)
                {
                    m_lastreadtime = millis();
                }
                m_rxbuffer[m_rxcntr++] = Serial.read();
            }
            Serial.flush();
        }
        break;

    case port1:
        if (Serial1.available())
        {
            while (Serial1.available())
            {
                if(m_rxcntr == 0)
                {
                    m_lastreadtime = millis();
                }
                m_rxbuffer[m_rxcntr++] = Serial1.read();
            }
            Serial1.flush();
        }
        break;

    case port2:
        if (Serial2.available())
        {
            while (Serial2.available())
            {
                if(m_rxcntr == 0)
                {
                    m_lastreadtime = millis();
                }
                m_rxbuffer[m_rxcntr++] = Serial2.read();
            }
            Serial2.flush();
        }
        break;
    default:
        // if break point comes here through laptop and goto himalaya
        break;
    }
}

egen2state gen2wifi::get_state(void)
{
    return m_state;
}

void gen2wifi::set_state(egen2state state)
{
    switch (state)
    {
    case state_none:
        break;
    case state_reading:
        m_state = state_reading;
        m_rxcntr = 0;
        memset(m_rxbuffer, '\0', GEN2_BUFFER_SIZE);
        break;
    case state_process:
        m_state = state_process;
        break;
    case state_internet:
        m_state = state_internet;
        break;
    case state_write:
        m_state = state_write;
        break;
    default:
        break;
    }
}

uint16_t gen2wifi::get_timeout(char asciihex[])
{
    char sChar[2];
    uint8_t byte = 0;
    uint16_t time;

    sChar[0] = asciihex[0];
    sChar[1] = asciihex[1];
    byte = convert_asciihex_to_hex(sChar);
    time = byte;
    time = time << 8;
    sChar[0] = asciihex[2];
    sChar[1] = asciihex[3];
    time = time | convert_asciihex_to_hex(sChar);

    return time;
}

uint8_t gen2wifi::convert_asciihex_to_hex(char asciihex[])
{
    char cCharLowerNibble = 0;
    char cCharHigherNibble = 0;
    uint8_t ui8HexByte = 0;

    cCharHigherNibble = asciihex[0];
    cCharLowerNibble = asciihex[1];

    if (cCharHigherNibble > '9')
    {
        cCharHigherNibble = cCharHigherNibble - 7;
    }
    cCharHigherNibble = cCharHigherNibble - '0';

    ui8HexByte = cCharHigherNibble;
    ui8HexByte = ui8HexByte << 4;

    if (cCharLowerNibble > '9')
    {
        cCharLowerNibble = cCharLowerNibble - 7;
    }
    cCharLowerNibble = cCharLowerNibble - '0';

    ui8HexByte = ui8HexByte | cCharLowerNibble;

    return ui8HexByte;
}

euartport gen2wifi::get_port(void)
{
    return m_cfg.cfg_uart.port;
}

uint8_t gen2wifi::get_rxpin(void)
{
    return m_cfg.cfg_uart.rxpin;
}

uint8_t gen2wifi::get_txpin(void)
{
    return m_cfg.cfg_uart.txpin;
}

uint32_t gen2wifi::get_baudrate(void)
{
    return m_cfg.cfg_uart.baudrate;
}

euartparity gen2wifi::get_parity(void)
{
    return m_cfg.cfg_uart.parity;    
}

euartstopbits gen2wifi::get_stop_bits(void)
{
    return m_cfg.cfg_uart.stop_bits;
}

euartdatabits gen2wifi::get_data_bits(void)
{
    return m_cfg.cfg_uart.data_bits;
}

uint8_t gen2wifi::get_flow_ctrl(void)
{
    return m_cfg.cfg_uart.flow_ctrl;
}

uint16_t gen2wifi::get_internlrxbufsize(void)
{
    return m_cfg.cfg_uart.internlrxbufsize;
}

uint16_t gen2wifi::get_servertimeout(void)
{
    return m_cfg.servertimeout;
}

uint8_t gen2wifi::get_statuspin(void)
{
    return m_cfg.statuspin;
}

uint8_t gen2wifi::get_internetstatus(void)
{
    return m_internetstatus;
}

egen2error gen2wifi::get_cfg_error(void)
{
    return m_error;
}

uint32_t gen2wifi::get_serialconfig(euartdatabits databits, euartparity parity, euartstopbits stopbits)
{
    if ((databits == databits5) & (parity == none) & (stopbits == stopbits1))
    {
        return SERIAL_5N1;
    }
    else if ((databits == databits6) & (parity == none) & (stopbits == stopbits1))
    {
        return SERIAL_6N1;
    }
    else if ((databits == databits7) & (parity == none) & (stopbits == stopbits1))
    {
        return SERIAL_7N1;
    }
    else if ((databits == databits8) & (parity == none) & (stopbits == stopbits1))
    {
        return SERIAL_8N1;
    }
    else if ((databits == databits5) & (parity == none) & (stopbits == stopbits2))
    {
        return SERIAL_5N2;
    }
    else if ((databits == databits6) & (parity == none) & (stopbits == stopbits2))
    {
        return SERIAL_6N2;
    }
    else if ((databits == databits7) & (parity == none) & (stopbits == stopbits2))
    {
        return SERIAL_7N2;
    }
    else if ((databits == databits8) & (parity == none) & (stopbits == stopbits2))
    {
        return SERIAL_8N2;
    }
    else if ((databits == databits5) & (parity == none) & (stopbits == stopbits1))
    {
        return SERIAL_5E1;
    }
    else if ((databits == databits6) & (parity == even) & (stopbits == stopbits1))
    {
        return SERIAL_6E1;
    }
    else if ((databits == databits7) & (parity == even) & (stopbits == stopbits1))
    {
        return SERIAL_7E1;
    }
    else if ((databits == databits8) & (parity == even) & (stopbits == stopbits1))
    {
        return SERIAL_8E1;
    }
    else if ((databits == databits5) & (parity == even) & (stopbits == stopbits2))
    {
        return SERIAL_5E2;
    }
    else if ((databits == databits6) & (parity == even) & (stopbits == stopbits2))
    {
        return SERIAL_6E2;
    }
    else if ((databits == databits7) & (parity == even) & (stopbits == stopbits2))
    {
        return SERIAL_7E2;
    }
    else if ((databits == databits8) & (parity == even) & (stopbits == stopbits2))
    {
        return SERIAL_8E2;
    }
    else if ((databits == databits5) & (parity == even) & (stopbits == stopbits2))
    {
        return SERIAL_5O1;
    }
    else if ((databits == databits6) & (parity == even) & (stopbits == stopbits1))
    {
        return SERIAL_6O1;
    }
    else if ((databits == databits7) & (parity == odd) & (stopbits == stopbits1))
    {
        return SERIAL_7O1;
    }
    else if ((databits == databits8) & (parity == odd) & (stopbits == stopbits1))
    {
        return SERIAL_8O1;
    }
    else if ((databits == databits5) & (parity == odd) & (stopbits == stopbits2))
    {
        return SERIAL_5O2;
    }
    else if ((databits == databits6) & (parity == odd) & (stopbits == stopbits2))
    {
        return SERIAL_6O2;
    }
    else if ((databits == databits7) & (parity == odd) & (stopbits == stopbits2))
    {
        return SERIAL_7O2;
    }
    else if ((databits == databits8) & (parity == odd) & (stopbits == stopbits2))
    {
        return SERIAL_8O2;
    }
    else
    {
        return SERIAL_8N1;
    }
}
// Preinstantiate Objects //////////////////////////////////////////////////////
#if !defined(NO_GLOBAL_INSTANCES) && !defined(NO_GLOBAL_GEN2WIFI)
gen2wifi gen2;
#endif