#ifndef _HEADER_FILE_H_
#define _HEADER_FILE_H_
#include <Arduino.h>
#include <ArduinoJson.h>

//srm config file related declarations
// This has to be changed if there is a change in json structure
const size_t srm_config_capacity = 1000;
const size_t srm_config_capacity_add = 100;
extern DynamicJsonDocument srm_config_doc;

//device file related declarations
//This has to be changed if there is a change in json structure
const size_t device_capacity = JSON_OBJECT_SIZE(20) + 380;
const size_t device_capacity_add = 100;
extern DynamicJsonDocument device_doc;

//wifi file related declarations
//This has to be changed if there is a change in json structure
const size_t wifi_capacity = JSON_OBJECT_SIZE(2) + JSON_OBJECT_SIZE(3) + JSON_OBJECT_SIZE(5) + 130;
const size_t wifi_capacity_add = 100;
extern DynamicJsonDocument wifi_doc;

//parameters file related declarations
//This has to be changed if there is a change in json structure
const size_t params_capacity = JSON_OBJECT_SIZE(2) + 2*JSON_OBJECT_SIZE(4) + 130;
const size_t params_capacity_add = 100;
extern DynamicJsonDocument params_doc;

void read_srm_config_file(void);
void read_device_file(void);
void read_wifi_file(void);
void read_params_file(void);

#endif