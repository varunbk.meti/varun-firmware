#ifndef _HEADER_METI_MQTT_H_
#define _HEADER_METI_MQTT_H_

#include <AsyncMqttClient.h>
#include <ArduinoJson.h>

//Constatnts

/* mqtt server/host */
#define MQTT_QOS_0 0        //fire and forget
#define MQTT_QOS_1 1        //publish with acknoledgement, no ack retry, with multiple acks
#define MQTT_QOS_2 2        //two times handshake, Guaranteed one time delivary

/* retain flag if client disconnect from server */
#define MQTT_RETAIN 1
#define MQTT_NO_RETAIN 0

/* onboard led pin number */
#define LED_PIN 2

#define PUBLISH_INTERVAL_MIN 10
#define RECONNECT_INTERVAL 15

//Variables
extern AsyncMqttClient mqttClient;
extern bool mqtt_enabled;
extern bool mqtt_status;
extern bool mqtt_start;
extern int mqtt_publish_interval_ctr;
extern int mqtt_reconnect_ctr;

//Functions
/* Mqtt functions */
void mqtt_setup(void);
void mqtt_setup_topics(void);
void mqtt_connect(void);
void mqtt_publish_sensor_values(void);
void mqtt_publish_timeout(void);

//callback functions
void mqtt_on_connect(bool sessionPresent);
void mqtt_on_disconnect(AsyncMqttClientDisconnectReason reason);
void mqtt_on_subscribe_ack(uint16_t packetId, uint8_t qos);
void mqtt_on_unsubscribe_ack(uint16_t packetId);
void mqtt_on_message(char *topic, char *payload, AsyncMqttClientMessageProperties properties, size_t len, size_t index, size_t total);
void mqtt_on_publish_ack(uint16_t packetId);
void mqtt_check_comm_status(void);

String mqtt_disconnect_reason(AsyncMqttClientDisconnectReason reason);

#endif