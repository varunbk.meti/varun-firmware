#ifndef _METI_SRM_H_
#define _METI_SRM_H_
#include <Arduino.h>
#include <ArduinoJson.h>


#define ROUTER_CONTROL_PIN          GPIO_NUM_23

#define ROUTER_STATE_NONE           0
#define ROUTER_STATE_RESET_INIT     1
#define ROUTER_STATE_RESET_DONE     2
#define ROUTER_STATE_BOOT_INIT      3
#define ROUTER_STATE_BOOT_DONE      4
#define ROUTER_STATE_WIFI_ERROR     5
#define ROUTER_STATE_WIFI_OKAY      6
#define ROUTER_STATE_NET_CHECK      7
#define ROUTER_STATE_NET_ERROR      8
#define ROUTER_STATE_NET_OKAY       9

void router_start(void);
void handle_router(void);
void check_internet_connection(void);

extern uint8_t ui8RouterState;
extern uint8_t ui8PingIntervalCounter;
extern uint8_t ui8PingInterval;

#endif