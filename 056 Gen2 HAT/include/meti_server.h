#ifndef _HEADER_METI_SERVER_H_
#define _HEADER_METI_SERVER_H_
//Constatnts

//Variables
extern bool file_send_inprogress;

//Functions
String get_content_type(String filename); // convert the file extension to the MIME type

/* ESP Server functions*/
void setup_server(void);
bool server_handle_file_read(AsyncWebServerRequest *request);		// send the right file to the client (if it exists)

// ESP url functions
void server_handle_root(AsyncWebServerRequest *request);
void server_update_config(AsyncWebServerRequest *request);
void server_update_Wifi_config(AsyncWebServerRequest *request);
void server_get_device_config(AsyncWebServerRequest *request);
void server_reset_device(AsyncWebServerRequest *request);
void server_factory_reset(AsyncWebServerRequest *request);


bool check_mode(void);
void parse_bytes(const char *str, char sep, byte *bytes, int maxBytes, int base);

#endif