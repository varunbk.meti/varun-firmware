#include <Arduino.h>
#include <unity.h>
#include <gen2wifi.h>

struct gen2_config cfg_gen2;

void setUp(void) {
    
}

void tearDown(void) {
}

void test_gen2_configuration(void)
{
    TEST_ASSERT_EQUAL_HEX8(cfg_gen2.cfg_uart.port,gen2.get_port());
    TEST_ASSERT_EQUAL_HEX8(cfg_gen2.cfg_uart.rxpin,gen2.get_rxpin());
    TEST_ASSERT_EQUAL_HEX8(cfg_gen2.cfg_uart.txpin,gen2.get_txpin());
    TEST_ASSERT_EQUAL_UINT32(cfg_gen2.cfg_uart.baudrate,gen2.get_baudrate());
    TEST_ASSERT_EQUAL_HEX8(cfg_gen2.cfg_uart.parity,gen2.get_parity());
    TEST_ASSERT_EQUAL_HEX8(cfg_gen2.cfg_uart.stop_bits,gen2.get_stop_bits());
    TEST_ASSERT_EQUAL_HEX8(cfg_gen2.cfg_uart.data_bits,gen2.get_data_bits());
    TEST_ASSERT_EQUAL_HEX8(cfg_gen2.cfg_uart.flow_ctrl,gen2.get_flow_ctrl());
    TEST_ASSERT_EQUAL_HEX16(cfg_gen2.cfg_uart.internlrxbufsize,gen2.get_internlrxbufsize());

    TEST_ASSERT_EQUAL_HEX16(cfg_gen2.servertimeout,gen2.get_servertimeout());
    TEST_ASSERT_EQUAL_HEX16(cfg_gen2.statuspin,gen2.get_statuspin());
}

void setup()
{
    
    /* set gen2 configuration */
    cfg_gen2.cfg_uart.port = port1;
    cfg_gen2.cfg_uart.baudrate = 115200;
    cfg_gen2.cfg_uart.data_bits = databits8;
    cfg_gen2.cfg_uart.flow_ctrl = 0;
    cfg_gen2.cfg_uart.parity = none;
    cfg_gen2.cfg_uart.internlrxbufsize = 250;
    cfg_gen2.cfg_uart.rxpin = 16;
    cfg_gen2.cfg_uart.txpin = 17;
    cfg_gen2.cfg_uart.stop_bits = stopbits1;

    cfg_gen2.servertimeout = 1000;
    cfg_gen2.statuspin = 2;

    gen2.start(cfg_gen2);
    
    delay(2000); // service delay

    UNITY_BEGIN();

    RUN_TEST(test_gen2_configuration);

    UNITY_END(); // stop unit testing
}

void loop()
{

}