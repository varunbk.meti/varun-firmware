#include <Arduino.h>
#include <ArduinoJson.h>
#include <SPIFFS.h>
#include <config_reader.h>
#include <main.h>

DynamicJsonDocument srm_config_doc(srm_config_capacity + srm_config_capacity_add);
DynamicJsonDocument device_doc(device_capacity + device_capacity_add);
DynamicJsonDocument wifi_doc(wifi_capacity + wifi_capacity_add);
DynamicJsonDocument params_doc(params_capacity + params_capacity_add);

void read_srm_config_file(void)
{
    String fileData;
    String path = "/config/srm_config.json";
    File configFile = SPIFFS.open(path, "r");

    if (!configFile)
    {
        g_pLogger->Write(LogLevel::Fatal, "read_srm_config_file", "Failed to open %s..!!!", path.c_str());
        return;
    }
    else
    {
        g_pLogger->Write(LogLevel::Debug, "read_srm_config_file", "Read file %s", path.c_str());
    }

    deserializeJson(srm_config_doc, configFile);
}

void read_device_file(void)
{
    String fileData;
    String path = "/config/device.json";
    File deviceFile = SPIFFS.open(path, "r");

    if (!deviceFile)
    {
        g_pLogger->Write(LogLevel::Fatal, "read_device_file", "Failed to open %s..!!!", path.c_str());
        return;
    }
    else
    {
        g_pLogger->Write(LogLevel::Debug, "read_device_file", "Read file %s", path.c_str());
    }

    deserializeJson(device_doc, deviceFile);
}

void read_wifi_file(void)
{
    String fileData;
    String path = "/config/wifi.json";
    File wifiFile = SPIFFS.open(path, "r");

    if (!wifiFile)
    {
        g_pLogger->Write(LogLevel::Fatal, "read_wifi_file", "Failed to open %s..!!!", path.c_str());
        return;
    }
    else
    {
        g_pLogger->Write(LogLevel::Debug, "read_wifi_file", "Read file %s", path.c_str());
    }

    deserializeJson(wifi_doc, wifiFile);
}

// void read_params_file(void)
// {
//     String fileData;
//     String path = "/config/params.json";
//     File paramsFile = SPIFFS.open(path, "r");

//     if (!paramsFile)
//     {
//         g_pLogger->Write(LogLevel::Fatal, "read_params_file", "Failed to open %s..!!!", path.c_str());
//         return;
//     }
//     else
//     {
//         g_pLogger->Write(LogLevel::Debug, "read_params_file", "Read file %s", path.c_str());
//     }

//     deserializeJson(params_doc, paramsFile);
// }