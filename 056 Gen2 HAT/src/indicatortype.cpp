#include<string>
#include<indicatortype.h>

using namespace std;

IndicatorType IndicatorType::Parse(int code)
{
    if (code == LedRed) {
        return LedRed;
    }
    else if (code == LedGreen) {
        return LedGreen;
    }
    else if (code == LedBlue) {
        return LedBlue;
    }
    
    return LedRed;
}

string IndicatorType::GetLabel()
{
    if (m_iCode == LedRed) {
        return "LedRed";
    }
    else if (m_iCode == LedGreen) {
        return "LedGreen";
    }
    else if (m_iCode == LedBlue) {
        return "LedBlue";
    }

    return "";
}

string IndicatorType::ToString()
{
    return this->GetLabel();
}

uint8 IndicatorType::GetCode()
{
    return m_iCode;
}

