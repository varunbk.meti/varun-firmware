#include<string>
#include<blinktype.h>

using namespace std;

BlinkType BlinkType::Parse(int code)
{
    if (code == Unknown) {
        return Unknown;
    }
    else if (code == Fast) {
        return Fast;
    }
    else if (code == Once) {
        return Once;
    }
    else if (code == Twice) {
        return Twice;
    }
    else if (code == BeforeFactoryReset) {
        return BeforeFactoryReset;
    }
    else if (code == BeforeMbrReset) {
        return BeforeMbrReset;
    }
    else if (code == Slow) {
        return Slow;
    }
    else if (code == OnceFor300Ms) {
        return OnceFor300Ms;
    }
    else if (code == BuzzOnce) {
        return BuzzOnce;
    }
    else if (code == BuzzTwice) {
        return BuzzTwice;
    }
    
    return Unknown;
}

string BlinkType::GetLabel()
{
    if (m_iCode == Unknown) {
        return "Unknown";
    }
    else if (m_iCode == Fast) {
        return "Fast";
    }
    else if (m_iCode == Once) {
        return "Once";
    }
    else if (m_iCode == Twice) {
        return "Twice";
    }
    else if (m_iCode == BeforeFactoryReset) {
        return "BeforeFactoryReset";
    }
    else if (m_iCode == BeforeMbrReset) {
        return "BeforeMbrReset";
    }
    else if (m_iCode == Slow) {
        return "Slow";
    }
    else if (m_iCode == OnceFor300Ms) {
        return "OnceFor300Ms";
    }
    else if (m_iCode == BuzzOnce) {
        return "BuzzOnce";
    }
    else if (m_iCode == BuzzTwice) {
        return "BuzzTwice";
    }

    return "Unknown";
}

string BlinkType::ToString()
{
    return this->GetLabel();
}

uint8 BlinkType::GetCode()
{
    return m_iCode;
}

