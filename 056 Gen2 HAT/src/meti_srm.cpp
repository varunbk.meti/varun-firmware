#include <Arduino.h>
#include <WiFi.h>
#include <HttpClient.h>
#include <meti_wifi.h>
#include <meti_srm.h>
#include <config_reader.h>
#include <ESP32Ping.h>
#include <meti_utils.h>
#include <main.h>
#include <meti_net_led.h>
#include <meti_nvs.h>
#include <esp_task_wdt.h>

uint8_t ui8RouterState = ROUTER_STATE_NONE;
uint8_t ui8RouterOfftime = 5;
uint8_t ui8RouterBoottime = 120;
uint8_t ui8PingInterval = 60;
uint8_t ui8PingRetries = 5;
uint8_t ui8RouterOfftimeCounter = 0;
uint8_t ui8RouterBoottimeCounter = 0;
uint8_t ui8PingIntervalCounter = 0;
uint8_t ui8PingRetriesCounter = 0;
uint8_t ui8OneSecondFlag = 0;
// uint8_t ui8WiFiStatus = 0;

#define MAX_PING_SERVERS 8
const char *const ping_server[MAX_PING_SERVERS] = {
    "8.8.8.8", //google
    "1.1.1.1",
    "104.199.222.154",  //meti live-goole
    "8.8.4.4",          //google
    "208.67.222.222",   //cisco
    "45.65.49.89",      //Airwaves Internet Private Limited, India
    "115.243.184.76",   // Jio, India
    "117.198.218.134"}; // National Internet Backbone, India

void router_start(void)
{
    pinMode(ROUTER_CONTROL_PIN, OUTPUT);
    digitalWrite(ROUTER_CONTROL_PIN, HIGH);
    ui8RouterState = ROUTER_STATE_BOOT_INIT;

    ui8RouterOfftime = srm_config_doc["off_time"];
    ui8RouterBoottime = srm_config_doc["boot_wait"];
    ui8PingInterval = srm_config_doc["ping_interval"];
    ui8PingRetries = srm_config_doc["ping_retries"];
}

void handle_router(void)
{
    switch (ui8RouterState)
    {
    case ROUTER_STATE_NONE:
        g_pLogger->Write(LogLevel::Debug, "handle_router", "ROUTER_STATE_NONE - Router off for %d secs", ui8RouterOfftime);
        digitalWrite(ROUTER_CONTROL_PIN, LOW);
        ui8RouterOfftimeCounter = 0;
        ui8RouterState = ROUTER_STATE_RESET_INIT;
        break;

    case ROUTER_STATE_RESET_INIT:
        ui8RouterOfftimeCounter++;
        if (ui8RouterOfftimeCounter > ui8RouterOfftime)
        {
            ui8RouterOfftimeCounter = 0;
            digitalWrite(ROUTER_CONTROL_PIN, HIGH);
            ui8RouterState = ROUTER_STATE_RESET_DONE;
            g_pLogger->Write(LogLevel::Debug, "handle_router", "ROUTER_STATE_RESET_DONE - Router Power ON");
        }
        break;

    case ROUTER_STATE_RESET_DONE:
        ui8RouterState = ROUTER_STATE_BOOT_INIT;
        g_pLogger->Write(LogLevel::Debug, "handle_router", "ROUTER_STATE_BOOT_INIT - Boot wait time : %d secs", ui8RouterBoottime);
        ui8RouterBoottimeCounter = 0;
        break;

    case ROUTER_STATE_BOOT_INIT:
        ui8RouterBoottimeCounter++;
        if (ui8RouterBoottimeCounter > ui8RouterBoottime)
        {
            ui8RouterBoottimeCounter = 0;
            ui8RouterState = ROUTER_STATE_BOOT_DONE;
            g_pLogger->Write(LogLevel::Debug, "handle_router", "ROUTER_STATE_BOOT_DONE");
        }
        else
        {
            if (wifi_got_ip == true)
            {
                ui8RouterBoottimeCounter = 0;
                ui8RouterState = ROUTER_STATE_BOOT_DONE;
                g_pLogger->Write(LogLevel::Debug, "handle_router", "ROUTER_STATE_BOOT_DONE, Got IP");
            }
        }
        break;

    case ROUTER_STATE_BOOT_DONE:
        if (wifi_got_ip == true)
        {
            ui8RouterState = ROUTER_STATE_WIFI_OKAY;
            g_pLogger->Write(LogLevel::Debug, "handle_router", "ROUTER_STATE_WIFI_OKAY, Ping Interval : %d secs", ui8PingInterval);
        }
        else
        {
            ui8RouterState = ROUTER_STATE_WIFI_ERROR;
            ui8PingIntervalCounter = 0;
            g_pLogger->Write(LogLevel::Debug, "handle_router", "ROUTER_STATE_WIFI_ERROR, Ping Interval : %d secs", ui8PingInterval);
        }
        break;

    case ROUTER_STATE_WIFI_OKAY:
    case ROUTER_STATE_WIFI_ERROR:
    case ROUTER_STATE_NET_OKAY:
    case ROUTER_STATE_NET_ERROR:
        if (wifi_scan_status == METI_WIFI_SCAN_IDLE)
        {
            check_internet_connection();
        }
        break;

    default:
        ui8RouterState = ROUTER_STATE_NONE;
        break;
    }
}

void check_internet_connection(void)
{
    uint8_t ui8NetStatus = 0;
    bool ping_status = false;

    ui8PingIntervalCounter++;
    if (ui8PingIntervalCounter >= ui8PingInterval)
    {
        ui8PingIntervalCounter = 0;
        if (wifi_got_ip == true)
        {
            for (size_t i = 0; i < MAX_PING_SERVERS; i++)
            {
                g_pLogger->Write(LogLevel::Info, "srm_internet_chk", "Pinging to server-%d: %s", i + 1, ping_server[i]);

                ping_status = Ping.ping(ping_server[i], 1);

                if (ping_status == true)
                {
                    i = MAX_PING_SERVERS;
                    ui8NetStatus = true;
                }
                else
                {
                    delay(1000);
                    //clear watchdog timer
                    esp_task_wdt_reset();
                }
            }
        }

        if (ui8NetStatus == true)
        {
            g_pLogger->Write(LogLevel::Info, "srm_internet_chk", "Internet : OK");
            ui8PingRetriesCounter = 0;
            ui8RouterState = ROUTER_STATE_NET_OKAY;
            wifi_internet_ok = true;
        }
        else
        {
            wifi_internet_ok = false;
            set_led_state(MULTI_LED_STATE_BLUE_BLINK_SLOW);
            ui8PingRetriesCounter++;
            g_pLogger->Write(LogLevel::Info, "srm_internet_chk", "Internet : Error !!! Ping retry count: %d of %d", ui8PingRetriesCounter, ui8PingRetries);

            ui8RouterState = ROUTER_STATE_NET_ERROR;
            if (ui8PingRetriesCounter > ui8PingRetries)
            {
                g_pLogger->Write(LogLevel::Warn, "srm_internet_chk", "Maximum ping retry done, restarting router !!!");
                ui8PingRetriesCounter = 0;

                if (wifi_got_ip == false)
                {
                    meti_device_restart();
                }
                else
                {
                    ui8RouterState = ROUTER_STATE_NONE;
                    g_pLogger->Write(LogLevel::Debug, "handle_router", "ROUTER_STATE_NONE");

                    //params.rrc++;
                    //meti_nvs_commit_val("params", "rrc", params.rrc);
                }
            }
        }
    }
}
