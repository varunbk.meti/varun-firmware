#include <Arduino.h>
#include <ArduinoJson.h>
#include <ESPAsyncWebServer.h>
#include <SPIFFS.h>
#include <main.h>
#include <meti_server.h>
#include <meti_wifi.h>
#include <config_reader.h>
#include <meti_nvs.h>
#include <meti_net_led.h>

bool file_send_inprogress = false;

void setup_server(void)
{
	/* Enable ESP Server function and start webserver */
	server.on("/", HTTP_GET, [](AsyncWebServerRequest *request) {
		server_handle_root(request);
		// server_update_Wifi_config(request);
	});

	server.on("/updateSrmConfig", HTTP_POST, [](AsyncWebServerRequest *request) {
		server_update_config(request);
	});

	server.on("/updateWifiConfig", HTTP_POST, [](AsyncWebServerRequest *request) {
		server_update_Wifi_config(request);
	});

	server.on("/getDeviceConfig", HTTP_POST, [](AsyncWebServerRequest *request) {
		server_get_device_config(request);
	});

	server.on("/resetDevice", HTTP_POST, [](AsyncWebServerRequest *request) {
		server_reset_device(request);
	});

	server.on("/getWifiDevices", HTTP_POST, [](AsyncWebServerRequest *request) {
		meti_wifi_scan_networks(request);
	});

	server.on("/factoryReset", HTTP_POST, [](AsyncWebServerRequest *request) {
		server_factory_reset(request);
	});

	server.onNotFound([](AsyncWebServerRequest *request) {
		if (!server_handle_file_read(request))
		{
			request->send(404, "text/plain", "404: Not Found"); // otherwise, respond with a 404 (Not Found) error
		}
		else
		{
			// Narayan - change this, put this function separately...
		}
	});

	server.begin();
}

bool server_handle_file_read(AsyncWebServerRequest *request)
{ // send the right file to the client (if it exists)
	String path = request->url().c_str();

	if (path.endsWith("/"))
		path += "views/index.html";				 // If a folder is requested, send the index file
	String contentType = get_content_type(path); // Get the MIME type
	String pathWithGz = path + ".gz";
	if (SPIFFS.exists(pathWithGz) || SPIFFS.exists(path))
	{ // If the file exists, either as a compressed archive, or normal

		if (SPIFFS.exists(pathWithGz))
		{				   // If there's a compressed version available
			path += ".gz"; // Use the compressed version

			AsyncWebServerResponse *response = request->beginResponse(SPIFFS, path, contentType, false);
			if (response != NULL)
			{
				response->addHeader("Content-encoding", "gzip");
				file_send_inprogress = true;
				request->send(response);
				file_send_inprogress = false;
			}
		}
		else
		{
			g_pLogger->Write(LogLevel::Info, "server_handle_file_read", "Sent file : %s", path.c_str());
			// delay(10);
			AsyncWebServerResponse *response = request->beginResponse(SPIFFS, path, contentType, false);
			if (response != NULL)
			{
				file_send_inprogress = true;
				request->send(response);
				file_send_inprogress = false;
			}
		}

		return true;
	}
	else
	{
		g_pLogger->Write(LogLevel::Error, "server_handle_file_read", "File Not Found: %s", path.c_str());
		return false; // If the file doesn't exist, return false
	}
}

String get_content_type(String filename)
{
	if (filename.endsWith(".htm"))
		return "text/html";
	else if (filename.endsWith(".html"))
		return "text/html";
	else if (filename.endsWith(".css"))
		return "text/css";
	else if (filename.endsWith(".js"))
		return "application/javascript";
	else if (filename.endsWith(".png"))
		return "image/png";
	else if (filename.endsWith(".gif"))
		return "image/gif";
	else if (filename.endsWith(".jpg"))
		return "image/jpeg";
	else if (filename.endsWith(".ico"))
		return "image/x-icon";
	else if (filename.endsWith(".xml"))
		return "text/xml";
	else if (filename.endsWith(".pdf"))
		return "application/x-pdf";
	else if (filename.endsWith(".zip"))
		return "application/x-zip";
	else if (filename.endsWith(".gz"))
		return "application/x-gzip";
	return "text/plain";
}

// This function funtions returns true  in AP mode
bool check_mode()
{
	// Narayan - remove this line in production...
	// return true;

	if (WiFi.getMode() == WIFI_MODE_AP)
	{
		return true;
	}
	else
	{
		return false;
	}
}

void parse_bytes(const char *str, char sep, byte *bytes, int maxBytes, int base)
{
	for (int i = 0; i < maxBytes; i++)
	{
		bytes[i] = strtoul(str, NULL, base); // Convert byte
		str = strchr(str, sep);				 // Find next separator
		if (str == NULL || *str == '\0')
		{
			break; // No more separators, exit
		}
		str++; // Point to next character after separator
	}
}

/* This handling root file */
void server_handle_root(AsyncWebServerRequest *request)
{
	String path = "/views/index.html";
	String contentType = "text/html";

	if (SPIFFS.exists(path))
	{
		file_send_inprogress = true;
		request->send(SPIFFS, path, contentType);
		file_send_inprogress = false;
	}
	else
	{
		request->send(200, "text/plain", "index.html not found...");
	}
}

void server_update_config(AsyncWebServerRequest *request)
{
	// allocate the memory for the document
	const size_t CAPACITY = JSON_OBJECT_SIZE(2);
	StaticJsonDocument<CAPACITY> doc;
	String response;
	String prtStr = "";

	// create an object
	JsonObject retVal = doc.to<JsonObject>();
	retVal["result"] = 0;
	retVal["message"] = "Unknown response...";

	// Check if all arguments are presents,
	// else return...
	if (request->hasArg("srm_config"))
	{
		g_pLogger->Write(LogLevel::Info, "server_update_config", "srm_config: %s", request->arg("srm_config").c_str());
	}
	else
	{
		retVal["result"] = 0;
		retVal["message"] = "Invalid parameters....";

		serializeJson(retVal, response);
		request->send(200, "text/json", response);
		return;
	}

	// if request from station mode, do not configure...
	if (check_mode() == true)
	{
		File file = SPIFFS.open("/config/srm_config.json", "w");

		if (!file)
		{
			//Narayan - Handle non availability of said file here...
			retVal["result"] = 0;
			retVal["message"] = "Error opening file for writing";
		}

		int bytesWritten = file.print(request->arg("srm_config"));
		file.close();

		if (bytesWritten > 0)
		{
			retVal["result"] = 1;
			retVal["message"] = "SRM - Configuration Saved...";
		}
		else
		{
			retVal["result"] = 0;
			retVal["message"] = "SRM - File write failed...";
		}
	}
	else
	{
		retVal["result"] = 0;
		retVal["message"] = "Unable to configure in station mode....";
	}

	serializeJson(retVal, response);
	g_pLogger->Write(LogLevel::Info, "server_update_config", "response: %s", response.c_str());

	request->send(200, "text/json", response);
}

void server_update_Wifi_config(AsyncWebServerRequest *request)
{
	// allocate the memory for the document
	const size_t CAPACITY = JSON_OBJECT_SIZE(2);
	StaticJsonDocument<CAPACITY> doc;
	String response;
	String prtStr = "";

	// create an object
	JsonObject retVal = doc.to<JsonObject>();
	retVal["result"] = 0;
	retVal["message"] = "Unknown response...";

	// Check if all arguments are presents,
	// else return...
	if (request->hasArg("wifi_config"))
	{
		g_pLogger->Write(LogLevel::Info, "server_update_Wifi_config", "wifi_config: %s", request->arg("wifi_config").c_str());
	}
	else
	{
		retVal["result"] = 0;
		retVal["message"] = "Invalid parameters....";

		serializeJson(retVal, response);
		request->send(200, "text/json", response);
		return;
	}

	// if request from station mode, do not configure...
	if (check_mode() == true)
	{
		if (request->arg("wifi_test").equals("1"))
		{
			deserializeJson(wifi_doc, request->arg("wifi_config"));
			// Start the wifi test mode
			wifi_test_mode_status = WIFI_TEST_MODE_START;

			retVal["result"] = 1;
			retVal["message"] = "Wifi testing initiated...";
		}
		else
		{
			File file = SPIFFS.open("/config/wifi.json", "w");

			if (!file)
			{
				//Narayan - Handle non availability of said file here...
				retVal["result"] = 0;
				retVal["message"] = "Wifi - Error opening file for writing";
			}

			int bytesWritten = file.print(request->arg("wifi_config"));

			if (bytesWritten > 0)
			{
				retVal["result"] = 1;
				retVal["message"] = "Wifi configuration Saved...";
			}
			else
			{
				retVal["result"] = 1;
				retVal["message"] = "Wifi - File write failed...";
			}
			file.close();
		}
	}
	else
	{
		retVal["result"] = 1;
		retVal["message"] = "Unable to configure in station mode...";
	}

	serializeJson(retVal, response);
	g_pLogger->Write(LogLevel::Info, "server_update_Wifi_config", "response: %s", request->arg("wifi_config").c_str());

	request->send(200, "text/json", response);
}

void server_get_device_config(AsyncWebServerRequest *request)
{
	String deviceConfig;
	serializeJson(device_doc, deviceConfig);
	request->send(200, "text/json", deviceConfig);
}

void server_reset_device(AsyncWebServerRequest *request)
{
	// allocate the memory for the document
	const size_t CAPACITY = JSON_OBJECT_SIZE(2);
	StaticJsonDocument<CAPACITY> doc;
	String response;

	// create an object
	JsonObject retVal = doc.to<JsonObject>();
	retVal["result"] = 0;
	retVal["message"] = "Device is being restarted..";

	serializeJson(retVal, response);
	request->send(200, "text/json", response);

	g_pLogger->Write(LogLevel::Warn, "server_reset_device", "Device is being restarted by client..");

	//params.drc++;
	//meti_nvs_commit_val("params", "drc", params.drc);

	//user restart from front end, restart in station mode...
	//meti_nvs_commit_val("params", "forced_ap", FORCED_AP_NVS_FALSE);

	//restart device
	meti_device_restart();
}

void server_factory_reset(AsyncWebServerRequest *request)
{
	// allocate the memory for the document
	const size_t CAPACITY = JSON_OBJECT_SIZE(2);
	StaticJsonDocument<CAPACITY> doc;
	String response;

	// create an object
	JsonObject retVal = doc.to<JsonObject>();

	if (check_mode() == true)
	{
		retVal["result"] = 0;
		retVal["message"] = "Device is being restarted..";
	}
	else
	{
		retVal["result"] = 1;
		retVal["message"] = "Unable to factory reset in station mode...";
	}

	serializeJson(retVal, response);
	request->send(200, "text/json", response);

	if (retVal["result"] == 1)
	{
		return;
	}

	delay(1000);
	//factory reset
	// narayan - start here...
	// meti_device_restart();
}
