#include <Arduino.h>
#include <ArduinoJson.h>
#include <ESPAsyncWebServer.h>
#include <SPIFFS.h>
#include <config_reader.h>
#include <WiFi.h>
#include <meti_wifi.h>
#include <led.h>
#include <main.h>
#include <meti_utils.h>
#include <meti_net_led.h>
#include <meti_srm.h>
#include <meti_nvs.h>
#include <meti_server.h>

#define METI_WIFI_DISCONNECTED 0
#define METI_WIFI_CONNECTED 1

IPAddress local_IP;
IPAddress gateway;
IPAddress subnet;

bool forced_ap_mode = false;
int forced_ap_mode_exit_ctr = FORCED_AP_MODE_EXIT_TIMEOUT;
int forced_ap_mode_config_file_access_ctr = FORCED_AP_MODE_NO_CONFIG_TIMEOUT;
bool wifi_internet_ok = false;
bool wifi_got_ip = false;
bool wifi_store_ip = false;
bool wifi_restart = false;
bool wifi_config_npt = false;
bool wifi_first_loop = true;

uint8_t wifi_test_mode_status = WIFI_TEST_MODE_OFF;
meti_wifi_scan_status_t wifi_scan_status = METI_WIFI_SCAN_IDLE;

//This function starts the wifi either in AP or Station mode
//based on the conditions
void meti_wifi_start(void)
{
    // delete old config
    WiFi.disconnect();
    delay(1000);
    WiFi.onEvent(meti_wifi_event);

    bool wf_sta_enabled = wifi_doc["sta_enabled"];
    //if station mode is not enabled, goto ap mode directly
    if (wf_sta_enabled == true)
    {
        //read sta_enable_pin is held by the user during boot
        //get into forced_soft_ap mode

        int sta_enable_pin = digitalRead(LED_WIFI_PIN);
        // int sta_enable_pin = false; //Narayan - testing

        if (sta_enable_pin == false || params.forced_ap == FORCED_AP_NVS_TRUE)
        {
            //entering forced ap mode...
            forced_ap_mode = true;
            forced_ap_mode_exit_ctr = FORCED_AP_MODE_EXIT_TIMEOUT;
            forced_ap_mode_config_file_access_ctr = FORCED_AP_MODE_NO_CONFIG_TIMEOUT;

            g_pLogger->Write(LogLevel::Info, "meti_wifi_start", "Restarting in Forced AP mode...");

            //store to NVS only entering thorugh switch press...
            if (params.forced_ap == FORCED_AP_NVS_FALSE)
            {
                //Narayan
                //meti_nvs_commit_val("params", "forced_ap", FORCED_AP_NVS_TRUE);
            }
            setup_soft_ap(true);
        }
        else
        {
            setup_wifi_station(true);
        }
    }
    else
    {
        //if user has chosen AP only mode..
        setup_soft_ap(true);
    }
}

void setup_soft_ap(bool serverStart)
{
    set_led_state(MULTI_LED_STATE_RED_BLINK_FAST);

    const char *dev_id = device_doc["dev_id"];

    local_IP.fromString(ACCESS_POINT_IP);
    gateway.fromString(ACCESS_POINT_GATEWAY);
    subnet.fromString(ACCESS_POINT_SUBNET);

    WiFi.disconnect();

    WiFi.mode(WIFI_AP);

    /* start soft access point connections */
    /* Important softAP has to be called before softAPConfig, else system will crash */
    //Narayan - ACCESS_POINT_PASSWORD should be configurable
    WiFi.softAP(dev_id, ACCESS_POINT_PASSWORD);
    /* Important after softAPConfig, a delay is required, else system will crash */

    delay(50);
    WiFi.softAPConfig(local_IP, gateway, subnet);

    g_pLogger->Write(LogLevel::Info, "setup_soft_ap", "Setting up AP mode : %s", WiFi.softAPIP().toString().c_str());

    if (serverStart == true)
    {
        delay(10);
        setup_server();
    }
}

void setup_wifi_station(bool serverStart)
{
    String wifi_config;
    set_led_state(MULTI_LED_STATE_BLUE_BLINK_FAST);
    g_pLogger->Write(LogLevel::Info, "setup_wifi_station", "Settting up WIFI in STATION mode...");

    serializeJson(wifi_doc, wifi_config);

    g_pLogger->Write(LogLevel::Info, "setup_wifi_station", "%s", wifi_config.c_str());

    JsonObject wf_w1 = wifi_doc["w1"];

    const char *wf_ssid = wf_w1["ssid"]; // "JioFi_102D129"
    const char *wf_pw = wf_w1["pw"];     // ""

    JsonObject wf_w1_static = wifi_doc["w1"]["static"];

    g_pLogger->Write(LogLevel::Info, "setup_wifi_station", "%s %s", wf_ssid, wf_pw);

    if (wf_w1_static["en"] == true)
    {
        g_pLogger->Write(LogLevel::Info, "setup_wifi_station", "Setting up Static IP...");

        const char *static_ip = wf_w1_static["ip"];
        const char *static_gw = wf_w1_static["gw"];
        const char *static_sn = wf_w1_static["sn"];
        const char *static_dns = wf_w1_static["dns"];

        IPAddress IP_ip;
        IPAddress IP_gw;
        IPAddress IP_sn;
        IPAddress IP_dns;
        IP_ip.fromString(static_ip);
        IP_gw.fromString(static_gw);
        IP_sn.fromString(static_sn);
        IP_dns.fromString(static_dns);

        if (!WiFi.config(IP_ip, IP_gw, IP_sn, IP_dns))
        {
            g_pLogger->Write(LogLevel::Error, "setup_wifi_station", "WIFI STA Failed to configure !!!");
        }
        device_doc["wf_ip_static"] = true;
    }
    else
    {
        device_doc["wf_ip_static"] = false;
    }

    WiFi.disconnect();
    WiFi.mode(WIFI_STA);
    WiFi.begin(wf_ssid, wf_pw); // Connect to the network

    g_pLogger->Write(LogLevel::Info, "setup_wifi_station", "Attempting to connect to WiFi network : %s", wf_ssid);

    if (serverStart == true)
    {
        setup_server();
    }
}

void wifi_station_display_once(void)
{
    static bool display = true;
    if (display == true)
    {
        if (WiFi.status() == WL_CONNECTED)
        {
            display = false;
        }
    }
}

// Narayan - test this function again...
float wifi_rssi_convert(float RssI)
{
    RssI = isnan(RssI) ? -100.0 : RssI;
    RssI = min(max(2 * (RssI + 100.0), 0.0), 100.0);

    return RssI;
}

void update_device_wifi_params(void)    
{
    device_doc["wf_mode"] = meti_wifi_get_mode_text();
    float rssi = get_rssi_avg();
    device_doc["wf_rssi"] = wifi_rssi_convert(rssi);

    // unsigned long time1 = millis();
    if (WiFi.getMode() == WIFI_STA)
    { //Station Mode
        device_doc["wf_conn_ssid"] = WiFi.SSID();
        device_doc["wf_status"] = meti_wifi_get_status_text();
        device_doc["wf_int_status"] = "GOK";
        device_doc["wf_conn_devs"] = 0;
    }
    else
    { //AP MODE
        // JsonObject wf_w1 = wifi_doc["w1"];
        const char *wf_ssid = wifi_doc["w1"]["ssid"];

        //In AP and APF mode, show  previously connected STA mode ssid
        device_doc["wf_conn_ssid"] = wf_ssid;
        device_doc["wf_status"] = WL_CONNECTED;
        device_doc["wf_int_status"] = "GOK";
        device_doc["wf_conn_devs"] = WiFi.softAPgetStationNum();
    }
}

void check_forced_ap_mode_entry(void)
{
    static int sta_enable_ctr = FORCED_AP_MODE_RESET_TIMEOUT;
    int sta_enable_pin = digitalRead(FORCED_AP_MODE_SWITCH);

    if (sta_enable_pin == false)
    {
        //if sta_enable_pin is pressed (active low)
        sta_enable_ctr--;
        if (sta_enable_ctr <= 0)
        {
            g_pLogger->Write(LogLevel::Info, "check_forced_ap_mode_entry", "Forced AP Mode - Resetting device...\n\n\n\n\n");

            // increment the device restart count....
            // Narayan
            // params.drc++;
            // meti_nvs_commit_val("params", "drc", params.drc);
            // meti_nvs_commit_val("params", "forced_ap", FORCED_AP_NVS_FALSE);

            //restart device
            meti_device_restart();
        }
    }
    else
    {
        sta_enable_ctr = FORCED_AP_MODE_RESET_TIMEOUT;
    }
}

void forced_ap_mode_timeout(void)
{
    if (forced_ap_mode == false)
    {
        return;
    }

    //check if there are any devices connected
    // JsonObject wf = device_doc["wf"];
    int wf_conn_devs = device_doc["wf_conn_devs"];

    if (wf_conn_devs == 0)
    {
        if (forced_ap_mode_exit_ctr-- <= 0)
        {
            g_pLogger->Write(LogLevel::Warn, "forced_ap_mode_timeout", "Restarting device...\n\n\n");

            //Narayan
            // meti_nvs_commit_val("params", "forced_ap", FORCED_AP_NVS_FALSE);

            // params.drc++;
            // meti_nvs_commit_val("params", "drc", params.drc);
            //restart device
            meti_device_restart();
        }
    }
    else
    {
        forced_ap_mode_exit_ctr = FORCED_AP_MODE_EXIT_TIMEOUT;

        if (forced_ap_mode_config_file_access_ctr-- <= 0)
        {
            g_pLogger->Write(LogLevel::Warn, "forced_ap_mode_timeout", "\n\n\nNo config page accessed... restarting device...\n\n\n");

            //Narayan
            // meti_nvs_commit_val("params", "forced_ap", FORCED_AP_NVS_FALSE);

            // params.drc++;
            // meti_nvs_commit_val("params", "drc", params.drc);
            //restart device
            meti_device_restart();
        }
    }
}

void meti_wifi_event(WiFiEvent_t event)
{
    switch (event)
    {
    case SYSTEM_EVENT_STA_CONNECTED:
        g_pLogger->Write(LogLevel::Info, "wifi_event", "Device WiFi got connected to external Router");
        break;
    case SYSTEM_EVENT_STA_DISCONNECTED:
        set_led_state(MULTI_LED_STATE_BLUE_BLINK_FAST);
        g_pLogger->Write(LogLevel::Warn, "wifi_event", "Device lost WiFi connection");
        wifi_got_ip = false;
        wifi_internet_ok = false;
        break;
    case SYSTEM_EVENT_STA_AUTHMODE_CHANGE:
        g_pLogger->Write(LogLevel::Info, "wifi_event", "Auth mode of exernal Router changed");
        break;
    case SYSTEM_EVENT_STA_GOT_IP:
        set_led_state(MULTI_LED_STATE_BLUE_ON);
        g_pLogger->Write(LogLevel::Info, "wifi_event", "Device WiFi got IP from external Router : %s", WiFi.localIP().toString().c_str());

        wifi_got_ip = true;
        wifi_store_ip = true;
        
        //trigger one ping immediately after got ip
        ui8PingIntervalCounter = ui8PingInterval;

        if (wifi_first_loop == true)
        {
            wifi_first_loop = false;
            wifi_config_npt = true;
        }
        // led_set_mode(LED_WIFI, LED_MODE_ON);
        break;

    case SYSTEM_EVENT_AP_STACONNECTED:
        set_led_state(MULTI_LED_STATE_RED_BLINK_SLOW);
        g_pLogger->Write(LogLevel::Info, "wifi_event", "External device connected to this Access point");
        break;
    case SYSTEM_EVENT_AP_STADISCONNECTED:
        if (WiFi.softAPgetStationNum() == 0)
        {
            set_led_state(MULTI_LED_STATE_RED_BLINK_FAST);
        }
        g_pLogger->Write(LogLevel::Info, "wifi_event", "External device disconnected from this Access point");
        break;
    case SYSTEM_EVENT_SCAN_DONE:
        g_pLogger->Write(LogLevel::Info, "wifi_event", "Wifi Scan Completed...");
        break;
    case SYSTEM_EVENT_STA_START:
    case SYSTEM_EVENT_WIFI_READY:
    case SYSTEM_EVENT_STA_STOP:
    case SYSTEM_EVENT_STA_LOST_IP:
    case SYSTEM_EVENT_STA_WPS_ER_SUCCESS:
    case SYSTEM_EVENT_STA_WPS_ER_FAILED:
    case SYSTEM_EVENT_STA_WPS_ER_TIMEOUT:
    case SYSTEM_EVENT_STA_WPS_ER_PIN:
    case SYSTEM_EVENT_STA_WPS_ER_PBC_OVERLAP:
    case SYSTEM_EVENT_AP_START:
    case SYSTEM_EVENT_AP_STOP:
    case SYSTEM_EVENT_AP_STAIPASSIGNED:
    case SYSTEM_EVENT_AP_PROBEREQRECVED:
    case SYSTEM_EVENT_GOT_IP6:
    case SYSTEM_EVENT_ETH_START:
    case SYSTEM_EVENT_ETH_STOP:
    case SYSTEM_EVENT_ETH_CONNECTED:
    case SYSTEM_EVENT_ETH_DISCONNECTED:
    case SYSTEM_EVENT_ETH_GOT_IP:
        break;
    case SYSTEM_EVENT_MAX:
        g_pLogger->Write(LogLevel::Info, "wifi_event", "SYSTEM_EVENT_MAX");
        break;
    }
}

String meti_wifi_get_mode_text(void)
{
    String retVal = "Unknown";

    // WIFI_OFF = 0, WIFI_STA = 1, WIFI_AP = 2, WIFI_AP_STA = 3,
    uint8_t mode = WiFi.getMode();

    switch (mode)
    {
    case WIFI_OFF:
        retVal = "WIFI OFF";
        break;

    case WIFI_STA:
        retVal = "STATION MODE";
        break;

    case WIFI_AP:
        if (forced_ap_mode == true)
        {
            retVal = "FORCED ACCESS POINT";
        }
        else
        {
            retVal = "ACCESS POINT";
        }
        break;

    case WIFI_AP_STA:
        retVal = "WIFI_AP_STA";
        break;
    }

    return retVal;
}

String meti_wifi_get_status_text(void)
{
    String retVal = "Unknown";
    wl_status_t staus = WiFi.status();

    switch (staus)
    {
    case WL_NO_SHIELD:
        retVal = "WL_NO_SHIELD";
        break;
    case WL_IDLE_STATUS:
        retVal = "WL_IDLE_STATUS";
        break;
    case WL_NO_SSID_AVAIL:
        retVal = "WL_NO_SSID_AVAIL";
        break;
    case WL_SCAN_COMPLETED:
        retVal = "WL_SCAN_COMPLETED";
        break;
    case WL_CONNECTED:
        retVal = "WL_CONNECTED";
        break;
    case WL_CONNECT_FAILED:
        retVal = "WL_CONNECT_FAILED";
        break;
    case WL_CONNECTION_LOST:
        retVal = "WL_CONNECTION_LOST";
        break;
    case WL_DISCONNECTED:
        retVal = "WL_DISCONNECTED";
        break;
    }
    return retVal;
}

void store_sta_ip_address(void)
{
    String device_str;

    if (wifi_got_ip == true)
    {
        if (wifi_store_ip == true)
        {
            wifi_store_ip = false;
            String new_ip = WiFi.localIP().toString();

            const char *wf_ip_addr = device_doc["wf_ip_addr"];

            if (!(new_ip.equals(wf_ip_addr)))
            {
                wifi_write_ip_address_to_file(new_ip, false);
            }
            else
            {
                g_pLogger->Write(LogLevel::Info, "store_sta_ip", "Old IP, skip storage : %s", new_ip.c_str());
            }
        }
    }
}

void meti_wifi_scan_networks(AsyncWebServerRequest *request)
{
    const size_t capacity = JSON_ARRAY_SIZE(3) + (SCAN_DEVICES_MAX * JSON_OBJECT_SIZE(3));
    DynamicJsonDocument doc(capacity + 1024);
    String response;
    wifi_mode_t wifi_curr_mode;

    if (request->hasArg("start_scan") == false)
    {
        doc["result"] = 0;
        doc["message"] = "Invalid parameters....";
        serializeJson(doc, response);
        request->send(200, "text/json", response);
        return;
    }

    g_pLogger->Write(LogLevel::Info, "wifi_scan_networks", "Wifi Scan Start....");

    wifi_curr_mode = WiFi.getMode();

    wifi_scan_status = METI_WIFI_SCAN_IN_PROGRESS;
    // WiFi.scanNetworks will return the number of networks found
    int found_networks = WiFi.scanNetworks();

    if (found_networks == WIFI_SCAN_FAILED)
    {
        doc["result"] = 0;
        doc["message"] = "Scan failed....";
    }
    else if (found_networks == 0)
    {
        doc["result"] = 0;
        doc["message"] = "No WiFi networks found..";
    }
    else
    {
        if (found_networks > SCAN_DEVICES_MAX)
        {
            found_networks = SCAN_DEVICES_MAX;
        }

        doc["result"] = 1;
        doc["message"] = "Wifi routers found...";
        doc["found_networks"] = found_networks;

        JsonArray list = doc.createNestedArray("list");

        for (uint8_t i = 0; i < found_networks; i++)
        {
            JsonObject listx = list.createNestedObject();

            listx["ssid"] = WiFi.SSID(i);
            listx["rssi"] = wifi_rssi_convert(WiFi.RSSI(i));
            listx["encryption"] = meti_wifi_get_encryption_type(i);
            delay(10);
        }
    }

    // if (wifi_curr_mode == WIFI_MODE_STA)
    // {
    //     wifi_scan_status = METI_WIFI_SCAN_DONE;
    // }
    // else
    // {
    //     WiFi.mode(wifi_curr_mode);
    //     wifi_scan_status = METI_WIFI_SCAN_IDLE;
    // }

    WiFi.mode(wifi_curr_mode);
    // wifi_scan_status = METI_WIFI_SCAN_IDLE;

    serializeJson(doc, response);
    request->send(200, "text/json", response);
}

String meti_wifi_get_encryption_type(uint8_t i)
{
    String retVal = "Unknown";
    wifi_auth_mode_t encryptionType = WiFi.encryptionType(i);

    switch (encryptionType)
    {
    case WIFI_AUTH_OPEN:
        retVal = "OPEN";
        break;
    case WIFI_AUTH_WEP:
        retVal = "WEP";
        break;
    case WIFI_AUTH_WPA_PSK:
        retVal = "WPA_PSK";
        break;
    case WIFI_AUTH_WPA2_PSK:
        retVal = "WPA2_PSK";
        break;
    case WIFI_AUTH_WPA_WPA2_PSK:
        retVal = "WPA_WPA2_PSK";
        break;
    case WIFI_AUTH_WPA2_ENTERPRISE:
        retVal = "WPA2_ENTERPRISE";
        break;
    case WIFI_AUTH_MAX:
        retVal = "Unknown";
        break;
    }
    return retVal;
}

void wifi_check_status(void)
{
    static uint8_t conn_no_ip_ctr = 0;
    static uint8_t wifi_disconnected_ctr = 0;

    if (WiFi.status() == WL_CONNECTED && wifi_got_ip == false)
    {
        conn_no_ip_ctr++;
        if (conn_no_ip_ctr > 10)
        {
            conn_no_ip_ctr = 0;
            g_pLogger->Write(LogLevel::Error, "wifi_check_status", "Wifi connected without IP address, reconnecting.....!!!");
            WiFi.reconnect();
        }
    }
    else
    {
        conn_no_ip_ctr = 0;
    }

    //If the device gets disconnected from network, retry after 15 seconds
    if (WiFi.status() == WL_DISCONNECTED || WiFi.status() == WL_IDLE_STATUS)
    {
        wifi_disconnected_ctr++;
        if (wifi_disconnected_ctr > 15)
        {
            wifi_disconnected_ctr = 0;
            g_pLogger->Write(LogLevel::Error, "wifi_check_status", "Wifi disconnected, reconnecting to wifi !!!\n\n\n");
            WiFi.reconnect();
        }
    }
}

float get_rssi_avg(void)
{
    float rssi = 0;

    for (size_t i = 0; i < 10; i++)
    {
        rssi = rssi + WiFi.RSSI();
    }
    rssi = rssi / 10;

    return rssi;
}

void set_rssi_led(void)
{
    float rssi = device_doc["wf_rssi"];
    // rssi = get_rssi_avg();
    // rssi = wifi_rssi_convert();

    if (rssi < 50)
    {
        set_led_state(MULTI_LED_STATE_GREEN_BLINK_FAST);
    }
    else if (rssi < 75)
    {
        set_led_state(MULTI_LED_STATE_GREEN_BLINK_SLOW);
    }
    else
    {
        set_led_state(MULTI_LED_STATE_GREEN_ON);
    }
}

void useless_code(void)
{
    static uint8_t wifi_scan_reconnect_ctr = 0;

    //Delay required before restart (after scan) else system will crash...
    switch (wifi_scan_status)
    {
    case METI_WIFI_SCAN_IDLE:
    case METI_WIFI_SCAN_IN_PROGRESS:
        //do nothing...
        break;

    case METI_WIFI_SCAN_DONE:
        if (forced_ap_mode == false && wifi_doc["sta_enabled"] == true)
        {
            wifi_scan_status = METI_WIFI_SCAN_RECONNECT;
            wifi_scan_reconnect_ctr = 0;
        }
        break;

    case METI_WIFI_SCAN_RECONNECT:
        wifi_scan_reconnect_ctr++;
        if (wifi_scan_reconnect_ctr >= 1)
        {
            wifi_scan_status = METI_WIFI_SCAN_IDLE;
            g_pLogger->Write(LogLevel::Info, "wifi_check_status", "Wifi scan completed, reconnecting to wifi !!!");
            WiFi.reconnect();
        }
    }
}

void wifi_test_mode(void)
{
    static uint16_t wait_ctr = 15;
    static uint8_t wait_ctr_multiplier = 10;
    
    static uint8_t wifi_test_mode_status_prev = 0xFF;

    if (wifi_test_mode_status != wifi_test_mode_status_prev) {
        g_pLogger->Write(LogLevel::Info, "wifi_test_mode", " %d", wifi_test_mode_status);
        wifi_test_mode_status_prev = wifi_test_mode_status;
    }

    switch (wifi_test_mode_status)
    {
    case WIFI_TEST_MODE_START:
        wifi_got_ip = false;
        setup_wifi_station(false);
        wait_ctr = 15 * wait_ctr_multiplier; //called every 100ms
        wifi_test_mode_status = WIFI_TEST_MODE_WAIT_FOR_CONNECT;
        break;

    case WIFI_TEST_MODE_WAIT_FOR_CONNECT:
        if (wifi_got_ip == true)
        {
            wait_ctr = 2 * wait_ctr_multiplier;
            wifi_test_mode_status = WIFI_TEST_MODE_WAIT_FOR_IP_DETAILS;
        }
        else if (wait_ctr-- == 0)
        {
            wifi_write_ip_address_to_file("0.0.0.0", true);
            wifi_test_mode_status = WIFI_TEST_MODE_DONE;
        }
        break;
    case WIFI_TEST_MODE_WAIT_FOR_IP_DETAILS:
        if (wait_ctr-- == 0)
        {
            wifi_write_ip_address_to_file(WiFi.localIP().toString(), false);
            wifi_test_mode_status = WIFI_TEST_MODE_DONE;
        }
        break;

    case WIFI_TEST_MODE_DONE:
        setup_soft_ap(true);
        wifi_test_mode_status = WIFI_TEST_MODE_OFF;
        break;

    case WIFI_TEST_MODE_OFF:
        break;

    default:
        wifi_test_mode_status = WIFI_TEST_MODE_OFF;
        break;
    }
}

void wifi_write_ip_address_to_file(String new_ip, bool reset_ip)
{
    String device_str;

    g_pLogger->Write(LogLevel::Info, "write_sta_ip", "Stored new IP address : %s", new_ip.c_str());

    if (reset_ip == true)
    {
        device_doc["wf_ip_addr"] = new_ip;
        device_doc["wf_conn_ssid"] = "Connection failed!!";
        device_doc["wf_gw"] = "0.0.0.0";
        device_doc["wf_sn"] = "0.0.0.0";
        device_doc["wf_dns"] = "0.0.0.0";
    }
    else
    {
        device_doc["wf_ip_addr"] = new_ip;
        device_doc["wf_conn_ssid"] = WiFi.SSID();
        device_doc["wf_gw"] = WiFi.gatewayIP().toString();
        device_doc["wf_sn"] = WiFi.subnetMask().toString();
        device_doc["wf_dns"] = WiFi.dnsIP().toString();
    }

    serializeJson(device_doc, device_str);
    File file = SPIFFS.open("/config/device.json", "w");
    file.print(device_str);
    file.close();

    wifi_store_ip = false;
}