/*******************************************************************************
 * Project Name: METI Smart Router Manager
 *
 * Version: 1.1
 *
 * Description:
 * In this project ESP32 handles web pages using SPIFFS
 * Owner: METI M2M India Pvt Ltd.,
 *
 *
 ********************************************************************************
 * Copyright (2020-21) , METI
 *******************************************************************************/

/* include headers */
#include <Arduino.h>
#include <WiFi.h>
#include <AsyncTCP.h>
#include <ESPAsyncWebServer.h>
#include <SPIFFS.h>
#include <ArduinoJson.h>
#include <config_reader.h>
#include <main.h>
#include <meti_wifi.h>
#include <meti_server.h>
#include <meti_utils.h>
#include <timers.h>
#include <led.h>
#include <meti_srm.h>
#include <stdio.h>
#include <main.h>
#include <logger.h>
#include <meti_time.h>
#include <esp_event_legacy.h>
#include <indicator.h>
#include <indicatortype.h>
#include <blinktype.h>
#include <meti_net_led.h>
#include <meti_mqtt.h>
#include <meti_nvs.h>
#include <esp_task_wdt.h>
#include <cdate.h>
#include <gen2wifi.h>

#define WDT_TIMEOUT 10

Logger *g_pLogger = NULL;
BlinkConfig *g_pBlinkConfig = NULL;
Indicator *g_pIndicators = NULL;

/* webserver object on port 80 */
AsyncWebServer server(80);

struct gen2_config cfg_gen2;

void setup()
{
	delay(100);
	Serial.end();
	Serial.begin(115200);

	// Use LogLevel::Info in production....Trace/Debug in testing
	LogLevel logLev = LogLevel::Debug;
	if (logLev.GetCode() <= LogLevel::Debug)
	{
		delay(1000);
	}

	g_pLogger = new Logger(logLev);
	g_pLogger->Write(LogLevel::Info, "setup", "METI Smart Router Manager Start...");

	start_gen2wifi();

	InitializeIndicators();
	InitializeBlinkProcess();
	initialize_net_led();
	set_led_state(MULTI_LED_STATE_RED_ON);

	start_file_storage();

	read_srm_config_file();
	read_device_file();
	read_wifi_file();
	// read_params_file();

#ifdef ENABLE_NVS
	meti_nvs_initialize();
	meti_nvs_read_all("params");
#endif

	setup_device_params();

	// NARAYAN - port fix (externally pulled up)
	pinMode(FORCED_AP_MODE_SWITCH, INPUT_PULLUP);

	timer_start();

	meti_wifi_start();

	// router_start();

	// setup_server();
#ifdef ENABLE_MQTT
	mqtt_setup();
	// MQTT CONNECT will happen after WIFI connection is established...
#endif

	// Enable watch dog
	esp_task_wdt_init(WDT_TIMEOUT, true); // enable panic so ESP32 restarts
	esp_task_wdt_add(NULL);				  // add current thread to WDT watch

	// Time setup
	// mtime_init() will be call after wifi is initialized and IP received...
}

void loop()
{
	// put your main code here, to run repeatedly:
	timer_check();

	// clear watchdog timer
	esp_task_wdt_reset();

	// gen2.activity(wifi_internet_ok);
	gen2.activity(1);
}

void start_gen2wifi(void)
{
	/* set gen2 configuration */
	cfg_gen2.cfg_uart.port = port1;
	cfg_gen2.cfg_uart.baudrate = 115200;
	cfg_gen2.cfg_uart.data_bits = databits8;
	cfg_gen2.cfg_uart.flow_ctrl = 0;
	cfg_gen2.cfg_uart.parity = none;
	cfg_gen2.cfg_uart.internlrxbufsize = 250;
	cfg_gen2.cfg_uart.rxpin = 16;
	cfg_gen2.cfg_uart.txpin = 17;
	cfg_gen2.cfg_uart.stop_bits = stopbits1;

	cfg_gen2.servertimeout = 1000;
	cfg_gen2.statuspin = 22;

	/* start gen2 module */
	gen2.start(cfg_gen2);

	Serial1.println("test");
}

void start_file_storage(void)
{
	/* Initialize SPIFFS */
	if (!SPIFFS.begin())
	{
		g_pLogger->Write(LogLevel::Fatal, "start_file_storage", "An Error has occurred while mounting SPIFFS !!");

		while (1)
			; // stop
	}
}

void timer_timeout_1ms(void)
{
	// 1ms Tasks
}

void timer_timeout_10ms(void)
{
	// 10ms Tasks
}

void timer_timeout_50ms(void)
{
	// 50ms Tasks
	// led_timer();
}

void timer_timeout_100ms(void)
{
	// 100ms Tasks
#ifdef ENABLE_WIFI
	wifi_test_mode();
#endif
}

void timer_timeout_500ms(void)
{
	// 500ms Tasks
}

int ctr = 0;
void timer_timeout_1000ms(void)
{
	// 1 second tasks
	g_pLogger->Write(LogLevel::Trace, "1-s", "Mode: %s, Status: %s, got_ip: %d, ip: %s", meti_wifi_get_mode_text().c_str(), meti_wifi_get_status_text().c_str(), wifi_got_ip, WiFi.localIP().toString().c_str());

	Serial.print(".");
	mandatory_restart_check();

	wifi_station_display_once();
	update_device_wifi_params();

	check_forced_ap_mode_entry();
	forced_ap_mode_timeout();

	store_sta_ip_address();

	if (WiFi.getMode() == WIFI_STA)
	{
		// handle_router();
		wifi_check_status();

		// wifi_config_npt will be set once wifi gets the IP address from external router
		if (wifi_config_npt == true)
		{
			wifi_config_npt = false;
			mtime_init();
		}

		if (wifi_got_ip == true)
		{
			mtime_handle_npt();
		}

#ifdef ENABLE_MQTT
		mqtt_enabled = true;
		if (mqtt_enabled == true)
		{
			mqtt_check_comm_status();
			if (file_send_inprogress == false)
			{
				mqtt_publish_timeout();
			}
		}
		update_params();
#endif

		if (wifi_internet_ok == true)
		{
			set_rssi_led();
		}
	}
}

void setup_device_params(void)
{
	// remove colon and replace with -

	String dev_id;
	String mac = WiFi.macAddress();

	// Remove all colons in device id and add MQ1....
	dev_id = "MGEN2-" + mac;
	dev_id.replace(":", "-");

	device_doc["dev_mac_addr"] = mac;
	device_doc["dev_id"] = dev_id;
	device_doc["dev_vdd"] = 3.3;

	g_pLogger->Write(LogLevel::Info, "setup_device_params", "Device ID: %s", mac.c_str());
}

void InitializeIndicators(void)
{
	Indicator::Initialize(IndicatorType::GetSize());
	g_pIndicators = Indicator::GetInstance();
}

void InitializeBlinkProcess(void)
{
	g_pBlinkConfig = new BlinkConfig[BlinkType::GetSize()];

	g_pBlinkConfig[BlinkType::Fast].Set(true, 50, 50, 1, 0, 0);
	g_pBlinkConfig[BlinkType::Once].Set(false, 50, 50, 1, 1, 0);
	g_pBlinkConfig[BlinkType::Twice].Set(false, 50, 200, 2, 1, 0);
	g_pBlinkConfig[BlinkType::BeforeFactoryReset].Set(false, 300, 200, 5, 1, 500);
	g_pBlinkConfig[BlinkType::BeforeMbrReset].Set(false, 300, 200, 3, 1, 1500);
	g_pBlinkConfig[BlinkType::Slow].Set(true, 500, 500, 1, 0, 0);
	g_pBlinkConfig[BlinkType::OnceFor300Ms].Set(false, 300, 300, 1, 1, 0);
	g_pBlinkConfig[BlinkType::BuzzOnce].Set(false, 100, 100, 1, 1, 0);
	g_pBlinkConfig[BlinkType::BuzzTwice].Set(false, 100, 100, 2, 1, 0);
}

void update_params(void)
{
	params_doc["device"]["on_time"] = (uint32_t)millis() / 1000;
}

void meti_device_restart(void)
{
	set_led_state(MULTI_LED_STATE_RED_ON);
	delay(100);
	ESP.restart();
}

void mandatory_restart_check(void)
{
	uint32_t currentMillis = millis();
	static uint32_t previousMillis = 0; // will store last time LED was updated
	// constants won't change:

	// Every midnight do a restart...
	// if time between 00:00:00 and 00:00:03
	// Note: Do not do check this for for 10 seconds...
	if (currentMillis > MANDATORY_RESTART_SKIP_INTERVAL)
	{
		CDate l_oTime;
		l_oTime = CDate::Now();

		if (l_oTime.GetHour() == 0 && l_oTime.GetMinute() == 0)
		{
			if (l_oTime.GetSecond() < 3)
			{
				previousMillis = currentMillis;
				g_pLogger->Write(LogLevel::Info, "loop", "Daily Midnight restart....");
				meti_device_restart();
			}
		}
	}

	// this is for double safety... if midnight restart does not happen
	// after 90000 seconds, do a restart...
	if (currentMillis - previousMillis >= MANDATORY_RESTART_INTERVAL_BACKUP)
	{
		// save the last time you blinked the LED
		previousMillis = currentMillis;
		g_pLogger->Write(LogLevel::Info, "loop", "Daily restart (BACKUP)....");
		meti_device_restart();
	}
}