#include <Arduino.h>
#include "esp_system.h"
#include "nvs_flash.h"
#include "nvs.h"
#include <meti_nvs.h>
#include <logger.h>
#include <main.h>

extern Logger *g_pLogger;

params_t params; 

void meti_nvs_initialize(void)
{
#ifdef ENABLE_NVS
    esp_err_t err = nvs_flash_init();
    if (err == ESP_ERR_NVS_NO_FREE_PAGES || err == ESP_ERR_NVS_NEW_VERSION_FOUND)
    {
        // NVS partition was truncated and needs to be erased
        // Retry nvs_flash_init
        ESP_ERROR_CHECK(nvs_flash_erase());
        err = nvs_flash_init();
        g_pLogger->Write(LogLevel::Warn, "nvs", "Initialization failed..Erasing and reinitializing...");
    }
    ESP_ERROR_CHECK(err);
#endif
}

void meti_nvs_read_all(const char *name)
{
#ifdef ENABLE_NVS
    //Default initialization if nvs read fails...
    params.drc = 0;
    params.rrc = 0;
    params.forced_ap = FORCED_AP_NVS_FALSE;

    // Read
    meti_nvs_get_val(name, "drc", &params.drc);
    meti_nvs_get_val(name, "rrc", &params.rrc);
    meti_nvs_get_val(name, "forced_ap", &params.forced_ap);
#endif
}

void meti_nvs_get_val(const char *name, const char *key, uint32_t *out_value)
{
#ifdef ENABLE_NVS
    esp_err_t err;
    nvs_handle my_handle;

    err = nvs_open(name, NVS_READONLY, &my_handle);
    if (err != ESP_OK)
    {
        g_pLogger->Write(LogLevel::Error, "Error opening NVS handle %s (%s)", name, esp_err_to_name(err));
    }
    else
    {
        err = nvs_get_u32(my_handle, key, out_value);

        switch (err)
        {
        case ESP_OK:
            g_pLogger->Write(LogLevel::Info, "nvs", "Reading %s -> %s = %lu", name, key, (unsigned long)*out_value);
            break;
        case ESP_ERR_NVS_NOT_FOUND:
            g_pLogger->Write(LogLevel::Warn, "nvs", "%s -> %s - not initialized yet!", name, key);
            break;
        default:
            g_pLogger->Write(LogLevel::Error, "nvs", "%s -> %s - Error (%s) reading!", name, key, esp_err_to_name(err));
            break;
        }

        // Close
    }
    nvs_close(my_handle);
#endif
}

void meti_nvs_commit_val(const char *name, const char *key, uint32_t out_value)
{
    // Narayan remove this in production....
    //return;
#ifdef ENABLE_NVS
    esp_err_t err;
    nvs_handle my_handle;

    err = nvs_open(name, NVS_READWRITE, &my_handle);
    if (err != ESP_OK)
    {
        g_pLogger->Write(LogLevel::Error, "Error opening NVS handle %s (%s)", name, esp_err_to_name(err));
    }
    else
    {
        err = nvs_set_u32(my_handle, key, out_value);

        if (err != ESP_OK)
        {
            g_pLogger->Write(LogLevel::Warn, "nvs", "%s -> %s - failed to set value (%s)...", name, key, esp_err_to_name(err));
        }
        else
        {
            // Commit written value.
            // After setting any values, nvs_commit() must be called to ensure changes are written
            // to flash storage. Implementations may write to storage at other times,
            // but this is not guaranteed.
            err = nvs_commit(my_handle);
            if (err != ESP_OK)
            {
                g_pLogger->Write(LogLevel::Warn, "Failed to commit NVS handle %s (%s)", name, esp_err_to_name(err));
            }
            else
            {
                g_pLogger->Write(LogLevel::Info, "nvs", "NVS Commit complete : %s -> %s = %lu", name, key, out_value);
            }
        }
    }
    // Close
    nvs_close(my_handle);
#endif
}