#include <Arduino.h>
#include <meti_utils.h>
#include <esp_int_wdt.h>
#include <esp_task_wdt.h>

void meti_esp_reset(void)
{
	esp_task_wdt_init(1, true);
	esp_task_wdt_add(NULL);
	while (true)
		;
}