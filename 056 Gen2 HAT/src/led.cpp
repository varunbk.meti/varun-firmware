#include <Arduino.h>
#include <led.h>

struct ledControl leds[MAX_LEDS] = {
	{LED_MODE_ON, PIN_HIGH, 0},
};

void led_setup(void)
{
	// pinMode(LED_WIFI_PIN, OUTPUT);
	pinMode(LED_RED_PIN, OUTPUT);
	pinMode(LED_GREEN_PIN, OUTPUT);
	pinMode(LED_BLUE_PIN, OUTPUT);

	led_set_mode(LED_RED, LED_MODE_ON);
	led_set_mode(LED_GREEN, LED_MODE_OFF);
	led_set_mode(LED_BLUE, LED_MODE_OFF);

	led_apply();
}

void led_set_mode(uint8_t led, uint8_t mode)
{
	leds[led].mode = mode;
}

void led_timer(void)
{
	for (uint8_t i = 0; i < MAX_LEDS; i++)
	{
		if (leds[i].mode == LED_MODE_ON)
		{
			leds[i].pin_state = PIN_HIGH;
		}
		else if (leds[i].mode == LED_MODE_OFF)
		{
			leds[i].pin_state = PIN_LOW;
		}
		else if (leds[i].mode == LED_MODE_BLINK)
		{
			if (leds[i].dc-- <= 0)
			{
				leds[i].dc = LED_BLINK_TIME;
				if (leds[i].pin_state == PIN_HIGH)
				{
					leds[i].pin_state = PIN_LOW;
				}
				else
				{
					leds[i].pin_state = PIN_HIGH;
				}
			}
		}
		else if (leds[i].mode == LED_MODE_FAST_BLINK)
		{
			if (leds[i].dc-- <= 0)
			{
				leds[i].dc = LED_FAST_BLINK_TIME;
				if (leds[i].pin_state == PIN_HIGH)
				{
					leds[i].pin_state = PIN_LOW;
				}
				else
				{
					leds[i].pin_state = PIN_HIGH;
				}
			}
		}
	}
	led_apply();
}

void led_apply(void)
{
	digitalWrite(LED_RED_PIN, leds[LED_RED].pin_state);
	digitalWrite(LED_GREEN_PIN, leds[LED_GREEN].pin_state);
	digitalWrite(LED_BLUE_PIN, leds[LED_BLUE].pin_state);
}
