//var device config database
var g_srm_config_db = {};
var g_wifi_db = {};

const LOGO_TEXT = "METI"
const SHOW_COPYRIGHT = true;
const LOCAL_HOST = false;

const HARDWARE_TYPE_RS485 = 1
const HARDWARE_TYPE_4_20MA = 2
const HARDWARE_TYPE_SRM = 3

const HARDWARE_TYPE = HARDWARE_TYPE_SRM

var ready_timer = setInterval(function () {
    console.log("timer");
    header_footer_logo();
}, 1000);

// $(document).ready(function () {
function header_footer_logo() {
    //check if object exits
    if ($("#logo_text").length) {
        //stop the check interval timers
        clearInterval(ready_timer);
        console.log("timer stop");

        $("#logo_text").html(LOGO_TEXT);

        //Footer logo
        if (SHOW_COPYRIGHT == true) {
            var footer_obj = $("<footer></footer>");

            var div_obj = $("<div></div>")
                .addClass("footer-copyright text-center py-3")
                .html("2021 Copyright:");

            var a_obj = $("<a></a>")
                .attr("href", "https://www.meti.in/")
                .html(" METI M2M India Pvt Ltd.,")

            div_obj.append(a_obj);
            footer_obj.append(div_obj);
            $("body").after(footer_obj);
        }
    }
}

// $(document).ready(function () {
function setupDeviceSpecificLinks() {
    var factory_settings_flag = false;

    $(".navbar").css("visibility", "visible");
    $("#link_factorySettings").attr("hidden", "hidden");
    $("#link_modbus").attr("hidden", "hidden");
    $("#link_sensorscaling").attr("hidden", "hidden");

    switch (HARDWARE_TYPE) {
        case HARDWARE_TYPE_RS485:
            $("#quickIot_title2").html("RS485");
            $("#link_modbus").removeAttr("hidden");
            break;

        case HARDWARE_TYPE_4_20MA:
            // $("#quickIot_title2").html("4-20mA");
            $("#quickIot_title2").html("BHM");
            $("#link_sensorscaling").removeAttr("hidden");
            break;

        case HARDWARE_TYPE_SRM:
            // $("#quickIot_title2").html("4-20mA");
            $("#quickIot_title2").html("GEN2");
            $("#link_sensorscaling").removeAttr("hidden");
            break;
    }

    $("#quickIot_title1").on("click", function () {
        if (HARDWARE_TYPE == HARDWARE_TYPE_4_20MA) {
            factory_settings_flag = true;
            setTimeout(function () {
                factory_settings_flag = false;
            }, 1000)
        }
    })

    $("#quickIot_title2").on("click", function () {
        if (HARDWARE_TYPE == HARDWARE_TYPE_4_20MA) {
            if (factory_settings_flag == true) {
                $("#link_factorySettings").removeAttr("hidden");
                $("#link_modbus").removeAttr("hidden");
            }
        }
    });
}
// });

function getCleanString(inputStr) {
    // var new_str = inputStr.replace(/[&\/\\#, +()$~%.'":*?<>{}]/g, '_');
    inputStr = inputStr.replace(" ", "_");
    inputStr = inputStr.replace("/", "_");
    inputStr = inputStr.replace("+", "_");
    inputStr = inputStr.replace("#", "_");

    return inputStr;
}

function copyTextToClipboard(text) {
    var input = document.createElement('textarea');
    input.innerHTML = text;
    document.body.appendChild(input);
    input.select();
    var result = document.execCommand('copy');
    document.body.removeChild(input);
    return result;
}

function openFullscreen(elem) {
    if (elem.requestFullscreen) {
        elem.requestFullscreen();
    } else if (elem.mozRequestFullScreen) { /* Firefox */
        elem.mozRequestFullScreen();
    } else if (elem.webkitRequestFullscreen) { /* Chrome, Safari & Opera */
        elem.webkitRequestFullscreen();
    } else if (elem.msRequestFullscreen) { /* IE/Edge */
        elem.msRequestFullscreen();
    }
}

function getModbusSensors1() {
    sensors = g_modbus_drivers.template_header.responseParser;
    // sensors1 = g_modbus_drivers.template_header.responseParser;
    // sensors2 = g_modbus_drivers.template_header.responseParser;

    sensors_full = sensors;
    // sensors_full = $.extend(sensors, sensors1);
    // sensors_full = $.extend(sensors_full, sensors2);


    return sensors_full;
}

function readConfigDb() {
    var abcd = readTextFile("config.txt");
    console.log(abcd);
    // console.log(readTextFile("config.txt"));
}


function updateSrmConfigFromFS(callback) {
    var rawFile = new XMLHttpRequest();
    rawFile.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            var allText = JSON.parse(rawFile.responseText);
            g_srm_config_db = allText;
            callback();
        }
    }

    rawFile.open("GET", "../config/srm_config.json", false);
    rawFile.send();
}

function updateWifiConfigFromFS(callback) {
    var rawFile = new XMLHttpRequest();
    rawFile.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            var allText = JSON.parse(rawFile.responseText);
            g_wifi_db = allText;
            callback();
        }
    }

    rawFile.open("GET", "../config/wifi.json", false);
    rawFile.send();
}

//var device config database
var g_modbus_drivers = {};
function getLocalModbusDrivers(callback) {
    var rawFile = new XMLHttpRequest();
    rawFile.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            var allText = JSON.parse(rawFile.responseText);
            g_modbus_drivers = allText;
            callback();
            return;
        }
    }

    //Narayan - this has to be made dynamic
    rawFile.open("GET", "../drivers/z_meti_99.json", false);
    rawFile.send();
}

// Returns the number of repetitions of a characters in a string
function char_count(str, letter) {
    var letter_Count = 0;
    for (var position = 0; position < str.length; position++) {
        if (str.charAt(position) == letter) {
            letter_Count += 1;
        }
    }
    return letter_Count;
}

function getDeviceParams(callBack, callBackError) {
    $('.loader').css('display', 'block');

    var url = "/getDeviceConfig";
    if (LOCAL_HOST == true) {
        var url = "http://localhost/play/index.php/QuickIoT_simulate/readDeviceParams";
    }

    var type = 'POST';
    var data = {
        dev_key: "ABCD"
    };

    $.ajax({
        url: url,
        type: type,
        data: data,
        success: function (response) {
            callBack(response);
        },
        error: function (jqXHR, exception, response) {
            callBackError(response);
        }
    });
}

function reset_device() {
    var url = "/resetDevice";
    if (LOCAL_HOST == true) {
        var url = "http://localhost/play/index.php/quickIoT/reset_device";
    }

    var type = 'POST';
    var data = {
        dev_key: "ABCD"
    };

    $.ajax({
        url: url,
        type: type,
        data: data,
        success: function (response) {
            alert(response.message);
        },
        error: function (jqXHR, exception, response) {
            // console.log('POST Error!');
        }
    });
}