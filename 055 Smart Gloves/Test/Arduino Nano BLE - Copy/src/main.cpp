#include <Arduino.h>
#include <ArduinoBLE.h>
#include "main.h"
#include <JY901.h>

#include "NRF52_MBED_TimerInterrupt.h"
#include "NRF52_MBED_ISR_Timer.h"

#define HW_TIMER_INTERVAL_MS 1

uint8_t *i2c_ptr;
uint8_t i2c_buff[I2C_Total_Bytes];

typedef struct
{
  uint8_t Flex1;
  uint8_t Flex2;
  uint8_t Flex3;
  uint8_t Flex4;
  uint8_t Flex5;
  uint8_t Flex6;
  uint8_t Flex7;
  uint8_t Flex8;
  uint8_t Flex9;
  uint8_t Flex10;
} i2c_read_struct;

i2c_read_struct flex_i2c_read;

uint8_t last_i2c_error = 0;

NRF52_MBED_Timer ITimer(NRF_TIMER_3);
NRF52_MBED_ISRTimer ISR_Timer;

long tm;
long previous;
bool ms_16_flag = false;

typedef struct
{
  int16_t array[DATA_SIZE];
} imuDataTDF;

int16_t temp_data[DATA_SIZE];

imuDataTDF imuData;

// 1101 is a custom service UUID (do not change this...)
BLEService userDataService("181C");

// 9001 is a characteristic defined for IMU data packet
BLECharacteristic imuDataChar("9001", BLENotify, sizeof imuData);

BLEService deviceInfoService("180A");
BLECharacteristic manufacturerChar("2A29", BLERead, "METI");
BLECharacteristic modelNoChar("2A24", BLERead, "001");
BLECharacteristic hwRevChar("2A27", BLERead, "100");
BLECharacteristic swRevChar("2A28", BLERead, "101");

BLEDevice central;

uint16_t PacketCounter = 0;
uint8_t idx = 0;
uint8_t sample_cntr = 4;

void TimerHandler()
{
  ISR_Timer.run();
}

void setup()
{
  BLE_Init();
  i2c_initialization();
  Timer_Setup();

  Serial.print("Begin");
}

void loop()
{
  bool check_connection;
  check_connection = Smart_ble_connect();
  if (ms_16_flag == true && check_connection)
  {
    ms_16_flag = false;
    JY901.receiveSerialData();
    i2c_read();
    Process_Data();
    tm = millis();
    Serial.print("Time=");
    Serial.println(previous - tm);
    previous = tm;
    sample_cntr--;
  }
  if (sample_cntr == 0)
  {
    sample_cntr = Number_of_samples;
    idx = 0;
    Send_data_over_ble();
  }
}

void Timer_Setup()
{
  // Interval in microsecs
  if (ITimer.attachInterruptInterval(HW_TIMER_INTERVAL_MS * 1000, TimerHandler))
  {
    Serial.print(F("Starting ITimer OK, millis() = "));
    Serial.println(millis());
  }
  else
    Serial.println(F("Can't set ITimer. Select another freq. or timer"));

  // ISR_Timer.setInterval(TIMER_INTERVAL_16ms, Task_16ms); Not Used
  ISR_Timer.setInterval(TIMER_INTERVAL_16ms, Task_16ms);
}

void Task_16ms()
{
  ms_16_flag = true;
}

void Task_100ms()
{
}

void i2c_initialization()
{
  Wire.begin();
}

void i2c_read(void)
{
  Read_Multiple_Bytes(I2C_Adress, Starting_Adress, I2C_Total_Bytes, i2c_buff);

  if (last_i2c_error == 0)
  {
    memcpy((char *)&flex_i2c_read, i2c_buff, I2C_Total_Bytes);
  }
  else
  {
    last_i2c_error = 0;
  }
}
void Read_Multiple_Bytes(uint8_t ui8I2CAdd, uint8_t ui8StartRegAdd, uint8_t ui8NumberOfReg, uint8_t *pauiReadBuffer)
{
  uint8_t ui8LoopCounter = 0;
  Wire.beginTransmission(ui8I2CAdd);
  Wire.endTransmission(false);
  Wire.requestFrom(ui8I2CAdd, ui8NumberOfReg);
  for (ui8LoopCounter = 0; ui8LoopCounter < ui8NumberOfReg; ui8LoopCounter++)
  {
    pauiReadBuffer[ui8LoopCounter] = Wire.read();
  }
}

void debug_print_data(void)
{
  Serial.print("=>");
  Serial.print(flex_i2c_read.Flex1);

  Serial.print("=>");
  Serial.print(flex_i2c_read.Flex2);

  Serial.print("=>");
  Serial.print(flex_i2c_read.Flex3);

  Serial.print("=>");
  Serial.print(flex_i2c_read.Flex4);

  Serial.print("=>");
  Serial.print(flex_i2c_read.Flex5);

  Serial.print("=>");
  Serial.print(flex_i2c_read.Flex6);

  Serial.print("=>");
  Serial.print(flex_i2c_read.Flex7);

  Serial.print("=>");
  Serial.print(flex_i2c_read.Flex8);

  Serial.print("=>");
  Serial.print(flex_i2c_read.Flex9);

  Serial.print("=>");
  Serial.print(flex_i2c_read.Flex10);
}

void BLE_Init(void)
{
  delay(100);
  Serial.begin(921600);
  Serial1.begin(921600);
  JY901.attach(Serial1);

  pinMode(LED_BUILTIN, OUTPUT);

  if (!BLE.begin())
  {
    Serial.println("BLE failed to Initiate");
    delay(500);
    while (1)
      ;
  }

  BLE.setLocalName("METI BLE 4");
  BLE.setAdvertisingInterval(500);

  deviceInfoService.addCharacteristic(manufacturerChar);
  deviceInfoService.addCharacteristic(modelNoChar);
  deviceInfoService.addCharacteristic(hwRevChar);
  deviceInfoService.addCharacteristic(swRevChar);

  BLE.addService(deviceInfoService);
  BLE.setAdvertisedService(deviceInfoService);
  BLE.setAdvertisingInterval(500);

  userDataService.addCharacteristic(imuDataChar);
  BLE.addService(userDataService);

  BLE.advertise();

  Serial.println("Bluetooth device is now active, waiting for connections...");

  imuData.array[0] = 0x7FF0;
}

void Process_Data()
{
  if (idx == 0)
  {
    imuData.array[idx++] = PacketCounter++;
    if (PacketCounter == 65535)
    {
      PacketCounter = 0;
    }
  }
  imuData.array[idx++] = JY901.getAccX() * 100;
  imuData.array[idx++] = JY901.getAccY() * 100;
  imuData.array[idx++] = JY901.getAccZ() * 100;
  imuData.array[idx++] = JY901.getGyroX() * 100;
  imuData.array[idx++] = JY901.getGyroY() * 100;
  imuData.array[idx++] = JY901.getGyroZ() * 100;
  imuData.array[idx++] = JY901.getMagX() * 100;
  imuData.array[idx++] = JY901.getMagY() * 100;
  imuData.array[idx++] = JY901.getMagZ() * 100;
  imuData.array[idx++] = JY901.getQuater_Q0() * 100;
  imuData.array[idx++] = JY901.getQuater_Q1() * 100;
  imuData.array[idx++] = JY901.getQuater_Q2() * 100;
  imuData.array[idx++] = JY901.getQuater_Q3() * 100;

  //Chandana
  float sum;
  float val1,val2,val3,val4;
  Serial.print("q0:");
  Serial.print((float)JY901.getQuater_Q0()/32768);
  Serial.print("  q1:");
  Serial.print((float)JY901.getQuater_Q1()/32768);
  Serial.print("  q2:");
  Serial.print((float)JY901.getQuater_Q2()/32768);
  Serial.print("  q3:");
  Serial.println((float)JY901.getQuater_Q3()/32768);
  val1=(float)JY901.getQuater_Q0()/32768;
  val1=sq(val1);
  val2=(float)JY901.getQuater_Q1()/32768;
  val2=sq(val2);
  val3=(float)JY901.getQuater_Q2()/32768;
  val3=sq(val3);
  val4=(float)JY901.getQuater_Q3()/32768;
  val4=sq(val4);
  sum=val1+val2+val3+val4;
  Serial.print("Sum: ");
  Serial.println(sum);
  delay(1000);

  



  imuData.array[idx] = flex_i2c_read.Flex1;
  imuData.array[idx] <<= 8;
  imuData.array[idx++] |= flex_i2c_read.Flex2;

  imuData.array[idx] = flex_i2c_read.Flex3;
  imuData.array[idx] <<= 8;
  imuData.array[idx++] |= flex_i2c_read.Flex4;

  imuData.array[idx] = flex_i2c_read.Flex5;
  imuData.array[idx] <<= 8;
  imuData.array[idx++] |= flex_i2c_read.Flex6;

  imuData.array[idx] = flex_i2c_read.Flex7;
  imuData.array[idx] <<= 8;
  imuData.array[idx++] |= flex_i2c_read.Flex8;

  imuData.array[idx] = flex_i2c_read.Flex9;
  imuData.array[idx] <<= 8;
  imuData.array[idx++] |= flex_i2c_read.Flex10;
}
bool Smart_ble_connect(void)
{
  static bool central_connected = false, smart_connect = false;

  central = BLE.central();

  if (central)
  {
    if (central_connected == false)
    {
      central_connected = true;
      Serial.print("Connected to central: ");
      Serial.println(central.address());
    }
    while (central.connected())
    {
      smart_connect = true;
      return smart_connect;
    }
  }
  else
  {
    if (central_connected == true)
    {
      central_connected = false;
      smart_connect = false;
      Serial.print("Disconnected from central: ");
    }
  }
  return smart_connect;
}

void Send_data_over_ble()
{
  imuDataChar.writeValue(imuData.array, sizeof imuData.array);
}

