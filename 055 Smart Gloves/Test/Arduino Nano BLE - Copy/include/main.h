#ifndef _Main_H_
#define _Main_H_

#include <Arduino.h>
#include <Wire.h>

#define I2C_Adress 16
#define Starting_Adress 0
#define I2C_Total_Bytes 10
#define clock_frequency 100000
#define Max_Byte_size 108
#define Number_of_samples 4

#define DATA_SIZE 109

#define TIMER_INTERVAL_100ms 100
#define TIMER_INTERVAL_16ms 16

void BLE_Init(void);
void i2c_read(void);
void i2c_initialization();
void Read_Multiple_Bytes(uint8_t ui8I2CAdd, uint8_t ui8StartRegAdd, uint8_t ui8NumberOfReg, uint8_t *pauiReadBuffer);
void Process_Data();
bool Smart_ble_connect(void);
void Send_data_over_ble();

void Timer_Setup();
void Task_100ms();
void Task_16ms();

#endif