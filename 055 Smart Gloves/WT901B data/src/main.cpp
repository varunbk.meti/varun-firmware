#include <Arduino.h>
#include <Wire.h>
#include <JY901.h>

void setup()
{
  Serial.begin(9600);
  Serial1.begin(921600);
}

void loop()
{
  while (Serial1.available())
  {
    // JY901.CopeSerialData(Serial1.read());
    // Serial.print("Acc:");
    // Serial.print((float)JY901.stcAcc.a[0]/ 32768 * 16);
    // Serial.print(" ");
    // Serial.print((float)JY901.stcAcc.a[1]/ 32768 * 16); //
    // Serial.print(" ");
    // Serial.print((float)JY901.stcAcc.a[2]/ 32768 * 16);
    // Serial.print(" ");

    // Serial.print("Gyro:");
    // Serial.print((float)JY901.stcGyro.w[0] / 32768 * 2000);
    // Serial.print(" ");
    // Serial.print((float)JY901.stcGyro.w[1] / 32768 * 2000);
    // Serial.print(" ");
    // Serial.print((float)JY901.stcGyro.w[2] / 32768 * 2000);
    // Serial.print(" ");

    // Serial.print("Mag:");
    // Serial.print(JY901.stcMag.h[0]);
    // Serial.print(" ");
    // Serial.print(JY901.stcMag.h[1]);
    // Serial.print(" ");
    // Serial.print(JY901.stcMag.h[2]);
    // Serial.print(" ");


    Serial.print("Quaternion");
    Serial.print(JY901.stcQuater.q0);
    Serial.print(" ");
    Serial.print(JY901.stcQuater.q1);
    Serial.print(" ");
    Serial.print(JY901.stcQuater.q2);
    Serial.print(" ");
    Serial.print(JY901.stcQuater.q3);
    Serial.println(" ");
  }

}

/*
  SerialEvent occurs whenever a new data comes in the
 hardware serial RX.  This routine is run between each
 time loop() runs, so using delay inside loop can delay
 response.  Multiple bytes of data may be available.
 */
// void serialEvent()
// {
//   while (Serial1.available())
//   {
//     JY901.CopeSerialData(Serial1.read()); // Call JY901 data cope function
//   }
// }
