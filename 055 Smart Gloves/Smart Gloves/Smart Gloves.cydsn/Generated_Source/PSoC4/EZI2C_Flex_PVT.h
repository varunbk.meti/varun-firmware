/***************************************************************************//**
* \file .h
* \version 4.0
*
* \brief
*  This private file provides constants and parameter values for the
*  SCB Component.
*  Please do not use this file or its content in your project.
*
* Note:
*
********************************************************************************
* \copyright
* Copyright 2013-2017, Cypress Semiconductor Corporation. All rights reserved.
* You may use this file only in accordance with the license, terms, conditions,
* disclaimers, and limitations in the end user license agreement accompanying
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_SCB_PVT_EZI2C_Flex_H)
#define CY_SCB_PVT_EZI2C_Flex_H

#include "EZI2C_Flex.h"


/***************************************
*     Private Function Prototypes
***************************************/

/* APIs to service INTR_I2C_EC register */
#define EZI2C_Flex_SetI2CExtClkInterruptMode(interruptMask) EZI2C_Flex_WRITE_INTR_I2C_EC_MASK(interruptMask)
#define EZI2C_Flex_ClearI2CExtClkInterruptSource(interruptMask) EZI2C_Flex_CLEAR_INTR_I2C_EC(interruptMask)
#define EZI2C_Flex_GetI2CExtClkInterruptSource()                (EZI2C_Flex_INTR_I2C_EC_REG)
#define EZI2C_Flex_GetI2CExtClkInterruptMode()                  (EZI2C_Flex_INTR_I2C_EC_MASK_REG)
#define EZI2C_Flex_GetI2CExtClkInterruptSourceMasked()          (EZI2C_Flex_INTR_I2C_EC_MASKED_REG)

#if (!EZI2C_Flex_CY_SCBIP_V1)
    /* APIs to service INTR_SPI_EC register */
    #define EZI2C_Flex_SetSpiExtClkInterruptMode(interruptMask) \
                                                                EZI2C_Flex_WRITE_INTR_SPI_EC_MASK(interruptMask)
    #define EZI2C_Flex_ClearSpiExtClkInterruptSource(interruptMask) \
                                                                EZI2C_Flex_CLEAR_INTR_SPI_EC(interruptMask)
    #define EZI2C_Flex_GetExtSpiClkInterruptSource()                 (EZI2C_Flex_INTR_SPI_EC_REG)
    #define EZI2C_Flex_GetExtSpiClkInterruptMode()                   (EZI2C_Flex_INTR_SPI_EC_MASK_REG)
    #define EZI2C_Flex_GetExtSpiClkInterruptSourceMasked()           (EZI2C_Flex_INTR_SPI_EC_MASKED_REG)
#endif /* (!EZI2C_Flex_CY_SCBIP_V1) */

#if(EZI2C_Flex_SCB_MODE_UNCONFIG_CONST_CFG)
    extern void EZI2C_Flex_SetPins(uint32 mode, uint32 subMode, uint32 uartEnableMask);
#endif /* (EZI2C_Flex_SCB_MODE_UNCONFIG_CONST_CFG) */


/***************************************
*     Vars with External Linkage
***************************************/

#if (EZI2C_Flex_SCB_IRQ_INTERNAL)
#if !defined (CY_REMOVE_EZI2C_Flex_CUSTOM_INTR_HANDLER)
    extern cyisraddress EZI2C_Flex_customIntrHandler;
#endif /* !defined (CY_REMOVE_EZI2C_Flex_CUSTOM_INTR_HANDLER) */
#endif /* (EZI2C_Flex_SCB_IRQ_INTERNAL) */

extern EZI2C_Flex_BACKUP_STRUCT EZI2C_Flex_backup;

#if(EZI2C_Flex_SCB_MODE_UNCONFIG_CONST_CFG)
    /* Common configuration variables */
    extern uint8 EZI2C_Flex_scbMode;
    extern uint8 EZI2C_Flex_scbEnableWake;
    extern uint8 EZI2C_Flex_scbEnableIntr;

    /* I2C configuration variables */
    extern uint8 EZI2C_Flex_mode;
    extern uint8 EZI2C_Flex_acceptAddr;

    /* SPI/UART configuration variables */
    extern volatile uint8 * EZI2C_Flex_rxBuffer;
    extern uint8   EZI2C_Flex_rxDataBits;
    extern uint32  EZI2C_Flex_rxBufferSize;

    extern volatile uint8 * EZI2C_Flex_txBuffer;
    extern uint8   EZI2C_Flex_txDataBits;
    extern uint32  EZI2C_Flex_txBufferSize;

    /* EZI2C configuration variables */
    extern uint8 EZI2C_Flex_numberOfAddr;
    extern uint8 EZI2C_Flex_subAddrSize;
#endif /* (EZI2C_Flex_SCB_MODE_UNCONFIG_CONST_CFG) */

#if (! (EZI2C_Flex_SCB_MODE_I2C_CONST_CFG || \
        EZI2C_Flex_SCB_MODE_EZI2C_CONST_CFG))
    extern uint16 EZI2C_Flex_IntrTxMask;
#endif /* (! (EZI2C_Flex_SCB_MODE_I2C_CONST_CFG || \
              EZI2C_Flex_SCB_MODE_EZI2C_CONST_CFG)) */


/***************************************
*        Conditional Macro
****************************************/

#if(EZI2C_Flex_SCB_MODE_UNCONFIG_CONST_CFG)
    /* Defines run time operation mode */
    #define EZI2C_Flex_SCB_MODE_I2C_RUNTM_CFG     (EZI2C_Flex_SCB_MODE_I2C      == EZI2C_Flex_scbMode)
    #define EZI2C_Flex_SCB_MODE_SPI_RUNTM_CFG     (EZI2C_Flex_SCB_MODE_SPI      == EZI2C_Flex_scbMode)
    #define EZI2C_Flex_SCB_MODE_UART_RUNTM_CFG    (EZI2C_Flex_SCB_MODE_UART     == EZI2C_Flex_scbMode)
    #define EZI2C_Flex_SCB_MODE_EZI2C_RUNTM_CFG   (EZI2C_Flex_SCB_MODE_EZI2C    == EZI2C_Flex_scbMode)
    #define EZI2C_Flex_SCB_MODE_UNCONFIG_RUNTM_CFG \
                                                        (EZI2C_Flex_SCB_MODE_UNCONFIG == EZI2C_Flex_scbMode)

    /* Defines wakeup enable */
    #define EZI2C_Flex_SCB_WAKE_ENABLE_CHECK       (0u != EZI2C_Flex_scbEnableWake)
#endif /* (EZI2C_Flex_SCB_MODE_UNCONFIG_CONST_CFG) */

/* Defines maximum number of SCB pins */
#if (!EZI2C_Flex_CY_SCBIP_V1)
    #define EZI2C_Flex_SCB_PINS_NUMBER    (7u)
#else
    #define EZI2C_Flex_SCB_PINS_NUMBER    (2u)
#endif /* (!EZI2C_Flex_CY_SCBIP_V1) */

#endif /* (CY_SCB_PVT_EZI2C_Flex_H) */


/* [] END OF FILE */
