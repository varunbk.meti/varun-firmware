/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/
#include "project.h"
#include "stdio.h"
#include "main.h"

//const float old_min=0,old_max=2048,new_min=0,new_max=255;

uint8_t i2c_buff[TOTAL_PACKET_SIZE];
//ADC_variables
int16_t Flex[sensor_size];
int16_t Flex_Previous[sensor_size];
uint8_t process_val[sensor_size];

//Timer flag
uint8_t timer_1ms_flag=0;
char buff[40];  //debug bbuffer

int main(void)
{
//    uint8_t ch=0;
    
    CyGlobalIntEnable; /* Enable global interrupts. */
    
    ADC_Initialize();
    I2C_Initialize();
    timer_start();
    
    Flex_Mux_Start();
    
    for(;;)
    {  
        ADC_read(0);
        
        ADC_read(1);
        
        ADC_read(2);
        
        ADC_read(3);
       
        ADC_read(4);
        
        ADC_read(5);
        
        UART_PutString("\r\n");
        
    }
}

//ADC_UART_Initial_Setup
void ADC_Initialize(void)
{
    UART_Start();
    ADC_Flex_Start();
    ADC_Flex_StartConvert();
}

//ADC_Read for Individual  Mux Channell
void ADC_read(uint8_t channel)
{     
    Flex_Mux_Select(channel);
    CyDelay(2);
    ADC_Flex_IsEndConversion(ADC_Flex_WAIT_FOR_RESULT);
    Flex[channel]=ADC_Flex_GetResult16(channel);
    sprintf(buff,"Flex%d=%d\t",channel,(Flex[channel]/division_factor));
    UART_PutString(buff);
}

int map(int16_t current_val_,float old_min_,float old_max_,float new_min_,float new_max_)
{
     current_val_= ( (current_val_ - old_min_)/ (old_max_ - old_min_) ) * (new_max_ - new_min_) + new_min_;
    return current_val_;
}

//I2C Initialization
void I2C_Initialize(void)
{
    EZI2C_Flex_EzI2CSetBuffer1(sizeof(i2c_buff),READ_ONLY_OFFSET,i2c_buff);
    EZI2C_Flex_Start();
}

//I2C Update
void I2C_Udate(void)
{
    for(uint8_t idx=0;idx<Max_Channels;idx++)
    {
       i2c_buff[idx]=Flex[idx]/100;;
    }
}

//Timer
//void timer_start(void)
//{
//    Timer_1ms_Start();
//    isr_1ms_StartEx(ISR_1ms_Handler);
//}
//
//CY_ISR(ISR_1ms_Handler)
//{
//   timer_1ms_flag=1;
//}


