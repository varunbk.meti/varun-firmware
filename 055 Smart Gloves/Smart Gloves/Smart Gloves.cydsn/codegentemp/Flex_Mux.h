/*******************************************************************************
* File Name: Flex_Mux.h
* Version 1.80
*
*  Description:
*    This file contains the constants and function prototypes for the Analog
*    Multiplexer User Module AMux.
*
*   Note:
*
********************************************************************************
* Copyright 2008-2010, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
********************************************************************************/

#if !defined(CY_AMUX_Flex_Mux_H)
#define CY_AMUX_Flex_Mux_H

#include "cyfitter.h"
#include "cyfitter_cfg.h"

#if ((CYDEV_CHIP_FAMILY_USED == CYDEV_CHIP_FAMILY_PSOC3) || \
         (CYDEV_CHIP_FAMILY_USED == CYDEV_CHIP_FAMILY_PSOC4) || \
         (CYDEV_CHIP_FAMILY_USED == CYDEV_CHIP_FAMILY_PSOC5))    
    #include "cytypes.h"
#else
    #include "syslib/cy_syslib.h"
#endif /* ((CYDEV_CHIP_FAMILY_USED == CYDEV_CHIP_FAMILY_PSOC3) */


/***************************************
*        Function Prototypes
***************************************/

void Flex_Mux_Start(void) ;
#define Flex_Mux_Init() Flex_Mux_Start()
void Flex_Mux_FastSelect(uint8 channel) ;
/* The Stop, Select, Connect, Disconnect and DisconnectAll functions are declared elsewhere */
/* void Flex_Mux_Stop(void); */
/* void Flex_Mux_Select(uint8 channel); */
/* void Flex_Mux_Connect(uint8 channel); */
/* void Flex_Mux_Disconnect(uint8 channel); */
/* void Flex_Mux_DisconnectAll(void) */


/***************************************
*         Parameter Constants
***************************************/

#define Flex_Mux_CHANNELS  10u
#define Flex_Mux_MUXTYPE   1
#define Flex_Mux_ATMOSTONE 0

/***************************************
*             API Constants
***************************************/

#define Flex_Mux_NULL_CHANNEL 0xFFu
#define Flex_Mux_MUX_SINGLE   1
#define Flex_Mux_MUX_DIFF     2


/***************************************
*        Conditional Functions
***************************************/

#if Flex_Mux_MUXTYPE == Flex_Mux_MUX_SINGLE
# if !Flex_Mux_ATMOSTONE
#  define Flex_Mux_Connect(channel) Flex_Mux_Set(channel)
# endif
# define Flex_Mux_Disconnect(channel) Flex_Mux_Unset(channel)
#else
# if !Flex_Mux_ATMOSTONE
void Flex_Mux_Connect(uint8 channel) ;
# endif
void Flex_Mux_Disconnect(uint8 channel) ;
#endif

#if Flex_Mux_ATMOSTONE
# define Flex_Mux_Stop() Flex_Mux_DisconnectAll()
# define Flex_Mux_Select(channel) Flex_Mux_FastSelect(channel)
void Flex_Mux_DisconnectAll(void) ;
#else
# define Flex_Mux_Stop() Flex_Mux_Start()
void Flex_Mux_Select(uint8 channel) ;
# define Flex_Mux_DisconnectAll() Flex_Mux_Start()
#endif

#endif /* CY_AMUX_Flex_Mux_H */


/* [] END OF FILE */
