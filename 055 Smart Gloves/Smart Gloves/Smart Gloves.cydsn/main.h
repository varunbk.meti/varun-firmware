/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/

/* [] END OF FILE */
#ifndef __main_h
#define __main_h
    
#include "project.h"

//macros
#define sensor_size 10 //for 6 flex sensor
#define Filter_co 0.8 //filter coefficient
#define division_factor  100
    
#define Max_Channels 10 //Max A_Mux Channels 
    
#define TOTAL_PACKET_SIZE (uint8_t) (0xA)
#define READ_ONLY_OFFSET    (0u)   
    
    
//Function declarations
void ADC_Initialize(void);
void ADC_read(uint8_t channel);
void I2C_Initialize(void);
void I2C_Udate(void);
void timer_start(void);
int map(int16_t current_val_,float old_min_,float old_max_,float new_min_,float new_max_);
CY_ISR_PROTO(ISR_1ms_Handler);
void Flex_Read(uint8_t channel);

#endif /* __main_h */