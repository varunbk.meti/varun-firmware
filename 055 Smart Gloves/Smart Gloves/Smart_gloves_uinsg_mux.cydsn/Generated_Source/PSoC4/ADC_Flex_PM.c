/*******************************************************************************
* File Name: ADC_Flex_PM.c
* Version 2.60
*
* Description:
*  This file provides Sleep/WakeUp APIs functionality.
*
* Note:
*
********************************************************************************
* Copyright 2008-2017, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions,
* disclaimers, and limitations in the end user license agreement accompanying
* the software package with which this file was provided.
*******************************************************************************/

#include "ADC_Flex.h"


/***************************************
* Local data allocation
***************************************/

static ADC_Flex_BACKUP_STRUCT  ADC_Flex_backup =
{
    ADC_Flex_DISABLED,
    0u    
};


/*******************************************************************************
* Function Name: ADC_Flex_SaveConfig
********************************************************************************
*
* Summary:
*  Saves the current user configuration.
*
* Parameters:
*  None.
*
* Return:
*  None.
*
*******************************************************************************/
void ADC_Flex_SaveConfig(void)
{
    /* All configuration registers are marked as [reset_all_retention] */
}


/*******************************************************************************
* Function Name: ADC_Flex_RestoreConfig
********************************************************************************
*
* Summary:
*  Restores the current user configuration.
*
* Parameters:
*  None.
*
* Return:
*  None.
*
*******************************************************************************/
void ADC_Flex_RestoreConfig(void)
{
    /* All configuration registers are marked as [reset_all_retention] */
}


/*******************************************************************************
* Function Name: ADC_Flex_Sleep
********************************************************************************
*
* Summary:
*  Stops the ADC operation and saves the configuration registers and component
*  enable state. Should be called just prior to entering sleep.
*
* Parameters:
*  None.
*
* Return:
*  None.
*
* Global Variables:
*  ADC_Flex_backup - modified.
*
*******************************************************************************/
void ADC_Flex_Sleep(void)
{
    /* During deepsleep/ hibernate mode keep SARMUX active, i.e. do not open
    *   all switches (disconnect), to be used for ADFT
    */
    ADC_Flex_backup.dftRegVal = ADC_Flex_SAR_DFT_CTRL_REG & (uint32)~ADC_Flex_ADFT_OVERRIDE;
    ADC_Flex_SAR_DFT_CTRL_REG |= ADC_Flex_ADFT_OVERRIDE;
    if((ADC_Flex_SAR_CTRL_REG  & ADC_Flex_ENABLE) != 0u)
    {
        if((ADC_Flex_SAR_SAMPLE_CTRL_REG & ADC_Flex_CONTINUOUS_EN) != 0u)
        {
            ADC_Flex_backup.enableState = ADC_Flex_ENABLED | ADC_Flex_STARTED;
        }
        else
        {
            ADC_Flex_backup.enableState = ADC_Flex_ENABLED;
        }
        ADC_Flex_StopConvert();
        ADC_Flex_Stop();
        
        /* Disable the SAR internal pump before entering the chip low power mode */
        if((ADC_Flex_SAR_CTRL_REG & ADC_Flex_BOOSTPUMP_EN) != 0u)
        {
            ADC_Flex_SAR_CTRL_REG &= (uint32)~ADC_Flex_BOOSTPUMP_EN;
            ADC_Flex_backup.enableState |= ADC_Flex_BOOSTPUMP_ENABLED;
        }
    }
    else
    {
        ADC_Flex_backup.enableState = ADC_Flex_DISABLED;
    }
}


/*******************************************************************************
* Function Name: ADC_Flex_Wakeup
********************************************************************************
*
* Summary:
*  Restores the component enable state and configuration registers.
*  This should be called just after awaking from sleep mode.
*
* Parameters:
*  None.
*
* Return:
*  None.
*
* Global Variables:
*  ADC_Flex_backup - used.
*
*******************************************************************************/
void ADC_Flex_Wakeup(void)
{
    ADC_Flex_SAR_DFT_CTRL_REG = ADC_Flex_backup.dftRegVal;
    if(ADC_Flex_backup.enableState != ADC_Flex_DISABLED)
    {
        /* Enable the SAR internal pump  */
        if((ADC_Flex_backup.enableState & ADC_Flex_BOOSTPUMP_ENABLED) != 0u)
        {
            ADC_Flex_SAR_CTRL_REG |= ADC_Flex_BOOSTPUMP_EN;
        }
        ADC_Flex_Enable();
        if((ADC_Flex_backup.enableState & ADC_Flex_STARTED) != 0u)
        {
            ADC_Flex_StartConvert();
        }
    }
}
/* [] END OF FILE */
