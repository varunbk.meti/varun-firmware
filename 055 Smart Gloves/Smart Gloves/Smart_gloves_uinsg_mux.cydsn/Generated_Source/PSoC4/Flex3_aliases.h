/*******************************************************************************
* File Name: Flex3.h  
* Version 2.20
*
* Description:
*  This file contains the Alias definitions for Per-Pin APIs in cypins.h. 
*  Information on using these APIs can be found in the System Reference Guide.
*
* Note:
*
********************************************************************************
* Copyright 2008-2015, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_PINS_Flex3_ALIASES_H) /* Pins Flex3_ALIASES_H */
#define CY_PINS_Flex3_ALIASES_H

#include "cytypes.h"
#include "cyfitter.h"
#include "cypins.h"


/***************************************
*              Constants        
***************************************/
#define Flex3_0			(Flex3__0__PC)
#define Flex3_0_PS		(Flex3__0__PS)
#define Flex3_0_PC		(Flex3__0__PC)
#define Flex3_0_DR		(Flex3__0__DR)
#define Flex3_0_SHIFT	(Flex3__0__SHIFT)
#define Flex3_0_INTR	((uint16)((uint16)0x0003u << (Flex3__0__SHIFT*2u)))

#define Flex3_INTR_ALL	 ((uint16)(Flex3_0_INTR))


#endif /* End Pins Flex3_ALIASES_H */


/* [] END OF FILE */
