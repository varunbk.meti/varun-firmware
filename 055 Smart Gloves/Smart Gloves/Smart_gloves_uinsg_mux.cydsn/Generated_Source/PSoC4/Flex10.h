/*******************************************************************************
* File Name: Flex10.h  
* Version 2.20
*
* Description:
*  This file contains Pin function prototypes and register defines
*
********************************************************************************
* Copyright 2008-2015, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_PINS_Flex10_H) /* Pins Flex10_H */
#define CY_PINS_Flex10_H

#include "cytypes.h"
#include "cyfitter.h"
#include "Flex10_aliases.h"


/***************************************
*     Data Struct Definitions
***************************************/

/**
* \addtogroup group_structures
* @{
*/
    
/* Structure for sleep mode support */
typedef struct
{
    uint32 pcState; /**< State of the port control register */
    uint32 sioState; /**< State of the SIO configuration */
    uint32 usbState; /**< State of the USBIO regulator */
} Flex10_BACKUP_STRUCT;

/** @} structures */


/***************************************
*        Function Prototypes             
***************************************/
/**
* \addtogroup group_general
* @{
*/
uint8   Flex10_Read(void);
void    Flex10_Write(uint8 value);
uint8   Flex10_ReadDataReg(void);
#if defined(Flex10__PC) || (CY_PSOC4_4200L) 
    void    Flex10_SetDriveMode(uint8 mode);
#endif
void    Flex10_SetInterruptMode(uint16 position, uint16 mode);
uint8   Flex10_ClearInterrupt(void);
/** @} general */

/**
* \addtogroup group_power
* @{
*/
void Flex10_Sleep(void); 
void Flex10_Wakeup(void);
/** @} power */


/***************************************
*           API Constants        
***************************************/
#if defined(Flex10__PC) || (CY_PSOC4_4200L) 
    /* Drive Modes */
    #define Flex10_DRIVE_MODE_BITS        (3)
    #define Flex10_DRIVE_MODE_IND_MASK    (0xFFFFFFFFu >> (32 - Flex10_DRIVE_MODE_BITS))

    /**
    * \addtogroup group_constants
    * @{
    */
        /** \addtogroup driveMode Drive mode constants
         * \brief Constants to be passed as "mode" parameter in the Flex10_SetDriveMode() function.
         *  @{
         */
        #define Flex10_DM_ALG_HIZ         (0x00u) /**< \brief High Impedance Analog   */
        #define Flex10_DM_DIG_HIZ         (0x01u) /**< \brief High Impedance Digital  */
        #define Flex10_DM_RES_UP          (0x02u) /**< \brief Resistive Pull Up       */
        #define Flex10_DM_RES_DWN         (0x03u) /**< \brief Resistive Pull Down     */
        #define Flex10_DM_OD_LO           (0x04u) /**< \brief Open Drain, Drives Low  */
        #define Flex10_DM_OD_HI           (0x05u) /**< \brief Open Drain, Drives High */
        #define Flex10_DM_STRONG          (0x06u) /**< \brief Strong Drive            */
        #define Flex10_DM_RES_UPDWN       (0x07u) /**< \brief Resistive Pull Up/Down  */
        /** @} driveMode */
    /** @} group_constants */
#endif

/* Digital Port Constants */
#define Flex10_MASK               Flex10__MASK
#define Flex10_SHIFT              Flex10__SHIFT
#define Flex10_WIDTH              1u

/**
* \addtogroup group_constants
* @{
*/
    /** \addtogroup intrMode Interrupt constants
     * \brief Constants to be passed as "mode" parameter in Flex10_SetInterruptMode() function.
     *  @{
     */
        #define Flex10_INTR_NONE      ((uint16)(0x0000u)) /**< \brief Disabled             */
        #define Flex10_INTR_RISING    ((uint16)(0x5555u)) /**< \brief Rising edge trigger  */
        #define Flex10_INTR_FALLING   ((uint16)(0xaaaau)) /**< \brief Falling edge trigger */
        #define Flex10_INTR_BOTH      ((uint16)(0xffffu)) /**< \brief Both edge trigger    */
    /** @} intrMode */
/** @} group_constants */

/* SIO LPM definition */
#if defined(Flex10__SIO)
    #define Flex10_SIO_LPM_MASK       (0x03u)
#endif

/* USBIO definitions */
#if !defined(Flex10__PC) && (CY_PSOC4_4200L)
    #define Flex10_USBIO_ENABLE               ((uint32)0x80000000u)
    #define Flex10_USBIO_DISABLE              ((uint32)(~Flex10_USBIO_ENABLE))
    #define Flex10_USBIO_SUSPEND_SHIFT        CYFLD_USBDEVv2_USB_SUSPEND__OFFSET
    #define Flex10_USBIO_SUSPEND_DEL_SHIFT    CYFLD_USBDEVv2_USB_SUSPEND_DEL__OFFSET
    #define Flex10_USBIO_ENTER_SLEEP          ((uint32)((1u << Flex10_USBIO_SUSPEND_SHIFT) \
                                                        | (1u << Flex10_USBIO_SUSPEND_DEL_SHIFT)))
    #define Flex10_USBIO_EXIT_SLEEP_PH1       ((uint32)~((uint32)(1u << Flex10_USBIO_SUSPEND_SHIFT)))
    #define Flex10_USBIO_EXIT_SLEEP_PH2       ((uint32)~((uint32)(1u << Flex10_USBIO_SUSPEND_DEL_SHIFT)))
    #define Flex10_USBIO_CR1_OFF              ((uint32)0xfffffffeu)
#endif


/***************************************
*             Registers        
***************************************/
/* Main Port Registers */
#if defined(Flex10__PC)
    /* Port Configuration */
    #define Flex10_PC                 (* (reg32 *) Flex10__PC)
#endif
/* Pin State */
#define Flex10_PS                     (* (reg32 *) Flex10__PS)
/* Data Register */
#define Flex10_DR                     (* (reg32 *) Flex10__DR)
/* Input Buffer Disable Override */
#define Flex10_INP_DIS                (* (reg32 *) Flex10__PC2)

/* Interrupt configuration Registers */
#define Flex10_INTCFG                 (* (reg32 *) Flex10__INTCFG)
#define Flex10_INTSTAT                (* (reg32 *) Flex10__INTSTAT)

/* "Interrupt cause" register for Combined Port Interrupt (AllPortInt) in GSRef component */
#if defined (CYREG_GPIO_INTR_CAUSE)
    #define Flex10_INTR_CAUSE         (* (reg32 *) CYREG_GPIO_INTR_CAUSE)
#endif

/* SIO register */
#if defined(Flex10__SIO)
    #define Flex10_SIO_REG            (* (reg32 *) Flex10__SIO)
#endif /* (Flex10__SIO_CFG) */

/* USBIO registers */
#if !defined(Flex10__PC) && (CY_PSOC4_4200L)
    #define Flex10_USB_POWER_REG       (* (reg32 *) CYREG_USBDEVv2_USB_POWER_CTRL)
    #define Flex10_CR1_REG             (* (reg32 *) CYREG_USBDEVv2_CR1)
    #define Flex10_USBIO_CTRL_REG      (* (reg32 *) CYREG_USBDEVv2_USB_USBIO_CTRL)
#endif    
    
    
/***************************************
* The following code is DEPRECATED and 
* must not be used in new designs.
***************************************/
/**
* \addtogroup group_deprecated
* @{
*/
#define Flex10_DRIVE_MODE_SHIFT       (0x00u)
#define Flex10_DRIVE_MODE_MASK        (0x07u << Flex10_DRIVE_MODE_SHIFT)
/** @} deprecated */

#endif /* End Pins Flex10_H */


/* [] END OF FILE */
