/*******************************************************************************
* File Name: Flex4.h  
* Version 2.20
*
* Description:
*  This file contains the Alias definitions for Per-Pin APIs in cypins.h. 
*  Information on using these APIs can be found in the System Reference Guide.
*
* Note:
*
********************************************************************************
* Copyright 2008-2015, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_PINS_Flex4_ALIASES_H) /* Pins Flex4_ALIASES_H */
#define CY_PINS_Flex4_ALIASES_H

#include "cytypes.h"
#include "cyfitter.h"
#include "cypins.h"


/***************************************
*              Constants        
***************************************/
#define Flex4_0			(Flex4__0__PC)
#define Flex4_0_PS		(Flex4__0__PS)
#define Flex4_0_PC		(Flex4__0__PC)
#define Flex4_0_DR		(Flex4__0__DR)
#define Flex4_0_SHIFT	(Flex4__0__SHIFT)
#define Flex4_0_INTR	((uint16)((uint16)0x0003u << (Flex4__0__SHIFT*2u)))

#define Flex4_INTR_ALL	 ((uint16)(Flex4_0_INTR))


#endif /* End Pins Flex4_ALIASES_H */


/* [] END OF FILE */
