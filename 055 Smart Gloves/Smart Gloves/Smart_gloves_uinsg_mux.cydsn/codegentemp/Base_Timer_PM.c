/*******************************************************************************
* File Name: Base_Timer_PM.c
* Version 2.80
*
*  Description:
*     This file provides the power management source code to API for the
*     Timer.
*
*   Note:
*     None
*
*******************************************************************************
* Copyright 2008-2017, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions,
* disclaimers, and limitations in the end user license agreement accompanying
* the software package with which this file was provided.
********************************************************************************/

#include "Base_Timer.h"

static Base_Timer_backupStruct Base_Timer_backup;


/*******************************************************************************
* Function Name: Base_Timer_SaveConfig
********************************************************************************
*
* Summary:
*     Save the current user configuration
*
* Parameters:
*  void
*
* Return:
*  void
*
* Global variables:
*  Base_Timer_backup:  Variables of this global structure are modified to
*  store the values of non retention configuration registers when Sleep() API is
*  called.
*
*******************************************************************************/
void Base_Timer_SaveConfig(void) 
{
    #if (!Base_Timer_UsingFixedFunction)
        Base_Timer_backup.TimerUdb = Base_Timer_ReadCounter();
        Base_Timer_backup.InterruptMaskValue = Base_Timer_STATUS_MASK;
        #if (Base_Timer_UsingHWCaptureCounter)
            Base_Timer_backup.TimerCaptureCounter = Base_Timer_ReadCaptureCount();
        #endif /* Back Up capture counter register  */

        #if(!Base_Timer_UDB_CONTROL_REG_REMOVED)
            Base_Timer_backup.TimerControlRegister = Base_Timer_ReadControlRegister();
        #endif /* Backup the enable state of the Timer component */
    #endif /* Backup non retention registers in UDB implementation. All fixed function registers are retention */
}


/*******************************************************************************
* Function Name: Base_Timer_RestoreConfig
********************************************************************************
*
* Summary:
*  Restores the current user configuration.
*
* Parameters:
*  void
*
* Return:
*  void
*
* Global variables:
*  Base_Timer_backup:  Variables of this global structure are used to
*  restore the values of non retention registers on wakeup from sleep mode.
*
*******************************************************************************/
void Base_Timer_RestoreConfig(void) 
{   
    #if (!Base_Timer_UsingFixedFunction)

        Base_Timer_WriteCounter(Base_Timer_backup.TimerUdb);
        Base_Timer_STATUS_MASK =Base_Timer_backup.InterruptMaskValue;
        #if (Base_Timer_UsingHWCaptureCounter)
            Base_Timer_SetCaptureCount(Base_Timer_backup.TimerCaptureCounter);
        #endif /* Restore Capture counter register*/

        #if(!Base_Timer_UDB_CONTROL_REG_REMOVED)
            Base_Timer_WriteControlRegister(Base_Timer_backup.TimerControlRegister);
        #endif /* Restore the enable state of the Timer component */
    #endif /* Restore non retention registers in the UDB implementation only */
}


/*******************************************************************************
* Function Name: Base_Timer_Sleep
********************************************************************************
*
* Summary:
*     Stop and Save the user configuration
*
* Parameters:
*  void
*
* Return:
*  void
*
* Global variables:
*  Base_Timer_backup.TimerEnableState:  Is modified depending on the
*  enable state of the block before entering sleep mode.
*
*******************************************************************************/
void Base_Timer_Sleep(void) 
{
    #if(!Base_Timer_UDB_CONTROL_REG_REMOVED)
        /* Save Counter's enable state */
        if(Base_Timer_CTRL_ENABLE == (Base_Timer_CONTROL & Base_Timer_CTRL_ENABLE))
        {
            /* Timer is enabled */
            Base_Timer_backup.TimerEnableState = 1u;
        }
        else
        {
            /* Timer is disabled */
            Base_Timer_backup.TimerEnableState = 0u;
        }
    #endif /* Back up enable state from the Timer control register */
    Base_Timer_Stop();
    Base_Timer_SaveConfig();
}


/*******************************************************************************
* Function Name: Base_Timer_Wakeup
********************************************************************************
*
* Summary:
*  Restores and enables the user configuration
*
* Parameters:
*  void
*
* Return:
*  void
*
* Global variables:
*  Base_Timer_backup.enableState:  Is used to restore the enable state of
*  block on wakeup from sleep mode.
*
*******************************************************************************/
void Base_Timer_Wakeup(void) 
{
    Base_Timer_RestoreConfig();
    #if(!Base_Timer_UDB_CONTROL_REG_REMOVED)
        if(Base_Timer_backup.TimerEnableState == 1u)
        {     /* Enable Timer's operation */
                Base_Timer_Enable();
        } /* Do nothing if Timer was disabled before */
    #endif /* Remove this code section if Control register is removed */
}


/* [] END OF FILE */
