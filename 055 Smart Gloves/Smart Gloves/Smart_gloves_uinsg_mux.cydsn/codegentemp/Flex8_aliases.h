/*******************************************************************************
* File Name: Flex8.h  
* Version 2.20
*
* Description:
*  This file contains the Alias definitions for Per-Pin APIs in cypins.h. 
*  Information on using these APIs can be found in the System Reference Guide.
*
* Note:
*
********************************************************************************
* Copyright 2008-2015, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_PINS_Flex8_ALIASES_H) /* Pins Flex8_ALIASES_H */
#define CY_PINS_Flex8_ALIASES_H

#include "cytypes.h"
#include "cyfitter.h"
#include "cypins.h"


/***************************************
*              Constants        
***************************************/
#define Flex8_0			(Flex8__0__PC)
#define Flex8_0_PS		(Flex8__0__PS)
#define Flex8_0_PC		(Flex8__0__PC)
#define Flex8_0_DR		(Flex8__0__DR)
#define Flex8_0_SHIFT	(Flex8__0__SHIFT)
#define Flex8_0_INTR	((uint16)((uint16)0x0003u << (Flex8__0__SHIFT*2u)))

#define Flex8_INTR_ALL	 ((uint16)(Flex8_0_INTR))


#endif /* End Pins Flex8_ALIASES_H */


/* [] END OF FILE */
