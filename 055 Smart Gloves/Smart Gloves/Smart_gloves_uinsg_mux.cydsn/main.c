/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/
#include "project.h"
#include "stdio.h"
#include "main.h"

#define Turn_On_15ms_Timer

#define Timer_1ms 0x01
#define Timer_15ms 0x02
#define Timer_100ms 0x04
#define Timer_1sec 0x08

uint8_t timer_flgs=0;
uint8_t timer_1ms_base=0;
uint8_t timer_15ms_base=15;
uint8_t timer_100ms_base=100;
uint16_t timer_1sec_base=1000;

int16_t Flex[sensor_size];

uint8_t i2c_buff[TOTAL_PACKET_SIZE];

char buff[40];  //debug bbuffer

CY_ISR(Base_Tmr)
{
    timer_15ms_base--;
    if(timer_15ms_base==0)
    {
        timer_15ms_base=15;
        timer_flgs|=Timer_15ms;
    }

    timer_100ms_base--;
    if(timer_100ms_base==0)
    {
        timer_100ms_base=100;
        timer_flgs|=Timer_100ms;
    }
    
    timer_1sec_base--;
    if(timer_1sec_base==0)
    {
        timer_1sec_base=1000;
        timer_flgs|=Timer_1sec;
    }
}

void Timer_Start()
{
    Base_Timer_Start();
    isr_1ms_Start();
    isr_1ms_StartEx(Base_Tmr);
}

int main(void)
{    
    CyGlobalIntEnable; /* Enable global interrupts. */
    
    ADC_Initialize();
    I2C_Initialize();
    Flex_Mux_Start();
    
    for(;;)
    {  
        for(uint8_t ch=0;ch<Max_Channels;ch++)
        { 
            Flex_Process(ch);      
        }
        
#ifdef Turn_On_15ms_Timer
    
        if(timer_flgs & Timer_15ms)
        {
            timer_flgs&=~Timer_15ms;
            I2C_Udate();
        }
#endif
        
        
        UART_PutString("\r\n");
    }
}

void ADC_Initialize(void)
{
    UART_Start();
    ADC_Flex_Start();
    ADC_Flex_StartConvert();
}

void Flex_Process(uint8_t channel)
{
    Flex_Mux_Select(channel);
    CyDelay(1);
    CyDelayUs(300); //min switch delay
    ADC_Flex_IsEndConversion(ADC_Flex_WAIT_FOR_RESULT);
    Flex[channel]=ADC_Flex_GetResult16(0);
    Flex[channel]/=100;
    
    sprintf(buff,"%3d,  ",Flex[channel]);
    UART_PutString(buff);  
}


//I2C Initialization
void I2C_Initialize(void)
{
    EZI2C_Flex_EzI2CSetBuffer1(sizeof(i2c_buff),READ_ONLY_OFFSET,i2c_buff);
    EZI2C_Flex_Start();
}

//I2C Update
void I2C_Udate(void)
{
    for(uint8_t idx=0;idx<Max_Channels;idx++)
    {
       i2c_buff[idx]=Flex[idx];
//       sprintf(buff,"%3d,  ",i2c_buff[idx]);
//       UART_PutString(buff);  
    }
}
