/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/

/* [] END OF FILE */
#ifndef __main_h
#define __main_h
    
#include "project.h"

//macros
#define sensor_size 10 //for 6 flex sensor
#define Filter_co 0.8 //filter coefficient
#define division_factor  10
    
#define Max_Channels 10 //Max A_Mux Channels 
    
#define TOTAL_PACKET_SIZE (uint8_t) (0xA)
#define READ_ONLY_OFFSET    (0u)   
        
//Function declarations
void ADC_Initialize(void);
void Flex_Process(uint8_t channel);
void I2C_Initialize(void);
void I2C_Udate(void);

#endif /* __main_h */