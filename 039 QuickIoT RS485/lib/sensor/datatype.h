#ifndef _DATA_TYPE_H_INCLUDED_
#define _DATA_TYPE_H_INCLUDED_ 1

#include<string>
#include<defs.h>

using namespace std;

class DataType
{
public:
    enum Value : uint8
    {
        DT_BIT0 = 0,
        DT_BIT1 = 1,
        DT_BIT2 = 2,
        DT_BIT3 = 3,
        DT_BIT4 = 4,
        DT_BIT5 = 5,
        DT_BIT6 = 6,
        DT_BIT7 = 7,
        DT_UINT8 = 8,
        DT_INT16 = 9,
        DT_UINT16 = 10,
        DT_INT32 = 11,
        DT_UINT32 = 12,
        DT_FLOAT = 13,
        DT_STREAM_4_BYTES = 14,
        DT_INT64 = 15,
        DT_UINT64 = 16,
        DT_DOUBLE = 17,
        DT_ASCII = 18
    };

    DataType() = default;
    DataType(Value code) : m_iCode(code) { }

    operator Value() const { return m_iCode; }
    explicit operator bool() = delete;
    constexpr bool operator==(DataType a) const { return m_iCode == a.m_iCode; }
    constexpr bool operator!=(DataType a) const { return m_iCode != a.m_iCode; }
    uint8 GetCode();
    string GetLabel();
    string ToString();

    uint8 GetSize();
    uint8 GetBitMask();
    bool IsSigned();

    static DataType Parse(int code);

private:
    Value m_iCode;
};

#endif
