#include<string>
#include<byteordertype.h>

using namespace std;

uint8 ByteOrderType::GetArray( uint8* dest)
{
    uint8 idx = 0;
    if ( m_iCode == BO_0123_3210 ) {
        dest[idx++] = 3;
        dest[idx++] = 2;
        dest[idx++] = 1;
        dest[idx++] = 0;
    }
    else if ( m_iCode == BO_0123_2301 ) {
        dest[idx++] = 2;
        dest[idx++] = 3;
        dest[idx++] = 0;
        dest[idx++] = 1;
    }
    else if ( m_iCode == BO_0123_0123 ) {
        dest[idx++] = 0;
        dest[idx++] = 1;
        dest[idx++] = 2;
        dest[idx++] = 3;
    }
    else if ( m_iCode == BO_0123_1032 ) {
        dest[idx++] = 1;
        dest[idx++] = 0;
        dest[idx++] = 3;
        dest[idx++] = 2;
    }
    else if ( m_iCode == BO_01_10 ) {
        dest[idx++] = 0;
        dest[idx++] = 0;
        dest[idx++] = 1;
        dest[idx++] = 0;
    }
    else if ( m_iCode == BO_01_01 ) {
        dest[idx++] = 0;
        dest[idx++] = 0;
        dest[idx++] = 0;
        dest[idx++] = 1;
    }
    else if ( m_iCode == BO_0_0 ) {
        dest[idx++] = 0;
        dest[idx++] = 0;
        dest[idx++] = 0;
        dest[idx++] = 0;
    }
    else if ( m_iCode == BO_1_0 ) {
        dest[idx++] = 0;
        dest[idx++] = 0;
        dest[idx++] = 0;
        dest[idx++] = 1;
    }
    else if ( m_iCode == BO_01234567_76543210 ) {
        dest[idx++] = 7;
        dest[idx++] = 6;
        dest[idx++] = 5;
        dest[idx++] = 4;
        dest[idx++] = 3;
        dest[idx++] = 2;
        dest[idx++] = 1;
        dest[idx++] = 0;
    }
    else if ( m_iCode == BO_01234567_67452301 ) {
        dest[idx++] = 6;
        dest[idx++] = 7;
        dest[idx++] = 4;
        dest[idx++] = 5;
        dest[idx++] = 2;
        dest[idx++] = 3;
        dest[idx++] = 0;
        dest[idx++] = 1;
    }
    else if ( m_iCode == BO_01234567_01234567 ) {
        dest[idx++] = 0;
        dest[idx++] = 1;
        dest[idx++] = 2;
        dest[idx++] = 3;
        dest[idx++] = 4;
        dest[idx++] = 5;
        dest[idx++] = 6;
        dest[idx++] = 7;
    }
    else if ( m_iCode == BO_01234567_10325476 ) {
        dest[idx++] = 1;
        dest[idx++] = 0;
        dest[idx++] = 3;
        dest[idx++] = 2;
        dest[idx++] = 5;
        dest[idx++] = 4;
        dest[idx++] = 7;
        dest[idx++] = 6;
    }
    else if ( m_iCode == BO_01234567_32107654 ) {
        dest[idx++] = 3;
        dest[idx++] = 2;
        dest[idx++] = 1;
        dest[idx++] = 0;
        dest[idx++] = 7;
        dest[idx++] = 6;
        dest[idx++] = 5;
        dest[idx++] = 4;
    }
    else if ( m_iCode == BO_01234567_23016745 ) {
        dest[idx++] = 2;
        dest[idx++] = 3;
        dest[idx++] = 0;
        dest[idx++] = 1;
        dest[idx++] = 6;
        dest[idx++] = 7;
        dest[idx++] = 4;
        dest[idx++] = 5;
    }
    else if ( m_iCode == BO_01234567_45670123 ) {
        dest[idx++] = 4;
        dest[idx++] = 5;
        dest[idx++] = 6;
        dest[idx++] = 7;
        dest[idx++] = 0;
        dest[idx++] = 1;
        dest[idx++] = 2;
        dest[idx++] = 3;
    }
    else if ( m_iCode == BO_01234567_54761032 ) {
        dest[idx++] = 5;
        dest[idx++] = 4;
        dest[idx++] = 7;
        dest[idx++] = 6;
        dest[idx++] = 1;
        dest[idx++] = 0;
        dest[idx++] = 3;
        dest[idx++] = 2;
    }

    return idx;
}
ByteOrderType ByteOrderType::Parse(int code)
{
    if ( code == BO_0123_3210 ) {
        return BO_0123_3210;
    }
    else if ( code == BO_0123_2301 ) {
        return BO_0123_2301;
    }
    else if ( code == BO_0123_0123 ) {
        return BO_0123_0123;
    }
    else if ( code == BO_0123_1032 ) {
        return BO_0123_1032;
    }
    else if ( code == BO_01_10 ) {
        return BO_01_10;
    }
    else if ( code == BO_01_01 ) {
        return BO_01_01;
    }
    else if ( code == BO_0_0 ) {
        return BO_0_0;
    }
    else if ( code == BO_1_0 ) {
        return BO_1_0;
    }
    else if ( code == BO_01234567_76543210 ) {
        return BO_01234567_76543210;
    }
    else if ( code == BO_01234567_67452301 ) {
        return BO_01234567_67452301;
    }
    else if ( code == BO_01234567_01234567 ) {
        return BO_01234567_01234567;
    }
    else if ( code == BO_01234567_10325476 ) {
        return BO_01234567_10325476;
    }
    else if ( code == BO_01234567_32107654 ) {
        return BO_01234567_32107654;
    }
    else if ( code == BO_01234567_23016745 ) {
        return BO_01234567_23016745;
    }
    else if ( code == BO_01234567_45670123 ) {
        return BO_01234567_45670123;
    }
    else if ( code == BO_01234567_54761032 ) {
        return BO_01234567_54761032;
    }

    return BO_0123_3210;
}

string ByteOrderType::GetLabel()
{
    if ( m_iCode == BO_0123_3210 ) {
        return "BO_0123_3210";
    }
    else if ( m_iCode == BO_0123_2301 ) {
        return "BO_0123_2301";
    }
    else if ( m_iCode == BO_0123_0123 ) {
        return "BO_0123_0123";
    }
    else if ( m_iCode == BO_0123_1032 ) {
        return "BO_0123_1032";
    }
    else if ( m_iCode == BO_01_10 ) {
        return "BO_01_10";
    }
    else if ( m_iCode == BO_01_01 ) {
        return "BO_01_01";
    }
    else if ( m_iCode == BO_0_0 ) {
        return "BO_0_0";
    }
    else if ( m_iCode == BO_1_0 ) {
        return "BO_1_0";
    }
    else if ( m_iCode == BO_01234567_76543210 ) {
        return "BO_01234567_76543210";
    }
    else if ( m_iCode == BO_01234567_67452301 ) {
        return "BO_01234567_67452301";
    }
    else if ( m_iCode == BO_01234567_01234567 ) {
        return "BO_01234567_01234567";
    }
    else if ( m_iCode == BO_01234567_10325476 ) {
        return "BO_01234567_10325476";
    }
    else if ( m_iCode == BO_01234567_32107654 ) {
        return "BO_01234567_32107654";
    }
    else if ( m_iCode == BO_01234567_23016745 ) {
        return "BO_01234567_23016745";
    }
    else if ( m_iCode == BO_01234567_45670123 ) {
        return "BO_01234567_45670123";
    }
    else if ( m_iCode == BO_01234567_54761032 ) {
        return "BO_01234567_54761032";
    }

    return "";
}

string ByteOrderType::ToString()
{
    return this->GetLabel();
}

uint8 ByteOrderType::GetCode()
{
    return m_iCode;
}

