#include<string>
#include<datatype.h>

using namespace std;

uint8 DataType::GetBitMask()
{
    if ( m_iCode == DT_BIT0 ) {
        return 0x01;
    }
    else if ( m_iCode == DT_BIT1 ) {
        return 0x02;
    }
    else if ( m_iCode == DT_BIT2 ) {
        return 0x04;
    }
    else if ( m_iCode == DT_BIT3 ) {
        return 0x08;
    }
    else if ( m_iCode == DT_BIT4 ) {
        return 0x10;
    }
    else if ( m_iCode == DT_BIT5 ) {
        return 0x20;
    }
    else if ( m_iCode == DT_BIT6 ) {
        return 0x40;
    }
    else if ( m_iCode == DT_BIT7 ) {
        return 0x80;
    }

    return 0x00;
}

uint8 DataType::GetSize()
{
    if ( m_iCode == DT_BIT0 ) {
        return 2;
    }
    else if ( m_iCode == DT_BIT1 ) {
        return 2;
    }
    else if ( m_iCode == DT_BIT2 ) {
        return 2;
    }
    else if ( m_iCode == DT_BIT3 ) {
        return 2;
    }
    else if ( m_iCode == DT_BIT4 ) {
        return 2;
    }
    else if ( m_iCode == DT_BIT5 ) {
        return 2;
    }
    else if ( m_iCode == DT_BIT6 ) {
        return 2;
    }
    else if ( m_iCode == DT_BIT7 ) {
        return 2;
    }
    else if ( m_iCode == DT_UINT8 ) {
        return 2;
    }
    else if ( m_iCode == DT_INT16 ) {
        return 2;
    }
    else if ( m_iCode == DT_UINT16 ) {
        return 2;
    }
    else if ( m_iCode == DT_INT32 ) {
        return 4;
    }
    else if ( m_iCode == DT_UINT32 ) {
        return 4;
    }
    else if ( m_iCode == DT_FLOAT ) {
        return 4;
    }
    else if ( m_iCode == DT_STREAM_4_BYTES ) {
        return 4;
    }
    else if ( m_iCode == DT_INT64 ) {
        return 0;
    }
    else if ( m_iCode == DT_UINT64 ) {
        return 0;
    }
    else if ( m_iCode == DT_DOUBLE ) {
        return 0;
    }
    else if ( m_iCode == DT_ASCII ) {
        return 0;
    }

    return 0;
}

bool DataType::IsSigned()
{
    if ( m_iCode == DT_INT16 ) {
        return true;
    }
    else if ( m_iCode == DT_INT32 ) {
        return true;
    }
    return false;
}

DataType DataType::Parse(int code)
{
    if ( code == DT_BIT0 ) {
        return DT_BIT0;
    }
    else if ( code == DT_BIT1 ) {
        return DT_BIT1;
    }
    else if ( code == DT_BIT2 ) {
        return DT_BIT2;
    }
    else if ( code == DT_BIT3 ) {
        return DT_BIT3;
    }
    else if ( code == DT_BIT4 ) {
        return DT_BIT4;
    }
    else if ( code == DT_BIT5 ) {
        return DT_BIT5;
    }
    else if ( code == DT_BIT6 ) {
        return DT_BIT6;
    }
    else if ( code == DT_BIT7 ) {
        return DT_BIT7;
    }
    else if ( code == DT_UINT8 ) {
        return DT_UINT8;
    }
    else if ( code == DT_INT16 ) {
        return DT_INT16;
    }
    else if ( code == DT_UINT16 ) {
        return DT_UINT16;
    }
    else if ( code == DT_INT32 ) {
        return DT_INT32;
    }
    else if ( code == DT_UINT32 ) {
        return DT_UINT32;
    }
    else if ( code == DT_FLOAT ) {
        return DT_FLOAT;
    }
    else if ( code == DT_STREAM_4_BYTES ) {
        return DT_STREAM_4_BYTES;
    }
    else if ( code == DT_INT64 ) {
        return DT_INT64;
    }
    else if ( code == DT_UINT64 ) {
        return DT_UINT64;
    }
    else if ( code == DT_DOUBLE ) {
        return DT_DOUBLE;
    }
    else if ( code == DT_ASCII ) {
        return DT_ASCII;
    }

    return DT_BIT0;
}

string DataType::GetLabel()
{
    if ( m_iCode == DT_BIT0 ) {
        return "DT_BIT0";
    }
    else if ( m_iCode == DT_BIT1 ) {
        return "DT_BIT1";
    }
    else if ( m_iCode == DT_BIT2 ) {
        return "DT_BIT2";
    }
    else if ( m_iCode == DT_BIT3 ) {
        return "DT_BIT3";
    }
    else if ( m_iCode == DT_BIT4 ) {
        return "DT_BIT4";
    }
    else if ( m_iCode == DT_BIT5 ) {
        return "DT_BIT5";
    }
    else if ( m_iCode == DT_BIT6 ) {
        return "DT_BIT6";
    }
    else if ( m_iCode == DT_BIT7 ) {
        return "DT_BIT7";
    }
    else if ( m_iCode == DT_UINT8 ) {
        return "DT_UINT8";
    }
    else if ( m_iCode == DT_INT16 ) {
        return "DT_INT16";
    }
    else if ( m_iCode == DT_UINT16 ) {
        return "DT_UINT16";
    }
    else if ( m_iCode == DT_INT32 ) {
        return "DT_INT32";
    }
    else if ( m_iCode == DT_UINT32 ) {
        return "DT_UINT32";
    }
    else if ( m_iCode == DT_FLOAT ) {
        return "DT_FLOAT";
    }
    else if ( m_iCode == DT_STREAM_4_BYTES ) {
        return "DT_STREAM_4_BYTES";
    }
    else if ( m_iCode == DT_INT64 ) {
        return "DT_INT64";
    }
    else if ( m_iCode == DT_UINT64 ) {
        return "DT_UINT64";
    }
    else if ( m_iCode == DT_DOUBLE ) {
        return "DT_DOUBLE";
    }
    else if ( m_iCode == DT_ASCII ) {
        return "DT_ASCII";
    }

    return "";
}

string DataType::ToString()
{
    return this->GetLabel();
}

uint8 DataType::GetCode()
{
    return m_iCode;
}

