#include<blinkconfig.h>

BlinkConfig::BlinkConfig()
{
	this->m_iBlinkCount = 0;
	this->m_iOnTime = 0;
	this->m_iOffTime = 0;
	this->m_iCycleCount = 0;
	this->m_iSleepInterval = 0;
}

BlinkConfig::~BlinkConfig()
{
	this->m_iBlinkCount = 0;
	this->m_iOnTime = 0;
	this->m_iOffTime = 0;
	this->m_iCycleCount = 0;
	this->m_iSleepInterval = 0;
}

void BlinkConfig::Set(bool p_bContinuous, uint16 p_iOnTime, uint16 p_iOffTime, uint8 p_iBlinkCount, uint8 p_iCycleCount, uint16 p_iSleepInterval)
{
	this->m_bContinuous = p_bContinuous;
	this->m_iOnTime = p_iOnTime;
	this->m_iOffTime = p_iOffTime;
	this->m_iBlinkCount = p_iBlinkCount;
	this->m_iCycleCount = p_iCycleCount;
	this->m_iSleepInterval = p_iSleepInterval;
}

uint8 BlinkConfig::GetBlinkCount()
{
	return this->m_iBlinkCount;
}

void BlinkConfig::SetBlinkCount(uint8 value)
{
	this->m_iBlinkCount = value;
}

uint16 BlinkConfig::GetOnTime()
{
	return this->m_iOnTime;
}

void BlinkConfig::SetOnTime(uint16 value)
{
	this->m_iOnTime = value;
}

uint16 BlinkConfig::GetOffTime()
{
	return this->m_iOffTime;
}

void BlinkConfig::SetOffTime(uint16 value)
{
	this->m_iOffTime = value;
}

uint8 BlinkConfig::GetCycleCount()
{
	return this->m_iCycleCount;
}

void BlinkConfig::SetCycleCount(uint8 value)
{
	this->m_iCycleCount = value;
}

uint16 BlinkConfig::GetSleepInterval()
{
	return this->m_iSleepInterval;
}

void BlinkConfig::SetSleepInterval(uint16 value)
{
	this->m_iSleepInterval = value;
}

bool BlinkConfig::IsContinuous()
{
	return this->m_bContinuous;
}

void BlinkConfig::EnableContinuous()
{
	this->m_bContinuous = true;
}

void BlinkConfig::DisableContinuous()
{
	this->m_bContinuous = false;
}

