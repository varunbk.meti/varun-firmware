#include<string>
#include<paritytype.h>

using namespace std;

ParityType ParityType::Parse(int code)
{
    if (code == Even) {
        return Even;
    }
    else if (code == Odd) {
        return Odd;
    }
    else if (code == MarkSpace) {
        return MarkSpace;
    }
    return None;
}

string ParityType::ToString()
{
    return this->GetLabel();
}

uint8 ParityType::GetCode()
{
    return m_iCode;
}

string ParityType::GetLabel()
{
    if (m_iCode == Even) {
        return "Even";
    }
    else if (m_iCode == Odd) {
        return "Odd";
    }
    else if (m_iCode == MarkSpace) {
        return "MarkSpace";
    }

    return "None";
}
