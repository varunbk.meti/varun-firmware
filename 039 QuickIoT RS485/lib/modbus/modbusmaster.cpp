
#include<stdio.h>
#include<string.h>
#include<modbusmaster.h>
#include<cdate.h>

using namespace std;

bool ModbusMaster::m_bDeleteLogger = true;
Logger* ModbusMaster::m_pLogger = new Logger();
void (*ModbusMaster::m_pExecuteCompleteCallBack)(void*) = NULL;

void ModbusMaster::Execute() {
	this->m_iCommandSize = 0;
	memset( this->m_cCommandBytes, 0x00, COMMAND_BUFFER_SIZE);
	this->m_iResponseSize = 0;
	memset( this->m_cResponseBytes, 0x00, RECEIVE_BUFFER_SIZE);

	this->CreateCommand();
	this->m_iExpectedBytes = this->m_oFunction.GetExpectedBytes(this->m_iRegisterCount);

	CDate l_oStartTime = CDate::Now();
	this->m_iRetries = 0;
	do {
		this->m_oCommError = ModbusCommError::ErrorTimeout;
		this->ExecuteCommand();
		this->m_iRetries++;
	} while ((this->m_iRetries < this->m_iRetryCount) && (ModbusCommError::DaqErrorNone != this->m_oCommError));

	this->m_lTimeTaken = l_oStartTime.TimeElapsed(CDate::Now(), IntervalType::MilliSecond);

	if (ModbusCommError::DaqErrorNone != this->m_oCommError) {
		return;
	}

	if (this->m_iResponseSize < 3) {
		this->m_oCommError = ModbusCommError::ErrorTimeout;
		return;
	}

	if (this->m_cResponseBytes[LOCATION_DEV_NUMBER] != this->m_iSlaveID) {
		this->m_oCommError = ModbusCommError::ErrorTimeout;
		return;
	}

	if (this->m_cResponseBytes[LOCATION_FUNC_CODE] != this->m_oFunction.GetCode()) {
		if ((this->m_cResponseBytes[LOCATION_FUNC_CODE] & 0x7F) != this->m_oFunction.GetCode()) {
			this->m_oCommError = ModbusCommError::InvalidFunction;
			return;
		}

		switch (this->m_cResponseBytes[LOCATION_ERROR_CODE]) {
		case 1:
			this->m_oCommError = ModbusCommError::InvalidFunction;
			break;

		case 2:
			this->m_oCommError = ModbusCommError::InvalidRegisterAddress;
			break;

		case 3:
			this->m_oCommError = ModbusCommError::InvalidDataValue;
			break;

		case 11:
			this->m_oCommError = ModbusCommError::GatewayFailed;
			break;

		case 100:
			this->m_oCommError = ModbusCommError::MetiNoNewData;
			break;

		default:
			this->m_oCommError = ModbusCommError::RtuErrorUnknown;
			break;
		};
	}

	if (ModbusCommError::DaqErrorNone == this->m_oCommError) {
		if (this->m_iResponseSize < this->m_iExpectedBytes) {
			this->m_oCommError = ModbusCommError::InvalidResponseLength;
		}
	}

	if (m_pExecuteCompleteCallBack != NULL) {
		m_pExecuteCompleteCallBack(this);
	}
}

void ModbusMaster::CreateCommand() {
	this->m_cCommandBytes[this->m_iCommandSize++] = this->m_iSlaveID;
	this->m_cCommandBytes[this->m_iCommandSize++] = this->m_oFunction.GetCode();
	this->m_cCommandBytes[this->m_iCommandSize++] = this->m_iStartRegister >> 8;
	this->m_cCommandBytes[this->m_iCommandSize++] = (uint8)this->m_iStartRegister;
	this->m_cCommandBytes[this->m_iCommandSize++] = this->m_iRegisterCount >> 8;
	this->m_cCommandBytes[this->m_iCommandSize++] = (uint8)this->m_iRegisterCount;
}

void ModbusMaster::ClearCommand() {
	memset( this->m_cCommandBytes, 0x00, COMMAND_BUFFER_SIZE);
	this->m_iCommandSize = 0;
}

void ModbusMaster::Clear() {
	this->m_iSlaveID = 0;
	this->m_oFunction = ModbusFunction::Unknown;
	this->m_iStartRegister = 0;
	this->m_iRegisterCount = 0;
	this->m_iReadTimeOut = 0;
	this->m_iRetryCount = 0;
	this->m_iRetries = 0;
	this->m_iCommandSize = 0;
	memset( this->m_cCommandBytes, 0x00, COMMAND_BUFFER_SIZE);
	this->m_iExpectedBytes = 0;
	this->m_oCommError = ModbusCommError::DaqErrorNone;
	this->m_lTimeTaken = 0;
	this->m_iExpectedBytes = 0;
	this->m_iResponseSize = 0;
	memset( this->m_cResponseBytes, 0x00, RECEIVE_BUFFER_SIZE);
}


ModbusMaster::ModbusMaster() {
	this->Clear();
}

ModbusMaster::~ModbusMaster() {
	this->Clear();
}

void ModbusMaster::Initialize(Logger* p_pValue, void (*p_pFunction)(void*))
{
	if (p_pValue == NULL) {
		m_pLogger->DisableConsole();
	}
	else {
		if (m_bDeleteLogger) {
			delete m_pLogger;
		}
		m_bDeleteLogger = false;
		m_pLogger = p_pValue;
	}
	m_pExecuteCompleteCallBack = p_pFunction;
}

void ModbusMaster::Release()
{
	if (m_bDeleteLogger) {
		delete m_pLogger;
	}
	m_bDeleteLogger = false;
	m_pLogger = NULL;
	m_pExecuteCompleteCallBack = NULL;
}

void ModbusMaster::SetSlaveID( uchar id) {
	this->m_iSlaveID = id;
}
uchar ModbusMaster::GetSlaveID() {
	return this->m_iSlaveID;
}

void ModbusMaster::SetStartRegister( uint16 startReg) {
	this->m_iStartRegister = startReg;
}

uint16 ModbusMaster::GetStartRegister() {
	return this->m_iStartRegister;
}

void ModbusMaster::SetRegisterCount( uint16 regCount) {
	this->m_iRegisterCount = regCount;
}

uint16 ModbusMaster::GetRegisterCount() {
	return this->m_iRegisterCount;
}

void ModbusMaster::SetFunction( ModbusFunction function) {
	this->m_oFunction = function;
}

ModbusFunction ModbusMaster::GetFunction() {
	return this->m_oFunction;
}

void ModbusMaster::SetReadTimeout( uint16 timeout) {
	this->m_iReadTimeOut = timeout;
}

uint16 ModbusMaster::GetReadTimeout() {
	return this->m_iReadTimeOut;
}

void ModbusMaster::SetRetryCount(uint8 cnt) {
	this->m_iRetryCount = cnt;
}

uint8 ModbusMaster::GetRetryCount() {
	return this->m_iRetryCount;
}

uint8 ModbusMaster::GetRetries() {
	return this->m_iRetries;
}

uint8 ModbusMaster::GetCommandSize() {
	return this->m_iCommandSize;
}

void ModbusMaster::GetCommandBytes( uchar* target) {
	if ( target == NULL ) return;

	if ( this->m_iCommandSize > 0 ) {
		memcpy ( target, this->m_cCommandBytes, this->m_iCommandSize);
	}
}

ModbusCommError ModbusMaster::GetCommError() {
	return this->m_oCommError;
}

uint16 ModbusMaster::GetTimeTaken() {
	return this->m_lTimeTaken;
}

uint16 ModbusMaster::GetResponseSize() {
	return this->m_iResponseSize;
}

void ModbusMaster::GetResponseBytes( uchar* target) {
	if ( target == NULL ) return;

	if ( this->m_iResponseSize > 0 ) {
		memcpy ( target, this->m_cResponseBytes, this->m_iResponseSize);
	}
}

bool ModbusMaster::GetRegisterData( uint16 registerAddress, uint8 bytes, uint8* target)
{
    if ( registerAddress < this->m_iStartRegister ) {
        this->m_pLogger->Write( LogLevel::Error, "GetRegisterData", "Invalid Register Address. CommanStartRegAddr:%d, DataStartRegAddr:%d", this->m_iStartRegister, registerAddress);
        return false;
    }

    uint16 skipRegisters = registerAddress - this->m_iStartRegister;
    uint16 skipBytes = MODBUS_HEADER_SIZE;
    skipBytes += skipRegisters * MODBUS_REGISTER_SIZE;

    if ( this->m_iResponseSize < (skipBytes + bytes) ) {
        this->m_pLogger->Write( LogLevel::Error, "GetRegisterData", "Invalid Register Address. skipBytes:%d, dataLength:%d", skipBytes +bytes, this->m_iResponseSize);
        return false;
    }

	// this->m_pLogger->Write( LogLevel::Trace, "GetRegisterData", "Copying %d bytes from %d", bytes, skipBytes);

	memcpy( target, this->m_cResponseBytes + skipBytes, bytes);

	return true;
}
