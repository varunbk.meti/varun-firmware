#ifndef _MODBUS_FUNCTION_H_INCLUDED_
#define _MODBUS_FUNCTION_H_INCLUDED_ 1

#include<string>
#include<defs.h>

using namespace std;

class ModbusFunction
{
public:
    enum Value : uint8
    {
        Unknown = 0,
        ReadCoilRegister = 1,
        ReadInputStatusRegister = 2,
        ReadHoldingRegister = 3,
        ReadInputRegister = 4,
        WriteSingleCoil = 5,
        WriteSingleRegister = 6,
        WriteMultipleCoils = 15,
        WriteMultipleRegisters = 16
    };

    ModbusFunction() = default;
    ModbusFunction(Value code) : m_iCode(code) { }

    operator Value() const { return m_iCode; }
    explicit operator bool() = delete;
    constexpr bool operator==(ModbusFunction a) const { return m_iCode == a.m_iCode; }
    constexpr bool operator!=(ModbusFunction a) const { return m_iCode != a.m_iCode; }
    uint8 GetCode();
    string GetLabel();
    string ToString();
    uint8 GetExpectedBytes( uint8 p_iRegisterCount);

    static ModbusFunction Parse(int code);

private:
    Value m_iCode;
};

#endif
