#ifndef _MODBUS_RTU_INCLUDED_
#define _MODBUS_RTU_INCLUDED_ 
#include <Arduino.h>
#include<modbusmaster.h>
#include<paritytype.h>
#include<speedtype.h>
#include<indicator.h>

#define MODBUS_RTU_CRC_SIZE 2

using namespace std;

class ModbusMasterRTU : public ModbusMaster {

public:

	ModbusMasterRTU();
	~ModbusMasterRTU();
	void Clear();

	SpeedType GetSpeed();
	void SetSpeed(SpeedType value);

	ParityType GetParity();
	void SetParity(ParityType value);

	uint8 GetDataBits();
	void SetDataBits( uint8 value);

	uint8 GetStopBits();
	void SetStopBits( uint8 value);

	void SetEnablePin( Indicator* value);

protected:
	bool CalculateCRC16( uint8* p_pMessage, uint8 p_iLength);
	bool CalculateCRC16( uint8* p_pMessage, uint8 p_iLength, bool p_bValidateOnly);
	void ExecuteCommand();
	void CreateCommand();

	SpeedType m_oSpeed;
	uint8 m_iDataBits;
	ParityType m_oParity;
	uint8 m_iStopBits;
	Indicator* m_pEnablePin;
};

#endif
