#include<string>
#include<modbusaddresstype.h>

using namespace std;

ModbusAddressType ModbusAddressType::Parse(int code)
{
    if (code == ModbusRTU1) {
        return ModbusRTU1;
    }
    else if (code == ModbusASCII) {
        return ModbusASCII;
    }
    else if (code == ModbusTCPV4) {
        return ModbusTCPV4;
    }

    return ModbusRTU0;
}

string ModbusAddressType::ToString()
{
    return this->GetLabel();
}

uint8 ModbusAddressType::GetCode()
{
    return m_iCode;
}

string ModbusAddressType::GetLabel()
{
    if (m_iCode == ModbusRTU1) {
        return "ModbusRTU1";
    }
    else if (m_iCode == ModbusASCII) {
        return "ModbusASCII";
    }
    else if (m_iCode == ModbusTCPV4) {
        return "ModbusTCPV4";
    }

    return "ModbusRTU0";
}
