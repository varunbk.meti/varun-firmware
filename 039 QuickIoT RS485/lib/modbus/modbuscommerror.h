#ifndef _MODBUS_COM_ERROR_H_INCLUDED_
#define _MODBUS_COM_ERROR_H_INCLUDED_ 1

#include<string>
#include<defs.h>

using namespace std;

class ModbusCommError
{
public:
    enum Value : uint8
    {
        DaqErrorNone = 0x00,
        ErrorCRC = 0xFF,
        ErrorTimeout = 0xFE,
        InvalidRegisterAddress = 0xFD,
        InvalidResponseLength = 0xFC,
        InvalidModbusCommand = 0xFB,
        InvalidFunction = 0xF9,
        InvalidDataValue = 0xF8,
        GatewayFailed = 0xF7,
        MetiNoNewData = 0xF6,
        RtuErrorUnknown = 0xF1
    };

    ModbusCommError() = default;
    ModbusCommError(Value code) : m_iCode(code) { }

    operator Value() const { return m_iCode; }
    explicit operator bool() = delete;
    constexpr bool operator==(ModbusCommError a) const { return m_iCode == a.m_iCode; }
    constexpr bool operator!=(ModbusCommError a) const { return m_iCode != a.m_iCode; }
    uint8 GetCode();
    string GetLabel();
    string ToString();

    static ModbusCommError Parse(int code);

private:
    Value m_iCode;
};

#endif
