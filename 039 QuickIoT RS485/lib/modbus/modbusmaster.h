#ifndef _MODBUS_INCLUDED_
#define _MODBUS_INCLUDED_ 

#include<iostream>
#include<string>
#include<iomanip>
#include<cstdlib>
#ifdef __cplusplus
extern "C" {
#include<stdio.h>
#include<errno.h>
#include<unistd.h>
#include<stdlib.h>
}
#endif

#include<defs.h>
#include<modbuscommerror.h>
#include<modbusfunction.h>
#include<logger.h>

using namespace std;

#define DEV_PARAM_SLAVE_ID 				248
#define DEV_PARAM_START_REGISTER 		0

#define IDX_CRC_LOW 					0
#define IDX_CRC_HIGH 					1

#define MODBUS_COMMAND_SIZE 			6


#define LOCATION_DEV_NUMBER 			0
#define LOCATION_FUNC_CODE 				1
#define LOCATION_REG_START_LO 			2
#define LOCATION_REG_START_HI 			3
#define LOCATION_REG_COUNT_LO 			4
#define LOCATION_REG_COUNT_HI 			5

#define LOCATION_ERROR_CODE 			2

#define MODBUS_MINIMUM_RESPONSE_SIZE 	3
#define MODBUS_HEADER_SIZE 				3

#define MODBUS_REGISTER_SIZE 			2


#define COMMAND_BUFFER_SIZE 			24
#define RECEIVE_BUFFER_SIZE 			256


class ModbusMaster {

public:

	void Execute();
	void Clear();


	static void Initialize(Logger* p_pValue, void (*p_pFunction)(void*));
	static void Release();

	ModbusMaster();
	~ModbusMaster();

	void SetSlaveID( uchar id);
	uchar GetSlaveID();

	void SetStartRegister( uint16 startReg);
	uint16 GetStartRegister();

	void SetRegisterCount( uint16 regCount);
	uint16 GetRegisterCount();

	void SetFunction( ModbusFunction function);
	ModbusFunction GetFunction();

	void SetReadTimeout( uint16 timeout);
	uint16 GetReadTimeout();
	void SetRetryCount(uint8 value);
	uint8 GetRetryCount();

	uint8 GetRetries();

	uint8 GetCommandSize();
	void GetCommandBytes( uchar* target);
	ModbusCommError GetCommError();
	uint16 GetTimeTaken();
	uint16 GetResponseSize();
	void GetResponseBytes( uchar* target);
	bool GetRegisterData( uint16 registerAddress, uint8 bytes, uint8* target);
	
protected:
	virtual void ExecuteCommand() = 0;
	virtual void CreateCommand();
	void ClearCommand();

	uchar 			m_iSlaveID;
	ModbusFunction 	m_oFunction;
	uint16 			m_iStartRegister;
	uint16 			m_iRegisterCount;
	uint16			m_iReadTimeOut;
	uint8			m_iRetryCount;
	
	uint8 			m_iCommandSize;
	uchar 			m_cCommandBytes[COMMAND_BUFFER_SIZE];
	uint16			m_iExpectedBytes;
	uint8			m_iRetries;

	ModbusCommError m_oCommError;
	uint16 			m_lTimeTaken;
	uint16 			m_iResponseSize;
	uchar 			m_cResponseBytes[RECEIVE_BUFFER_SIZE];

	static bool		m_bDeleteLogger;
	static Logger* m_pLogger;
	static void (*m_pExecuteCompleteCallBack)(void*);
};

#endif
