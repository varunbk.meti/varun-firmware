#include <Arduino.h>
#include <ArduinoJson.h>
#include <string>
#include <string.h>
#include <mbrconfig.h>
#include <modbusmasterrtu.h>
#include <indicator.h>
#include <indicatortype.h>
#include <blinktype.h>
#include <meti_modbus.h>
#include <main.h>
#include <config_reader.h>

using namespace std;

extern Indicator *g_pIndicators;
bool MBRConfig::m_bGlobalAbsTimeScanMode = false;
bool MBRConfig::m_bDeleteLogger = true;
extern BlinkConfig *g_pBlinkConfig;
Logger *MBRConfig::m_pLogger = new Logger();
extern Logger *g_pLogger;

extern AsyncEventSource modbus_events;

void (*MBRConfig::dataReceivedCallback)(void) = NULL;

void MBRConfig::RunScan()
{
	ModbusMasterRTU modbusComm;
	uchar l_pBuff[1024];
	char l_pPrintBuff[32];
	string l_sPacket = "";

	modbusComm.SetEnablePin(&g_pIndicators[IndicatorType::ModbusTxEnable]);

	g_pLogger->Write(LogLevel::Trace, "MBRConfig::RunScan", "Start....");

	this->m_bScanTriggered = false;

	if (ModbusAddressType::ModbusRTU0 != this->m_oPort)
	{
		return;
	}

	this->m_bScanActive = true;
	g_pLogger->Write(LogLevel::Trace, "RunScan", "MBR[%d-%s]. ModbusMaster started......", this->m_iCommandIndex, this->m_sName.c_str());

	g_pIndicators[IndicatorType::LedModbusTx].SwitchOn();

	modbusComm.SetSpeed(this->GetComSpeed());
	modbusComm.SetParity(this->GetParity());
	modbusComm.SetStopBits(this->GetStopBit());

	modbusComm.SetSlaveID(this->GetSlaveAddress());
	modbusComm.SetFunction(this->GetFunction());
	modbusComm.SetStartRegister(this->GetStartRegAddress());
	modbusComm.SetRegisterCount(this->GetRegisterCount());
	modbusComm.SetRetryCount(this->GetRetryCount());
	modbusComm.SetReadTimeout(this->m_iTimeoutMilliSec);
	modbusComm.Execute();
	this->m_bScanActive = false;

	memset(l_pBuff, 0x00, 1024);
	modbusComm.GetCommandBytes(l_pBuff);
	l_sPacket = "";
	for (int idx = 0; idx < modbusComm.GetCommandSize(); idx++)
	{
		sprintf(l_pPrintBuff, " %02X", l_pBuff[idx]);
		l_sPacket.append(l_pPrintBuff);
	}
	g_pLogger->Write(LogLevel::Debug, "RunScan", "MBR[%d]. Command: %u-%s", this->GetCommandIndex(), modbusComm.GetCommandSize(), l_sPacket.c_str());

	modbus_send_event_command(this->GetCommandIndex(), l_sPacket.c_str());

	memset(l_pBuff, 0x00, 1024);
	modbusComm.GetResponseBytes(l_pBuff);
	
	l_sPacket = "";
	for (int idx = 0; idx < modbusComm.GetResponseSize(); idx++)
	{
		if (idx >= 3 && (idx % 2) == 0)
		{
			sprintf(l_pPrintBuff, "%02X", l_pBuff[idx]);
		}
		else
		{
			sprintf(l_pPrintBuff, " %02X", l_pBuff[idx]);
		}
		l_sPacket.append(l_pPrintBuff);
	}
	g_pLogger->Write(LogLevel::Debug, "RunScan", "MBR[%d]. Error:%u, Response:%u-%s", this->GetCommandIndex(), modbusComm.GetCommError().GetCode(), modbusComm.GetResponseSize(), l_sPacket.c_str());

	if (modbusComm.GetResponseSize() > 0)
	{
		modbus_send_event_response(this->GetCommandIndex(), l_sPacket.c_str());
	}

	g_pIndicators[IndicatorType::LedModbusTx].SwitchOff();

	if (ModbusCommError::DaqErrorNone == modbusComm.GetCommError())
	{
		g_pIndicators[IndicatorType::LedModbusRx].Blink(g_pBlinkConfig[BlinkType::Once]);
	}
	else
	{
		g_pIndicators[IndicatorType::LedModbusErr].Blink(g_pBlinkConfig[BlinkType::Once]);
	}

	// if( ModbusCommError::DaqErrorNone == modbusComm.GetCommError() ) {
	for (uint8 l_iIdx = 0; l_iIdx < this->m_iSensorCount; l_iIdx++)
	{
		this->m_pSensors[l_iIdx].ExtractData(&modbusComm);
	}
	// }

	if (dataReceivedCallback != NULL)
	{
		// const size_t capacity = JSON_OBJECT_SIZE(2) + JSON_OBJECT_SIZE(8) + 8 * JSON_OBJECT_SIZE(13) + JSON_OBJECT_SIZE(15);
		// DynamicJsonDocument doc(capacity);

		//Thingsboard format
		JsonObject root = sensorValues_doc.to<JsonObject>();

		//Store the time
		sensorValues_doc["time"] = this->m_oScanTriggeredTime.ToUTC();

		for (uint8 l_iIdx = 0; l_iIdx < this->m_iSensorCount; l_iIdx++)
		{
			JsonObject obj = root.createNestedObject(this->m_pSensors[l_iIdx].GetKey());
			obj["error"] = this->m_pSensors[l_iIdx].GetLastDataError().GetCode();
			obj["value"] = this->m_pSensors[l_iIdx].GetLastDataValueString();
		}

		// dataReceivedCallback(sensorValues_doc);
		//get time in hex values
		sprintf(l_pPrintBuff, "%02X%02X%02X%02X%02X%02X",
				this->m_oScanTriggeredTime.GetYear() - 2000,
				this->m_oScanTriggeredTime.GetMonth() + 1,
				this->m_oScanTriggeredTime.GetDay(),
				this->m_oScanTriggeredTime.GetHour(),
				this->m_oScanTriggeredTime.GetMinute(),
				this->m_oScanTriggeredTime.GetSecond());

		live_mbr_doc["time"] = l_pPrintBuff;
		//get the index
		live_mbr_doc["mbr_idx"] = this->GetCommandIndex();
		live_mbr_doc["err"] = modbusComm.GetCommError().GetCode();
		live_mbr_doc["bytes"] = modbusComm.GetResponseSize();
		live_mbr_doc["resp_time"] = modbusComm.GetTimeTaken();
		live_mbr_doc["retries"] = modbusComm.GetRetries() - 1;

		//mark if timeout
		if (ModbusCommError::ErrorTimeout == modbusComm.GetCommError())
			live_mbr_doc["timeout"] = true;
		else
			live_mbr_doc["timeout"] = false;

		//get the command
		memset(l_pBuff, 0x00, 1024);
		modbusComm.GetCommandBytes(l_pBuff);
		l_sPacket = "";
		for (int idx = 0; idx < modbusComm.GetCommandSize(); idx++)
		{
			sprintf(l_pPrintBuff, "%02X", l_pBuff[idx]);
			l_sPacket.append(l_pPrintBuff);
		}
		live_mbr_doc["cmd"] = l_sPacket;

		//get the response
		memset(l_pBuff, 0x00, 1024);
		modbusComm.GetResponseBytes(l_pBuff);
		l_sPacket = "";
		for (int idx = 0; idx < modbusComm.GetResponseSize(); idx++)
		{
			sprintf(l_pPrintBuff, "%02X", l_pBuff[idx]);
			l_sPacket.append(l_pPrintBuff);
		}
		live_mbr_doc["resp"] = l_sPacket.c_str();

		//Web Display/mqtt/live format
		JsonObject root1 = sensorVals_doc.to<JsonObject>();
		const char *dev_id = device_doc["dev_id"];

		sensorVals_doc["q_dev_id"] = dev_id;

		JsonObject sensor_root = root1.createNestedObject("sensors");

		// If no modbus errors, then add the sensor values....
		if (ModbusCommError::DaqErrorNone == modbusComm.GetCommError())
		{
			String str = "s_";
			for (uint8 l_iIdx1 = 0; l_iIdx1 < this->m_iSensorCount; l_iIdx1++)
			{
				JsonObject obj = sensor_root.createNestedObject(str + l_iIdx1);
				obj["val"] = this->m_pSensors[l_iIdx1].GetLastDataValueString();
			}
		}

		// Narayan - this is for testing only, comment in production
		// serializeJson(sensorVals_doc, Serial);

		dataReceivedCallback();
	}
}

bool MBRConfig::IsScanTriggered(CDate &p_oDate)
{
	if (!this->IsActive())
	{
		return false;
	}

	bool l_bCurrentTriggerStatus = false;

	if (this->m_bInAbsoluteTimeScan)
	{
		if (this->m_oScanTriggeredTime.TimeElapsed(p_oDate, IntervalType::Second) != 0)
		{
			if (this->m_iRepeatInterval < 60)
			{
				if (p_oDate.GetSecond() % this->m_iRepeatInterval == 0)
				{
					l_bCurrentTriggerStatus = true;
				}
			}
			else if (p_oDate.GetSecond() == 0)
			{
				if ((p_oDate.GetMinute() * 60) % this->m_iRepeatInterval == 0)
				{
					l_bCurrentTriggerStatus = true;
				}
			}
		}
	}
	else
	{
		long val1 = this->m_oScanTriggeredTime.TimeElapsed(CDate::Now(), IntervalType::MilliSecond);
		long val2 = this->m_iRepeatInterval * 1000;

		if (val1 >= val2)
		{
			l_bCurrentTriggerStatus = true;
		}
	}

	if (l_bCurrentTriggerStatus)
	{
		// Serial.printf(" Scan triggered for %s", this->m_sName.c_str());
		this->m_bScanTriggered = true;
		this->m_oScanTriggeredTime = p_oDate;
	}

	return this->m_bScanTriggered;
}

void MBRConfig::Parse(JsonObject src)
{
	const size_t capacity = JSON_OBJECT_SIZE(25);
	DynamicJsonDocument doc(capacity);

	this->m_lCommandPK = src["driver_id"].as<long>();
	doc["m_lCommandPK"] = this->m_lCommandPK;
	this->m_sName = src["name"].as<string>();
	doc["m_sName"] = this->m_sName;
	this->m_oPort = ModbusAddressType::ModbusRTU0;
	doc["m_oPort"] = this->m_oPort.GetLabel();
	this->m_bActive = true;
	doc["m_bActive"] = this->m_bActive;

	this->m_oComSpeed = SpeedType::Parse(src["serial"]["baud"].as<int>());
	doc["m_oComSpeed"] = this->m_oComSpeed.GetLabel();
	this->m_oParity = ParityType::Parse(src["serial"]["parity"].as<int>());
	doc["m_oParity"] = this->m_oParity.GetLabel();
	this->m_iStopBit = src["serial"]["stopbits"].as<int>();
	doc["m_iStopBit"] = this->m_iStopBit;
	
	this->m_iSlaveAddress = src["command"]["slaveId"].as<int>();
	doc["m_iSlaveAddress"] = this->m_iSlaveAddress;
	this->m_oFunction = ModbusFunction::Parse(src["command"]["function"].as<int>());
	doc["m_oFunction"] = this->m_oFunction.GetLabel();
	this->m_iStartRegAddress = src["command"]["startAddr"].as<int>();
	doc["m_iStartRegAddress"] = this->m_iStartRegAddress;
	this->m_iRegisterCount = src["command"]["regCount"].as<int>();
	doc["m_iRegisterCount"] = this->m_iRegisterCount;
	this->m_iRegisterSize = 2;
	doc["m_iRegisterSize"] = this->m_iRegisterSize;
	this->m_iTimeoutMilliSec = src["command"]["timeOut"].as<int>();
	doc["m_iTimeoutMilliSec"] = this->m_iTimeoutMilliSec;
	this->m_iRetryCount = src["command"]["retries"].as<int>();
	doc["m_iRetryCount"] = this->m_iRetryCount;
	this->m_iRepeatInterval = src["command"]["repeat_int"].as<int>();
	if (this->m_iRepeatInterval == 0) {
		this->m_iRepeatInterval = 5;
	}
	doc["m_iRepeatInterval"] = this->m_iRepeatInterval;

	this->m_iDuplicateCRCTxFreq = 0;
	doc["m_iDuplicateCRCTxFreq"] = this->m_iDuplicateCRCTxFreq;
	this->m_bStorageEnabled = true;
	doc["m_bStorageEnabled"] = this->m_bStorageEnabled;
	this->m_bHighSpeedDevice = false;
	doc["m_bHighSpeedDevice"] = this->m_bHighSpeedDevice;

	String params;
	serializeJson(doc, params);
	g_pLogger->Write(LogLevel::Debug, "mbrconfig", "%s", params.c_str());

	JsonObject sensorSrc = src["command"]["responseParser"].as<JsonObject>();
	this->m_iSensorCount = 0;
	for (JsonObject::iterator itr = sensorSrc.begin(); itr != sensorSrc.end(); ++itr)
	{
		this->m_pSensors[this->m_iSensorCount].Parse(itr->value());
		this->m_pSensors[this->m_iSensorCount++].SetKey(itr->key().c_str());
		g_pLogger->Write(LogLevel::Trace, "mbrconfig", "itr->Key:%s", itr->key().c_str());
	}
}

MBRConfig::MBRConfig()
{
	this->m_lCommandPK = 0;
	this->m_sName = "";
	this->m_iCommandIndex = 0;
	this->m_bActive = false;
	this->m_oPort = ModbusAddressType::ModbusRTU0;
	this->m_iRepeatInterval = 0;

	this->m_oComSpeed = SpeedType::Speed_38400;
	this->m_oParity = ParityType::None;

	this->m_iSlaveAddress = 0;
	this->m_oFunction = ModbusFunction::Unknown;
	this->m_iStartRegAddress = 0;
	this->m_iRegisterCount = 0;

	this->m_iTimeoutMilliSec = 0;
	this->m_iRetryCount = 0;

	memset(m_pSlaveIpAddress, 0, IPV4_ADDR_LEN);
	this->m_iSlaveIpPort = 0;

	this->m_iDuplicateCRCTxFreq = 0;
	this->m_bStorageEnabled = false;

	this->m_iLastReceivedCRC = 0;
	this->m_bScanTriggered = false;
	this->m_iRepeatInterval = 0;
	this->m_bInAbsoluteTimeScan = false;
	this->m_oScanTriggeredTime = CDate::Now();

	this->m_iSensorCount = 0;
}

MBRConfig::~MBRConfig()
{
}

string MBRConfig::ToString()
{
	return this->m_sName;
}

bool MBRConfig::IsScanActive()
{
	return this->m_bScanActive;
}

void MBRConfig::SetCallBack(void (*p_pFunction)(void))
{
	dataReceivedCallback = p_pFunction;
}

void MBRConfig::Initialize(Logger *p_pLogger)
{
	if (p_pLogger == NULL)
	{
		m_pLogger->DisableConsole();
	}
	else
	{
		if (m_bDeleteLogger)
		{
			delete m_pLogger;
			m_bDeleteLogger = false;
		}
		m_pLogger = p_pLogger;
	}
}

void MBRConfig::Release()
{
	if (m_bDeleteLogger)
	{
		delete m_pLogger;
	}
	m_bDeleteLogger = false;
	m_pLogger = NULL;
}

void MBRConfig::EnableGlobalAbsoluteTimeScan()
{
	m_bGlobalAbsTimeScanMode = true;
}

void MBRConfig::DisableGlobalAbsoluteTimeScan()
{
	m_bGlobalAbsTimeScanMode = false;
}

void MBRConfig::SetCommandIndex(uint8 value)
{
	this->m_iCommandIndex = value;
}

uint8 MBRConfig::GetCommandIndex()
{
	return this->m_iCommandIndex;
}

void MBRConfig::SetActive(bool value)
{
	this->m_bActive = value;
}

bool MBRConfig::IsActive()
{
	return this->m_bActive;
}

void MBRConfig::SetRepeatInterval(uint16 value)
{
	this->m_iRepeatInterval = value;

	this->m_bInAbsoluteTimeScan = false;
	if (m_bGlobalAbsTimeScanMode)
	{
		switch (this->m_iRepeatInterval)
		{
		case 15:   // 15 SECONDS
		case 30:   // 30 SECONDS
		case 60:   // 1 MINUTE
		case 120:  // 2 MINUTES
		case 300:  // 5 MINUTES
		case 600:  // 10 MINUTES
		case 900:  // 15 MINUTES
		case 1200: // 20 MINUTES
		case 1800: // 30 MINUTES
		case 3600: // 60 MINUTES ( 1 HOUR )
			this->m_bInAbsoluteTimeScan = true;
			break;
		};
	}
}

uint16 MBRConfig::GetRepeatInterval()
{
	return this->m_iRepeatInterval;
}

SpeedType MBRConfig::GetComSpeed()
{
	return this->m_oComSpeed;
}

void MBRConfig::SetComSpeed(SpeedType value)
{
	this->m_oComSpeed = value;
}

ParityType MBRConfig::GetParity()
{
	return this->m_oParity;
}

void MBRConfig::SetParity(ParityType value)
{
	this->m_oParity = value;
}

void MBRConfig::SetSlaveAddress(uint8 value)
{
	this->m_iSlaveAddress = value;
}

uint8 MBRConfig::GetSlaveAddress()
{
	return this->m_iSlaveAddress;
}

void MBRConfig::SetFunction(ModbusFunction value)
{
	this->m_oFunction = value;
}

ModbusFunction MBRConfig::GetFunction()
{
	return this->m_oFunction;
}

void MBRConfig::SetStartRegAddress(uint16 value)
{
	this->m_iStartRegAddress = value;
}

uint16 MBRConfig::GetStartRegAddress()
{
	return this->m_iStartRegAddress;
}

void MBRConfig::SetRegisterCount(uint16 value)
{
	this->m_iRegisterCount = value;
}

uint16 MBRConfig::GetRegisterCount()
{
	return this->m_iRegisterCount;
}

void MBRConfig::SetTimeoutMilliSec(uint16 value)
{
	this->m_iTimeoutMilliSec = value;
}

uint16 MBRConfig::GetTimeoutMilliSec()
{
	return this->m_iTimeoutMilliSec;
}

void MBRConfig::SetRetryCount(uint8 value)
{
	this->m_iRetryCount = value;
}

uint8 MBRConfig::GetRetryCount()
{
	return this->m_iRetryCount;
}

string MBRConfig::GetSlaveIpAddress()
{
	string l_sIpAddress = "";
	char l_pBuff[8];

	for (int l_iIdx = 0; l_iIdx < IPV4_ADDR_LEN; l_iIdx++)
	{
		if (l_iIdx > 0)
		{
			sprintf(l_pBuff, ".%d", this->m_pSlaveIpAddress[l_iIdx]);
		}
		else
		{
			sprintf(l_pBuff, "%d", this->m_pSlaveIpAddress[l_iIdx]);
		}
		l_sIpAddress.append(l_pBuff);
	}

	return l_sIpAddress;
}

void MBRConfig::SetSlaveIpAddress(uint8 *value)
{
	memcpy(this->m_pSlaveIpAddress, value, IPV4_ADDR_LEN);
}

uint16 MBRConfig::GetSlaveIpPort()
{
	return this->m_iSlaveIpPort;
}

void MBRConfig::SetSlaveIpPort(uint16 value)
{
	this->m_iSlaveIpPort = value;
}

uint8 MBRConfig::GetDuplicateCRCTxFreq()
{
	return this->m_iDuplicateCRCTxFreq;
}

void MBRConfig::SetDuplicateCRCTxFreq(uint8 value)
{
	this->m_iDuplicateCRCTxFreq = value;
}

bool MBRConfig::IsStorageEnabled()
{
	return this->m_bStorageEnabled;
}

void MBRConfig::EnableStorage()
{
	this->m_bStorageEnabled = true;
}

void MBRConfig::DisableStorage()
{
	this->m_bStorageEnabled = false;
}

void MBRConfig::SetPort(ModbusAddressType value)
{
	this->m_oPort = value;
}

ModbusAddressType MBRConfig::GetPort()
{
	return this->m_oPort;
}

void MBRConfig::SetName(string value)
{
	this->m_sName = value;
}

string MBRConfig::GetName()
{
	return this->m_sName;
}

void MBRConfig::SetDataBits(uint8 value)
{
	this->m_iDataBits = value;
}
uint8 MBRConfig::GetDataBits()
{
	return this->m_iDataBits;
}

void MBRConfig::SetStopBit(uint8 value)
{
	this->m_iStopBit = value;
}

uint8 MBRConfig::GetStopBit()
{
	return this->m_iStopBit;
}

void MBRConfig::SetHighSpeedDevice(bool value)
{
	this->m_bHighSpeedDevice = value;
}

bool MBRConfig::IsHighSpeedDevice()
{
	return this->m_bHighSpeedDevice;
}

uint8 MBRConfig::GetAddressType()
{
	return this->m_iAddressType;
}

void MBRConfig::SetAddressType(uint8 value)
{
	this->m_iAddressType = value;
}
