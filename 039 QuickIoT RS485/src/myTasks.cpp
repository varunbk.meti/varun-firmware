#include <myTasks.h>
#include <main.h>
#include <meti_net_led.h>
#include <meti_wifi.h>
#include <esp_task_wdt.h>
#include <meti_modbus.h>

TaskHandle_t tasks_100_ms_handle = NULL;

void task_100_ms_start(void)
{
    /* we create a new task here */
    xTaskCreatePinnedToCore(
        task_100_ms,          /* Task function. */
        "100ms Second Tasks", /* name of task. */
        20000,                /* Stack size of task */
        NULL,                 /* parameter of the task */
        2,                    /* priority of the task */
        &tasks_100_ms_handle, /* Task handle to keep track of created task */
        1);                   /* Task to be executed in Core-1 only */

    g_pLogger->Write(LogLevel::Info, "Tasks", "Started tasks_100ms..");
}

void task_setup(void)
{
    task_100_ms_start();
}

void task_100_ms(void *parameter)
{
    TickType_t xLastWakeTime;
    static uint8_t one_sec_ctr = 10;
    static uint8_t ms500_ctr = 5;

    esp_task_wdt_add(NULL); //add current thread to WDT watch

    const TickType_t xFrequency = (100 / portTICK_PERIOD_MS);

    // Initialise the xLastWakeTime variable with the current time.
    xLastWakeTime = xTaskGetTickCount();

    /* loop forever */
    for (;;)
    {
        // Wait for the next cycle.
        vTaskDelayUntil(&xLastWakeTime, xFrequency);
        esp_task_wdt_reset(); //kick the watchdog

//100ms activities
#ifdef ENABLE_WIFI
        wifi_test_mode();
#endif

        if (ms500_ctr-- == 0)
        {
            ms500_ctr = 5;
#ifdef ENABLE_MODBUS
            if (g_valid_subscription == true)
            {
                modbus_500ms_task();
            }
#endif
        }

        //1000ms activities
        if (one_sec_ctr-- == 0)
        {
            one_sec_ctr = 10;
            Serial.print(".");
#ifdef ENABLE_WIFI
            wifi_1000_ms_activities();
#endif
            if (g_valid_subscription == true)
            {
                modbus_1000ms_tasks();
            }
        }
    }
}
