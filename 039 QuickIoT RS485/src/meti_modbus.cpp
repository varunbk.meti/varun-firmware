#include <Arduino.h>
#include <ArduinoJson.h>
#include <SPIFFS.h>
#include <meti_modbus.h>
#include <config_reader.h>
#include <logger.h>
#include <modbusmasterrtu.h>
#include <node.h>
#include <main.h>
#include <meti_server.h>
#include <meti_wifi.h>
#include <meti_live.h>
#include <meti_AsyncTCP.h>
#include <myTasks.h>

modbus_vars_struct modbus_vars;

modbus_cmd_struct modbus_cmd[MODBUS_COMMANDS_MAX];
String modbus_events_string;

Node *g_pNode = NULL;
ModbusScanService *g_pScanner = NULL;

extern Logger *g_pLogger;

void modbus_initialize(void)
{
	modbus_vars.live_server_connect_error_ctr = LIVE_SERVER_CONNECT_ERROR_COUNT_MAX;
	modbus_vars.restart_device = false;

	if (g_valid_subscription == true)
	{
		modbus_events_string.clear();

		modbus_cmd_struct_init();
		g_pLogger->Write(LogLevel::Info, "modbus_initialize", "Modbus Command Struct initialized...");
		//modbus_led_initialize();
	}
	task_setup();

	meti_AsyncClientSetup();
}

void modbus_cmd_struct_init(void)
{
	modbus_vars.tx_set_interval = devComm_doc["live"]["tx_interval"].as<int>();
	if (modbus_vars.tx_set_interval < LIVE_TX_INTERVAL_MIN)
	{
		modbus_vars.tx_set_interval = LIVE_TX_INTERVAL_MIN;
	}
}

void modbus_live_process_pkt(void)
{
	//get the index
	uint8_t mbr_idx = live_mbr_doc["mbr_idx"].as<int>();
	//store the values into modbus cmd array struct
	modbus_cmd[mbr_idx].comm_error = live_mbr_doc["err"].as<int>();
	strcpy(modbus_cmd[mbr_idx].live_time_format, live_mbr_doc["time"]);
	strncpy(modbus_cmd[mbr_idx].cmd_str, live_mbr_doc["cmd"], LIVE_CMD_LEN_STR_FIXED - 1);
	modbus_cmd[mbr_idx].rx_byte_count = live_mbr_doc["bytes"].as<int>();
	modbus_cmd[mbr_idx].retries = live_mbr_doc["retries"].as<int>();
	modbus_cmd[mbr_idx].resp_time = live_mbr_doc["resp_time"].as<int>();

	//clear previous response if any
	memset(modbus_cmd[mbr_idx].response_str, 0, MODBUS_RESPOSNE_LEN_MAX);

	//set response only if not timed out
	if (live_mbr_doc["timeout"] == false)
	{
		strncpy(modbus_cmd[mbr_idx].response_str, live_mbr_doc["resp"], LIVE_RESP_LEN_STR_FIXED - 1);
		modbus_cmd[mbr_idx].response_recd = true;
		modbus_cmd[mbr_idx].time_out = false;
	}
	else
	{
		modbus_cmd[mbr_idx].response_recd = false;
		modbus_cmd[mbr_idx].time_out = true;
	}
}

void modbus_500ms_task(void)
{
#ifdef ENABLE_MODBUS
	if (node_initialized == true)
	{
		g_pScanner->StateMachine();
	}
#endif
}

void modbus_1000ms_tasks(void)
{
	static bool live_first_ping = true;
#ifdef ENABLE_WIFI
	if (wifi_got_ip == true)
	{
		if (live_first_ping == true)
		{
			live_first_ping = false;
			live_send_ping_pkt();
		}

		if (modbus_vars.tx_interval_ctr-- == 0)
		{
			modbus_vars.tx_interval_ctr = modbus_vars.tx_set_interval;
			modbus_mark_tx_for_live();
		}

		modbus_tx_next_live_pkt();
	}
#endif
}

void modbus_mark_tx_for_live(void)
{
	for (uint8_t idx = 0; idx < MODBUS_COMMANDS_MAX; idx++)
	{
		if (modbus_cmd[idx].response_recd == true ||
			modbus_cmd[idx].time_out == true)
		{
			modbus_cmd[idx].live_tx_ready = true;
		}
	}
}

void modbus_tx_next_live_pkt(void)
{
	//if client is free, then send next packet...
	if (meti_AsyncClientReady() == true)
	{
		for (uint8_t idx = 0; idx < MODBUS_COMMANDS_MAX; idx++)
		{
			if (wifi_got_ip == true)
			{
				if (modbus_cmd[idx].live_tx_ready == true)
				{
					modbus_cmd[idx].live_tx_ready = false;

					g_pLogger->Write(LogLevel::Info, "tx_live_pkt", "%d Err:%d -> %s",
									 idx, modbus_cmd[idx].comm_error, modbus_cmd[idx].cmd_str);

					live_send_mbr_rp3(idx); //varun
					memset(modbus_cmd[idx].response_str, 0, MODBUS_RESPOSNE_LEN_MAX);
					memset(modbus_cmd[idx].cmd_str, 0, MODBUS_CMD_LEN_MAX);
					modbus_cmd[idx].response_recd = false;
					modbus_cmd[idx].time_out = false;

					idx = MODBUS_COMMANDS_MAX; //send one data and exit loop....
				}
			}
		}
	}
}

void eventDataReceived(void)
{
	String str;
	serializeJson(sensorVals_doc, str);
	g_pLogger->Write(LogLevel::Info, "eventDataReceived", "SensorData :%.200s", str.c_str());

	str.clear();
	serializeJson(live_mbr_doc, str);
	g_pLogger->Write(LogLevel::Info, "eventDataReceived", "LiveMbrData :%.200s", str.c_str());
	//Process data here...
	modbus_live_process_pkt(); //varun 
}

void InitializeMBRConfig()
{
	g_pLogger->Write(LogLevel::Info, "main", "Initializing scanner...");
	MBRConfig::SetCallBack(eventDataReceived);
	MBRConfig *l_pConfig = g_pNode->GetMBRConfig();
	uint8 l_iMbrCount = g_pNode->GetMBRCount();
	g_pScanner = ModbusScanService::GetInstance();
	g_pScanner->Initialize(l_iMbrCount, l_pConfig, g_pLogger);
}

bool InitializeNode(void)
{
	bool retVal = false;
	g_pLogger->Write(LogLevel::Info, "main", "Initializing node...");

	DynamicJsonDocument driver_doc(modbusDriverSize);

	File root = SPIFFS.open("/drivers");
	File file = root.openNextFile();

	String fName;

	while (file)
	{
		fName = file.name();
		if (fName.substring(0, 13) == "/drivers/mmd_")
		{
			deserializeJson(driver_doc, file);
			file.close();

			if (driver_doc.containsKey("driver_id"))
			{
				if (driver_doc["active"] == true)
				{
					g_pLogger->Write(LogLevel::Debug, "InitializeNode", "Using driver file: %s", fName.c_str());
					g_pNode = Node::GetInstance(driver_doc.as<JsonObject>());

					if (g_pNode == NULL)
					{
						g_pLogger->Write(LogLevel::Debug, "InitializeNode", "Invalid node configuration..");
						// fprintf(stderr, "Invalid node configuration.....");
						break;
					}
					else
					{
						retVal = true;
						// g_pNode->Initialize(g_pLogger);
						active_driver_id = driver_doc["driver_id"];
						g_sSiliconID = g_pNode->GetSiliconID();
						g_pLogger->Write(LogLevel::Info, "InitializeNode", "Configured Silicon:%s", g_sSiliconID.c_str());
						// Only one mbr allowed...
						break;
					}
				}
				else
				{
					g_pLogger->Write(LogLevel::Debug, "InitializeNode", "Modbus Driver Inactive : %s", fName.c_str());
				}
			}
			else
			{
				g_pLogger->Write(LogLevel::Debug, "InitializeNode", "Modbus Driver Invalid : %s", fName.c_str());
			}
		}
		file = root.openNextFile();
	}

	file.close();
	root.close();
	return retVal;
}

void modbus_send_event_command(uint8_t cmd_idx, String cmd_str)
{
	char hh_mm_ss[TIME_STRING_SIZE];

	//Copy the new content
	mtime_get_hhmmss(hh_mm_ss);
	modbus_events_string.concat("$^");
	modbus_events_string.concat(hh_mm_ss);
	modbus_events_string.concat("^");
	modbus_events_string.concat("COMMAND");
	modbus_events_string.concat("^");
	modbus_events_string.concat(cmd_str.c_str());
	modbus_events_string.concat("^");
	modbus_events_string.concat(cmd_idx);
	modbus_events_string.concat("^#");
	modbus_events.send(modbus_events_string.c_str(), "message", millis(), 10000);
	modbus_events_string.clear();
}

void modbus_send_event_response(uint8_t cmd_idx, String resp_str)
{
	char hh_mm_ss[TIME_STRING_SIZE];

	//Copy the new content
	mtime_get_hhmmss(hh_mm_ss);
	modbus_events_string.concat("$^");
	modbus_events_string.concat(hh_mm_ss);
	modbus_events_string.concat("^");
	modbus_events_string.concat("RESPONSE");
	modbus_events_string.concat("^");
	modbus_events_string.concat(resp_str.c_str());
	modbus_events_string.concat("^");
	modbus_events_string.concat(cmd_idx);
	modbus_events_string.concat("^#");
	modbus_events.send(modbus_events_string.c_str(), "message", millis(), 10000);
	modbus_events_string.clear();
}

void modbus_send_event_json(String json_str)
{
	char hh_mm_ss[TIME_STRING_SIZE];

	//Copy the new content
	mtime_get_hhmmss(hh_mm_ss);
	modbus_events_string.concat("$^");
	modbus_events_string.concat(hh_mm_ss);
	modbus_events_string.concat("^");
	modbus_events_string.concat("JSON");
	modbus_events_string.concat("^");
	modbus_events_string.concat(json_str.c_str());
	modbus_events_string.concat("^");
	modbus_events_string.concat("-1"); //when sending json, as of now we are not sending cmd index
	modbus_events_string.concat("^#");
	modbus_events.send(modbus_events_string.c_str(), "message", millis(), 10000);
	modbus_events_string.clear();
}