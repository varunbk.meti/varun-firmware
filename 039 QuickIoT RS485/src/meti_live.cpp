#include <Arduino.h>
#include <main.h>
#include <meti_time.h>
#include <meti_live.h>
#include "time.h"
#include <sys/time.h>
#include <meti_AsyncTCP.h>
#include <config_reader.h>
#include <utils.h>
#include <meti_modbus.h>

header_pkt_struct header;
pkt_struct_mbr_rp3 mbr_rp3;
char live_send_buffer[LIVE_SEND_BUFFER_LEN_MAX];

char *live_getServerResponse(char *buff, uint16_t *len)
{
    char *start_ptr = NULL;
    char *end_ptr = NULL;

    end_ptr = strstr(buff, "</cdms></xml>");
    if (end_ptr != NULL)
    {
        end_ptr = '\0';

        start_ptr = strstr(buff, "<xml><cdms>");
        *len = 0;

        if (start_ptr != NULL)
        {
            start_ptr = start_ptr + 11;
            end_ptr = strstr(buff, "</cdms></xml>");
            if (end_ptr != NULL)
            {
                *len = end_ptr - start_ptr;
            }
        }
    }
    return start_ptr;
}

void live_sync_device_time(char *response)
{
    char substr[3];
    int yy, mm, dd, hh, ii, ss;

    //Year
    sprintf(substr, "%.2s", response + 25);
    yy = strtol(substr, NULL, 16) + 2000;

    //Month
    sprintf(substr, "%.2s", response + 27);
    mm = strtol(substr, NULL, 16);

    //Date
    sprintf(substr, "%.2s", response + 29);
    dd = strtol(substr, NULL, 16);

    //Hour
    sprintf(substr, "%.2s", response + 31);
    hh = strtol(substr, NULL, 16);

    //Minutes
    sprintf(substr, "%.2s", response + 33);
    ii = strtol(substr, NULL, 16);

    //Seconds
    sprintf(substr, "%.2s", response + 35);
    ss = strtol(substr, NULL, 16);

    // g_pLogger->Write(LogLevel::Info, "live_sync_device_time", "Time %d %d %d %d %d %d", ss, ii, hh, dd, mm, yy);

    rtc.setTime(ss, ii, hh, dd, mm, yy);

    g_pLogger->Write(LogLevel::Info, "live_sync_device_time", "Time sync'd to METI Live Server... %s", rtc.getDateTime().c_str());
    // rtc.setTime(30, 24, 15, 17, 1, 2021);
}

void live_get_standard_header(uint8_t pkt_code, uint8_t pkt_version, char *pkt_buff, char *date_time_str)
{
    char siliconId[24]; //with additional chars for processing

    live_get_silicon_id(siliconId);
    header.packet_code = pkt_code;
    header.packet_version = pkt_version;
    strcpy(header.silcon_id, siliconId);
    header.communication_type = LIVE_COMM_TYPE;
    header.rssi = device_doc["wf_rssi"];

    sprintf(pkt_buff, "!%02X%02X%.16s%02X%02X%.12s,",
            header.packet_code,
            header.packet_version,
            header.silcon_id,
            header.communication_type,
            header.rssi,
            date_time_str);
}

void live_get_ping_pkt(char *pkt_buff)
{
    sprintf(pkt_buff, "%02X%02X", 0, 1);
}

void live_get_mbr_rp3(char *pkt_buff, uint8_t modbus_cmd_idx)
{
    // char temp_str[512];
    uint16_t resp_len = 0;
    uint8_t cmd_len = 0;

    //Narayan handle error here...
    mbr_rp3.packet_error_code = 0;
    mbr_rp3.mbr_command_index = modbus_cmd_idx;
    mbr_rp3.mbr_comm_error = modbus_cmd[modbus_cmd_idx].comm_error;
    mbr_rp3.mbr_received_byte_count = modbus_cmd[modbus_cmd_idx].rx_byte_count;
    mbr_rp3.mbr_response_time_in_ms = modbus_cmd[modbus_cmd_idx].resp_time;
    mbr_rp3.mbr_retry_count = modbus_cmd[modbus_cmd_idx].retries;

    strcpy(mbr_rp3.mbr_responsestring, modbus_cmd[modbus_cmd_idx].response_str);
    resp_len = strlen(mbr_rp3.mbr_responsestring);

    strcpy(mbr_rp3.mbr_command_issued_string, modbus_cmd[modbus_cmd_idx].cmd_str);
    cmd_len = strlen(modbus_cmd[modbus_cmd_idx].cmd_str);

    sprintf(pkt_buff, "%02X%02X%02X%02X%04X%02X%s%0*d%s%0*d",
            mbr_rp3.packet_error_code,
            mbr_rp3.mbr_command_index,
            mbr_rp3.mbr_comm_error,
            mbr_rp3.mbr_received_byte_count,
            mbr_rp3.mbr_response_time_in_ms,
            mbr_rp3.mbr_retry_count,
            mbr_rp3.mbr_responsestring,
            LIVE_RESP_LEN_STR_FIXED - resp_len, //zero padding len - response
            0,                                  //zero padding val
            mbr_rp3.mbr_command_issued_string,
            LIVE_CMD_LEN_STR_FIXED - cmd_len, //zero padding len - command
            0                                 //zero padding val
    );

}

void live_get_checksum(char *pkt_buff)
{
    char buff[3];
    uint8_t checksum = 0;
    uint16_t pkt_buff_len = strlen(pkt_buff);
    for (int idx = 0; idx < pkt_buff_len; idx++)
    {
        checksum += pkt_buff[idx];
    }
    sprintf(buff, "%02X", checksum);

    //copy checksum to pkt buffer
    pkt_buff[pkt_buff_len] = buff[0];
    pkt_buff[pkt_buff_len + 1] = buff[1];

    //null terminate end...
    pkt_buff[pkt_buff_len + 2] = 0;
}

void live_send_mbr_rp3(uint8_t mbr_cmd_idx)
{
    memset(live_send_buffer, 0, LIVE_SEND_BUFFER_LEN_MAX);
    live_get_standard_header(LIVE_PKT_CODE_MBR_RP, LIVE_PKT_VER_MBR_RP, live_send_buffer, modbus_cmd[mbr_cmd_idx].live_time_format);
    live_get_mbr_rp3(live_send_buffer + LIVE_HEADER_PKT_ASCII_LEN_MAX, mbr_cmd_idx);
    live_get_checksum(live_send_buffer);
    meti_AsyncClientRun();
}

void live_send_ping_pkt(void)
{
    char time_buff[TIME_STRING_SIZE];
    strcpy(time_buff, "000000000000");
    memset(live_send_buffer, 0, LIVE_SEND_BUFFER_LEN_MAX);
    live_get_standard_header(LIVE_PKT_CODE_PING, LIVE_PKT_VER_PING, live_send_buffer, time_buff);
    live_get_ping_pkt(live_send_buffer + LIVE_HEADER_PKT_ASCII_LEN_MAX);
    live_get_checksum(live_send_buffer);
    meti_AsyncClientRun();
}

void live_get_silicon_id(char *siliconId)
{
    sprintf(siliconId, "%s%s", "MQI1", WiFi.macAddress().c_str());
    remove_all_chars(siliconId, ':');
    // g_pLogger->Write(LogLevel::Info, "live_get_silicon_id", "%s", siliconId);
}

void live_get_time_format(char *buff)
{
    time_t now = time(NULL);
    struct tm *tmp = gmtime(&now);
    uint16_t y, m, d, h, i, s;
    y = tmp->tm_year - 100;
    m = tmp->tm_mon + 1;
    d = tmp->tm_mday;
    h = tmp->tm_hour;
    i = tmp->tm_min;
    s = tmp->tm_sec;
    // g_pLogger->Write(LogLevel::Info, "live_get_time_format", "%d %d %d %d %d %d ",  y,m,d,h,i,s);
    sprintf(buff, "%02X%02X%02X%02X%02X%02X", y, m, d, h, i, s);
}