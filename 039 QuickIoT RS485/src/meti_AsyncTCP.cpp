#include <Arduino.h>
#include <WiFi.h>
#include <AsyncTCP.h>
#include <ArduinoJson.h>
#include <meti_AsyncTCP.h>
#include <config_reader.h>
#include <logger.h>
#include <meti_live.h>
#include <meti_net_led.h>
#include <meti_modbus.h>

static AsyncClient *aClient = NULL;

extern Logger *g_pLogger;

// // String target_host = "scada.meti.in";
// uint16_t target_port = 80;
// String target_uri = "/services/nodeping.php?pkt=";

char target_host[64];
uint16_t target_port;
char target_uri[128];

void meti_AsyncClientSetup(void)
{
    memset(target_host, 0, sizeof(target_host));
    memset(target_uri, 0, sizeof(target_uri));

    strcpy(target_host, devComm_doc["live"]["url"]);
    strcpy(target_uri, "/services/nodeping.php?pkt=");
    target_port = devComm_doc["live"]["port"];

    // strcpy(target_host, "scada.meti.in");
    // strcpy(target_uri, "/services/nodeping.php?pkt=");
    // target_port = 80;
}

bool meti_AsyncClientReady(void)
{
    if (aClient) //client already exists
        return false;
    else
        return true;
}

void meti_AsyncClientRun(void)
{
    if (aClient) //client already exists
        return;

    aClient = new AsyncClient();
    if (!aClient) //could not allocate client
        return;

    aClient->onError([](void *arg, AsyncClient *client, int error) {
        net_led_set_state(MULTI_LED_STATE_RED_ON);
        g_pLogger->Write(LogLevel::Info, "Client onConnect", "Error!!!");

        //If connect error... wait for LIVE_SERVER_CONNECT_ERROR_COUNT_MAX times
        //before restart...
        modbus_vars.live_server_connect_error_ctr--;
        if (modbus_vars.live_server_connect_error_ctr == 0)
        {
            g_pLogger->Write(LogLevel::Info, "Client onError", "Filed to connect for %d times... Restarting device..", LIVE_SERVER_CONNECT_ERROR_COUNT_MAX);

            modbus_vars.restart_device = true;
        }
        // Serial.println("Connect Error");
        aClient = NULL;
        delete client;
        net_led_set_state(MULTI_LED_STATE_BLUE_ON);
    },
                     NULL);

    aClient->onConnect([](void *arg, AsyncClient *client) {
        g_pLogger->Write(LogLevel::Info, "Client onConnect", "Connected...");
        net_led_set_state(MULTI_LED_STATE_GREEN_ON);

        modbus_vars.live_server_connect_error_ctr = LIVE_SERVER_CONNECT_ERROR_COUNT_MAX;

        // Serial.println("Connected...");
        aClient->onError(NULL, NULL);

        client->onDisconnect([](void *arg, AsyncClient *c) {
            // Serial.println("Disconnected");
            net_led_set_state(MULTI_LED_STATE_BLUE_ON);
            aClient = NULL;
            delete c;
        },
                             NULL);

        client->onData([](void *arg, AsyncClient *c, void *data, size_t len) {
            char inDataBuff[512];
            uint16_t resp_len;
            char *start_ptr = live_getServerResponse((char *)data, &resp_len);
            if (start_ptr != NULL && resp_len > 0)
            {
                strncpy(inDataBuff, start_ptr, resp_len);
                inDataBuff[resp_len] = 0;
                g_pLogger->Write(LogLevel::Info, "TCP_Client RxData", "Len: %d, live_resp:%s", len, inDataBuff);
                live_sync_device_time(inDataBuff);
            }
            else
            {
                net_led_set_state(MULTI_LED_STATE_RED_ON);

                uint8_t *d = (uint8_t *)data;
                g_pLogger->Write(LogLevel::Info, "TCP_Client RxData", "Len: %d, Invalid response.. (Displaying 400 chars below..)", len);
                for (size_t i = 0; i < 512; i++)
                    Serial.write(d[i]);
            }
            net_led_set_state(MULTI_LED_STATE_BLUE_ON);
        },
                       NULL);

        String output;
        // serializeJson(sensorVals_doc, output);
        //output = "!1103MQI184CCA868B0C4021D150B1609072A,0000000F01090001030A03E803E803E803E8138CBE670000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000001030095000595E500008C";

        char strBuff[LIVE_SEND_BUFFER_LEN_MAX + 100];
        sprintf(strBuff, "GET %s%s HTTP/1.1\r\n", target_uri, live_send_buffer);

        g_pLogger->Write(LogLevel::Info, "Live TxData", "%s", strBuff);
        // Post request
        client->write(strBuff);

        // Host
        sprintf(strBuff, "Host: %s\r\n", target_host);
        client->write(strBuff);

        // Application type
        //client->write("Content-Type: application/json\r\n");

        // Content-Length comes from json string.
        //sprintf(strBuff, "Content-Length: %d\r\n", output.length());
        //client->write(strBuff);

        // Connection is closed after sending data...
        client->write("Connection: close\r\n");

        // Actual payload goes here...
        // client->write(output.c_str());
        client->write("\r\n");
        client->write("\r\n");
    },
                       NULL);

    g_pLogger->Write(LogLevel::Info, "TCP_Client", "Connecting to: %s, port: %d", target_host, target_port);

    // if (!aClient->connect("www.google.com", 80))
    if (!aClient->connect(target_host, target_port))
    {
        net_led_set_state(MULTI_LED_STATE_RED_ON);
        g_pLogger->Write(LogLevel::Info, "TCP_Client", "Failed to connect: %s, port: %d", target_host, target_port);
        // Serial.println("Connect Fail");
        AsyncClient *client = aClient;
        aClient = NULL;
        delete client;
        net_led_set_state(MULTI_LED_STATE_BLUE_ON);
    }
}
