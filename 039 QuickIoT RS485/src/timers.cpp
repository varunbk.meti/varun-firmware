#include <Arduino.h>
#include <timers.h>
#include <main.h>

bool timeout = false;
hw_timer_t *timer = NULL;

struct timeSlots ts[MAX_TIMERS] = {
    {1, 1, false},       //TIMER_1_MS
    {50, 50, false},     //TIMER_50_MS
    {100, 100, false},   //TIMER_100_MS
    {500, 500, false},   //TIMER_500_MS
    {1000, 1000, false}, //TIMER_1000_MS
    {10, 10, false}      //TIMER_10_MS
};

void ICACHE_RAM_ATTR onTimerISR()
{
    for (int i = 0; i < MAX_TIMERS; i++)
    {
        ts[i].dc--;
        if (ts[i].dc <= 0)
        {
            ts[i].dc = ts[i].ms;
            ts[i].done = true;
        }
    }
}

void timer_start(void)
{
    timer = timerBegin(0, 80, true);        //Divice 80MHz clock by 80 to get 1MHz tick
    timerAttachInterrupt(timer, &onTimerISR, true);
    timerAlarmWrite(timer, 1000, true);     //Divide by 1MHz clock by 1000 to get 1ms ISR
    timerAlarmEnable(timer);
}

void timer_check(void)
{
    //1ms Activities
    // if (ts[TIMER_1_MS].done == true) {
    // 	ts[TIMER_1_MS].done = false;
    //     // timer_timeout_1ms();
    // }

    //10ms Activities
    if (ts[TIMER_10_MS].done == true)
    {
        ts[TIMER_10_MS].done = false;
        timer_timeout_10ms();
    }

    //50ms Activities
    if (ts[TIMER_50_MS].done == true)
    {
        ts[TIMER_50_MS].done = false;
        timer_timeout_50ms();
    }

    //100ms Activities
    if (ts[TIMER_100_MS].done == true)
    {
        ts[TIMER_100_MS].done = false;
        timer_timeout_100ms();
    }

    //
    if (ts[TIMER_500_MS].done == true)
    {
        ts[TIMER_500_MS].done = false;
        timer_timeout_500ms();
    }

    if (ts[TIMER_1000_MS].done == true)
    {
        ts[TIMER_1000_MS].done = false;
        timer_timeout_1000ms();
    }
}
