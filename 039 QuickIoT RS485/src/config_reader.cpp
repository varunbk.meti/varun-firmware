#include <Arduino.h>
#include <ArduinoJson.h>
#include <SPIFFS.h>
#include <config_reader.h>
#include <main.h>

DynamicJsonDocument device_doc(device_capacity + device_capacity_add);
DynamicJsonDocument wifi_doc(wifi_capacity + wifi_capacity_add);
DynamicJsonDocument sensorScaling_doc(sensorScaling_capacity + sensorScaling_capacity_add);

DynamicJsonDocument params_doc(params_capacity + params_capacity_add);
DynamicJsonDocument sensorValues_doc(sensorValues_capacity + sensorValues_capacity_add);
DynamicJsonDocument modbusDriver_doc(modbusDriver_capacity + modbusDriver_capacity_add);
DynamicJsonDocument sensorVals_doc(sensorVals_capacity);
DynamicJsonDocument live_mbr_doc(live_mbr_capacity);
DynamicJsonDocument devComm_doc(devComm_capacity);

void read_device_file(void)
{
    String fileData;
    String path = "/config/device.json";
    File deviceFile = SPIFFS.open(path, "r");

    if (!deviceFile)
    {
        g_pLogger->Write(LogLevel::Fatal, "read_device_file", "Failed to open %s..!!!", path.c_str());
        return;
    }
    else
    {
        g_pLogger->Write(LogLevel::Debug, "read_device_file", "Read file %s", path.c_str());
    }

    deserializeJson(device_doc, deviceFile);
}

void read_wifi_file(void)
{
    String fileData;
    String path = "/config/wifi.json";
    File wifiFile = SPIFFS.open(path, "r");

    if (!wifiFile)
    {
        g_pLogger->Write(LogLevel::Fatal, "read_wifi_file", "Failed to open %s..!!!", path.c_str());
        return;
    }
    else
    {
        g_pLogger->Write(LogLevel::Debug, "read_wifi_file", "Read file %s", path.c_str());
    }

    deserializeJson(wifi_doc, wifiFile);
}

void read_sensor_scaling_file(void)
{
    String fileData;
    String path = "/config/sensorScaling.json";
    File sensorScalingFile = SPIFFS.open(path, "r");

    if (!sensorScalingFile)
    {
        g_pLogger->Write(LogLevel::Fatal, "read_sensorScaling_file", "Failed to open %s..!!!", path.c_str());
        return;
    }
    else
    {
        g_pLogger->Write(LogLevel::Debug, "read_sensorScaling_file", "Read file %s", path.c_str());
    }

    deserializeJson(sensorScaling_doc, sensorScalingFile);
}

// void read_params_file(void)
// {
//     String fileData;
//     String path = "/config/params.json";
//     File paramsFile = SPIFFS.open(path, "r");

//     if (!paramsFile)
//     {
//         g_pLogger->Write(LogLevel::Fatal, "read_params_file", "Failed to open %s..!!!", path.c_str());
//         return;
//     }
//     else
//     {
//         g_pLogger->Write(LogLevel::Debug, "read_params_file", "Read file %s", path.c_str());
//     }

//     deserializeJson(params_doc, paramsFile);
// }

void read_modbus_driver_file(void)
{
    String fileData;

    if (active_driver_id > 0)
    {
        String path = "/drivers/mmd_";
        path = path + active_driver_id;
        path = path + ".json";

        File driverFile = SPIFFS.open(path, "r");

        if (!driverFile)
        {
            g_pLogger->Write(LogLevel::Fatal, "read_modbus_driver", "Failed to open %s..!!!", path.c_str());
            return;
        }
        else
        {
            g_pLogger->Write(LogLevel::Debug, "read_modbus_driver", "Read file %s", path.c_str());
        }

        deserializeJson(modbusDriver_doc, driverFile);
    }
}

void read_devComm_file(void)
{
    String fileData;
    String path = "/config/devComm.json";
    File devCommFile = SPIFFS.open(path, "r");

    if (!devCommFile)
    {
        g_pLogger->Write(LogLevel::Fatal, "read_sniffer_file", "Failed to open %s..!!!", path.c_str());
        return;
    }
    else
    {
        g_pLogger->Write(LogLevel::Debug, "read_sniffer_file", "Read file %s", path.c_str());
    }

    deserializeJson(devComm_doc, devCommFile);
}