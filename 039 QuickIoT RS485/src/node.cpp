#include <string>
#include <string.h>
#include <node.h>
/*
#include<connectionfactory.h>
#include<sqlexception.h>
#include<packetfactory.h>
#include<packet_gcp_v3.h>
#include<packet_acp_v4.h>
*/

using namespace std;

Node *Node::m_pSingleInstance = NULL;

void Node::RemoveAllData()
{
}

void Node::Parse(JsonObject src)
{
	if (this->IsAbsoluteTimeScanEnabled())
	{
		MBRConfig::EnableGlobalAbsoluteTimeScan();
	}
	else
	{
		MBRConfig::DisableGlobalAbsoluteTimeScan();
	}

	MBRConfig::DisableGlobalAbsoluteTimeScan();

	this->m_pMBRConfig[this->m_iMBRCount++].Parse(src);
	// this->m_pMBRConfig[this->m_iMBRCount++].Parse( src["template_header1"].as<JsonObject>());

	/*
	JsonObject l_oMbrData = src["template_header"].as<JsonObject>();

	for( JsonObject::iterator itr = l_oMbrData.begin(); itr != l_oMbrData.end(); ++itr) {
		this->m_pMBRConfig[this->m_iMBRCount++].Parse( itr->value());
	}
	*/

	/*
	this->m_iMBRCount = 1;
	this->m_pMBRConfig = new MBRConfig[1];
	uint8 l_iIdx = 0;
	this->m_pMBRConfig[l_iIdx].SetName( "Command 0");
	this->m_pMBRConfig[l_iIdx].SetCommandIndex( l_iIdx);
	this->m_pMBRConfig[l_iIdx].SetAddressType( ModbusAddressType::ModbusRTU0);
	this->m_pMBRConfig[l_iIdx].SetActive( true);
	this->m_pMBRConfig[l_iIdx].SetRepeatInterval( 20);
	this->m_pMBRConfig[l_iIdx].SetComSpeed( SpeedType::Speed_9600);
	this->m_pMBRConfig[l_iIdx].SetParity( ParityType::None);
	this->m_pMBRConfig[l_iIdx].SetSlaveAddress( 0x0B);
	this->m_pMBRConfig[l_iIdx].SetFunction( ModbusFunction::ReadHoldingRegister);
	this->m_pMBRConfig[l_iIdx].SetStartRegAddress( 0);
	this->m_pMBRConfig[l_iIdx].SetRegisterCount( 35);
	this->m_pMBRConfig[l_iIdx].SetTimeoutMilliSec( 500);
	this->m_pMBRConfig[l_iIdx].SetRetryCount( 1);
	this->m_pMBRConfig[l_iIdx].m_iSensorCount = 5;
	this->m_pMBRConfig[l_iIdx].m_pSensors = new Sensor[this->m_pMBRConfig[l_iIdx].m_iSensorCount];

	uint8 l_iSensorIdx = 0;
	this->m_pMBRConfig[l_iIdx].m_pSensors[l_iSensorIdx].SetID( l_iSensorIdx + 1);
	this->m_pMBRConfig[l_iIdx].m_pSensors[l_iSensorIdx].SetName( "Input Voltage");
	this->m_pMBRConfig[l_iIdx].m_pSensors[l_iSensorIdx].SetByteOrder( ByteOrderType::BO_01_01);
	this->m_pMBRConfig[l_iIdx].m_pSensors[l_iSensorIdx].SetDataType( DataType::DT_UINT16);
	this->m_pMBRConfig[l_iIdx].m_pSensors[l_iSensorIdx].SetStartRegisterAddress( 1);
	this->m_pMBRConfig[l_iIdx].m_pSensors[l_iSensorIdx].SetRegisterCount( 1);
	this->m_pMBRConfig[l_iIdx].m_pSensors[l_iSensorIdx].SetScalingNumerator( 1);
	this->m_pMBRConfig[l_iIdx].m_pSensors[l_iSensorIdx].SetScalingDenominator( 1000);
	this->m_pMBRConfig[l_iIdx].m_pSensors[l_iSensorIdx].SetOffsetValue( 0.0);
	this->m_pMBRConfig[l_iIdx].m_pSensors[l_iSensorIdx].SetDecimalPresision( 2);
	l_iSensorIdx++;

	this->m_pMBRConfig[l_iIdx].m_pSensors[l_iSensorIdx].SetID( l_iSensorIdx + 1);
	this->m_pMBRConfig[l_iIdx].m_pSensors[l_iSensorIdx].SetName( "Device Temp");
	this->m_pMBRConfig[l_iIdx].m_pSensors[l_iSensorIdx].SetByteOrder( ByteOrderType::BO_01_01);
	this->m_pMBRConfig[l_iIdx].m_pSensors[l_iSensorIdx].SetDataType( DataType::DT_UINT16);
	this->m_pMBRConfig[l_iIdx].m_pSensors[l_iSensorIdx].SetStartRegisterAddress( 2);
	this->m_pMBRConfig[l_iIdx].m_pSensors[l_iSensorIdx].SetRegisterCount( 1);
	this->m_pMBRConfig[l_iIdx].m_pSensors[l_iSensorIdx].SetScalingNumerator( 1);
	this->m_pMBRConfig[l_iIdx].m_pSensors[l_iSensorIdx].SetScalingDenominator( 10);
	this->m_pMBRConfig[l_iIdx].m_pSensors[l_iSensorIdx].SetOffsetValue( 0.0);
	this->m_pMBRConfig[l_iIdx].m_pSensors[l_iSensorIdx].SetDecimalPresision( 2);
	l_iSensorIdx++;

	this->m_pMBRConfig[l_iIdx].m_pSensors[l_iSensorIdx].SetID( l_iSensorIdx + 1);
	this->m_pMBRConfig[l_iIdx].m_pSensors[l_iSensorIdx].SetName( "Machine 1 Count");
	this->m_pMBRConfig[l_iIdx].m_pSensors[l_iSensorIdx].SetByteOrder( ByteOrderType::BO_0123_0123);
	this->m_pMBRConfig[l_iIdx].m_pSensors[l_iSensorIdx].SetDataType( DataType::DT_UINT32);
	this->m_pMBRConfig[l_iIdx].m_pSensors[l_iSensorIdx].SetStartRegisterAddress( 3);
	this->m_pMBRConfig[l_iIdx].m_pSensors[l_iSensorIdx].SetRegisterCount( 2);
	this->m_pMBRConfig[l_iIdx].m_pSensors[l_iSensorIdx].SetScalingNumerator( 1);
	this->m_pMBRConfig[l_iIdx].m_pSensors[l_iSensorIdx].SetScalingDenominator( 1);
	this->m_pMBRConfig[l_iIdx].m_pSensors[l_iSensorIdx].SetOffsetValue( 0.0);
	this->m_pMBRConfig[l_iIdx].m_pSensors[l_iSensorIdx].SetDecimalPresision( 0);
	l_iSensorIdx++;

	this->m_pMBRConfig[l_iIdx].m_pSensors[l_iSensorIdx].SetID( l_iSensorIdx + 1);
	this->m_pMBRConfig[l_iIdx].m_pSensors[l_iSensorIdx].SetName( "Machine 2 Count");
	this->m_pMBRConfig[l_iIdx].m_pSensors[l_iSensorIdx].SetByteOrder( ByteOrderType::BO_0123_0123);
	this->m_pMBRConfig[l_iIdx].m_pSensors[l_iSensorIdx].SetDataType( DataType::DT_UINT32);
	this->m_pMBRConfig[l_iIdx].m_pSensors[l_iSensorIdx].SetStartRegisterAddress( 11);
	this->m_pMBRConfig[l_iIdx].m_pSensors[l_iSensorIdx].SetRegisterCount( 2);
	this->m_pMBRConfig[l_iIdx].m_pSensors[l_iSensorIdx].SetScalingNumerator( 1);
	this->m_pMBRConfig[l_iIdx].m_pSensors[l_iSensorIdx].SetScalingDenominator( 1);
	this->m_pMBRConfig[l_iIdx].m_pSensors[l_iSensorIdx].SetOffsetValue( 0.0);
	this->m_pMBRConfig[l_iIdx].m_pSensors[l_iSensorIdx].SetDecimalPresision( 0);
	l_iSensorIdx++;

	this->m_pMBRConfig[l_iIdx].m_pSensors[l_iSensorIdx].SetID( l_iSensorIdx + 1);
	this->m_pMBRConfig[l_iIdx].m_pSensors[l_iSensorIdx].SetName( "Door Count");
	this->m_pMBRConfig[l_iIdx].m_pSensors[l_iSensorIdx].SetByteOrder( ByteOrderType::BO_0123_0123);
	this->m_pMBRConfig[l_iIdx].m_pSensors[l_iSensorIdx].SetDataType( DataType::DT_UINT32);
	this->m_pMBRConfig[l_iIdx].m_pSensors[l_iSensorIdx].SetStartRegisterAddress( 31);
	this->m_pMBRConfig[l_iIdx].m_pSensors[l_iSensorIdx].SetRegisterCount( 2);
	this->m_pMBRConfig[l_iIdx].m_pSensors[l_iSensorIdx].SetScalingNumerator( 1);
	this->m_pMBRConfig[l_iIdx].m_pSensors[l_iSensorIdx].SetScalingDenominator( 1);
	this->m_pMBRConfig[l_iIdx].m_pSensors[l_iSensorIdx].SetOffsetValue( 0.0);
	this->m_pMBRConfig[l_iIdx].m_pSensors[l_iSensorIdx].SetDecimalPresision( 0);
	l_iSensorIdx++;

	for (int idx = 0; idx < this->m_iMBRCount; idx++) {
		this->m_pLogger->Write(LogLevel::Trace, "FetchMbrConfig", "MBR[%d]:%d-%s", idx, this->m_pMBRConfig[idx].IsActive(), this->m_pMBRConfig[idx].ToString().c_str());
	}
	*/
}

Node *Node::GetInstance(JsonObject src)
{
	if (m_pSingleInstance == NULL)
	{
		m_pSingleInstance = new Node();
		m_pSingleInstance->Parse(src);
	}

	return m_pSingleInstance;
}

Node::Node()
{
	this->m_lNodeID = 0;
	this->m_sSiliconID = "SILICONID";
	this->m_sName = "ESP32 NODE";
	this->m_sServerHostName = "scada.meti.in";
	this->m_iServerPort = 80;
	this->m_sServerPageName = "services/nodeping.php";
	this->m_iPingInterval = 60;
	this->m_iBacklogInterval = 30;
	this->m_iMaxBacklogPacket = 10;
	this->m_bAbsoluteTimeScanEnabled = false;
	this->m_bBackLogPresent = true;

	this->m_iIPAddressType = 0;
	memset(this->m_pIPV4AddrBytes, 0, IPV4_ADDR_LEN);
	memset(this->m_pIPV4GwBytes, 0, IPV4_ADDR_LEN);

	this->m_iMBRCount = 0;

	this->m_bDeleteLogger = true;
	this->m_pLogger = new Logger();
}

Node::~Node()
{
	this->m_iMBRCount = 0;
	MBRConfig::Release();

	if (this->m_bDeleteLogger)
	{
		delete this->m_pLogger;
	}
	this->m_bDeleteLogger = false;
	this->m_pLogger = NULL;
}

string Node::ToString()
{
	return "Node";
}

void Node::Initialize(Logger *p_pLogger)
{
	if (p_pLogger == NULL)
	{
		this->m_pLogger->DisableConsole();
	}
	else
	{
		if (this->m_bDeleteLogger)
		{
			delete this->m_pLogger;
			this->m_bDeleteLogger = false;
		}
		this->m_pLogger = p_pLogger;
	}
}
void Node::SetNodeID(ulong value)
{
	this->m_lNodeID = value;
}

ulong Node::GetNodeID()
{
	return this->m_lNodeID;
}

void Node::SetSiliconID(string value)
{
	this->m_sSiliconID = value;
}

string Node::GetSiliconID()
{
	return this->m_sSiliconID;
}

void Node::SetName(string value)
{
	this->m_sName = value;
}

string Node::GetName()
{
	return this->m_sName;
}

void Node::SetServerHostName(string value)
{
	this->m_sServerHostName = value;
}

string Node::GetServerHostName()
{
	return this->m_sServerHostName;
}

void Node::SetServerPort(uint16 value)
{
	this->m_iServerPort = value;
}

uint16 Node::GetServerPort()
{
	return this->m_iServerPort;
}

void Node::SetServerPageName(string value)
{
	this->m_sServerPageName = value;
}

string Node::GetServerPageName()
{
	return this->m_sServerPageName;
}

void Node::SetPingInterval(uint16 value)
{
	this->m_iPingInterval = value;
}

uint16 Node::GetPingInterval()
{
	return this->m_iPingInterval;
}

void Node::EnableAbsoluteTimeScan()
{
	this->m_bAbsoluteTimeScanEnabled = true;
}

void Node::DisableAbsoluteTimeScan()
{
	this->m_bAbsoluteTimeScanEnabled = false;
}

bool Node::IsAbsoluteTimeScanEnabled()
{
	return this->m_bAbsoluteTimeScanEnabled;
}

MBRConfig *Node::GetMBRConfig()
{
	return this->m_pMBRConfig;
}

uint8 Node::GetMBRCount()
{
	return this->m_iMBRCount;
}

uint16 Node::GetBacklogTxInterval()
{
	return this->m_iBacklogInterval;
}

void Node::SetAPN(string value)
{
	this->m_sAPN = value;
}

string Node::GetAPN()
{
	return this->m_sAPN;
}
