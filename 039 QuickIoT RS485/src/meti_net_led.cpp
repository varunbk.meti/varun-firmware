#include <Arduino.h>
#include <meti_net_led.h>
#include <logger.h>
#include <indicator.h>
#include <indicatortype.h>
#include <blinktype.h>

multi_led_state_t multi_led_state = MULTI_LED_STATE_RED_ON;

extern Logger *g_pLogger;
extern BlinkConfig *g_pBlinkConfig;
extern Indicator *g_pIndicators;

void net_led_initialize(void)
{
	g_pIndicators[IndicatorType::LedRed].SetPin(PIN_LED_RED);
	g_pIndicators[IndicatorType::LedGreen].SetPin(PIN_LED_GREEN);
	g_pIndicators[IndicatorType::LedBlue].SetPin(PIN_LED_BLUE);
}

void net_led_set_state(uint8_t led_state)
{
	switch (led_state)
	{
	case MULTI_LED_STATE_ALL_OFF:
		g_pIndicators[IndicatorType::LedRed].SwitchOff();
		g_pIndicators[IndicatorType::LedBlue].SwitchOff();
		g_pIndicators[IndicatorType::LedGreen].SwitchOff();
		break;

	case MULTI_LED_STATE_GREEN_ON:
		g_pIndicators[IndicatorType::LedRed].SwitchOff();
		g_pIndicators[IndicatorType::LedBlue].SwitchOff();
		g_pIndicators[IndicatorType::LedGreen].SwitchOn();
		break;

	case MULTI_LED_STATE_GREEN_BLINK_SLOW:
		g_pIndicators[IndicatorType::LedRed].SwitchOff();
		g_pIndicators[IndicatorType::LedBlue].SwitchOff();
		g_pIndicators[IndicatorType::LedGreen].Blink(g_pBlinkConfig[BlinkType::Slow]);
		break;

	case MULTI_LED_STATE_GREEN_BLINK_FAST:
		g_pIndicators[IndicatorType::LedRed].SwitchOff();
		g_pIndicators[IndicatorType::LedBlue].SwitchOff();
		g_pIndicators[IndicatorType::LedGreen].Blink(g_pBlinkConfig[BlinkType::Fast]);
		break;

	case MULTI_LED_STATE_RED_ON:
		g_pIndicators[IndicatorType::LedRed].SwitchOn();
		g_pIndicators[IndicatorType::LedBlue].SwitchOff();
		g_pIndicators[IndicatorType::LedGreen].SwitchOff();
		break;

	case MULTI_LED_STATE_RED_BLINK_SLOW:
		g_pIndicators[IndicatorType::LedRed].Blink(g_pBlinkConfig[BlinkType::Slow]);
		g_pIndicators[IndicatorType::LedBlue].SwitchOff();
		g_pIndicators[IndicatorType::LedGreen].SwitchOff();
		break;

	case MULTI_LED_STATE_RED_BLINK_FAST:

		g_pIndicators[IndicatorType::LedRed].Blink(g_pBlinkConfig[BlinkType::Fast]);
		g_pIndicators[IndicatorType::LedBlue].SwitchOff();
		g_pIndicators[IndicatorType::LedGreen].SwitchOff();
		break;

	case MULTI_LED_STATE_BLUE_ON:
		g_pIndicators[IndicatorType::LedRed].SwitchOff();
		g_pIndicators[IndicatorType::LedBlue].SwitchOn();
		g_pIndicators[IndicatorType::LedGreen].SwitchOff();
		break;

	case MULTI_LED_STATE_BLUE_BLINK_SLOW:
		g_pIndicators[IndicatorType::LedRed].SwitchOff();
		g_pIndicators[IndicatorType::LedBlue].Blink(g_pBlinkConfig[BlinkType::Slow]);
		g_pIndicators[IndicatorType::LedGreen].SwitchOff();
		break;

	case MULTI_LED_STATE_BLUE_BLINK_FAST:
		g_pIndicators[IndicatorType::LedRed].SwitchOff();
		g_pIndicators[IndicatorType::LedBlue].Blink(g_pBlinkConfig[BlinkType::Fast]);
		g_pIndicators[IndicatorType::LedGreen].SwitchOff();
		break;
	}
}

void net_led_test(void)
{
	while (1)
	{
		for (size_t i = 0; i < MULTI_LED_STATE_MAX; i++)
		{
			net_led_set_state(i);
			delay(3000);
		}
	}
}
