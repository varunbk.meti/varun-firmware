/*******************************************************************************
* Project Name: METI QuickIoT Modbus 
*
* Version: 1.2
*
* Description:
* In this project ESP32 handles web pages using SPIFFS
* Owner: METI M2M India Pvt Ltd.,
* 
*
********************************************************************************
* Copyright (2020-21) , METI
*******************************************************************************/

/* include headers */
#include <Arduino.h>
#include <WiFi.h>
#include <AsyncTCP.h>
#include <ESPAsyncWebServer.h>
#include <SPIFFS.h>
#include <ArduinoJson.h>
#include <config_reader.h>
#include <main.h>
#include <meti_wifi.h>
#include <meti_server.h>
#include <meti_utils.h>
#include <led.h>
#include <stdio.h>
#include <logger.h>
#include <meti_time.h>
#include <esp_event_legacy.h>
#include <indicator.h>
#include <indicatortype.h>
#include <blinktype.h>
#include <meti_net_led.h>
#include <meti_modbus_led.h>
#include <meti_mqtt.h>
#include <meti_nvs.h>
#include <serialport.h>
#include <meti_modbus_fixed.h>
#include <meti_modbus.h>
#include <MD5Builder.h>
#include <esp_task_wdt.h>
#include <config.h>
#include <myTasks.h>

#define WDT_TIMEOUT 10

Logger *g_pLogger = NULL;
BlinkConfig *g_pBlinkConfig = NULL;
Indicator *g_pIndicators = NULL;

uint32_t active_driver_id = 0;

bool node_initialized = false;
bool mbr_initialized = false;
bool g_valid_subscription = false;
// Narayan - revisit the following two lines...
string g_sDeviceID = "";
string g_sSiliconID = "";

MD5Builder _md5;
String g_dev_md5 = "";

uint8_t calibrateId = 0;

//This will be used in calibration when needed
uint16_t prevRawValue = 0;

uint8_t hardwareType = HARDWARE_TYPE;

/* webserver object on port 80 */
AsyncWebServer server(80);

// New
AsyncEventSource events("/debug_event");
unsigned long lastTime = 0;

AsyncEventSource modbus_events("/modbus_event");

ESP32Time rtc;

void setup()
{
	pinMode(PIN_LED_RED, OUTPUT);
	digitalWrite(PIN_LED_RED, HIGH);
	delay(100);

	Serial.end();
	Serial.begin(115200);
	delay(500);

	//Use LogLevel::Info in production....Trace/Debug in testing
	LogLevel logLev = LogLevel::Debug;
	if (logLev.GetCode() <= LogLevel::Debug)
	{
		delay(1000);
	}

	// test_routine();

	g_pLogger = new Logger(logLev);
	g_pLogger->Write(LogLevel::Info, "setup", "METI QuickIoT modbus  Start...");

	InitializeBlinkProcess();
	InitializeIndicators();
	net_led_initialize();
	net_led_set_state(MULTI_LED_STATE_RED_ON);

	start_file_storage();

	read_device_file();

#ifdef ENABLE_MODBUS
	read_sensor_scaling_file();
	modbus_led_initialize();
#endif

#ifdef ENABLE_WIFI
	read_wifi_file();
#endif
	// read_params_file();

#ifdef ENABLE_NVS
	meti_nvs_initialize();
	meti_nvs_read_all("params");
#endif
	setup_device_params();

#ifdef ENABLE_WIFI
	// NARAYAN - port fix (externally pulled up)
	pinMode(PIN_FORCED_AP_MODE_SWITCH, INPUT);

	g_pLogger->Write(LogLevel::Info, "setup", "Wifi start");

	meti_wifi_start();
	setup_server();

#endif

#ifdef ENABLE_MODBUS
	// Initialize Modbus
	node_initialized = InitializeNode();
	g_pLogger->Write(LogLevel::Trace, "main", "node_initialized : %d", node_initialized);

	if (node_initialized == true)
	{
		InitializeMBRConfig();
	}
	else
	{
		g_pLogger->Write(LogLevel::Info, "main", "Node initialization failed");
		// Narayan check this...
		g_pIndicators[IndicatorType::LedModbusErr].Blink(g_pBlinkConfig[BlinkType::Fast]);
	}
#endif

#ifdef ENABLE_WIFI
#ifdef ENABLE_MQTT
	//MQTT CONNECT will happen after WIFI connection is established...
	read_modbus_driver_file();
	mqtt_setup();
#endif
#endif

	read_devComm_file();

	// Enable watch dog
	esp_task_wdt_init(WDT_TIMEOUT, true); //enable panic so ESP32 restarts
	esp_task_wdt_add(NULL);				  //add current thread to WDT watch

	// Time setup
	// mtime_init() will be call after wifi is initialized and IP received..

#ifdef ENABLE_DEVICE_SUBSCRIPTION_CHECK
	validateDeviceSubscription();
#else
	g_valid_subscription = true;
#endif

	modbus_initialize();
}

void loop()
{
	//Narayan - VERY IMPORTANT
	//mandatory delay in main loop for the other tasks to start properly
	delay(1000);

	esp_task_wdt_reset();
	mandatory_restart_check();
}

void start_file_storage(void)
{
	/* Initialize SPIFFS */
	if (!SPIFFS.begin())
	{
		g_pLogger->Write(LogLevel::Fatal, "start_file_storage", "An Error has occurred while mounting SPIFFS !!");

		while (1)
			; //stop
	}
}

void setup_device_params(void)
{
	// remove colon and replace with -

	String dev_id;
	String mac = WiFi.macAddress();

	//Remove all colons in device id and add MQ1....
	dev_id = "MQI1-" + mac;
	dev_id.replace(":", "-");

	device_doc["dev_mac_addr"] = mac;
	device_doc["dev_id"] = dev_id;
	device_doc["dev_vdd"] = 3.3;

	g_dev_md5 = md5(dev_id + "quickiot dev_id");
	g_pLogger->Write(LogLevel::Info, "setup_device_params", "Device ID: %s", mac.c_str());
}

void InitializeIndicators(void)
{
	Indicator::Initialize(IndicatorType::GetSize());
	g_pIndicators = Indicator::GetInstance();

	g_pIndicators[IndicatorType::ModbusTxEnable].SetPin(PIN_MODBUS_TX_ENABLE);
}

void InitializeBlinkProcess(void)
{
	g_pBlinkConfig = new BlinkConfig[BlinkType::GetSize()];

	g_pBlinkConfig[BlinkType::Fast].Set(true, 50, 50, 1, 0, 0);
	g_pBlinkConfig[BlinkType::Once].Set(false, 200, 200, 1, 1, 0);
	g_pBlinkConfig[BlinkType::Twice].Set(false, 50, 200, 2, 1, 0);
	g_pBlinkConfig[BlinkType::BeforeFactoryReset].Set(false, 300, 200, 5, 1, 500);
	g_pBlinkConfig[BlinkType::BeforeMbrReset].Set(false, 300, 200, 3, 1, 1500);
	g_pBlinkConfig[BlinkType::Slow].Set(true, 500, 500, 1, 0, 0);
	g_pBlinkConfig[BlinkType::OnceFor300Ms].Set(false, 300, 300, 1, 1, 0);
	g_pBlinkConfig[BlinkType::BuzzOnce].Set(false, 100, 100, 1, 1, 0);
	g_pBlinkConfig[BlinkType::BuzzTwice].Set(false, 100, 100, 2, 1, 0);
}

void meti_device_restart(void)
{
	net_led_set_state(MULTI_LED_STATE_RED_ON);
	delay(100);
	ESP.restart();
}

String md5(String str)
{
	_md5.begin();
	_md5.add(String(str));
	_md5.calculate();
	return _md5.toString();
}

void wifi_1000_ms_activities(void)
{
	//debug_pin_toggle();
	if (modbus_vars.restart_device == true)
	{
		modbus_vars.restart_device = false;
		meti_device_restart();
	}

#ifdef ENABLE_WIFI
	update_device_wifi_params();

	if (wifi_test_mode_status == WIFI_TEST_MODE_OFF)
	{
		check_forced_ap_mode_entry();
		forced_ap_mode_timeout();
		//This needs to be checked, where should this function be?
		store_sta_ip_address();
	}

	//Narayan check this
	//wifi_station_display_once();

	if (WiFi.getMode() == WIFI_STA)
	{
		wifi_check_status();

#ifdef ENABLE_NTP
		//wifi_config_npt will be set once wifi gets the IP address from external router
		if (wifi_config_npt == true)
		{
			wifi_config_npt = false;
			mtime_init();
		}
#endif

		if (wifi_got_ip == true)
		{
#ifdef ENABLE_NTP
			mtime_handle_npt();
#endif
		}

#ifdef ENABLE_MQTT
		// mqtt_enabled = true;
		if (mqtt_enabled == true)
		{
			if (wifi_got_ip == true)
			{
				mqtt_check_comm_status();
				if (mqtt_status == true)
				{
					if (file_send_inprogress == false)
					{
						mqtt_publish_timeout();
					}
				}
			}
		}
#endif

		if (wifi_internet_ok == true)
		{
			set_rssi_led();
		}
	}
#endif
}

bool validateDeviceSubscription(void)
{
	g_valid_subscription = false;

	if (devComm_doc.containsKey("enabled") && devComm_doc.containsKey("dev_key"))
	{
		if (devComm_doc["dev_key"] == g_dev_md5)
		{
			if (devComm_doc["enabled"] == true)
			{
				g_valid_subscription = true;
			}
		}
	}

	if (g_valid_subscription == false)
	{
		g_pLogger->Write(LogLevel::Error, "validateDeviceSubscription", "Device Registration Failed!!");
		// display_error();
		g_pIndicators[IndicatorType::LedModbusErr].Blink(g_pBlinkConfig[BlinkType::Slow]);
	}
	else
	{
		g_pLogger->Write(LogLevel::Error, "validateDeviceSubscription", "Registered Device..");
	}

	return g_valid_subscription;
}

void mandatory_restart_check(void)
{
	uint32_t currentMillis = millis();
	static uint32_t previousMillis = 0; // will store last time LED was updated
	// constants won't change:

	time_t now = time(NULL);
	struct tm *tmp = gmtime(&now);

	//Every midnight do a restart...
	//if time between 00:00:00 and 00:00:03
	//Note: Do not do check this for for 10 seconds...
	if (currentMillis > MANDATORY_RESTART_SKIP_INTERVAL)
	{
		if (tmp->tm_hour == 0 && tmp->tm_min == 0)
		{
			if (tmp->tm_sec < 3)
			{
				previousMillis = currentMillis;
				g_pLogger->Write(LogLevel::Info, "loop", "Daily Midnight restart....");
				meti_device_restart();
			}
		}
	}

	//this is for double safety... if midnight restart does not happen
	//after 90000 seconds, do a restart...
	if (currentMillis - previousMillis >= MANDATORY_RESTART_INTERVAL_BACKUP)
	{
		// save the last time you blinked the LED
		previousMillis = currentMillis;
		g_pLogger->Write(LogLevel::Info, "loop", "Daily restart (BACKUP)....");
		meti_device_restart();
	}
}