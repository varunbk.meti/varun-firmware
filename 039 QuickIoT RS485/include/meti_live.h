#ifndef _HEADER_METI_LIVE_H_
#define _HEADER_METI_LIVE_H_
#include <Arduino.h>

//Constants
#define LIVE_SEND_BUFFER_LEN_MAX 512
#define LIVE_HEADER_PKT_ASCII_LEN_MAX 38
#define LIVE_COMM_TYPE 2

#define LIVE_CMD_LEN_STR_FIXED 20
#define LIVE_RESP_LEN_STR_FIXED 254

#define LIVE_PKT_CODE_MBR_RP 17
#define LIVE_PKT_VER_MBR_RP 3

#define LIVE_PKT_CODE_PING 7
#define LIVE_PKT_VER_PING 1

#define LIVE_SERVER_CONNECT_ERROR_COUNT_MAX 5

//Variables
typedef struct
{
    uint8_t packet_code;
    uint8_t packet_version;
    char silcon_id[16];
    uint8_t communication_type;
    uint8_t rssi;
} header_pkt_struct;

typedef struct
{
    uint8_t packet_error_code;
    uint8_t mbr_command_index;
    uint8_t mbr_comm_error;
    uint8_t mbr_received_byte_count;
    uint16_t mbr_response_time_in_ms;
    uint8_t mbr_retry_count;
    char mbr_responsestring[LIVE_RESP_LEN_STR_FIXED];
    char mbr_command_issued_string[LIVE_CMD_LEN_STR_FIXED];
    uint8_t checksum;
} pkt_struct_mbr_rp3;

extern header_pkt_struct header;
extern pkt_struct_mbr_rp3 mbr_rp3;
extern char live_send_buffer[LIVE_SEND_BUFFER_LEN_MAX];

//Functions
char *live_getServerResponse(char *buff, uint16_t *len);
void live_sync_device_time(char *response);
void live_get_standard_header(uint8_t pkt_code, uint8_t pkt_version, char *pkt_buff, char *date_time_str);
void live_get_mbr_rp3(char *pkt_buff, uint8_t modbus_cmd_idx);
void live_get_checksum(char *pkt_buff);
void live_send_mbr_rp3(uint8_t mbr_cmd_idx);
void live_get_silicon_id(char *siliconId);
void live_get_time_format(char *buff);
void live_send_ping_pkt(void);

#endif
