#ifndef _HEADER_MYTASKS_H_
#define _HEADER_MYTASKS_H_
#include <Arduino.h>

void task_setup(void);

void task_100_ms_start(void);

void task_100_ms(void *parameter);

#endif