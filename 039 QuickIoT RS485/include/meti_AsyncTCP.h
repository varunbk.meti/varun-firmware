#ifndef _HEADER_METI_ASYNCTCP_H_
#define _HEADER_METI_ASYNCTCP_H_
#include <Arduino.h>

//Constants

//Variables

//Functions
void meti_AsyncClientSetup(void);
void meti_AsyncClientRun(void);
bool meti_AsyncClientReady(void);
#endif
