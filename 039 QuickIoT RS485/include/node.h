#ifndef _NODE_H_INCLUDED_
#define _NODE_H_INCLUDED_ 1

#include<Arduino.h>
#include<ArduinoJson.h>

#include<string>
#include<mbrconfig.h>
// #include<dataset.h>

using namespace std;

#define IPV4_ADDR_LEN 		4
#define MAX_MOBILE_NO_LEN 	15

#define MAX_MBR_COUNT 		4	//Narayan - Device restarts if set to 8

class Node
{
public:
	void RemoveAllData();
	void Parse( JsonObject src);

	MBRConfig* GetMBRConfig();
	uint8 GetMBRCount();

	~Node();
	string ToString();

	ulong GetNodeID();
	void SetNodeID(ulong value);
	string GetSiliconID();
	void SetSiliconID(string value);
	string GetName();
	void SetName(string value);
	string GetServerHostName();
	void SetServerHostName(string value);
	uint16 GetServerPort();
	void SetServerPort(uint16 value);
	string GetServerPageName();
	void SetServerPageName(string value);
	uint16 GetPingInterval();
	void SetPingInterval(uint16 value);
	void EnableAbsoluteTimeScan();
	void DisableAbsoluteTimeScan();
	bool IsAbsoluteTimeScanEnabled();
	uint16 GetBacklogTxInterval();
	string GetAPN();
	void SetAPN(string value);

	static Node* GetInstance( JsonObject src);
	void Initialize(Logger* p_pLogger);

private:
	Node();

	ulong	m_lNodeID;
	string	m_sSiliconID;
	string	m_sName;
	string	m_sSosNumber1;
	string	m_sSosNumber2;
	string	m_sServerHostName;
	uint16	m_iServerPort;
	string	m_sServerPageName;
	string	m_sAPN;
	uint16	m_iPingInterval;
	uint16	m_iBacklogInterval;
	uint8	m_iMaxBacklogPacket;
	bool	m_bAbsoluteTimeScanEnabled;
	bool	m_bBackLogPresent;
	uint8	m_iMBRCount;
	MBRConfig m_pMBRConfig[MAX_MBR_COUNT];

	uint8	m_iIPAddressType;
	uint8	m_pIPV4AddrBytes[IPV4_ADDR_LEN];
	uint8	m_pIPV4GwBytes[IPV4_ADDR_LEN];

	bool m_bDeleteLogger;
	Logger* m_pLogger;
	static Node* m_pSingleInstance;
};

#endif
