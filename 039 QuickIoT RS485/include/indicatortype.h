#ifndef _INDICATOR_TYPE_H_INCLUDED_
#define _INDICATOR_TYPE_H_INCLUDED_ 1

#include<defs.h>

using namespace std;

class IndicatorType
{
public:
    enum Value : uint8
    {
        LedRed = 0,
        LedGreen = 1,
        LedBlue = 2,
        ModbusTxEnable = 3,
        LedModbusTx = 4,
        LedModbusRx = 5,
        LedModbusErr = 6
        /*
        Scan = 1,
        Data = 2,
        NetworkStatus = 3,
        Buzzer = 4
        */
    };
    
    static uint8 GetSize() { return 7; }

    IndicatorType() = default;
    IndicatorType(Value code) : m_iCode(code) { }

    operator Value() const { return m_iCode; }
    explicit operator bool() = delete;
    constexpr bool operator==(IndicatorType a) const { return m_iCode == a.m_iCode; }
    constexpr bool operator!=(IndicatorType a) const { return m_iCode != a.m_iCode; }
    uint8 GetCode();
    string GetLabel();
    string ToString();

    static IndicatorType Parse(int code);

private:
    Value m_iCode;
};

#endif
