#ifndef _METI_MODBUS_H_INCLUDED_
#define _METI_MODBUS_H_INCLUDED_ 1
#include <Arduino.h>
#include <ArduinoJson.h>
#include <logger.h>
#include <meti_time.h>
#include <modbusscanservice.h>

//These are from sniffer
#define MODBUS_COMMANDS_MAX 1

#define MODBUS_PACKET_ERROR 0
#define MODBUS_COMMAND 1
#define MODBUS_RESPONSE_ERR 2
#define MODBUS_RESPONSE_NORMAL 3

#define IDX_ADDR 0
#define IDX_FUNC 1
#define IDX_RESP_COUNT 2
#define IDX_WRITE_COUNT 6

#define MODBUS_CMD_LEN_MAX 32
#define MODBUS_RESPOSNE_LEN_MAX 300

#define MODBUS_CMD_ERROR_NONE   0
#define MODBUS_CMD_ERROR_TIMEOUT   254

#define LIVE_TX_INTERVAL_MIN  15

//Variables
typedef struct
{
    char live_time_format[TIME_STRING_SIZE];
    char cmd_str[MODBUS_CMD_LEN_MAX];
    char response_str[MODBUS_RESPOSNE_LEN_MAX];
    uint8_t rx_byte_count;
    uint8_t comm_error;
    uint8_t retries;
    uint16_t resp_time;
    bool time_out;
    bool response_recd;
    bool live_tx_ready;
} modbus_cmd_struct;

extern modbus_cmd_struct modbus_cmd[MODBUS_COMMANDS_MAX];

typedef struct
{
    uint16_t tx_set_interval;
    uint16_t tx_interval_ctr;
    bool live_tx_busy;
    uint8_t live_server_connect_error_ctr;
    bool restart_device;
} modbus_vars_struct;

extern modbus_vars_struct modbus_vars;
extern String modbus_events_string;

extern bool node_initialized;
extern bool mbr_initialized;
extern ModbusScanService *g_pScanner;

//Functions
void modbus_initialize(void);
void modbus_cmd_struct_init(void);
void modbus_live_process_pkt(void);

void modbus_500ms_task(void);
void modbus_1000ms_tasks(void);
void modbus_mark_tx_for_live(void);
void modbus_tx_next_live_pkt(void);

void InitializeMBRConfig(void);
bool InitializeNode(void);
void eventDataReceived(void);
void modbus_process_pkt(void);

void modbus_send_event_command(uint8_t cmd_idx, String cmd_str);
void modbus_send_event_response(uint8_t cmd_idx, String resp_str);
void modbus_send_event_json(String json_str);

#endif