#ifndef _MODBUS_LED_H_INCLUDED_
#define _MODBUS_LED_H_INCLUDED_ 
#include <Arduino.h>

//Constatnts
#define PIN_LED_MODBUS_TX GPIO_NUM_4
#define PIN_LED_MODBUS_RX GPIO_NUM_32
#define PIN_LED_MODBUS_ERR GPIO_NUM_19

//Variables

//Functions
void modbus_led_initialize(void);
void modbus_led_set_state(uint8_t led_state);
void modbus_led_test(void);

#endif