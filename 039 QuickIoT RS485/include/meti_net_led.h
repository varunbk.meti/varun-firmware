#ifndef _NET_LED_H_INCLUDED_
#define _NET_LED_H_INCLUDED_ 

//Constatnts
#define PIN_LED_RED GPIO_NUM_25
#define PIN_LED_GREEN GPIO_NUM_23
#define PIN_LED_BLUE GPIO_NUM_22

//Variables
typedef enum
{
	MULTI_LED_STATE_ALL_OFF = 0,
	MULTI_LED_STATE_GREEN_ON = 1,
	MULTI_LED_STATE_GREEN_BLINK_SLOW = 2,
	MULTI_LED_STATE_GREEN_BLINK_FAST = 3,
	MULTI_LED_STATE_RED_ON = 4,
	MULTI_LED_STATE_RED_BLINK_SLOW = 5,
	MULTI_LED_STATE_RED_BLINK_FAST = 6,
	MULTI_LED_STATE_BLUE_ON = 7,
	MULTI_LED_STATE_BLUE_BLINK_SLOW = 8,
	MULTI_LED_STATE_BLUE_BLINK_FAST = 9,
	MULTI_LED_STATE_MAX = 10
} multi_led_state_t;

extern multi_led_state_t multi_led_state;

//Functions
void net_led_initialize(void);
void net_led_set_state(uint8_t led_state);
void net_led_test(void);

#endif