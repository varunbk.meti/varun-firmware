#include <Arduino.h>

#define HARDWARE_TYPE_RS485 1
#define HARDWARE_TYPE_4_20MA 2

#define HARDWARE_TYPE HARDWARE_TYPE_RS485

extern uint8_t hardwareType;