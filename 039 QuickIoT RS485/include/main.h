#ifndef _HEADER_MAIN_H_
#define _HEADER_MAIN_H_
#include <Arduino.h>
#include <ArduinoJson.h>
#include <ESPAsyncWebServer.h>
#include <logger.h>
#include <ESP32Time.h>

//Constatnts
#define PIN_FORCED_AP_MODE_SWITCH GPIO_NUM_18

#define ENABLE_MQTT
#define ENABLE_WIFI
#define ENABLE_MODBUS
// #define ENABLE_NTP
// #define ENABLE_NVS
#define ENABLE_DEVICE_SUBSCRIPTION_CHECK

#define MANDATORY_RESTART_INTERVAL_BACKUP 9000 * 1000
#define MANDATORY_RESTART_SKIP_INTERVAL 10 * 1000

//Variables
extern AsyncWebServer server;
extern AsyncEventSource modbus_events;

extern Logger *g_pLogger;
extern uint32_t active_driver_id;
extern bool g_valid_subscription;

//Functions
void timer_timeout_1ms(void);
void timer_timeout_10ms(void);
void timer_timeout_50ms(void);
void timer_timeout_100ms(void);
void timer_timeout_500ms(void);
void timer_timeout_1000ms(void);
extern String g_dev_md5;
extern string g_sSiliconID;

extern ESP32Time rtc;

//Functions
void start_file_storage(void);
void update_device_sensor_values(void);
void setup_device_params(void);
void store_sta_ip_address(void);

void InitializeIndicators(void);
void InitializeBlinkProcess(void);
void update_params(void);
void meti_device_restart(void);

String md5(String str);
bool validateDeviceSubscription(void);

void wifi_1000_ms_activities(void);
void mandatory_restart_check(void);

#endif