#ifndef _HEADER_FILE_H_
#define _HEADER_FILE_H_
#include <Arduino.h>
#include <ArduinoJson.h>

//device file related declarations
//This has to be changed if there is a change in json structure
const size_t device_capacity = JSON_OBJECT_SIZE(20) + 380;
const size_t device_capacity_add = 100;
extern DynamicJsonDocument device_doc;

//wifi file related declarations
//This has to be changed if there is a change in json structure
// const size_t wifi_capacity = JSON_OBJECT_SIZE(2) + JSON_OBJECT_SIZE(3) + JSON_OBJECT_SIZE(5) + 130;
const size_t wifi_capacity = 512;
const size_t wifi_capacity_add = 100;
extern DynamicJsonDocument wifi_doc;

//sensor_scaling file related declarations
//This has to be changed if there is a change in json structure
const size_t sensorScaling_capacity = 128;
const size_t sensorScaling_capacity_add = 100;
extern DynamicJsonDocument sensorScaling_doc;

//parameters file related declarations
//This has to be changed if there is a change in json structure
const size_t params_capacity = JSON_OBJECT_SIZE(2) + 2*JSON_OBJECT_SIZE(4) + 130;
const size_t params_capacity_add = 100;
extern DynamicJsonDocument params_doc;

// this contains the sensors values (parsed by modbus) - can handle 64 sensors
const size_t sensorValues_capacity = 8*JSON_OBJECT_SIZE(2) + JSON_OBJECT_SIZE(9) + 150;
const size_t sensorValues_capacity_add = sensorValues_capacity * 7;
extern DynamicJsonDocument sensorValues_doc;

// this is for api access - can handle 64 sensors
const size_t sensorVals_capacity = 4096;
extern DynamicJsonDocument sensorVals_doc;

const size_t devComm_capacity = 1024;
extern DynamicJsonDocument devComm_doc;

const size_t live_mbr_capacity = 2048;
extern DynamicJsonDocument live_mbr_doc;

// This contains a single modbus driver (mmd file)... can handle upto 64 sensors...
// const size_t modbusDriver_capacity = JSON_OBJECT_SIZE(4) + 3*JSON_OBJECT_SIZE(8) + 8*JSON_OBJECT_SIZE(19) + 1160;
const size_t modbusDriver_capacity = JSON_OBJECT_SIZE(4) + 2*JSON_OBJECT_SIZE(8) + JSON_OBJECT_SIZE(16) + 16*JSON_OBJECT_SIZE(19) + 2000;
const size_t modbusDriver_capacity_add = modbusDriver_capacity * 2 + 1000;
const size_t modbusDriverSize = modbusDriver_capacity + modbusDriver_capacity_add;
extern DynamicJsonDocument modbusDriver_doc;

void read_mqtt_file(void);
void read_device_file(void);
void read_wifi_file(void);
void read_params_file(void);
void read_modbus_driver_file(void);
void read_sensor_scaling_file(void);
void read_devComm_file(void);

#endif