#ifndef _HEADER_METI_WIFI_H_
#define _HEADER_METI_WIFI_H_
#include <Arduino.h>
#include <ArduinoJson.h>
#include <ESPAsyncWebServer.h>

//Constants
#define ACCESS_POINT_NAME "MetiQuickIoT"
#define ACCESS_POINT_PASSWORD "meti1234"
#define ACCESS_POINT_IP "191.161.1.1"
#define ACCESS_POINT_GATEWAY "191.161.1.0"
#define ACCESS_POINT_SUBNET "255.255.255.0"

#define FORCED_AP_MODE_RESET_TIMEOUT 3       //3 seconds(cheked every 50ms)
#define FORCED_AP_MODE_EXIT_TIMEOUT 120       //120 seconds(cheked every 1 second)
#define FORCED_AP_MODE_NO_CONFIG_TIMEOUT 300 //300 seconds(cheked every 1 second)

typedef enum {
    METI_WIFI_SCAN_IDLE = 0,
    METI_WIFI_SCAN_IN_PROGRESS = 1,
    METI_WIFI_SCAN_DONE = 2,
    METI_WIFI_SCAN_RECONNECT = 3
} meti_wifi_scan_status_t;

#define SCAN_DEVICES_MAX 32

#define WIFI_TEST_MODE_OFF                  0
#define WIFI_TEST_MODE_START                1
#define WIFI_TEST_MODE_WAIT_FOR_CONNECT     2
#define WIFI_TEST_MODE_WAIT_FOR_IP_DETAILS  3
#define WIFI_TEST_MODE_DONE                 4

//Variables
extern IPAddress local_IP;
extern IPAddress gateway;
extern IPAddress subnet;
extern bool forced_ap_mode;
extern int forced_ap_mode_exit_ctr;
extern int forced_ap_mode_config_file_access_ctr;

extern bool wifi_got_ip;
extern bool wifi_internet_ok;
extern bool wifi_store_ip;
extern bool wifi_restart;
extern bool wifi_config_npt;
extern bool wifi_first_loop;

extern uint8_t wifi_test_mode_status;
extern meti_wifi_scan_status_t wifi_scan_status;

//Functions
void meti_wifi_start(void);
void setup_soft_ap(bool serverStart);
void setup_wifi_station(bool serverStart);
float get_rssi_avg(void);
float wifi_rssi_convert(float RssI);
void wifi_station_display_once(void);
void update_device_wifi_params(void);
void forced_ap_mode_timeout(void);
void check_forced_ap_mode_entry(void);
void wifi_check_status(void);
void set_rssi_led(void);

void meti_wifi_event(WiFiEvent_t event);

String meti_wifi_get_mode_text(void);
String meti_wifi_get_status_text(void);
void meti_wifi_scan_networks(AsyncWebServerRequest *request);
String meti_wifi_get_encryption_type(uint8_t i);
void wifi_test_mode(void);
void wifi_write_ip_address_to_file(String new_ip, bool reset_ip);


#endif