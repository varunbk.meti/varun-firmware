//var device config database
var g_wifi_db = {};
var g_sensorScaling_db = {};
var g_dev_comm_db = {};

var g_device_id = '';

const LOGO_TEXT = "METI"
const SHOW_COPYRIGHT = true;

const LOCAL_HOST = false;

const HARDWARE_TYPE_RS485 = 1
const HARDWARE_TYPE_4_20MA = 2
const HARDWARE_TYPE_SNIFFER = 3

const HARDWARE_TYPE = HARDWARE_TYPE_RS485

var ready_timer = setInterval(function () {
    // console.log("timer");
    header_footer_logo();
}, 300);

// $(document).ready(function () {
function header_footer_logo() {
    //check if object exits
    if ($("#logo_text").length) {
        //stop the check interval timers
        clearInterval(ready_timer);
        console.log("timer stop");

        $("#logo_text").html(LOGO_TEXT);

        //Footer logo
        if (SHOW_COPYRIGHT == true) {
            var footer_obj = $("<footer></footer>");

            var div_obj = $("<div></div>")
                .addClass("footer-copyright text-center py-3")
                .html("2021 Copyright:");

            var a_obj = $("<a></a>")
                .attr("href", "https://www.meti.in/")
                .html(" METI M2M India Pvt Ltd.,")

            div_obj.append(a_obj);
            footer_obj.append(div_obj);
            $("body").after(footer_obj);
        }
    }
}

// $(document).ready(function () {
function setupDeviceSpecificLinks() {
    var factory_settings_flag = false;

    $(".navbar").css("visibility", "visible");
    $("#link_factorySettings").attr("hidden", "hidden");
    $("#link_modbus").attr("hidden", "hidden");
    $("#link_sensorscaling").attr("hidden", "hidden");

    switch (HARDWARE_TYPE) {
        case HARDWARE_TYPE_RS485:
            $("#quickIot_title2").html("RS485");
            $("#link_modbus").removeAttr("hidden");
            break;

        case HARDWARE_TYPE_4_20MA:
            $("#quickIot_title2").html("4-20mA");
            $("#link_sensorscaling").removeAttr("hidden");
            break;

        case HARDWARE_TYPE_SNIFFER:
            $("#quickIot_title2").html("Sniffer");
            $("#link_modbus").removeAttr("hidden");
            $("#sensor_base").attr("hidden", "hidden");
            $("#dashboard_base").attr("hidden", "hidden");
            break;
    }

    $("#quickIot_title1").on("click", function () {
        factory_settings_flag = true;
        setTimeout(function () {
            factory_settings_flag = false;
        }, 1000)
    })

    $("#quickIot_title2").on("click", function () {
        if (factory_settings_flag == true) {
            $("#link_factorySettings").removeAttr("hidden");
        }
    });
}
// });

function getCleanString(inputStr) {
    // var new_str = inputStr.replace(/[&\/\\#, +()$~%.'":*?<>{}]/g, '_');
    inputStr = inputStr.replace(" ", "_");
    inputStr = inputStr.replace("/", "_");
    inputStr = inputStr.replace("+", "_");
    inputStr = inputStr.replace("#", "_");

    return inputStr;
}

function copyTextToClipboard(text) {
    var input = document.createElement('textarea');
    input.innerHTML = text;
    document.body.appendChild(input);
    input.select();
    var result = document.execCommand('copy');
    document.body.removeChild(input);
    return result;
}

function openFullscreen(elem) {
    if (elem.requestFullscreen) {
        elem.requestFullscreen();
    } else if (elem.mozRequestFullScreen) { /* Firefox */
        elem.mozRequestFullScreen();
    } else if (elem.webkitRequestFullscreen) { /* Chrome, Safari & Opera */
        elem.webkitRequestFullscreen();
    } else if (elem.msRequestFullscreen) { /* IE/Edge */
        elem.msRequestFullscreen();
    }
}

function getModbusSensors1() {
    sensors = g_modbus_drivers.template_header.responseParser;
    // sensors1 = g_modbus_drivers.template_header.responseParser;
    // sensors2 = g_modbus_drivers.template_header.responseParser;

    sensors_full = sensors;
    // sensors_full = $.extend(sensors, sensors1);
    // sensors_full = $.extend(sensors_full, sensors2);


    return sensors_full;
}

function readConfigDb() {
    var abcd = readTextFile("config.txt");
    console.log(abcd);
    // console.log(readTextFile("config.txt"));
}

function readSensorScalingFromFS(callback) {
    var rawFile = new XMLHttpRequest();
    rawFile.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            var allText = JSON.parse(rawFile.responseText);
            g_sensorScaling_db = allText;
            callback();
        }
    }

    rawFile.open("GET", "../config/sensorScaling.json", false);
    rawFile.send();
}

function updateWifiConfigFromFS(callback) {
    var rawFile = new XMLHttpRequest();
    rawFile.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            var allText = JSON.parse(rawFile.responseText);
            g_wifi_db = allText;
            callback();
        }
    }

    rawFile.open("GET", "../config/wifi.json", false);
    rawFile.send();
}

//var device config database
var g_modbus_drivers = {};
// function getLocalModbusDrivers(callback) {
//     var rawFile = new XMLHttpRequest();
//     rawFile.onreadystatechange = function () {
//         if (this.readyState == 4 && this.status == 200) {
//             var allText = JSON.parse(rawFile.responseText);
//             g_modbus_drivers = allText;
//             callback();
//             return;
//         }
//     }

//     //Narayan - this has to be made dynamic
//     rawFile.open("GET", "../drivers/mmd_99.json", false);
//     rawFile.send();
// }

function getModbusDriverById(driver_id, callback) {
    var rawFile = new XMLHttpRequest();
    rawFile.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            var allText = JSON.parse(rawFile.responseText);
            callback(allText);
            return;
        }
    }

    if (LOCAL_HOST == true) {
        fileName = "http://localhost/play/quickIoT_drivers/mmd_" + driver_id + ".json";
        console.log(fileName);
    } else {
        fileName = "../drivers/mmd_" + driver_id + ".json";
    }

    rawFile.open("GET", fileName, false);
    rawFile.send();
}

// Returns the number of repetitions of a characters in a string
function char_count(str, letter) {
    var letter_Count = 0;
    for (var position = 0; position < str.length; position++) {
        if (str.charAt(position) == letter) {
            letter_Count += 1;
        }
    }
    return letter_Count;
}

function getDeviceParams(callBack, callBackError) {
    $('.loader').css('display', 'block');

    var url = "/getDeviceConfig";
    if (LOCAL_HOST == true) {
        var url = "http://localhost/play/index.php/QuickIoT_simulate/readDeviceParams";
    }

    var type = 'POST';
    var data = {
        dev_key: "ABCD"
    };

    $.ajax({
        url: url,
        type: type,
        data: data,
        success: function (response) {
            callBack(response);
        },
        error: function (jqXHR, exception, response) {
            callBackError(response);
        }
    });
}

function getSensorValues(callBack, callBackError) {
    $('.loader').css('display', 'block');

    var url = "/getSensorValues";
    if (LOCAL_HOST == true) {
        var url = "http://localhost/play/index.php/quickiot_simulate/getSensorValues";
    }

    var type = 'POST';
    var data = {
        dev_key: "ABCD"
    };

    $.ajax({
        url: url,
        type: type,
        data: data,
        success: function (response) {
            callBack(response);
        },
        error: function (jqXHR, exception, response) {
            callBackError(response);
        }
    });
}

function reset_device() {
    var url = "/resetDevice";
    if (LOCAL_HOST == true) {
        var url = "http://localhost/play/index.php/quickIoT/reset_device";
    }

    var type = 'POST';
    var data = {
        dev_key: "ABCD"
    };

    $.ajax({
        url: url,
        type: type,
        data: data,
        success: function (response) {
            alert(response.message);
        },
        error: function (jqXHR, exception, response) {
            // console.log('POST Error!');
        }
    });
}

function factory_reset() {
    var r = confirm("Factory reset will re-initialize Wifi, MQTT and Modbus Device Drivers...\n\nAre you sure?");
    if (r == false) {
        return;
    }
    else {
        var pw = window.prompt("Enter password...");
        if (pw != "meti1234") {
            return;
        }
    }

    var url = "/factoryReset";
    if (LOCAL_HOST == true) {
        var url = "http://localhost/play/index.php/quickIoT/factory_reset";
    }

    var type = 'POST';
    var data = {
        // Naryan this has to be changed to device id
        dev_key: "ABCD"
    };

    $.ajax({
        url: url,
        type: type,
        data: data,
        success: function (response) {
            alert(response.message);
        },
        error: function (jqXHR, exception, response) {
            // console.log('POST Error!');
        }
    });
}

function table_add_rows(table_name, row_data_array) {
    var table_name_obj = $(table_name).DataTable();
    table_name_obj.rows.add(row_data_array).draw();
}

function table_clear(table_name) {
    //Destroy all the data table properties before re-gerating the table...
    var table_name_obj = $(table_name).DataTable();
    table_name_obj.clear().draw();
}

function table_destroy(table_name) {
    var table_name_obj = $(table_name).dataTable();
    table_name_obj.fnDestroy();
}


// This function is called after 300 ms
setTimeout(function () {
    getDeviceId();
}, 500);

function getDeviceId() {
    var url = "/getDeviceId";
    if (LOCAL_HOST == true) {
        url = "http://localhost/play/index.php/quickiot_simulate/getDeviceId";
    }

    var type = 'POST';
    var data = {
        app_key: "quickIoT"
    };

    $.ajax({
        url: url,
        type: type,
        data: data,
        success: function (response) {
            var result = response.result;
            if (result == 1) {
                g_device_id = response.dev_id;
                $("#bs_device_id").html(g_device_id);
            }
            else if (result == 0) {
                g_device_id = '';
                console.log(response.message);
            }
        },
        error: function (jqXHR, exception, response) {
            console.log('POST Error!! Unable to get device deails...');
        }
    });
}

function readDevCommConfigFromFS(callback) {
    var rawFile = new XMLHttpRequest();
    rawFile.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            var allText = JSON.parse(rawFile.responseText);
            g_dev_comm_db = allText;
            callback();
        }
    }

    rawFile.open("GET", "../config/devComm.json", false);
    rawFile.send();
}
